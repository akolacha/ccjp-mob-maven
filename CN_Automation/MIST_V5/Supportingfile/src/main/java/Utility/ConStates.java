package Utility;

import static Utility.FunctionLibrary.driver;
import static Utility.R_Start.TTlogs;
import static Utility.R_Start.ConditionStatus;
import static Utility.R_Start.TestRowCount;
import static Utility.ReusableFunctions.CurrentReusablefunction;
import static Utility.ReusableFunctions.ReUseCurRow;
import static Utility.ReusableFunctions.ReuseFunctionRowcount;
import static Controller.Controller.Pbar;

import java.awt.Color;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import org.fife.ui.rsyntaxtextarea.modes.TclTokenMaker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ConStates  extends R_Start {

	
	public static int NewForStart; public static int NewForEnd = 0;
	public ConStates() throws InterruptedException, IOException, ClassNotFoundException {
		super(ConditionStatus);
		// TODO Auto-generated constructor stub
	}
	
	public static String IF_ObjCondition() throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, InterruptedException
	{
   
		try {
		Logs.Ulog("=== Starting IF_ObjCondition === "+ TC.Keyword + "=== Execute =  "+ TC.ExecuteTest);
		Class c = Class.forName("Utility.R_Start");
		String KW = TC.Keyword;
		try
		{
			TC.Keyword = MyMethods.get(TC.Keyword.toString().trim().replace(" ", ""));
			if (TC.Keyword.equals(null))
			{
				 TC.Keyword =  KW;
			}
		}
		 catch(Throwable t)
		 {
			 TC.Keyword =  KW;
		 }
		Method m = c.getMethod(TC.InputData, null);
		// m.invoke(null, null);
		ConditionStatus = (String) m.invoke(null, null);
		
		if(ConditionStatus == "true")
		{
		
			
			return "true";
			
		}
		else
		{
			for (Testrow = Testrow ; Testrow <= Testdata
					.getRowCount(TC.TestModuleName); Testrow++) {
				TC.LoadTestData(Testrow);
//				if (!TC.Module.equals("") || !TC.TCID.equals(""))
//					UpdateResult.UpDateModule();

				
				Logs.Ulog("=== Starting of Keywords === " + TC.Keyword
						+ "=== Execute =  " + TC.ExecuteTest);
				// System.out.println(" *********** Executing Keywords  ************  "
				// + TC.Keyword);
				
				if (TC.PauseExecution.equals("YY"))
				{
					 while (TC.PauseExecution.equals("YY")) {
						 System.out.println("Script Paused .....Please click on Run button in progress bar to resume "  );
				            //JOptionPane.showMessageDialog(null, "Script Paused .......... Please click on pause/Run button agin " );
				           Pbar.p.setBackground(Color.YELLOW);
				            TimeUnit.SECONDS.sleep(3);
				           Pbar.p.setBackground(Color.WHITE);
				        }
					 
					 TC.PauseExecution = "NN";
					 Pbar.p.setBackground(new Color(189,183,107));
				}
				
				if (TC.ExecuteTest.equals(TC.ExecuteTestYes)) {
					
					Pbar.Testprogress(Testrow, PrvKeyword, "Executing "
							+ TC.TestObjects + "." + TC.Keyword + " "
							+ TC.InputData + " @ Row  " + Testrow + " of "
							+ TestRowCount );
					if (TC.Keyword.startsWith("End_IF"))
					{
					
						return "true";
				    }
				}
			}// End of test for
		} // End of Test Execution if
		
		}
		catch (Exception e) {
			Logs.Ulog(" === Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === "
					+ TC.Keyword
					+ " === Result State  "
					+ PrvKeyword);
			Logs.Ulog("ERROR ------ Error While Executing KEYWORD  " + e.getMessage());
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription =" === In IF Condition Block Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === " + e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
			
		}
		return ConditionStatus;
		
	
	}
	public static String ReuseIF_ObjCondition() throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, InterruptedException
	{
   
		try {
		Logs.Ulog("=== Starting IF_ObjCondition === "+ TC.Keyword + "=== Execute =  "+ TC.ExecuteTest);
		Class c = Class.forName("Utility.R_Start");
		String KW=TC.Keyword;
		try
		{
			TC.Keyword = MyMethods.get(TC.Keyword.toString().trim().replace(" ", ""));
			if (TC.Keyword.equals(null))
			{
				 TC.Keyword =  KW;
			}
			 if(TC.Keyword.equalsIgnoreCase("wait"))
					TC.Keyword = "Wait";
		}
		catch(Throwable t)
		{
			
		}
		 
		Method m = c.getMethod(TC.InputData, null);
		// m.invoke(null, null);
		ConditionStatus = (String) m.invoke(null, null);
		
		if(ConditionStatus == "true")
		{
		
			
			return "true";
			
		}
		else
		{
			for (ReUseCurRow = ReUseCurRow; ReUseCurRow <= (Integer) StoreTable
					.get("End" + CurrentReusablefunction); ReUseCurRow++) {
				TC.LoadReuseTestData(ReUseCurRow);
//				if (!TC.Module.equals("") || !TC.TCID.equals(""))
//					UpdateResult.UpDateModule();

				
				Logs.Ulog("=== Starting of Keywords === " + TC.Keyword
						+ "=== Execute =  " + TC.ExecuteTest);
				// System.out.println(" *********** Executing Keywords  ************  "
				// + TC.Keyword);
				if (TC.ExecuteTest.equals(TC.ExecuteTestYes)) {
					
					Pbar.Testprogress(Testrow, PrvKeyword, "@ Step " + Testrow + " of "
							+ TestRowCount + "  "
							+ TC.TestObjects + "." + TC.Keyword + " "
							+ TC.InputData );
//Added by Avanish
					m = c.getMethod(TC.Keyword, null);
					Status = (String) m.invoke(null, null);
					if(Status.equals(TC.BLOCK)){
						ReUseCurRow = (Integer) StoreTable
								.get("End" + CurrentReusablefunction)+1;
						
						m=c.getMethod("CloseBrowser", null);
						m.invoke(null, null);
						return TC.FAIL;
						
					}
//added by Avanish to update testlink block status
					TC.stepCtr++;
					if(Status.equalsIgnoreCase(TC.FAIL)|| Status.equalsIgnoreCase("false")){
						TC.failStepCtr++;
					}
//Added till here					
					if (TC.Keyword.startsWith("End_IF"))
					{
					
						return "true";
				    }
				}
			}// End of test for
		} // End of Test Execution if
		
		}
		catch (Exception e) {
			Logs.Ulog(" === Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === "
					+ TC.Keyword
					+ " === Result State  "
					+ PrvKeyword);
			Logs.Ulog("ERROR ------ Error While Executing KEYWORD  " + e.getMessage());
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription =" === In IF Condition Block Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === ";
			UpdateResult.UpdateStatus();
			return TC.FAIL;
			
		}
		return ConditionStatus;
		
	
	}
	
	public static String ResueIF_VarCondition() throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, InterruptedException
	{
   
		try {
		
			Logs.Ulog("=== Starting IF_VarCondition === "+ TC.Keyword + "=== Execute =  "+ TC.ExecuteTest);
			Class c = Class.forName("Utility.R_Start");
			String KW = TC.Keyword;
			try
			{
				TC.Keyword = MyMethods.get(TC.Keyword.toString().trim().replace(" ", ""));
				if (TC.Keyword.equals(null))
				{
					 TC.Keyword =  KW;
				}
			}
			 catch(Throwable t)
			 {
				 TC.Keyword =  KW;
			 }
			 if(TC.Keyword.equalsIgnoreCase("wait"))
					TC.Keyword = "Wait";
			Method m = c.getMethod(TC.InputData, null);
			// m.invoke(null, null);
			ConditionStatus = (String) m.invoke(null, null);
		
		if(ConditionStatus == "true")
		{
		
				
			return "true";
			
		}
		else
		{
			for (ReUseCurRow = ReUseCurRow; ReUseCurRow <= (Integer) StoreTable
					.get("End" + CurrentReusablefunction); ReUseCurRow++) {
				TC.LoadReuseTestData(ReUseCurRow);
//				if (!TC.Module.equals("") || !TC.TCID.equals(""))
//					UpdateResult.UpDateModule();

				
				Logs.Ulog("=== Starting of Keywords === " + TC.Keyword
						+ "=== Execute =  " + TC.ExecuteTest);
				// System.out.println(" *********** Executing Keywords  ************  "
				// + TC.Keyword);
				
				if (TC.PauseExecution.equals("YY"))
				{
					 while (TC.PauseExecution.equals("YY")) {
						 System.out.println("Script Paused .....Please click on Run button in progress bar to resume "  );
				            //JOptionPane.showMessageDialog(null, "Script Paused .......... Please click on pause/Run button agin " );
				           Pbar.p.setBackground(Color.YELLOW);
				            TimeUnit.SECONDS.sleep(3);
				           Pbar.p.setBackground(Color.WHITE);
				        }
					 
					 TC.PauseExecution = "NN";
					 Pbar.p.setBackground(new Color(189,183,107));
				}
				if (TC.ExecuteTest.equals(TC.ExecuteTestYes)) {
					
					Pbar.Testprogress(Testrow, PrvKeyword, " @ Step  " + Testrow + " of "
							+ TestRowCount + "  "
							+ TC.TestObjects + "." + TC.Keyword + " "
							+ TC.InputData );
					if (TC.Keyword.startsWith("End_IF"))
					{
					
						return "true";
				    }
				}
			}// End of test for
		} // End of Test Execution if
		
		}
		catch (Exception e) {
			Logs.Ulog(" === Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === "
					+ TC.Keyword
					+ " === Result State  "
					+ PrvKeyword);
			Logs.Ulog("ERROR ------ Error While Executing SendKeys " + e.getMessage());
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription =" ===IF_VarCondition Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === " ;
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}
		return ConditionStatus;
		
	
	}
	
	
	public static String IF_VarCondition() throws IOException, InterruptedException  
	{
   
		try {
		
			Logs.Ulog("=== Starting IF_VarCondition === "+ TC.Keyword + "=== Execute =  "+ TC.ExecuteTest);
			Class c = Class.forName("Utility.R_Start");
			String KW = TC.Keyword;
			try
			{
				TC.Keyword = MyMethods.get(TC.Keyword.toString().trim().replace(" ", ""));
				if (TC.Keyword.equals(null))
				{
					 TC.Keyword =  KW;
				}
			}
			 catch(Throwable t)
			 {
				 TC.Keyword =  KW;
			 }
			 if(TC.Keyword.equalsIgnoreCase("wait"))
					TC.Keyword = "Wait";
			Method m = c.getMethod(TC.InputData, null);
			// m.invoke(null, null);
			ConditionStatus = (String) m.invoke(null, null);
		
		if(ConditionStatus == "true")
		{
		
			
			return "true";
			
		}
		else
		{
			for (Testrow = Testrow ; Testrow <= Testdata
					.getRowCount(TC.TestModuleName); Testrow++) {
				TC.LoadTestData(Testrow);
//				if (!TC.Module.equals("") || !TC.TCID.equals(""))
//					UpdateResult.UpDateModule();

				
				Logs.Ulog("=== Starting of Keywords === " + TC.Keyword
						+ "=== Execute =  " + TC.ExecuteTest);
				// System.out.println(" *********** Executing Keywords  ************  "
				// + TC.Keyword);
				
				if (TC.PauseExecution.equals("YY"))
				{
					 while (TC.PauseExecution.equals("YY")) {
						 System.out.println("Script Paused .....Please click on Run button in progress bar to resume "  );
				            //JOptionPane.showMessageDialog(null, "Script Paused .......... Please click on pause/Run button agin " );
				           Pbar.p.setBackground(Color.YELLOW);
				            TimeUnit.SECONDS.sleep(3);
				           Pbar.p.setBackground(Color.WHITE);
				        }
					 
					 TC.PauseExecution = "NN";
					 Pbar.p.setBackground(new Color(189,183,107));
				}
				if (TC.ExecuteTest.equals(TC.ExecuteTestYes)) {
					
					Pbar.Testprogress(Testrow, PrvKeyword," @ Step  " + Testrow + " of "
							+ TestRowCount + "  "
							+ TC.TestObjects + "." + TC.Keyword + " "
							+ TC.InputData );
					if (TC.Keyword.startsWith("End_IF"))
					{
					
						return "true";
				    }
				}
			}// End of test for
		} // End of Test Execution if
		
		}
		catch (Exception e) {
			Logs.Ulog(" === Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === "
					+ TC.Keyword
					+ " === Result State  "
					+ PrvKeyword);
			Logs.Ulog("ERROR ------ Error While Executing SendKeys " + e.getMessage());
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription =" ===IF_VarCondition Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === " + e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}
		return ConditionStatus;
		
	
	}
	
	
	public static String ReuseForLoop() throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InterruptedException, IOException
	{
   
	try
	{
		
		TC.TestObjects = NoExpReuseismodifydata(TC.TestObjects);
		String f[]= TC.TestObjects.replaceAll(" ", "").split("To");
		
		NewForStart=ReUseCurRow+1;  
		StoreTable.put("Forindex", "0");
		StoreTable.put("Loopindex", "0");
			int innerforcount = 0;
			for (ReUseCurRow = ReUseCurRow; ReUseCurRow <= ReuseFunctionRowcount; ReUseCurRow++)
				
			{
				TC.NoexcepLoadReuseTestData(ReUseCurRow);
								
					if(TC.Keyword.equalsIgnoreCase("End_ReuseForLoop") )
					{
						NewForEnd=ReUseCurRow;
						break;
					}
					
					
			}
			int Forindex = 1;
			
			if(Integer.parseInt(f[1])==0)
			{
				ReUseCurRow = NewForEnd;
				return  "EXIT";
			}
				
			int Loopindex = 0;
			for( int i = Integer.parseInt( f[0]); i<=Integer.parseInt( f[1]); i++)
			{	
				String a = String.valueOf(i);
				StoreTable.put("Forindex", Integer.toString(i));
				Loopindex = Loopindex + 1;
				StoreTable.put("Loopindex",Integer.toString( Loopindex));
				
				
				for (int K = NewForStart; K <= NewForEnd; K++) {
					TC.LoadReuseTestData(K);
//					if (!TC.Module.equals("") && TC.TCID.equals(""))
						if (!TC.Module.equals(""))
						UpdateResult.UpDateModule();
						

					
					Logs.Ulog("=== Starting of Keywords === " + TC.Keyword
							+ "=== Execute =  " + TC.ExecuteTest);
					// System.out.println(" *********** Executing Keywords  ************  "
					// + TC.Keyword);
					
					if (TC.PauseExecution.equals("YY"))
					{
						 while (TC.PauseExecution.equals("YY")) {
							 System.out.println("Script Paused .....Please click on Run button in progress bar to resume "  );
					            //JOptionPane.showMessageDialog(null, "Script Paused .......... Please click on pause/Run button agin " );
					           Pbar.p.setBackground(Color.YELLOW);
					            TimeUnit.SECONDS.sleep(3);
					           Pbar.p.setBackground(Color.WHITE);
					        }
						 
						 TC.PauseExecution = "NN";
						 Pbar.p.setBackground(new Color(189,183,107));
					}
					if (TC.ExecuteTest.equals(TC.ExecuteTestYes)) {
						Logs.Ulog("=== Keywords === " + TC.Keyword
								+ "=== Execute =  " + TC.ExecuteTest);
						Logs.Ulog("Executing " + TC.TestObjects + "."
								+ TC.Keyword + " " + TC.InputData + " @ Row  "
								+ Testrow + " of " + TestRowCount);
						Pbar.Testprogress(K, PrvKeyword,  " @ Step " + K + " of "
								+ NewForEnd
								+ TC.TestObjects + "." + TC.Keyword + " "
								+ TC.InputData  );

						UpdateResult.UpDateKeyword();
						PrvKeyword = TC.Keyword;
						if (TC.Keyword.startsWith("IF_ObjCondition") ) {
							Logs.Ulog("=== is a IF IF_ObjCondition =======");
							ConStates.ReuseIF_ObjCondition();
						}
						else if  (TC.Keyword.startsWith("IF_VarCondition") ) {
							Logs.Ulog("=== is a IF IF_VarCondition =======");
							ConStates.ResueIF_VarCondition();
						}
						
						else if  (TC.Keyword.startsWith("ReUse_")) {
							Logs.Ulog("=== is a Reuse keyword =======");
							ReusableFunctions
									.ExeCurrentReusbleFunction(TC.Keyword);
						}
						
						
						else {
							try {
								if (!TC.Keyword.startsWith("IF_") && !TC.Keyword.equalsIgnoreCase("ReuseForLoop") && !TC.Keyword.equalsIgnoreCase("End_IF") && !TC.Keyword.equalsIgnoreCase("End_ReuseForLoop")  )
								{
										Logs.Ulog("=== Starting Keywords === "
												+ TC.Keyword + "=== Execute =  "
												+ TC.ExecuteTest);
										
							
										// Class c =
										// Class.forName("FunctionLibrary.LibraryFunction");
										Class c = Class.forName("Utility.R_Start");
		//								String TPAck_Class = ExecutionModUleName
		//										+ TC.TestModuleName;
		//								Class c = Class.forName(TPAck_Class);
										String KW = TC.Keyword;
										try
										{
											TC.Keyword = MyMethods.get(TC.Keyword.toString().trim().replace(" ", ""));
											if (TC.Keyword.equals(null))
											{
												 TC.Keyword =  KW;
											}
										}
										 catch(Throwable t)
										 {
											 TC.Keyword =  KW;
										 }
										 if(TC.Keyword.equalsIgnoreCase("wait"))
												TC.Keyword = "Wait";
										Method m = c.getMethod(TC.Keyword, null);
										// m.invoke(null, null);
										Status = (String) m.invoke(null, null);
//added by Avanish to update testlink block status
										if(Status.equals(TC.BLOCK)){
											ReUseCurRow = ReuseFunctionRowcount+1;
											
											m=c.getMethod("CloseBrowser", null);
											m.invoke(null, null);
											return "EXIT";
											
										}
										TC.stepCtr++;
										if(Status.equalsIgnoreCase(TC.FAIL)|| Status.equalsIgnoreCase("false")){
											TC.failStepCtr++;
										}
										Logs.Ulog("=== End of Keywords === "
												+ TC.Keyword + "=== Execute =  "
												+ TC.ExecuteTest);
		
										TimeUnit.SECONDS.sleep(Steptimedelay);
										Logs.Ulog("=== Execution Delay === "
												+ Steptimedelay);
										long Et = (System.currentTimeMillis() - startTime) / 1000;
										Logs.Ulog("------- EXECTION TIME TAKEN--------  = "
												+ String.valueOf(Et));
										StepSync();
								}

							} catch (Exception e) {
								Logs.Ulog(" === Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === "
										+ TC.Keyword
										+ " === Result State  "
										+ PrvKeyword);
								Logs.Ulog("ERROR ------ Error While Executing SendKeys " + e.getMessage());
								UpdateResult.ActualData = TC.FAIL;
								TC.FailDescription =" ===ReuseForLoop Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === " + e.getMessage();
								UpdateResult.UpdateStatus();
								return TC.FAIL;
							}

							if(!(Status==null))
							{
								if (Status.equals("FAIL") && TC.OnFailExit.equalsIgnoreCase(TC.StopTestYes) )
								{						
									Logs.Ulog(" === Test Failed on  === "+ TC.Keyword + " === Result State " + PrvKeyword);
									break ;
							    }
							}
						}// End of keyword else

					} // End of Test Execution if
				}// End of test for
				
			  
			}
				
			
	}
	catch(Throwable T)
	{
		Logs.Ulog(" ===Error  While For Loop initialization,  Please  correct it to example : ForLoop , Tobj col =   1 to 10 ");
		
		UpdateResult.ActualData = TC.FAIL;
		TC.ExpectedData = " ===ForLoop Error  While For Loop initialization,  Please  correct it to example : ForLoop , Tobj col =   1 to 10  === " + T.getMessage();
		TC.FailDescription =" ===ForLoop Error  While For Loop initialization,  Please  correct it to example : ForLoop , Tobj col =   1 to 10  === " + T.getMessage();
		UpdateResult.UpdateStatus();
	}
	return ConditionStatus;	
	}
	
	public static String ForLoop() throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InterruptedException, IOException
	{
   
	try
	{
		String f[]= TC.TestObjects.replaceAll(" ", "").split("To");
		int NewForStart=Testrow;int NewForEnd = 0;
		StoreTable.put("Forindex", "0");
		StoreTable.put("Loopindex", "0");
			int innerforcount = 0;
			for (Testrow = Testrow; Testrow <= Testdata
					.getRowCount(TC.TestModuleName); Testrow++)
				
			{
				TC.NoexcepLoadTestData(Testrow);
								
					if(TC.Keyword.equalsIgnoreCase("End_ForLoop") )
					{
						NewForEnd=Testrow;
						break ;
					}
					
					
			}
			int Loopindex = 0;
			NewForStart=NewForStart+1;  
			
			for( int i = Integer.parseInt( f[0]); i<=Integer.parseInt( f[1]); i++)
			{	
				StoreTable.put("Forindex", Integer.toString(i));
				Loopindex = Loopindex + 1;
				StoreTable.put("Loopindex",Integer.toString( Loopindex));
				for (Testrow = NewForStart; Testrow <= NewForEnd; Testrow++) {
					
					
					
					TC.LoadTestData(Testrow);
//					if (!TC.Module.equals("") && TC.TCID.equals(""))
						if (!TC.Module.equals(""))
						UpdateResult.UpDateModule();
						

					
					Logs.Ulog("=== Starting of Keywords === " + TC.Keyword
							+ "=== Execute =  " + TC.ExecuteTest);
					// System.out.println(" *********** Executing Keywords  ************  "
					// + TC.Keyword);
					
					if (TC.PauseExecution.equals("YY"))
					{
						 while (TC.PauseExecution.equals("YY")) {
							 System.out.println("Script Paused .....Please click on Run button in progress bar to resume "  );
					            //JOptionPane.showMessageDialog(null, "Script Paused .......... Please click on pause/Run button agin " );
					           Pbar.p.setBackground(Color.YELLOW);
					            TimeUnit.SECONDS.sleep(3);
					           Pbar.p.setBackground(Color.WHITE);
					        }
						 
						 TC.PauseExecution = "NN";
						 Pbar.p.setBackground(new Color(189,183,107));
					}
					if (TC.ExecuteTest.equals(TC.ExecuteTestYes)) {
						Logs.Ulog("=== Keywords === " + TC.Keyword
								+ "=== Execute =  " + TC.ExecuteTest);
						Logs.Ulog("Executing " + TC.TestObjects + "."
								+ TC.Keyword + " " + TC.InputData + " @ Row  "
								+ Testrow + " of " + TestRowCount);
						Pbar.Testprogress(Testrow, PrvKeyword, "Executing "
								+ TC.TestObjects + "." + TC.Keyword + " "
								+ TC.InputData + " @ Row  " + Testrow + " of "
								+ TestRowCount);

						UpdateResult.UpDateKeyword();
						PrvKeyword = TC.Keyword;
						if (TC.Keyword.startsWith("IF_ObjCondition") ) {
							Logs.Ulog("=== is a IF IF_ObjCondition =======");
							ConStates.IF_ObjCondition();
						}
						else if  (TC.Keyword.startsWith("IF_VarCondition") ) {
							Logs.Ulog("=== is a IF IF_VarCondition =======");
							ConStates.IF_VarCondition();
						}
						
						else if  (TC.Keyword.startsWith("ReuseIF_ObjCondition") ) {
							Logs.Ulog("=== is a IF IF_VarCondition =======");
							ConStates.ReuseIF_ObjCondition();
														
						}
												
						else if  (TC.Keyword.startsWith("ReUse_")) {
							Logs.Ulog("=== is a Reuse keyword =======");
							ReusableFunctions
									.ExeCurrentReusbleFunction(TC.Keyword);
						}
						
						
						else {
							try {
								if (!TC.Keyword.startsWith("IF_") && !TC.Keyword.startsWith("ReuseIF_")  && !TC.Keyword.equalsIgnoreCase("ForLoop") && !TC.Keyword.equalsIgnoreCase("End_IF") && !TC.Keyword.equalsIgnoreCase("End_ForLoop")  )
								{
										Logs.Ulog("=== Starting Keywords === "
												+ TC.Keyword + "=== Execute =  "
												+ TC.ExecuteTest);
										// Class c =
										// Class.forName("FunctionLibrary.LibraryFunction");
										Class c = Class.forName("Utility.R_Start");
		//								String TPAck_Class = ExecutionModUleName
		//										+ TC.TestModuleName;
		//								Class c = Class.forName(TPAck_Class);
										String KW = TC.Keyword;
										try
										{
											TC.Keyword = MyMethods.get(TC.Keyword.toString().trim().replace(" ", ""));
											if (TC.Keyword.equals(null))
											{
												 TC.Keyword =  KW;
											}
										}
										 catch(Throwable t)
										 {
											 TC.Keyword =  KW;
										 }
										 if(TC.Keyword.equalsIgnoreCase("wait"))
												TC.Keyword = "Wait";
										Method m = c.getMethod(TC.Keyword, null);
										// m.invoke(null, null);
										Status = (String) m.invoke(null, null);
//added by Avanish to update testlink block status
										TC.stepCtr++;
										if(Status.equalsIgnoreCase(TC.FAIL)|| Status.equalsIgnoreCase("false")){
											TC.failStepCtr++;
										}
										
										Logs.Ulog("=== End of Keywords === "
												+ TC.Keyword + "=== Execute =  "
												+ TC.ExecuteTest);
		
										TimeUnit.SECONDS.sleep(Steptimedelay);
										Logs.Ulog("=== Execution Delay === "
												+ Steptimedelay);
										long Et = (System.currentTimeMillis() - startTime) / 1000;
										Logs.Ulog("------- EXECTION TIME TAKEN--------  = "
												+ String.valueOf(Et));
										StepSync();
								}

							} catch (Exception e) {
								Logs.Ulog(" === Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === "
										+ TC.Keyword
										+ " === Result State  "
										+ PrvKeyword);
								Logs.Ulog("ERROR ------ Error While Executing SendKeys " + e.getMessage());
								UpdateResult.ActualData = TC.FAIL;
								TC.FailDescription =" ===ForLoop Wrong KEYWORD Unable to find the keyword Check the key word in keyword column === " + e.getMessage();
								UpdateResult.UpdateStatus();
								return TC.FAIL;
							}

							if(!(Status==null))
							{
								if (Status.equals("FAIL") && TC.OnFailExit.equalsIgnoreCase(TC.StopTestYes) )
								{						
									Logs.Ulog(" === Test Failed on  === "+ TC.Keyword + " === Result State " + PrvKeyword);
									break ;
							    }
							}
						}// End of keyword else

					} // End of Test Execution if
				}// End of test for
				
			  
			}
			Testrow = NewForEnd;	
			
	}
	catch(Throwable T)
	{
Logs.Ulog(" ===Error  While For Loop initialization,  Please  correct it to example : ForLoop , Tobj col =   1 to 10 ");
		
		UpdateResult.ActualData = TC.FAIL;
		TC.ExpectedData = " ===ForLoop Error  While For Loop initialization,  Please  correct it to example : ForLoop , Tobj col =   1 to 10  === " + T.getMessage();
		TC.FailDescription =" ===ForLoop Error  While For Loop initialization,  Please  correct it to example : ForLoop , Tobj col =   1 to 10  === " + T.getMessage();
		UpdateResult.UpdateStatus();
	}
	return ConditionStatus;	
	}
}
