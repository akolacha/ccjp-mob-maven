package entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResponsibleTeam {
	
	int id;

	public ResponsibleTeam(int id) {
		this.id = id;
	}
	
	public ResponsibleTeam(){
		
	}

}
