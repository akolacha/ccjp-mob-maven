package Utility;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import org.apache.commons.io.DirectoryWalker.CancelException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import static Utility.R_Start.StoreTable;
import static Utility.R_Start.Testrow;
import static Utility.R_Start.ResPathFolderOuter;
import static Controller.Controller.ModuleDriver;
import static Utility.R_Start.AllPassMsgBody;
import static Utility.R_Start.HeaderAllPassMsgBody;
/*
 ********************************************************************************
 ********************************************************************************
 //  Automation Selenium
 //  Start date  Jan 2014
 //  Author : Santhosha HC
 //  Test :   Generate HTML Results 

 ********************************************************************************
 ********************************************************************************/
public class HTMLResults {
public static String HtmlResMainFolPath;
public static String  ResPathFolderPath;
public static String  UserNameFolderPath;
public static String  TestSetFolderPath ;
public static String  AllTestFolderPath ;
public static String  ResImageFolderPath;
public static String TestLogsPath ; 

public static String Timestamp; 
public static String  MainResultFilePath;
public static String  DetailedReportFilePath; 
public static String  DetailedReportPDFFilePath;
public static String  LogReportFilePath ;
public static BufferedWriter MainHTMLWrite;
public static BufferedWriter ModuleMainHTMLWrite;
public static BufferedWriter LogHTMLWrite;
public static BufferedWriter DetailedHTMLWrite;
public static String MsgBody = "";
public static String AllModulesMsgBody = "";
public static int S_no = 0;
public static int ModuleS_no = 0;
public static int PassReportTitle= 0;
public static PDDocument PDfdocument;
public static PDPage Pdfpage;

public static  PDPageContentStream PdfPagecontent;
public static  PDImageXObject pdfImageObj;

public static String FrontMainResultFilePath;
public HTMLResults( ) throws IOException
{
	
	//File ResPathFolder =  new File( "C:\\SeleniumAutomation_Test_Frame_Work_Result" );
	File ResPathFolder =  new File( StoreTable.get("ResultPathHTML") +  "\\SeleniumAutomationResult" );
	ResPathFolder.mkdir();
	
	ResPathFolderPath = ResPathFolder.getAbsolutePath();
  
//	File  UserNameFolder =new File(  ResPathFolder.getAbsolutePath() + "\\" + System.getProperty("user.name"));
//	UserNameFolder.mkdir();
//	  UserNameFolderPath = UserNameFolder.getAbsolutePath();
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
    String formattedDate = sdf.format(date);
		
	File TestSetFolder =new File(  ResultExcel.ResPathFolderDate.getAbsolutePath() + "\\" + "HTMLResults");
	TestSetFolder.mkdir();
	TestSetFolderPath = TestSetFolder.getAbsolutePath();
	
	
	File AllTestFolder = new File( TestSetFolder.getAbsolutePath() + "\\" + "All_Test_Set_Report");
	AllTestFolder.mkdir();
	AllTestFolderPath = AllTestFolder.getAbsolutePath();
	 

//	File ResImageFolder =new File(  AllTestFolder.getAbsolutePath()  + "\\" + "Screen_Shots");
//	ResImageFolder.mkdir();
//	ResImageFolderPath = ResImageFolder.getAbsolutePath();
//	
//	
//	File TestLogsFolder= new File(  AllTestFolder.getAbsolutePath() + "\\" + "Log Files");
//	TestLogsFolder.mkdir();
//	TestLogsPath = TestLogsFolder.getAbsolutePath();
	 date = new Date();
	 sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
     formattedDate = sdf.format(date);

       
    	        
 MainResultFilePath = TestSetFolderPath + "\\" + ModuleDriver +StoreTable.get("Execute ON Browser")+formattedDate+".html";
 //  MainResultFilePath = TestSetFolderPath + "\\" + "Main Automated Test Result"+ "Browser Nme_ver "+".html";
   File MainResultFile= new File( MainResultFilePath); 
   MainResultFile.createNewFile();
  
//LogReportFilePath  = TestLogsPath + "\\"+StoreTable.get("Execute ON Browser") + " - " +Timestamp + "Log.Html";
//// LogReportFilePath  = TestLogsPath + "\\"+"Execute ON Browser" + "-" +Timestamp + "Log.Html";
//   File LogReportFile = new File(  LogReportFilePath); 		
//   LogReportFile.createNewFile();
}
	public static void main(String[] args) throws IOException {
		
		
		HTMLResults Rhtml = new HTMLResults();
		Rhtml.FWGenerateMainReport();
		
	}
	
	
	public static  void setHeader(BufferedWriter out) {
		try {
			
			File inputFile = new File(System.getProperty("user.dir") + "/config/Header.css");
			BufferedReader bReader = new BufferedReader(new FileReader(inputFile));
			String line;
			while ((line = bReader.readLine()) != null) {
				out.write(line);
			}

		} catch (Throwable  T) {
			T.printStackTrace();
			
		}
	}
	
	
	public static void setChart(BufferedWriter out,String pass, String fail, String skipped) throws IOException{
		String embedscript = "";
		
		embedscript = "<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>"
	    +"<script type=\"text/javascript\">"
		
		  +"google.charts.load('current', {'packages':['corechart']});"
	      +"google.charts.setOnLoadCallback(drawChart);"
	      +"function drawChart() {"

	        +"var data = google.visualization.arrayToDataTable(["
	          +"['Result', 'No of Tests'],"
	          +"['Passed',  "+ pass +"],"
	          +"['Failed',  "+ fail +"],"
	          +"['Skipped', "+ skipped +"],"
	        +"]);"
	        
//	        + "var options = {title: 'Test Results'};"
			+ "var options = {title: '',slices: {0: { color: 'green' },1: { color: 'red' },2: { color: 'orange' }},"
			+ "chartArea:{left:\"15%\",top:\"5%\",width:\"100%\",height:\"80%\"},height:500, width:600};"

			+"var chart = new google.visualization.PieChart(document.getElementById('piechart'));" 

	        +"chart.draw(data, options);"
	      +"}</script>";
		
		System.out.println("Pass: "+ pass + "Fail: "+ fail);
		out.write(embedscript);
		  //+"<div id=\"piechart\" style=\"width: 900px; height: 500px;\"></div>";
		
	}
	
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
	
	public static void  FWGenerateAllModulesMainReport() throws IOException
	{
		String logoPath = System.getProperty("user.dir") + "/config/Logo2.png";
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy-h_mm_ss a");
	    String formattedDate = sdf.format(date);
		  FrontMainResultFilePath = ResPathFolderOuter + "\\" + "AllModulesMainAutomatedTestResult"+StoreTable.get("Execute ON Browser")+formattedDate.replace("-", "").replace(" ", "")+".html";
		     //  MainResultFilePath = TestSetFolderPath + "\\" + "Main Automated Test Result"+ "Browser Nme_ver "+".html";
		       File FrontMainResultFile= new File( FrontMainResultFilePath); 
		       FrontMainResultFile.createNewFile();  
		
	 //FileWriter FW = new FileWriter(FrontMainResultFilePath );
	 ModuleMainHTMLWrite = new  BufferedWriter(new OutputStreamWriter(  new FileOutputStream(FrontMainResultFilePath), "UTF-8"));
	 
	 setHeader(ModuleMainHTMLWrite);
	 ModuleMainHTMLWrite.write( "<HTML> ");
	 ModuleMainHTMLWrite.write( "<HEAD>");
	 ModuleMainHTMLWrite.write( "<TITLE> All Modules Automated Test Results </TITLE>");
	 
	 
	 ModuleMainHTMLWrite.write( "<style type="+"text/css"+""+">");
	 ModuleMainHTMLWrite.write( "td {FONT-SIZE: 75%; MARGIN: 0px; COLOR: #000000;}");
	 ModuleMainHTMLWrite.write( "td {FONT-FAMILY: verdana,helvetica,arial,sans-serif}");
	 ModuleMainHTMLWrite.write( "a {TEXT-DECORATION: none;}");
	 ModuleMainHTMLWrite.write( "</style>");
	
	 ModuleMainHTMLWrite.write( "</head>");
	
	 ModuleMainHTMLWrite.write( "<BODY>");
	 ModuleMainHTMLWrite.write( " <meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>");
	 
//ModuleMainHTMLWrite.write( "	<center><a href=\"#\" class=\"myButton\"><b> "+StoreTable.get("Header")+"</b></a></center>");
ModuleMainHTMLWrite.write( "<table width=\"100%\"><tbody><tr>"
				+ "<td align=\"left\"><img src="+logoPath+" alt='Logo' height=\"80\" width=\"150\"></td>"
				+ "<td><center><a href=\"#\" class=\"myButton\"><b>"+StoreTable.get("Header")+"</b></a></center></td>"
				+ "<td align=\"right\" width=\"25%\"></td>"
				+ "</tr> </tbody></table>");

//Main Table
ModuleMainHTMLWrite.write( "	<table  cellSpacing=0 cellPadding=0 id='borderedChart'><tr>");
ModuleMainHTMLWrite.write( "<td>");

//Metrics Table
ModuleMainHTMLWrite.write( "	<table width=650 height=300 border=1px");
	 ModuleMainHTMLWrite.write( "	<tr>");
ModuleMainHTMLWrite.write( "	<h4> <FONT COLOR=black FACE=Arial SIZE=4.5> Test Details: </h4>");

ModuleMainHTMLWrite.write( "	<td width=50 align=left bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run Date</b></font></strong></td>");
 	date = new Date();
 	sdf = new SimpleDateFormat("MM-dd-yyyy : h-mm-ss a");
 ModuleMainHTMLWrite.write( "	<td width=100 align=left><FONT COLOR=#153E7E FACE=Arial SIZE=3><b>"+ sdf.format(date) +"</b></td>");
 ModuleMainHTMLWrite.write( "	</tr>");	 
 ModuleMainHTMLWrite.write( "	<tr>");
 ModuleMainHTMLWrite.write( "	<td width=50 align= left  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Server Name</b></font></strong></td>");
 ModuleMainHTMLWrite.write( "	<td width=100 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b> "+ StoreTable.get("Server")+" </b></td>");
 ModuleMainHTMLWrite.write( "	</tr>");
 ModuleMainHTMLWrite.write( "	<tr>");
 ModuleMainHTMLWrite.write( "	<td width=50 align= left  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Release</b></font></strong></td>");
 ModuleMainHTMLWrite.write( "	<td width=100 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>"+ StoreTable.get("Build Version_Description") + "</b></td>");
 ModuleMainHTMLWrite.write( "	</tr>");
 
 ModuleMainHTMLWrite.write( "	<tr>");
 ModuleMainHTMLWrite.write( "	<td width=50 align= left  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Current User</b></font></strong></td>");
 ModuleMainHTMLWrite.write( "	<td width=100 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>"+ System.getProperty("user.name") + "</b></td>");
 ModuleMainHTMLWrite.write( "	</tr>");
 
 ModuleMainHTMLWrite.write( "	<tr>");
 ModuleMainHTMLWrite.write( "	<td width=50 align= left  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Operating System</b></font></strong></td>");
 ModuleMainHTMLWrite.write( "	<td width=100 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>"+ System.getProperty("os.name") + "</b></td>");
 ModuleMainHTMLWrite.write( "	</tr>");
 
	 
	 ModuleMainHTMLWrite.write( "	</table>");
	 
	//Chart table
	 ModuleMainHTMLWrite.write( "	</td><td>"
	 		+ "<table><tr><td><div id=\"piechart\" style=\"width: 650px; height: 500px;\"></div> </td> </tr> </table>"
	 		+ "</td></tr>");
	 
	 //Adding Chart
//	 ModuleMainHTMLWrite.write( "</td><td><table  cellSpacing=0 cellPadding=0 id='bordered'> <tr><td><div id=\"piechart\" style=\"width: 900px; height: 500px;\"> </td></tr></td></table>");
//	 ModuleMainHTMLWrite.write( "	</div><div id=\"piechart\" style=\"width: 900px; height: 500px;\"></div></div>");

	 ModuleMainHTMLWrite.write( "	<table border=0 cellspacing=0 cellpadding=0 ><tr>");
	 ModuleMainHTMLWrite.write( "	<td><FONT COLOR= #000066  FACE= Arial  SIZE=2.75><b></b></td>");
	 ModuleMainHTMLWrite.write( "	</tr></table>");
	 ModuleMainHTMLWrite.write( "	<h5> <FONT COLOR=black FACE= Arial  SIZE=4.5> Detailed Report:</h5>");
	// ModuleMainHTMLWrite.write( "	<h5> <FONT COLOR=660000 FACE= Arial  SIZE=2.5> <u> Smoke Test  Report :</u></h5>");
	  
	 ModuleMainHTMLWrite.write( "	<table  cellSpacing=0 cellPadding=0 id='bordered'>");
	 ModuleMainHTMLWrite.write( "	<tr>");
	 ModuleMainHTMLWrite.write( "	<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Sl.No</b></font></strong></td>");
	 ModuleMainHTMLWrite.write( "	<td width=10%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Module Name</b></font></strong></td>");
	 
	 ModuleMainHTMLWrite.write( "	<td width=5% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Status</b></font></strong></td>");
	 
	 ModuleMainHTMLWrite.write( "	<td width=5% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Testcases Passed</b></font></strong></td>");
	 ModuleMainHTMLWrite.write( "	<td width=5% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Testcases Failed</b></font></strong></td>");
	 
	 ModuleMainHTMLWrite.write( "	<td width=10% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run Start Time</b></font></strong></td>");
	 ModuleMainHTMLWrite.write( "	<td width=10% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run End Time</b></font></strong></td>");
	 ModuleMainHTMLWrite.write( "	<td width=10% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Total Time(sec)</b></font></strong></td>");
	 ModuleMainHTMLWrite.write( "	<td width=20% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Detailed Report</b></font></strong></td>");
	//  ModuleMainHTMLWrite.write( "	<td width=30% align= center  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>Previous Runs</b></td>");
	 ModuleMainHTMLWrite.write( "	</tr>");
	// ModuleMainHTMLWrite.write( "</table>");
	 
 
	 
	 ModuleMainHTMLWrite.flush();
	 //ModuleMainHTMLWrite.close();
	 AllModulesMsgBody = "<HTML> <HEAD>"
	 		+ "<TITLE> All Modules Automated Test Results </TITLE>"
	 		+ "<style type=text/css>td {FONT-SIZE: 75%; MARGIN: 0px; COLOR: #000000;}td {FONT-FAMILY: verdana,helvetica,arial,sans-serif}a {TEXT-DECORATION: none;}</style></head>"
	 		+ "<BODY>  <h2 align=left><FONT COLOR=black FACE=AriaL SIZE=2><b> "	+ StoreTable.get("Body") +"</b>"
	 				+ "</h2> <center><a href='#' class='myButton'><b> "+ StoreTable.get("Header")+"</b></a></center>"
	 						+ "</h4> <table  cellSpacing=0 cellPadding=0 id='bordered' >	"
	 						+ "<tr>	<h4> <FONT COLOR=660000 FACE=Arial SIZE=2.5> <u>Test Details: </u></h4>	"
	 						+ "<td width=150 align=left bgcolor=#153E7E><strong><font color='black' SIZE=2.9><b>Run Date</b></font></strong></td>	"
	 						+ "<td width=250 align=left><FONT COLOR=#153E7E FACE=Arial SIZE=2.75><b>"+ sdf.format(date) +"</b></td>	</tr>	"
	 								+ "<tr>	<td width=150 align= left  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Server Name</b></font></strong></td>	"
	 								+ "<td width=250 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>" + StoreTable.get("Server") +" </b></td>	</tr>	"
	 				+ "<tr>	<td width=150 align= left  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Release</b></font></strong></td>	"
	 				+ "<td width=250 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>"+ StoreTable.get("Build Version_Description") + "</b></td>	</tr>	"
	 				+ "<tr>	<td width=150 align= left  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>User</b></font></strong></td>	"
	 				+ "<td width=250 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>"+ System.getProperty("user.name") + "</b></td>	</tr>	"
	 				+ "<tr>	<td width=150 align= left  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Operationg System</b></font></strong></td>	"
	 				+ "<td width=250 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>"+ System.getProperty("os.name") + "</b></td>	</tr>	"
	 				
	 						+ "</table>	<table cellSpacing=0 cellPadding=0 id='bordered' ><tr>	<td><FONT COLOR= #000066  FACE= Arial  SIZE=2.75><b></b></td>	</tr></table>	"
	 						+ "<h5> <FONT COLOR=660000 FACE= Arial  SIZE=2.5> Detailed Report:</h5>	"
	 						+ "<table  cellSpacing=0 cellPadding=0 id='bordered' >	"
	 						+ "<tr>	 <td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Sl.No</b></font></strong></td>"
	 						+ "<td width=10%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Test Case Name</b></font></strong></td>	"
	 						+ "<td width=5% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Status</b></font></strong></td>	"
	 						+ "<td width=5% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>TestCase Passed</b></font></strong></td>	"
	 						+ "<td width=5% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>TestCase Failed</b></font></strong></td>	"
	 						+ "<td width=10% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run Start Time</b></font></strong></td>	"
	 						+ "<td width=10% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run End Time</b></font></strong></td>	"
	 						+ "<td width=10% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Total Time(Sec)</b></font></strong></td>	"
	 						+ "<td width=20% align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Detailed Report</b></font></strong></td>	</tr>	<tr>";	
	 
	 
	}
	
	public static void  UpdateModuleMainReport() throws IOException
	{
		   ModuleS_no++;
	       ModuleMainHTMLWrite.write( "	<tr>");
	       ModuleMainHTMLWrite.write( "<td width=5%  align= center  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+ ModuleS_no  +"</b></td>");
	       ModuleMainHTMLWrite.write( "	<td width=5%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> " + ModuleDriver +  " </b></td>");
	       AllModulesMsgBody = AllModulesMsgBody + "	<tr><td width=5%  align= center  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+ ModuleS_no  +"</b></td>" +
	       		"<td width=5%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> " + ModuleDriver +  " </b></td>";
	
		     if( TC.SkipModule )
		    	   
		   	{
		    	   AllModulesMsgBody = AllModulesMsgBody +"	<td width=5% align= center  bgcolor=Yellow><FONT COLOR=#000000 FACE= Arial  SIZE=2><b>SKIPPED</b></td>" ;
		    	   ModuleMainHTMLWrite.write( "	<td width=5% align= center  bgcolor=Yellow><FONT COLOR=#000000 FACE= Arial  SIZE=2><b>SKIPPED</b></td>");
		   	
		   	}
		   	else
		   	{
		      
				  if (UpdateResult.FinalStatus.toString().equals("FAIL"))
				 {
					 ModuleMainHTMLWrite.write( "<td width=5% align= center  bgcolor=Red><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.FinalStatus+"</b></td>");
					 AllModulesMsgBody = AllModulesMsgBody + "	<td width=5% align= center  bgcolor=Red><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.FinalStatus+"</b></td>";
					 PrograssBar.DriverProgressBar.setForeground(Color.RED);
					 PrograssBar.TestProgressBar.setForeground(Color.RED);
					 
				 }
				 else
				 {
					 ModuleMainHTMLWrite.write( "	<td width=5% align= center  bgcolor=Green><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.FinalStatus+ " </b></td>");
					 AllModulesMsgBody = AllModulesMsgBody + "	<td width=5% align= center  bgcolor=Green><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.FinalStatus+ " </b></td>";
				 }
		   	}
	 
			 ModuleMainHTMLWrite.write( "	<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + UpdateResult.ModuleTestCasePassed + "</b></td>");
			 ModuleMainHTMLWrite.write( "	<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+ UpdateResult.ModuleTestCaseFailed + "</b></td>");
			 
			 ModuleMainHTMLWrite.write( "	<td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.FinalScriptStartTime+"</b></td>");
			 ModuleMainHTMLWrite.write( "	<td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptEndTime+"</b></td>");
			 ModuleMainHTMLWrite.write( "	<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.FinaltotalTime+"</b></td>");
			 ModuleMainHTMLWrite.write("<td align=center width=20% bgcolor=#EFFBFB><FONT COLOR=#ACF4FB FACE=Arial SIZE=2><b><a href=" + MainResultFilePath +" target=_blank>View Detailed Report</a></b></td>");
			
			 ModuleMainHTMLWrite.write( "	</tr>");
			
			 ModuleMainHTMLWrite.flush();
			 AllModulesMsgBody = AllModulesMsgBody + "<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + UpdateResult.ModuleTestCasePassed + 
					 "</b></td><td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+ UpdateResult.ModuleTestCaseFailed + 
					 "</b></td><td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.FinalScriptStartTime+
					 "</b></td><td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptEndTime+
					 "</b></td><td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.FinaltotalTime+
					 "</b></td><td align=center width=20% bgcolor=#EFFBFB><FONT COLOR=#ACF4FB FACE=Arial SIZE=2><b><a href=" + MainResultFilePath +" target=_blank>View Detailed Report</a></b></td></tr>" ;
			 
				 
		 	 ModuleMainHTMLWrite.flush();
		 }
	
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static void  UpdateTotalModuleMainReport() throws IOException
	{
		
		   //ModuleS_no++;
	       ModuleMainHTMLWrite.write( "	<tr>");
	       ModuleMainHTMLWrite.write( "<td width=5%  align= center  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+ ModuleS_no  +"</b></td>");
	       ModuleMainHTMLWrite.write( "<td width=5%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> Total </b></td>");
	       AllModulesMsgBody = AllModulesMsgBody + "	<tr><td width=5%  align= center  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+ ModuleS_no  +"</b></td><td width=5%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> Total </b></td>";
		 if (UpdateResult.ModuleFinalStatus.toString().equals("FAIL"))
		 {
			 ModuleMainHTMLWrite.write( "<td width=5% align= center  bgcolor=Red><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.ModuleFinalStatus+"</b></td>");
			 AllModulesMsgBody = AllModulesMsgBody + "	<td width=5% align= center  bgcolor=Red><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.ModuleFinalStatus+"</b></td>";
			 PrograssBar.DriverProgressBar.setForeground(Color.RED);
			 PrograssBar.TestProgressBar.setForeground(Color.RED);	 
			 
		 }
		 else
		 {
			 ModuleMainHTMLWrite.write( "	<td width=5% align= center  bgcolor=Green><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.ModuleFinalStatus+ " </b></td>");
			 AllModulesMsgBody = AllModulesMsgBody + "	<td width=5% align= center  bgcolor=Green><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.ModuleFinalStatus+ " </b></td>";
		 }
		
	 
		 ModuleMainHTMLWrite.write( "	<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + UpdateResult.ModuleTotalTestCasePassed +"</b></td>");
		 ModuleMainHTMLWrite.write( "	<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+ UpdateResult.ModuleTotalTestCaseFailed + "</b></td>");
		 ModuleMainHTMLWrite.write( "	<td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ModuleFinalScriptStartTime +"</b></td>");
		 ModuleMainHTMLWrite.write( "	<td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptEndTime+"</b></td>");
		 ModuleMainHTMLWrite.write( "	<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ModuleFinaltotalTime +"</b></td>");
		 ModuleMainHTMLWrite.write( "	<td width=20%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> &nbsp </b></td>");
		 ModuleMainHTMLWrite.write( "	</tr>");
		
		
		 AllModulesMsgBody = AllModulesMsgBody + "<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + UpdateResult.ModuleTotalTestCasePassed
				 + "</b></td><td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+ UpdateResult.ModuleTotalTestCaseFailed
				 + "</b></td><td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ModuleFinalScriptStartTime+
				 "</b></td><td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptEndTime+
				 "</b></td><td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ModuleFinaltotalTime+"</b></td><td width=20%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> &nbsp </b></td></tr>" ;
		 
		//Adding the pie chart data
 		String pass = Integer.toString(UpdateResult.ModuleTotalTestCasePassed) != null ? Integer.toString(UpdateResult.ModuleTotalTestCasePassed) : "0";
		String fail = Integer.toString(UpdateResult.ModuleTotalTestCaseFailed) != null ? Integer.toString(UpdateResult.ModuleTotalTestCaseFailed) : "0";
		String skipped ="";
		if(UpdateResult.ModuleTotalTestCasePassed == 0 && UpdateResult.ModuleTotalTestCaseFailed == 0){
			skipped = "1";
		}else{
			skipped = "0";
		}
		System.out.println("Test:" + Integer.toString(UpdateResult.ModuleTotalTestCasePassed));
		setChart(ModuleMainHTMLWrite,pass,fail,skipped);
		
		ModuleMainHTMLWrite.flush();
				 
	}
	
	
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static void  FWGenerateMainReport() throws IOException
	{
		System.out.println("Enter FWGenerateMainReport");
	 
	 FileWriter FW = new FileWriter(MainResultFilePath);
	 MainHTMLWrite = new BufferedWriter(FW);	
	 setHeader(MainHTMLWrite);
	 MainHTMLWrite.write( "<HTML> ");
	 MainHTMLWrite.write( "<HEAD>");
	 MainHTMLWrite.write( "<TITLE>Automated Test Results </TITLE>");
	 
	 
	 MainHTMLWrite.write( "<style type="+"text/css"+""+">");
	 MainHTMLWrite.write( "td {FONT-SIZE: 75%; MARGIN: 0px; COLOR: #000000;}");
	 MainHTMLWrite.write( "td {FONT-FAMILY: verdana,helvetica,arial,sans-serif}");
	 MainHTMLWrite.write( "a {TEXT-DECORATION: none;}");
	 MainHTMLWrite.write( "</style>");
	
	 MainHTMLWrite.write( "</head>");
	
	 MainHTMLWrite.write( "<BODY>");
	 MainHTMLWrite.write("<table  border=1 cellspacing=1 cellpadding=1 width=100%>");
	 MainHTMLWrite.write("<tr> ");
	 MainHTMLWrite.write("<td align=center width=10% bgcolor=#EFFBFB><FONT COLOR=#ACF4FB FACE=Arial SIZE=2><b><a href=" + FrontMainResultFilePath +" > Click to Go Back </a></b></td>");				
	 
	 MainHTMLWrite.write("</table>");
	 MainHTMLWrite.write("</tr> ");
	 MainHTMLWrite.write( "	<center><a href='#' class='myButton'><b>Module - "+ ModuleDriver +"</b></a></center>");
	 
///*	 MainHTMLWrite.write( "	<table  border=1 cellspacing=1 cellpadding=1 >");
//	 MainHTMLWrite.write( "	<tr>");
//	 
//	 
//	 MainHTMLWrite.write( "	<h4> <FONT COLOR=660000 FACE=Arial SIZE=2.5> <u>Test Details :</u></h4>");
//
//	 MainHTMLWrite.write( "	<td width=150 align=left bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE=Arial SIZE=2.75><b>Run Date</b></td>");*/
	 Date date = new Date();
	 SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy : h-mm-ss a");
	
	   
//	 MainHTMLWrite.write( "	<td width=250 align=left><FONT COLOR=#153E7E FACE=Arial SIZE=2.75><b>"+ sdf.format(date) +"</b></td>");
//	 MainHTMLWrite.write( "	</tr>");	 
//	 MainHTMLWrite.write( "	<tr>");
//	 MainHTMLWrite.write( "	<td width=150 align= left  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2.75><b>Server Name</b></td>");
//	 MainHTMLWrite.write( "	<td width=250 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b> "+ StoreTable.get("Server")+" </b></td>");
//	 MainHTMLWrite.write( "	</tr>");
//	 MainHTMLWrite.write( "	<tr>");
//	 MainHTMLWrite.write( "	<td width=150 align= left  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2.75><b>Release</b></td>");
//	 MainHTMLWrite.write( "	<td width=250 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>"+ StoreTable.get("Build Version_Description") + "</b></td>");
//	 MainHTMLWrite.write( "	</tr>");
//	 MainHTMLWrite.write( "	</table>");

	/* MainHTMLWrite.write( "	<table  cellSpacing=0 cellPadding=0 id='bordered'><tr>");
	 MainHTMLWrite.write( "	<td><FONT COLOR= #000066  FACE= Arial  SIZE=2.75><b></b></td>");
	 MainHTMLWrite.write( "	</tr></table>");*/
	 MainHTMLWrite.write( "	<h5> <FONT COLOR=660000 FACE= Arial  SIZE=2.5> Detailed Report: </h5>");
	  
	 MainHTMLWrite.write( "	<table  cellSpacing=0 cellPadding=0 id='bordered'>");
	 MainHTMLWrite.write( "	<tr>");
	 MainHTMLWrite.write( "	<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Sl.No</b></font></strong></td>");
	 MainHTMLWrite.write( "	<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Test Case Name</b></font></strong></td>");
	 
	 MainHTMLWrite.write( "	<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Status</b></font></strong></td>");
	 
	 MainHTMLWrite.write( "	<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Test Steps Passed</b></font></strong></td>");
	 MainHTMLWrite.write( "	<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Test Steps Failed</b></font></strong></td>");
	 
	 MainHTMLWrite.write( "	<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run Start Time</b></font></strong></td>");
	 MainHTMLWrite.write( "	<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run End Time</b></font></strong></td>");
	 MainHTMLWrite.write( "	<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Total Time(Sec)</b></font></strong></td>");
	 MainHTMLWrite.write( "	<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Detailed Report</b></font></strong></td>");
	//  MainHTMLWrite.write( "	<td width=30% align= center  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>Previous Runs</b></td>");
	 MainHTMLWrite.write( "	</tr>");
	// MainHTMLWrite.write( "</table>");
	 MainHTMLWrite.flush();
	 //MainHTMLWrite.close();
	 MsgBody = "<HTML> <HEAD><TITLE>Automated Test Results </TITLE>"
	 		+ "<style type=text/css>td {FONT-SIZE: 75%; MARGIN: 0px; COLOR: #000000;}td {FONT-FAMILY: verdana,helvetica,arial,sans-serif}a {TEXT-DECORATION: none;}</style></head>"
	 		+ "<BODY>  <h2 align=left><FONT COLOR=black FACE=AriaL SIZE=2><b> "	+ StoreTable.get("Body") +"</b></h2> "
	 				+ "<h4 align=center><FONT COLOR=660066 FACE=AriaL SIZE=4><b><u> "+ StoreTable.get("Header")+"</u></b></h4> "
			+ "<table  cellSpacing=0 cellPadding=0 id='bordered'>"
			+ "<tr><h4> <FONT COLOR=660000 FACE=Arial SIZE=2.5> <u>Test Details: </u></h4>	"
			+ "<td width=150 align=left bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run Date</b></font></strong></td>	"
			+ "<td width=250 align=left><FONT COLOR=#153E7E FACE=Arial SIZE=2.75><b>"+ sdf.format(date) +"</b></td>	</tr>	"
				+ "<tr>	<td width=150 align= left  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Server Name</b></font></strong></td>	"
					+ "	<td width=250 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>" + StoreTable.get("Server") +" </b></td>	</tr>	"
			+ "<tr>	<td width=150 align= left  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Release</b></font></strong></td>	"
			+ "<td width=250 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>"+ StoreTable.get("Build Version_Description") + "</b></td>	</tr>	</table>	"
			
			+ "<table cellSpacing=0 cellPadding=0 id='bordered' >"
			+ "<tr>	<td><FONT COLOR= #000066  FACE= Arial  SIZE=2.75><b></b></td>	</tr></table>	"
			+ "<h5> <FONT COLOR=660000 FACE= Arial  SIZE=2.5> Detailed Report:</h5>	"
			+ "<table  cellSpacing=0 cellPadding=0 id='bordered'>	"
			+ "<tr>	 <td width=3%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Sl.No</b></font></strong></td>"
			+ "<td width=7%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Testcase Name</b></font></strong></td>"
			+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Status</b></font></strong></td>"
			+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Testcase Passed</b></font></strong></td>"
			+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Testcase Failed</b></font></strong></td>"
			+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run Start Time</b></font></strong></td>"
			+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run End Time</b></font></strong></td>"
			+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Total Time(Sec)</b></font></strong></td>"
			+ "<td width=20% align= center  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>Detailed Report</b></td>	</tr>	<tr>";	
	 
		
		if (PassReportTitle == 0)
		  {
			HeaderAllPassMsgBody = "<HTML> <HEAD><TITLE>Automated Test Results </TITLE><style type=text/css>td {FONT-SIZE: 75%; MARGIN: 0px; COLOR: #000000;}td {FONT-FAMILY: verdana,helvetica,arial,sans-serif}a {TEXT-DECORATION: none;}</style></head><BODY>  <h2 align=left><FONT COLOR=black FACE=AriaL SIZE=2><b> "	+ StoreTable.get("Body") +"</b></h2> <h4 align=center><FONT COLOR=660066 FACE=AriaL SIZE=4><b><u> "
						+ StoreTable.get("Header")+"</u></b></h4> <table  cellSpacing=0 cellPadding=0 id='bordered'>	"
								+ "<tr>	<h4> <FONT COLOR=660000 FACE=Arial SIZE=2.5> <u>Test Details :</u></h4>	"
									+ "<td width=150 align=left bgcolor=#599bb3><FONT COLOR=#E0E0E0 FACE=Arial SIZE=2.9><b>Run Date</b></td>"
									+ "<td width=250 align=left><FONT COLOR=#153E7E FACE=Arial SIZE=2.75><b>"+ sdf.format(date) +"</b></td>	</tr>	"
								+ "<tr>	"
									+ "<td width=150 align= left  bgcolor=#599bb3><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2.9><b>Server Name</b></td>"
									+ "<td width=250 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>" + StoreTable.get("Server") +" </b></td>	</tr>	"
								+ "<tr>	"
									+ "<td width=150 align= left  bgcolor=#599bb3><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2.9><b>Release</b></td>	"
									+ "<td width=250 align= left ><FONT COLOR=#153E7E FACE= Arial  SIZE=2.75><b>"+ StoreTable.get("Build Version_Description") + "</b></td>	</tr>	</table>	"
								+ "<table border=0 cellspacing=0 cellpadding=0 ><tr>	"
								+ "<td><FONT COLOR= #000066  FACE= Arial  SIZE=2.75><b></b></td>	</tr></table>	<h5> <FONT COLOR=660000 FACE= Arial  SIZE=2.5> Detailed Report :</h5>"; 
			
						 					
			  PassReportTitle  = PassReportTitle +1;
		  }
		  
		AllPassMsgBody = "<center><a href='#' class='myButton'><b> Detailed Report - Module: "+ ModuleDriver +"</b></a></center>"  
				+ "<table  cellSpacing=0 cellPadding=0 id='bordered'>	"
				+ "<tr>	 <td width=3%  align= center  bgcolor=#599bb3><strong><font color='black'><b>Sl.No</b></font></strong></td>"
				+ "<td width=7%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Testcase Name</b></font></strong></td>"
				+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Status</b></font></strong></td>"
				+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Testcase Passed</b></font></strong></td>"
				+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Testcase Failed</b></font></strong></td>"
				+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run Start Time</b></font></strong></td>"
				+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Run End Time</b></font></strong></td>"
				+ "<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Total Time(Sec)</b></font></strong></td>"
				+ "<td width=20% align= center  bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>Detailed Report</b></td>	</tr>	<tr>";	
		  
		System.out.println("EXIT FWGenerateMainReport");
	}
	
	
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static void  UpdateMainReport() throws IOException
	{
	    
		System.out.println("Enter UpdateMainReport");
		
	       MainHTMLWrite.write( "	<tr> <td width=5%  align= center  bgcolor=#599bb3><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+ S_no++  +"</b></td>"
	       		+ "<td width=20%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+TC.TestModuleName +"</b></td>");
	      
	       MsgBody = MsgBody + " <tr> <td width=5%  align= center  bgcolor=#599bb3><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+ S_no  +"</b></td>"
	       		+ "<td width=20%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + TC.TestModuleName + "</b></td>";
	       AllPassMsgBody = AllPassMsgBody +  " <tr> <td width=5%  align= center  bgcolor=#599bb3><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+ S_no  +"</b></td>"
	       		+ "<td width=20%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + TC.TestModuleName + "</b></td>";
	       
	       if(TC.SkipTestCase || UpdateResult.TestCasePassed==0 )	
	   	{
	   		 MsgBody = MsgBody +"	<td width=5% align= center  bgcolor=Yellow><FONT COLOR=#000000 FACE= Arial  SIZE=2><b>SKIPPED</b></td>" ;
	   		 MainHTMLWrite.write( "	<td width=5% align= center  bgcolor=Yellow><FONT COLOR=#000000 FACE= Arial  SIZE=2><b>SKIPPED</b></td>");
	   	
	   	}	       	       
	   	else
	   	{
	       
			 if (UpdateResult.GlobalStatus.toString().equals("FAIL"))
			 {
				 UpdateResult.ModuleTestCaseFailed++;
				 UpdateResult.ModuleTotalTestCaseFailed++;
				 MsgBody = MsgBody +"	<td width=5% align= center  bgcolor=Red><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+UpdateResult.GlobalStatus+"</b></td>" ;
				 MainHTMLWrite.write( "	<td width=5% align= center  bgcolor=Red><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+UpdateResult.GlobalStatus+"</b></td>");
			 }
			 else
			 {
				 UpdateResult.ModuleTestCasePassed++;
				 UpdateResult.ModuleTotalTestCasePassed ++;
				 MainHTMLWrite.write( "	<td width=5% align= center  bgcolor=Green><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> PASS </b></td>");
				 MsgBody = MsgBody + "	<td width=5% align= center  bgcolor=Green><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> PASS </b></td>";
				 AllPassMsgBody = AllPassMsgBody +"	<td width=5% align= center  bgcolor=Green><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> PASS </b></td>";
			 }
			 
	   	}
		 MainHTMLWrite.write( "	<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + UpdateResult.TestCasePassed + "</b></td>");
		 MainHTMLWrite.write( "	<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+ UpdateResult.TestCaseFailed + "</b></td>");
		 
		 MainHTMLWrite.write( "	<td width=15% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptStartTime+"</b></td>");
		 MainHTMLWrite.write( "	<td width=15% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptEndTime+"</b></td>");
		 MainHTMLWrite.write( "	<td width=10% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.totalTime+"</b></td>");
		 MainHTMLWrite.write("<td align=center width=20% bgcolor=#EFFBFB><FONT COLOR=#ACF4FB FACE=Arial SIZE=2><b><a href=" + DetailedReportFilePath +" target=_blank>View Detailed Report</a></b></td>");
		
		
		 MainHTMLWrite.write( "	</tr>");
		//**********************************************************************************************
		 MsgBody = MsgBody +  "<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + UpdateResult.TestCasePassed + 
				 "</b></td> <td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+ UpdateResult.TestCaseFailed +
				 "</b></td><td width=15% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptStartTime+
				 "</b></td> <td width=15% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptEndTime+
				 "</b></td>	<td width=10% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.totalTime+
				 "</b></td> <td align=center width=20% bgcolor=#EFFBFB><FONT COLOR=#ACF4FB FACE=Arial SIZE=2><b><a href=" + DetailedReportFilePath +
				 " target=_blank>View Detailed Report</a></b></td></str>";
		 
		 AllPassMsgBody = AllPassMsgBody +   "<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + UpdateResult.TestCasePassed + 
				 "</b></td> <td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+ UpdateResult.TestCaseFailed +
				 "</b></td><td width=15% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptStartTime+
				 "</b></td> <td width=15% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptEndTime+
				 "</b></td>	<td width=10% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.totalTime+
				 "</b></td> <td align=center width=20% bgcolor=#EFFBFB><FONT COLOR=#ACF4FB FACE=Arial SIZE=2><b><a href=" + DetailedReportFilePath +
				 " target=_blank>View Detailed Report</a></b></td></str>";
		//**********************************************************************************************
		 
		 
			MainHTMLWrite.flush();
			
			System.out.println("EXIT UpdateMainReport");
	 
	}
	
	
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static void  UpdateTotalMainReport() throws IOException
	{
		System.out.println("Enter UpdateTotalMainReport");
		
	       MainHTMLWrite.write( "	<tr>");
	       MainHTMLWrite.write( "<td width=5%  align= center  bgcolor=#599bb3><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+ S_no  +"</b></td>");
	       MainHTMLWrite.write( "	<td width=5%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> Total </b></td>");
	       MsgBody = MsgBody + "	<tr><td width=5%  align= center  bgcolor=#599bb3><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+ S_no  +"</b></td> <td width=5%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> Total </b></td>";
	       AllPassMsgBody = AllPassMsgBody + "	<tr><td width=5%  align= center  bgcolor=#599bb3><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b>"+ S_no  +"</b></td> <td width=5%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> Total </b></td>";
	       
	 if (UpdateResult.FinalStatus.toString().equals("FAIL"))
	 {
		 MainHTMLWrite.write( "	<td width=5% align= center  bgcolor=Red><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.FinalStatus+"</b></td>");
		 MsgBody = MsgBody + "	<td width=5% align= center  bgcolor=Red><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.FinalStatus+"</b></td>";
		  PrograssBar.DriverProgressBar.setForeground(Color.RED);
		  PrograssBar.TestProgressBar.setForeground(Color.RED);
	 }
	 else
	 {
		 MainHTMLWrite.write( "	<td width=5% align= center  bgcolor=Green><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.FinalStatus+ " </b></td>");
		 MsgBody = MsgBody + "	<td width=5% align= center  bgcolor=Green><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.FinalStatus+ " </b></td>";
		 AllPassMsgBody = AllPassMsgBody +"	<td width=5% align= center  bgcolor=Green><FONT COLOR=#E0E0E0 FACE= Arial  SIZE=2><b> "+UpdateResult.FinalStatus+ " </b></td>";
	 }
		
	 
		 MainHTMLWrite.write( "	<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + UpdateResult.TotalTestCasePassed + "</b></td>");
		 MainHTMLWrite.write( "	<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+ UpdateResult.TotalTestCaseFailed + "</b></td>");
		 
		 MainHTMLWrite.write( "	<td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.FinalScriptStartTime+"</b></td>");
		 MainHTMLWrite.write( "	<td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptEndTime+"</b></td>");
		 MainHTMLWrite.write( "	<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.FinaltotalTime+"</b></td>");
		 MainHTMLWrite.write( "	<td width=20%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> &nbsp </b></td>");
		
		
		 MainHTMLWrite.write( "	</tr>");
		 
		 
		 MainHTMLWrite.flush();
		 MsgBody = MsgBody + "<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + UpdateResult.TotalTestCasePassed + "</b></td><td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+ UpdateResult.TotalTestCaseFailed + "</b></td><td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.FinalScriptStartTime+"</b></td><td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptEndTime+"</b></td><td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.FinaltotalTime+"</b></td><td width=20%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> &nbsp </b></td></tr>" ;
		 AllPassMsgBody = AllPassMsgBody + "<td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>" + UpdateResult.TotalTestCasePassed + "</b></td><td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+ UpdateResult.TotalTestCaseFailed + "</b></td><td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.FinalScriptStartTime+"</b></td><td width=20% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.ScriptEndTime+"</b></td><td width=5% align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b>"+R_Start.FinaltotalTime+"</b></td><td width=20%  align= center bgcolor=#EFFBFB ><FONT COLOR=#190707 FACE= Arial  SIZE=2><b> &nbsp </b></td></tr>" ;
		 
		 System.out.println("Exit UpdateTotalMainReport");
	}
	
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
	public static void  HTMLDetailedReportInitialize () throws IOException, CancelException
	{

		FileWriter FW = new FileWriter(DetailedReportFilePath);
		
	DetailedHTMLWrite = new  BufferedWriter(new OutputStreamWriter(  new FileOutputStream(DetailedReportFilePath), "UTF-8"));
	setHeader(DetailedHTMLWrite);
	  //DetailedHTMLWrite = new BufferedWriter(FW); 
	DetailedHTMLWrite.write("<html>");
	DetailedHTMLWrite.write("<head>");
	DetailedHTMLWrite.write(" <style>  table {table-layout:fixed; }   table td {word-wrap:break-word;}   ");
	
	DetailedHTMLWrite.write("[tooltip]:before {	    /* needed - do not touch */ 	    content: attr(tooltip);	    position: absolute;	    opacity: 0;	    	    /* customizable */	    transition: all 0.15s ease;	    padding: 10px;	    color: #333;	    border-radius: 10px;	    box-shadow: 2px 2px 1px silver;  	}	[tooltip]:hover:before {	    /* needed - do not touch */	    opacity: 1;   /* customizable */   background: #58ACFA;	    margin-top: -50px;	    margin-left: 20px;   	}	[tooltip]:not([tooltip-persistent]):before {	    pointer-events: none;	}	</style>");	
	
	DetailedHTMLWrite.write("<title>");
	DetailedHTMLWrite.write(TC.TestModuleName +  " - Detailed Report");
	DetailedHTMLWrite.write("</title>");
	DetailedHTMLWrite.write("</head>");
	DetailedHTMLWrite.write("<body>");
	DetailedHTMLWrite.write( " <meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>");
	
	 DetailedHTMLWrite.write("<table  border=1 cellspacing=1    cellpadding=1 width=100%>");
	 DetailedHTMLWrite.write("<tr> ");
	 DetailedHTMLWrite.write("<td align=center width=10% bgcolor=#EFFBFB><FONT COLOR=#ACF4FB FACE=Arial SIZE=2><b><a href=" + MainResultFilePath +" > Click to Go Back </a></b></td>");				
	
	 DetailedHTMLWrite.write("</table>");
	 DetailedHTMLWrite.write("</tr> ");
	 
	 DetailedHTMLWrite.write("<h4> <FONT COLOR=660000 FACE=Arial SIZE=3> Detailed Report :  " + TC.TestModuleName + "</h4>");
	 DetailedHTMLWrite.write("<table  cellSpacing=0 cellPadding=0 id='bordered' >");
	 DetailedHTMLWrite.write("<tr> ");
	 DetailedHTMLWrite.write("<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Step#</b></font></strong></td>");
	 DetailedHTMLWrite.write("<td width=7%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Module Name</b></font></strong></td>");
	 DetailedHTMLWrite.write("<td width=7%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Requirements</b></font></strong></td>");
	 DetailedHTMLWrite.write("<td width=10%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Test Description</b></font></strong></td>");
	 DetailedHTMLWrite.write("<td width=10%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Keywords</b></font></strong></td>");
	 DetailedHTMLWrite.write("<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Pass Description</b></font></strong></td>");
	 DetailedHTMLWrite.write("<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Fail Description</b></font></strong></td>");
	
	 DetailedHTMLWrite.write("<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Expected Results</b></font></strong></td>");
	 DetailedHTMLWrite.write("<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Actual Results</b></font></strong></td>");
	 DetailedHTMLWrite.write("<td width=5%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Result</b></font></strong></td>");
	 DetailedHTMLWrite.write("<td width=4%  align= center  bgcolor=#599bb3><strong><font color='black' SIZE=2.9><b>Screen Shot</b></font></strong></td>");
	 //DetailedHTMLWrite.write("<td align=center width=15% align=center bgcolor=#153E7E><FONT COLOR=#E0E0E0 FACE=Arial SIZE=2><b>Result Log</b></td>");
	 DetailedHTMLWrite.write("</tr>");
	 DetailedHTMLWrite.flush();
	 PFileGenerate();
		
	 }
public static void PFileGenerate() throws IOException, CancelException
{

	PDfdocument = new PDDocument();
	 Pdfpage= new PDPage();
	 PDfdocument.addPage(Pdfpage);	
	PdfPagecontent = new PDPageContentStream(PDfdocument, Pdfpage, AppendMode.APPEND, true);
	 
    
	PdfPagecontent.setFont(PDType1Font.TIMES_BOLD_ITALIC, 20);
	PdfPagecontent.setNonStrokingColor(Color.blue);
    
     PdfPagecontent.beginText();
     PdfPagecontent.moveTextPositionByAmount(200, 770 );
     try {
		PdfPagecontent.drawString( " Detailed Report :  " + TC.TestModuleName  );
	} catch (Throwable  e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     PdfPagecontent.endText();
     PdfPagecontent.setFont(PDType1Font.TIMES_BOLD_ITALIC, 12);
     PdfPagecontent.setNonStrokingColor(Color.BLACK);
     PdfPagecontent.drawLine(0, 750, 612, 750);
     PdfPagecontent.beginText();
     PdfPagecontent.moveTextPositionByAmount(150, 740 );
     Date date = new Date();
     SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy : h-mm-ss a");
     try {
		PdfPagecontent.drawString( " Execution Date                                                  :  " + sdf.format(date)   );
	} catch (Throwable  e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     PdfPagecontent.endText();
     PdfPagecontent.drawLine(0, 730, 612, 730);
     PdfPagecontent.beginText();
     PdfPagecontent.moveTextPositionByAmount(150 ,720);
     try {
		PdfPagecontent.drawString( " Executed on Server                                        :  " + StoreTable.get("Server")   );
	} catch (Throwable  e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     PdfPagecontent.endText();
     PdfPagecontent.drawLine(0, 710, 612, 710);
     PdfPagecontent.beginText();
     PdfPagecontent.moveTextPositionByAmount(150, 700 );
     try {
		PdfPagecontent.drawString( " Release                                                               :  " + StoreTable.get("Build Version_Description")  );
	} catch (Throwable  e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     PdfPagecontent.endText();
     PdfPagecontent.drawLine(0, 690, 612, 690);
     PdfPagecontent.beginText();
     PdfPagecontent.moveTextPositionByAmount(150, 680 );
     try {
		PdfPagecontent.drawString( "Executed by                                                        :  " +  System.getProperty("user.name")  );
	} catch (Throwable  e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     PdfPagecontent.endText();
     PdfPagecontent.drawLine(0, 670, 612, 670);
     PdfPagecontent.beginText();
     PdfPagecontent.moveTextPositionByAmount(150 ,660  );
     try {
		PdfPagecontent.drawString( "Operating System                                                 :  " +  System.getProperty("os.name")  );
	} catch (Throwable  e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     PdfPagecontent.endText();
     PdfPagecontent.drawLine(0, 650, 612, 650);
     PdfPagecontent.beginText();
     PdfPagecontent.moveTextPositionByAmount( 150, 640);
     try {
		PdfPagecontent.drawString( "Executed Browser/Device                                  :  " +  StoreTable.get("Execute ON Browser")  );
	} catch (Throwable  e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     PdfPagecontent.endText();     
     PdfPagecontent.drawLine(0, 630, 612, 630);
     PdfPagecontent.close();
     //PDfdocument.save(DetailedReportPDFFilePath);
     //PDfdocument.close();
}
	
	public static String UpdateHtmlStatusPass() throws IOException
	{	
		 
         DetailedHTMLWrite.write("<tr> ");
		 DetailedHTMLWrite.write("<td wrap align=center width=5%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>"+TC.TCID+"</b></td>");
		 DetailedHTMLWrite.write("<td wrap  align=center width=30%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +TC.Module +"</b></td>");
		 DetailedHTMLWrite.write("<td  wrap align=center width=5%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>"+TC.Requirements+"</b></td>");
		 DetailedHTMLWrite.write("<td  tooltip= \"" + TC.TestObjects + "---" + TC.FindByProp + "---" + TC.FindByPropVal +"\" wrap align=center width=30%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" + TC.TestDescription +"</b></td>");
       String Kw;
       if(TC.Keyword.length()>20)
	    {
		 Kw = TC.Keyword.substring(0,20) + " "+ TC.Keyword.substring(15, TC.Keyword.length()) ;
       
		 DetailedHTMLWrite.write("<td  wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +Kw+"_ "+ R_Start.Testrow+"</b></td>");
	    }
	    else
	    {
	    	 DetailedHTMLWrite.write("<td  wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +TC.Keyword+"_ "+ R_Start.Testrow+"</b></td>");
	    }
		 DetailedHTMLWrite.write("<td  wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +TC.PassDescription+"</b></td>");
		 DetailedHTMLWrite.write("<td  wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
		 DetailedHTMLWrite.write("<td  wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +TC.ExpectedData+"</b></td>");		 
		 DetailedHTMLWrite.write("<td  wrap  align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +UpdateResult.ActualData+"</b></td>");
		 DetailedHTMLWrite.write("<td  wrap width=10% align= center  bgcolor=#BCE954><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> PASS </b></td>");
		 if(StoreTable.get("CaptureScreeShotOnEachStep").toString().equalsIgnoreCase("YY"))
		 DetailedHTMLWrite.write("<td  wrap  align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b><a href=" +UpdateResult.CurrentFilepath+" target=_blank>Screen Shot</a></b></td>");
		 else
		 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");	 
		 
		 DetailedHTMLWrite.write("</tr>");
		 DetailedHTMLWrite.flush();
		 
		 return TC.PASS; 
	}
	
	public static String UpdatePDFStatusPass() throws IOException
	{	
		 
		try
		{
		PDPage Pdfpage= new PDPage();
	    PDfdocument.addPage(Pdfpage);	
		 PDPageContentStream PdfPagecontent = new PDPageContentStream(PDfdocument, Pdfpage, AppendMode.APPEND, true);    
		 
		 PdfPagecontent.setFont(PDType1Font.TIMES_BOLD_ITALIC, 12);
		PdfPagecontent.setNonStrokingColor(Color.GREEN);
				    
		PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 770 );
	    try {
			PdfPagecontent.drawString( " STEP NO              :  " + TC.TCID  + " <==> STEP STATUS : PASS" );
		} catch (Throwable  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.setNonStrokingColor(Color.BLACK);
	    PdfPagecontent.setFont(PDType1Font.TIMES_BOLD_ITALIC, 9);
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 760 );
	       
	    try {
			PdfPagecontent.drawString( " Module                   :  " + TC.Module  );
		} catch (Throwable  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 750 );
	    try {
			PdfPagecontent.drawString( " Requirements          :  " + TC.Requirements  );
		} catch (Throwable  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 740 );
	    try {
			PdfPagecontent.drawString( " Test Descrption         :  " +  TC.TestDescription );
		} catch (Throwable  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 730 );
	    try {
			PdfPagecontent.drawString( " Test Object           :  " +  TC.TestObjects + "---" + TC.FindByProp + "---" + TC.FindByPropVal  );
		} catch (Throwable  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 720 );
	    try {
			PdfPagecontent.drawString( "Keyword                :  " +TC.Keyword+"_ "+ R_Start.Testrow  );
		} catch (Throwable  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 710 );
	    try {
			PdfPagecontent.drawString( "Pass Description  :  " +TC.PassDescription );
		} catch (Throwable  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 700 );
	    try {
			PdfPagecontent.drawString( "Expeted Data       :  " +TC.ExpectedData );
		} catch (Throwable  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 690 );
	    try {
			PdfPagecontent.drawString( "Actual Data       :  " +UpdateResult.ActualData);
		} catch (Throwable  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    PdfPagecontent.endText();
	    	
	    //PdfPagecontent.closePath();	
        if(StoreTable.get("CaptureScreeShotOnEachStep").toString().equalsIgnoreCase("YY"))
        {	
	    	 pdfImageObj = PDImageXObject.createFromFile(UpdateResult.CurrentFilepath, PDfdocument);
	    	 PdfPagecontent.drawImage(pdfImageObj, 0,0, 612, 600);
	    }
	   
	   
	   
       
        PdfPagecontent.close();
	    PDfdocument.save(DetailedReportPDFFilePath);
	}
	    catch(Throwable t)
		 {
			 Logs.Ulog(" === Error while Updating the PDF reports Pass status " + t.getMessage());
			 try
			 {
				 	
				    PDfdocument.save(DetailedReportPDFFilePath);
			 }
			 catch(Throwable T)
			 {
				 Logs.Ulog(" === Error while Updating the PDF reports Pass status " + t.getMessage());
			 }
			 
		 }
		//PDfdocument.close();
		
		
		 return TC.PASS; 
	}
	
	public static String UpdatePDFStatusFAIL() throws IOException
	{	
		 try
		 {
			 
		 
		 
		PDPage Pdfpage= new PDPage();
	    PDfdocument.addPage(Pdfpage);	
		 PDPageContentStream PdfPagecontent = new PDPageContentStream(PDfdocument, Pdfpage, AppendMode.APPEND, true);    
		 
		 PdfPagecontent.setFont(PDType1Font.TIMES_BOLD_ITALIC, 12);
		PdfPagecontent.setNonStrokingColor(Color.RED);
				    
		PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 770 );
	    try {
			PdfPagecontent.drawString( " STEP NO              :  " + TC.TCID  + " <==> STEP STATUS : FAIL" );
		} catch (Exception e6) {
			// TODO Auto-generated catch block
			e6.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.setNonStrokingColor(Color.BLACK);
	    PdfPagecontent.setFont(PDType1Font.TIMES_BOLD_ITALIC, 9);
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 760 );
	    try {
			PdfPagecontent.drawString( " Module                   :  " + TC.Module  );
		} catch (Exception e5) {
			// TODO Auto-generated catch block
			e5.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 750 );
	    try {
			PdfPagecontent.drawString( " Requirements          :  " + TC.Requirements  );
		} catch (Exception e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 740 );
	    try {
			PdfPagecontent.drawString( " Test Descrption         :  " +  TC.TestDescription );
		} catch (Exception e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 730 );
	    try {
			PdfPagecontent.drawString( " Test Object           :  " +  TC.TestObjects + "---" + TC.FindByProp + "---" + TC.FindByPropVal  );
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 720 );
	    try {
			PdfPagecontent.drawString( "Keyword                :  " +TC.Keyword+"_ "+ R_Start.Testrow  );
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.setNonStrokingColor(Color.RED);
	    PdfPagecontent.moveTextPositionByAmount(0, 710 );
	    if (TC.FailDescription.length()>100)	    	
	    {
	    	String a = TC.FailDescription.substring(0,100);
	    	PdfPagecontent.drawString( "Fail Description  :  " + a  );
	    	
	    }
	    else
	    {
	    	PdfPagecontent.drawString( "Fail Description  :  " + TC.FailDescription  );
	    }
	    
	    
	    PdfPagecontent.setNonStrokingColor(Color.BLACK);
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 700 );
	    try {
			PdfPagecontent.drawString( "Expeted Data       :  " +TC.ExpectedData );
		} catch (Throwable  T) {
			// TODO Auto-generated catch block
			T.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 690 );
	    try {
			PdfPagecontent.drawString( "Actual Data       :  " +UpdateResult.ActualData);
		} catch (Throwable  T) {
			// TODO Auto-generated catch block
			T.printStackTrace();
		}
	    PdfPagecontent.endText();
	   
	    try
	    {
	    	
	    
        if(StoreTable.get("CaptureScreeShotOnError").toString().equalsIgnoreCase("YY"))
        {	
	    	 pdfImageObj = PDImageXObject.createFromFile(UpdateResult.CurrentFilepath, PDfdocument);
	    	 PdfPagecontent.drawImage(pdfImageObj, 0,0, 612, 600);
	    }
	   
	    }
	    catch (Throwable T)
	    {
	    	 Logs.Ulog(" === Error while Updating the PDF capturing the screen reports fail status " + T.getMessage());
	    }
	   
	   
	    PdfPagecontent.close();
	    PDfdocument.save(DetailedReportPDFFilePath);
	   
		 }
		 catch(Throwable t)
		 {
			 Logs.Ulog(" === Error while Updating the PDF reports fail status " + t.getMessage());
			 try
			 {
				
				    PDfdocument.save(DetailedReportPDFFilePath);
			 }
			 catch(Throwable T)
			 {
				 Logs.Ulog(" === Error while Updating the PDF reports Pass status " + t.getMessage());
			 }
		 }
		
		 return TC.PASS; 
	}
	
	
	
	public static String UpdateHtmlStatusFail() throws IOException
	{	
		
	 DetailedHTMLWrite.write("<tr> ");
	 DetailedHTMLWrite.write("<td wrap align=center width=5%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>"+TC.TCID+"</b></td>");
	 DetailedHTMLWrite.write("<td  wrap align=center width=30%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +TC.Module +"</b></td>");
	 DetailedHTMLWrite.write("<td wrap align=center width=5%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>"+TC.Requirements+"</b></td>");
	 DetailedHTMLWrite.write("<td tooltip= \"" + TC.TestObjects + "---" + TC.FindByProp + "---" + TC.FindByPropVal +"\"   wrap align=center width=30%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" + TC.TestDescription +"</b></td>");
	 String Kw;
	 if(TC.Keyword.length()>20)
	    {
		 Kw = TC.Keyword.substring(0,20) + " "+ TC.Keyword.substring(15, TC.Keyword.length()) ;
    
		 DetailedHTMLWrite.write("<td  wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +Kw+"_ "+ R_Start.Testrow+"</b></td>");
	    }
	    else
	    {
	    	 DetailedHTMLWrite.write("<td  wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +TC.Keyword+"_ "+ R_Start.Testrow+"</b></td>");
	    }
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>"+TC.FailDescription+"</b></td>");	 
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +TC.ExpectedData+"</b></td>");	
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +UpdateResult.ActualData+"</b></td>");
	 DetailedHTMLWrite.write("<td wrap width=10% align= center  bgcolor=Red><FONT COLOR=#153E7E FACE= Arial  SIZE=2><b> FAIL </b></td>");
	 if(StoreTable.get("CaptureScreeShotOnError").toString().equalsIgnoreCase("YY") || StoreTable.get("CaptureScreeShotOnEachStep").toString().equalsIgnoreCase("YY"))
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b><a href=" +UpdateResult.CurrentFilepath+" target=_blank>Screen Shot</a></b></td>");
	 else
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
	 
	 DetailedHTMLWrite.write("</tr>");
	 DetailedHTMLWrite.flush();
	 UpdateResult.GlobalStatus = TC.FAIL;
	 return TC.FAIL; 		
		
	}
	
	public static String UpdateHtmlmodule() throws IOException
	{		
		
	 DetailedHTMLWrite.write("<tr bgcolor=#00FFFF> ");
	 DetailedHTMLWrite.write("<td wrap align=center width=5%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>"+TC.TCID+"</b></td>");
	 DetailedHTMLWrite.write("<td  wrap align=center width=30%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +TC.Module +"</b></td>");
     DetailedHTMLWrite.write("<td wrap align=center width=5%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>"+TC.Requirements+"</b></td>");
     DetailedHTMLWrite.write("<td  wrap align=center width=30%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>"+TC.TestDescription+"</b></td>");
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");		
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
	 DetailedHTMLWrite.write("<td wrap width=10% align= center  bgcolor=#00FFFF><FONT COLOR=#153E7E FACE= Arial  SIZE=2><b> &nbsp  </b></td>");
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
	 DetailedHTMLWrite.write("</tr>");
	 DetailedHTMLWrite.flush();
		 return TC.FAIL; 	
	}
	
	public static String UpdateHtmlDone() throws IOException
	{		
		
	 DetailedHTMLWrite.write("<tr > ");
	 DetailedHTMLWrite.write("<td wrap align=center width=5%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>"+TC.TCID+"</b></td>");
	 DetailedHTMLWrite.write("<td  wrap align=center width=30%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +TC.Module +"</b></td>");
     DetailedHTMLWrite.write("<td wrap align=center width=5%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>"+TC.Requirements+"</b></td>");
     DetailedHTMLWrite.write("<td tooltip= "+TC.TestObjects +"wrap  align=center width=30%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> "+TC.TestDescription+" </b></td>");
     String Kw;
     TestNgReportsForDashboard.TestNgSteps.add(TC.TCID  +" "+ TC.Module +" "+ TC.Requirements +" "+ TC.TestDescription +" : "+ "DONE");
		
     if(TC.Keyword.length()>20)
	    {
		 Kw = TC.Keyword.substring(0,20) + " "+ TC.Keyword.substring(15, TC.Keyword.length()) ;
    
		 DetailedHTMLWrite.write("<td  wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +Kw+"_ "+ R_Start.Testrow+"</b></td>");
	    }
	    else
	    {
	    	 DetailedHTMLWrite.write("<td  wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b>" +TC.Keyword+"_ "+ R_Start.Testrow+"</b></td>");
	    }
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");		
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
	 DetailedHTMLWrite.write("<td wrap width=10% align= center  bgcolor=#BCE954><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> DONE </b></td>");
	 DetailedHTMLWrite.write("<td wrap align=center width=10%><FONT COLOR=#153E7E FACE=Arial SIZE=2><b> &nbsp </b></td>");
	 DetailedHTMLWrite.write("</tr>");
	 DetailedHTMLWrite.flush();
		 return TC.PASS; 	
	}
	public static String UpdatePDFStatusDone() throws IOException
	{	
		 
		try
		{
		PDPage Pdfpage= new PDPage();
	    PDfdocument.addPage(Pdfpage);	
		 PDPageContentStream PdfPagecontent = new PDPageContentStream(PDfdocument, Pdfpage, AppendMode.APPEND, true);    
		 
		 PdfPagecontent.setFont(PDType1Font.TIMES_BOLD_ITALIC, 12);
		PdfPagecontent.setNonStrokingColor(Color.GREEN);
				    
		PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 770 );
	    try {
			PdfPagecontent.drawString( " STEP NO              :  " + TC.TCID  + " <==> STEP STATUS : DONE" );
		} catch (Exception e8) {
			// TODO Auto-generated catch block
			e8.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.setNonStrokingColor(Color.BLACK);
	    PdfPagecontent.setFont(PDType1Font.TIMES_BOLD_ITALIC, 9);
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 760 );
	    try {
			PdfPagecontent.drawString( " Module                   :  " + TC.Module  );
		} catch (Exception e7) {
			// TODO Auto-generated catch block
			e7.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 750 );
	    try {
			PdfPagecontent.drawString( " Requirements          :  " + TC.Requirements  );
		} catch (Exception e6) {
			// TODO Auto-generated catch block
			e6.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 740 );
	    try {
			PdfPagecontent.drawString( " Test Descrption         :  " +  TC.TestDescription );
		} catch (Exception e5) {
			// TODO Auto-generated catch block
			e5.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 730 );
	    try {
			PdfPagecontent.drawString( " Test Object           :  " +  TC.TestObjects + "---" + TC.FindByProp + "---" + TC.FindByPropVal  );
		} catch (Exception e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 720 );
	    try {
			PdfPagecontent.drawString( "Keyword                :  " +TC.Keyword+"_ "+ R_Start.Testrow  );
		} catch (Exception e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 710 );
	    try {
			PdfPagecontent.drawString( "Pass Description  :  " +TC.PassDescription );
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 700 );
	    try {
			PdfPagecontent.drawString( "Expeted Data       :  " +TC.ExpectedData );
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    PdfPagecontent.endText();
	    PdfPagecontent.beginText();
	    PdfPagecontent.moveTextPositionByAmount(0, 690 );
	    try {
			PdfPagecontent.drawString( "Actual Data       :  " +UpdateResult.ActualData);
		} catch (Throwable  T) {
			// TODO Auto-generated catch block
			T.printStackTrace();
		}
	    PdfPagecontent.endText();
	       
	    PdfPagecontent.close();	
	    PDfdocument.save(DetailedReportPDFFilePath);
	    
	 }
	 catch(Throwable t)
	 {
		 Logs.Ulog(" === Error while Updating the PDF reports Done status " + t.getMessage());
		 try
		 {
			 PdfPagecontent.close();	
			    PDfdocument.save(DetailedReportPDFFilePath);
		 }
		 catch(Throwable T)
		 {
			 Logs.Ulog(" === Error while Updating the PDF reports Pass status " + t.getMessage());
		 }
	 }
		//PDfdocument.close();
	    return TC.PASS; 
	}
	
	
}
