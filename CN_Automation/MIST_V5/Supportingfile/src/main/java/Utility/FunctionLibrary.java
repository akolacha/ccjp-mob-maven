package Utility;

import static Utility.FunctionLibrary.driver;
import static Utility.R_Start.StoreTable;
import static Utility.R_Start.WinScreen;
import static Utility.R_Start.Window;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.apache.pdfbox.io.RandomAccessRead;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/*import autoitx4java.AutoItX;*/


import Controller.Server;

import com.asprise.util.ocr.OCR;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;
import com.google.common.base.Function;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class FunctionLibrary extends UserdefinedLibrary {

	public FunctionLibrary() throws InterruptedException {
		super();
	}

	public static ProfilesIni allProfiles;
	public static RemoteWebDriver driver;
	public static int StoreNum1, StoreNum2, StoreNum3, StoreNum4;
	public static String Storeval1, Storeval2, StoreVal3, Storeval4,
			RANDOMCONST, RANDOMCONSTPlusStoreval1, RANDOMStoreval1;
	public static FluentWait<WebDriver> Sync;
	public static JavascriptExecutor js;
	public static Database Db;
	public static Alert aa;
	public static RobotMouse_key C_RoboMouse;
	public static Robot RMouse;
	public static int implicitWaitTime;
	public static List<String> ItemList;
	public static ExcelObj Report;
	public static String nanoStart;
	public static String milliStart;
	public static String nanoEnd;
	public static String milliEnd;
	public final static String OPT_WIN_TITLE_MATCH_MODE = "WinTitleMatchMode";
	public static long nanoTime;
	public static long milliTime;
	public static String Executeon;

	protected static String BrowserSync() throws InterruptedException,
			IOException {
		try {
			Logs.Ulog(TC.TestDescription);
			Sync.until(ExpectedConditions.invisibilityOfElementLocated(By
					.xpath((String) StoreTable.get("PageSyncObject"))));
			return TC.PASS;
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String CatchStatementWebElement(String ErrMsg)
			throws IOException, InterruptedException {

		Logs.Ulog(TC.TestDescription);
		Logs.Ulog("ERROR ------ Unable to find the object " + ErrMsg);
		UpdateResult.ActualData = TC.FAIL;
		TC.FailDescription = "May be wrong inputs Script Error ";
		UpdateResult.UpdateStatus();
		return TC.FAIL;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String BrowserRefresh() throws IOException,
			InterruptedException {

		Logs.Ulog("Refresh Browser");
		driver.navigate().refresh();
		UpdateResult.Done();

		return TC.PASS;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String BrowserBack() throws IOException, InterruptedException {

		Logs.Ulog("Refresh BrowserBack");
		driver.navigate().back();
		UpdateResult.Done();
		return TC.PASS;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String BrowserForward() throws IOException,
			InterruptedException {

		Logs.Ulog("Refresh BrowserForward");
		driver.navigate().forward();
		UpdateResult.Done();

		return TC.PASS;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String CheckCheckBox() throws IOException,
			InterruptedException {

		Logs.Ulog("checkbox checked");
		try {
			WebElement Chkbox = TORObj();
			String checked = TORObj().getAttribute("checked");
			if (checked == null)// checkbox is unchecked
				Chkbox.click();
			BrowserSync();
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String Click() throws IOException, InterruptedException {

		UpdateDescription("Click on ");
		Logs.Ulog("Executing ObjClick ");
		try {
			WebElement Obj = TORObj();
			// System.out.println(Obj.getText());
			// HighlightElement();
			// ((JavascriptExecutor)
			// driver).executeScript("arguments[0].scrollIntoView(true);",Obj);
			// Obj.click();

			try {
				WrapsDriver wrappedElement = (WrapsDriver) Obj;
				JavascriptExecutor js = (JavascriptExecutor) driver;
				wrappedElement.getWrappedDriver();
				js.executeScript("arguments[0].click();", Obj);
			} catch (Throwable e) {
				Obj.click();
			}

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;
			// Logs.Ulog("Clicked on obj " + Obj.getText());
			// Sync();
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String CloseBrowser() {

		Logs.Ulog("Closing the browser");
		try {

			try {
				driver.close();

			} catch (Throwable e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				driver.quit();
			} catch (Throwable e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			return UpdateResult.Done();
		} catch (Throwable e) {
			return TC.FAIL + "Unable to close browser. Check if its open"
					+ e.getMessage();
		}

	}

	public static String CloseBrowser_Window_Tab() throws IOException,
			InterruptedException {

		Logs.Ulog("Clsoing Browser Window / Tab " + TC.InputData);
		try {
			Set<String> WinID = driver.getWindowHandles();
			Iterator<String> it = WinID.iterator();
			// WinID.size();
			for (int WID = 1; WID <= WinID.size(); WID++) {
				String[] a = TC.InputData.split("Tab");

				int Tno = Integer.parseInt(a[1]);
				String TabID = it.next();
				if (Tno == WID) {
					driver.switchTo().window(TabID).close();
				}
			}

			Logs.Ulog("Successfully closed tabbed/new window" + TC.InputData);
			return UpdateResult.Done();
		} catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			try {
				UpdateResult.FailDescription = e.getMessage();
			} catch (Throwable e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String dublicatecheck() throws IOException,
			InterruptedException {

		Logs.Ulog("Selecting from list");
		TC.InputData = TC.InputData.replace(".0", "");
		TC.InputData = TC.InputData.replaceAll(" ", "");
		try {

			WebElement Q1List = TORObj();

			String Q2 = getORObject(TC.Param1);
			WebElement Q2List = TORObj();

			String Q3 = getORObject(TC.Param2);
			WebElement Q3List = TORObj();

			// HighlightElement();
			List<String> nlist = new ArrayList<String>();
			List<WebElement> q1options = Q1List.findElements(By
					.tagName("option"));
			List<WebElement> q2options = Q2List.findElements(By
					.tagName("option"));
			List<WebElement> q3options = Q3List.findElements(By
					.tagName("option"));

			for (WebElement q1option : q1options) {

				nlist.add(q1option.getText());

			}
			for (WebElement q2option : q2options) {

				nlist.add(q2option.getText());

			}
			for (WebElement q3option : q3options) {

				nlist.add(q3option.getText());

			}

			Set<String> allqset = new HashSet<String>(nlist);

			int sizeofset = allqset.size();
			System.out.println("sizeofset =" + sizeofset);
			if ((nlist.size() - 2) == allqset.size()) {
				UpdateResult.ActualData = "No Dublicates";
			} else {
				UpdateResult.ActualData = "Dublicates in the selected list";
			}
			// Sync();
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			Logs.Ulog("Error ---  while checking dublicates in list");
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String CloseifAlertPresent() throws InterruptedException,
			IOException {

		try {

			// Check the presence of alert
			Alert alert = driver.switchTo().alert();
			// Alert present; set the flag
			UpdateResult.ActualData = alert.getText();

			// if present consume the alert
			alert.accept();
			BrowserSync();
			TC.ExpectedData = "Alert Should not popup";
			return UpdateResult.UpdateStatus();

		} catch (NoAlertPresentException ex) {
			// Alert not present

			Logs.Ulog("Alert not present" + ex.getMessage());
			return CatchStatementWebElement(ex.getMessage());
		}

	}

	public static String CloseifAlertPresentCheckBoxSelect()
			throws InterruptedException, AWTException, IOException {

		try {

			// Check the presence of alert
			Alert alert = driver.switchTo().alert();
			// Alert present; set the flag
			UpdateResult.ActualData = alert.getText();

			TC.ExpectedData = alert.getText();
			return UpdateResult.UpdateStatus();

		} catch (NoAlertPresentException ex) {
			// Alert not present

			Logs.Ulog("Alert not present" + ex.getMessage());
			return CatchStatementWebElement(ex.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String DbClick() throws IOException, InterruptedException {

		Logs.Ulog("Executing ObjDbClick ");
		try {
			WebElement Obj = TORObj();
			// System.out.println(TORObj().toString());
			// System.out.println(TORObj().getText());
			Actions Mouse = new Actions(driver);
			Logs.Ulog(" Double Clicked on obj " + Obj.getText());
			Mouse.doubleClick(Obj).build().perform();
			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String DbClickJS() throws IOException, InterruptedException {

		Logs.Ulog("Executing ObjDbClick ");
		try {
			WebElement Obj = TORObj();
			TORObj().click();
			((JavascriptExecutor) driver)
					.executeScript(
							"var evt = document.createEvent('MouseEvents'); evt.initMouseEvent('dblClick',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0,null); arguments[0].dispatchEvent(evt);",
							Obj);

			Actions Mouse = new Actions(driver);
			Logs.Ulog(" Double Clicked on obj " + Obj.getText());
			Mouse.doubleClick(Obj).build().perform();
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].dblclick", Obj);
			// String autofocus = (String) ((JavascriptExecutor)
			// driver).executeScript("return arguments[0].innerHTML", Obj);
			// System.out.println(autofocus);
			// ((JavascriptExecutor)
			// driver).executeScript("alert('Hello from Selenium!');");
			// JavascriptExecutor js = (JavascriptExecutor) driver;
			// js.executeScript("alert('Value is "+Obj.getText()+");");

			((JavascriptExecutor) driver)
					.executeScript(
							"var evt = document.createEvent('MouseEvents'); evt.initMouseEvent('dblClick',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0,null); arguments[0].dispatchEvent(evt);",
							Obj);

			((JavascriptExecutor) driver).executeScript("arguments[0].click",
					Obj);
			// TimeUnit.SECONDS.sleep(2);

			((JavascriptExecutor) driver)
					.executeScript(
							"var evt = document.createEvent('MouseEvents'); evt.initMouseEvent('dblclick', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0,null); arguements[0].dispatchEvent(evt);",
							Obj);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String DragandDropToElement() throws IOException,
			InterruptedException {

		Logs.Ulog("Start of DragandDropToElement");

		try

		{
			WebElement SourceElement = TORObj();

			getORObject(TC.InputData);

			WebElement DescElement = TORObj();

			(new Actions(driver)).dragAndDrop(SourceElement, DescElement)
					.perform();

			return TC.PASS;

		}

		catch (Throwable e)

		{

			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String DragandDropToElementCoOrdinates() throws IOException,
			InterruptedException {

		Logs.Ulog("Start of DragandDropToElementCoOrdinates");

		try

		{
			driver.switchTo().defaultContent();
			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			getORObject("Page(Content).LeftTree(Image)");
			C_RoboMouse.MoveMouseToWebEleCoord(TORObj());
			C_RoboMouse.robotPoweredMouseDown();

			driver.switchTo().frame("cq-cf-frame");
			getORObject("Dialog(\"EditComponent\").Image(\"Drop an Image\")");

			C_RoboMouse.MoveMouseToWebEleCoord(TORObj());
			C_RoboMouse.robotPoweredClick();
			C_RoboMouse.MouseRelease();
			C_RoboMouse.MouseRelease();
			System.out.println("Done");

			return TC.PASS;

		}

		catch (Throwable e)

		{

			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String GenerateRandomStringWithDate() throws IOException,
			InterruptedException {

		Logs.Ulog("Get Object count before");

		try

		{

			Storeval1 = (new Date()).toString().replace(":", "")
					.replace(" ", "");

			return Storeval1;

		}

		catch (Throwable e)

		{
			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String GenerateRANDOMCONST() throws IOException,
			InterruptedException {

		Logs.Ulog("Get Object count before");

		try

		{

			RANDOMCONST = (new Date()).toString().replace(":", "")
					.replace(" ", "");

			return RANDOMCONST;

		}

		catch (Throwable e)

		{
			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String GenerateRandomSSN() throws IOException,
			InterruptedException {

		Logs.Ulog("Get Object count before");

		try

		{

			int m = new Date().getMinutes();
			int dt = new Date().getDate();
			int h = new Date().getHours();
			int Da = new Date().getDay();
			int mo = new Date().getMonth();
			int s = new Date().getSeconds();
			int y = new Date().getYear();
			int y1 = new Date().getYear();
			String RSSN = m + "" + s + "" + h + "" + Da + "" + mo + y + "" + y1;
			String SSN1 = RSSN.substring(0, 3) + "-" + RSSN.substring(3, 5)
					+ "-" + RSSN.substring(5, 9);

			Storeval1 = SSN1;
			// Storeval1 =RSSN.substring(0,9);

			return Storeval1;

		}

		catch (Throwable e)

		{
			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String GetAttributeVal_Verify() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the VerifyAttributeVal");
		try {
			UpdateResult.ActualData = TORObj().getAttribute(TC.InputData);

			// TORObj().isDisplayed();
			// TC.ExpectedData = TC.Param1;
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String GetAttributeVal_Verify_ByIndex() throws IOException,
			InterruptedException {
		Logs.Ulog("Verifying the text GetAttributeVal_Verify_ByIndex ");
		try {
			List<WebElement> EleCollection = WebElementsTORObj();
			int index = (int) Double.parseDouble(TC.InputData);
			UpdateResult.ActualData = EleCollection.get(index).getAttribute(
					TC.Param1);
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	public static String GetAttributeVal_Verify_WebElems() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the GetAttributeVal_Verify_WebElems ");
		try {
			List<WebElement> ObjElems = WebElementsTORObj();
			int Count = 0;
			int Icount = ObjElems.size();
			for (int i = 0; i < ObjElems.size(); i++) {
				if (ObjElems.get(i).getAttribute(TC.InputData)
						.equals(TC.ExpectedData)) {
					Count++;
				}
			}
			if (Icount == Count) {
				UpdateResult.ActualData = Icount + TC.ExpectedData;
				TC.ExpectedData = Count + TC.ExpectedData;
				return UpdateResult.UpdateStatus();
			} else {
				UpdateResult.ActualData = Icount + TC.ExpectedData;
				TC.ExpectedData = Count + TC.ExpectedData;
				return UpdateResult.UpdateStatus();
			}
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String GetAttributeValWebElements_Verify_Byref()
			throws IOException, InterruptedException {

		Logs.Ulog("Executing GetAttributeValWebElements_Verify_Byref Menu");
		String ByRefObj = TC.Param2;
		String ByRefAttr = TC.Param3;
		String ByRefAttrVal = TC.Param4;

		List<WebElement> EleCollection = WebElementsTORObj();
		getORObject(ByRefObj);
		List<WebElement> EleCollectionRef = WebElementsTORObj();
		int Count = 0;
		try {
			for (int i = 0; i < EleCollectionRef.size(); i++) {

				if (EleCollectionRef.get(i).getAttribute(ByRefAttr)
						.equals(ByRefAttrVal)) {

					if (EleCollection.get(i).getAttribute(TC.InputData)
							.equals(TC.ExpectedData)) {

						Count++;
						String Edata = TC.ExpectedData;
						UpdateResult.ActualData = EleCollection.get(i)
								.getAttribute(TC.InputData);
						UpdateResult.UpdateStatus();
						TC.ExpectedData = Edata;
					}

				}

			}

			int no_Ele = (int) Double.parseDouble(TC.Param1);
			if (Count == no_Ele) {
				UpdateResult.ActualData = "Itemas Found = " + Count;
				TC.ExpectedData = "Itemas Found = " + no_Ele;
				Logs.Ulog("Verifying the text Get ElementsByRef Passed ");
				return UpdateResult.UpdateStatus();

			} else {
				UpdateResult.ActualData = "Attributes not matched";
				TC.ExpectedData = "";
				Logs.Ulog("Verifying the text Get ElementsByRef failed ");
				return UpdateResult.UpdateStatus();
			}

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String GetPostionWebEleAfter() throws IOException,
			InterruptedException {

		Logs.Ulog("Get Object GetPostionWebEleAfter");

		try

		{
			WebElement element = TORObj();
			Point elementLoc = element.getLocation();
			StoreNum3 = elementLoc.getX();
			StoreNum4 = elementLoc.getY();
			return TC.PASS;
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	public static String GetPostionWebEleBefore() throws IOException,
			InterruptedException {
		Logs.Ulog("Get Object GetPostionWebEleBefore");
		try {
			WebElement element = TORObj();
			Point elementLoc = element.getLocation();
			StoreNum1 = elementLoc.getX();
			StoreNum2 = elementLoc.getY();
			return TC.PASS;
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	public static String dsuGetPostion() throws IOException,
			InterruptedException {
		Logs.Ulog("Get Object GetPostionWebEleBefore");
		try {
			WebElement element = TORObj();
			Point elementLoc = element.getLocation();
			Dimension size = element.getSize();
			// System.out.println(elementLoc);
			// System.out.println(size);
			TC.ExpectedData = elementLoc.toString() + size.toString();
			UpdateResult.ActualData = elementLoc.toString() + size.toString();
			if (TC.ExpectedData == TC.InputData)
				Logs.Ulog("Verifying the DSUGetPaosition ");
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	public static String HighlightElement() throws InterruptedException {
		Logs.Ulog("High light on object");
		WebElement element = null;
		try {
			element = TORObj();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			for (int i = 0; i < 2; i++) {
				WrapsDriver wrappedElement = (WrapsDriver) element;
				JavascriptExecutor js = (JavascriptExecutor) driver;
				wrappedElement.getWrappedDriver();

				js.executeScript(
						"arguments[0].setAttribute('style',arguments[1]);",
						element, "color: black ; border: 6px solid black;");
				Thread.sleep(250L); // color: black ; border: 4px solid yellow;
				js.executeScript(
						"arguments[0].setAttribute('style',arguments[1]);",
						element, "");
				Thread.sleep(250L);
			}
		} catch (Throwable t) {
			Logs.Ulog("ERROR----Failed on High light on object");
		}
		// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
		// element, "color: black ; border: 6px solid black;");
		// Thread.sleep(350L);
		// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
		// element, "");

		Logs.Ulog("Object Highlighted");
		return TC.PASS;
	}

	public static String ClickElement() throws InterruptedException,
			IOException {
		Logs.Ulog("Click on obj");
		WebElement element = TORObj();
		try {

			WrapsDriver wrappedElement = (WrapsDriver) element;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			wrappedElement.getWrappedDriver();

			js.executeScript("arguments[0].click", element);

		} catch (Throwable t) {
			Logs.Ulog("ERROR----Failed to click on object");
		}
		// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
		// element, "color: black ; border: 6px solid black;");
		// Thread.sleep(350L);
		// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
		// element, "");

		Logs.Ulog("Object clicked");
		return TC.PASS;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String HighlightElements(WebElement element)
			throws InterruptedException {
		Logs.Ulog("High light on object");

		try {
			for (int i = 0; i < 2; i++) {
				WrapsDriver wrappedElement = (WrapsDriver) element;
				JavascriptExecutor js = (JavascriptExecutor) driver;
				wrappedElement.getWrappedDriver();

				js.executeScript(
						"arguments[0].setAttribute('style',arguments[1]);",
						element, "color: black ; border: 6px solid black;");
				Thread.sleep(250L); // color: black ; border: 4px solid yellow;
				js.executeScript(
						"arguments[0].setAttribute('style',arguments[1]);",
						element, "");
				Thread.sleep(250L);
			}
		} catch (Throwable t) {
			Logs.Ulog("ERROR----Failed on High light on object");
		}
		// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
		// element, "color: black ; border: 6px solid black;");
		// Thread.sleep(350L);
		// js.executeScript("arguments[0].setAttribute('style',arguments[1]);",
		// element, "");

		Logs.Ulog("Object Highlighted");
		return TC.PASS;
	}

	// ######################################################################################################################

	public static String InputText() throws IOException, InterruptedException {

		Logs.Ulog(" Executing InputText " + TC.InputData);

		try {
			if (TC.InputData.startsWith("RANDOMCONST+")) {
				if (RANDOMCONST == null)
					GenerateRANDOMCONST();

				String[] v1 = TC.InputData.split("\\+");
				String RandVar1 = RANDOMCONST + v1[1];
				RANDOMCONSTPlusStoreval1 = RandVar1;
				TORObj().clear();
				TORObj().sendKeys(RANDOMCONSTPlusStoreval1);
				UpdateResult.ActualData = RANDOMCONSTPlusStoreval1;
				TC.ExpectedData = RandVar1;
				Logs.Ulog("InputText on obj Done ");
			} else if (TC.InputData.startsWith("RANDOM+")) {
				TORObj().clear();
				String[] v1 = TC.InputData.split("\\+");
				String RandVar1 = GenerateRandomStringWithDate() + v1[1];
				TORObj().sendKeys(RandVar1);
				UpdateResult.ActualData = RandVar1;
				TC.ExpectedData = RandVar1;
				Logs.Ulog("InputText on obj Done ");
			} else {
				switch (TC.InputData) {
				case "RANDOM":
					TORObj().clear();
					TORObj().sendKeys(GenerateRandomStringWithDate());
					RANDOMStoreval1 = GenerateRandomStringWithDate();
					UpdateResult.ActualData = RANDOMStoreval1;
					TC.ExpectedData = RANDOMStoreval1;
					Logs.Ulog("InputText on obj Done ");
					break;
				case "RANDOMCONST":
					if (RANDOMCONST == null)
						GenerateRANDOMCONST();
					TORObj().clear();
					TORObj().sendKeys(RANDOMCONST);
					UpdateResult.ActualData = RANDOMCONST;
					TC.ExpectedData = RANDOMCONST;
					Logs.Ulog("InputText on obj Done ");

				case "RANDOMSSN":

					TORObj().clear();
					TORObj().sendKeys(GenerateRandomSSN());
					UpdateResult.ActualData = Storeval1;
					TC.ExpectedData = Storeval1;
					Logs.Ulog("InputText on obj Done ");
					break;

				default:
					TORObj().clear();
					String str = TC.InputData.trim();
					TORObj().sendKeys(TC.InputData.trim());
					UpdateResult.ActualData = TC.InputData.trim();
					TC.ExpectedData = TC.InputData.trim();
					Logs.Ulog("InputText on obj Done ");
					break;
				}

			}
			UpdateDescription("Enter text  " + TC.ExpectedData + " in ");
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String SetInputText() throws IOException,
			InterruptedException {

		Logs.Ulog(" Executing InputText ");

		try {
			WebElement Obj = TORObj();
			Logs.Ulog("InputText on obj " + Obj.getText());
			Obj.clear();
			if (TC.InputData.equalsIgnoreCase("RANDOM")) {
				Obj.sendKeys("T");
				Obj.clear();
				js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].value='"
						+ GenerateRandomStringWithDate() + "'", Obj);
				UpdateResult.ActualData = Storeval1;
				TC.ExpectedData = Storeval1;
				Logs.Ulog("InputText on obj Done ");
			} else {
				Obj.sendKeys("T");
				Obj.clear();
				js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].value='" + TC.InputData + "'",
						Obj);

			}
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String SetAttribute() throws IOException,
			InterruptedException {

		Logs.Ulog(" Executing SetAttribute ");

		try {
			WebElement Obj = TORObj();
			Logs.Ulog("SetAttribute on obj " + Obj.getText());

			js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0]." + TC.Param1 + "='" + TC.InputData
					+ "'", Obj);

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String InputTextElementsByIndex() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the text InputTextElementsByIndex ");
		try {
			List<WebElement> EleCollection = WebElementsTORObj();
			int index = (int) Double.parseDouble(TC.InputData);
			EleCollection.get(index).clear();

			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			if (TC.Param1.equalsIgnoreCase("RANDOM")) {
				EleCollection.get(index).clear();
				EleCollection.get(index).sendKeys(
						GenerateRandomStringWithDate());
				UpdateResult.ActualData = Storeval1;
				TC.ExpectedData = Storeval1;
			} else {
				EleCollection.get(index).clear();
				EleCollection.get(index).sendKeys(TC.Param1);
				UpdateResult.ActualData = TC.Param1;
				TC.ExpectedData = TC.Param1;
			}
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String SetInputTextByIndex() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the text InputTextElementsByIndex ");
		try {
			List<WebElement> EleCollection = WebElementsTORObj();
			int index = (int) Double.parseDouble(TC.InputData);
			EleCollection.get(index).clear();

			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			if (TC.Param1.equalsIgnoreCase("RANDOM")) {
				EleCollection.get(index).clear();
				EleCollection.get(index).sendKeys("T");

				EleCollection.get(index).clear();
				js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].value='"
						+ GenerateRandomStringWithDate() + "'",
						EleCollection.get(index));
				UpdateResult.ActualData = Storeval1;
				TC.ExpectedData = Storeval1;
			} else {
				EleCollection.get(index).clear();
				EleCollection.get(index).sendKeys("T");
				EleCollection.get(index).clear();
				js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].value='" + TC.Param1 + "'",
						EleCollection.get(index));
				UpdateResult.ActualData = TC.Param1;
				TC.ExpectedData = TC.Param1;
			}
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String InputTextElementsByValue() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the text InputTextElementsByIndex ");
		try {
			List<WebElement> EleCollection = WebElementsTORObj();

			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			if (TC.InputData.equalsIgnoreCase("RANDOM")) {
				for (int i = 0; i < EleCollection.size(); i++) {
					if (EleCollection.get(i).getAttribute("value")
							.equals(TC.Param1)) {
						EleCollection.get(i).clear();
						EleCollection.get(i).sendKeys(
								GenerateRandomStringWithDate());
						// Thread.sleep(1000L);
					}
				}

			} else {
				for (int i = 0; i <= EleCollection.size(); i++) {
					if (EleCollection.get(i).getAttribute("value")
							.equals(TC.Param1)) {
						EleCollection.get(i).clear();
						EleCollection.get(i).sendKeys(TC.Param2);

					}
				}

			}
			return UpdateResult.Done();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String SetInputTextByValue() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the text InputTextElementsByIndex ");
		try {
			List<WebElement> EleCollection = WebElementsTORObj();

			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			if (TC.InputData.equalsIgnoreCase("RANDOM")) {
				for (int i = 0; i < EleCollection.size(); i++) {
					if (EleCollection.get(i).getAttribute("value")
							.equals(TC.Param1)) {
						EleCollection.get(i).clear();
						EleCollection.get(i).sendKeys("T");
						EleCollection.get(i).clear();
						js = (JavascriptExecutor) driver;
						js.executeScript("arguments[0].value='"
								+ GenerateRandomStringWithDate() + "'",
								EleCollection.get(i));
						Thread.sleep(1000L);
					}
				}

			} else {
				for (int i = 0; i <= EleCollection.size(); i++) {
					if (EleCollection.get(i).getAttribute("value")
							.equals(TC.Param1)) {
						EleCollection.get(i).clear();
						EleCollection.get(i).sendKeys("T");
						EleCollection.get(i).clear();
						js = (JavascriptExecutor) driver;
						js.executeScript("arguments[0].value='" + TC.Param2
								+ "'", EleCollection.get(i));

					}
				}

			}
			return UpdateResult.Done();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String MaximizeWindow() throws IOException,
			InterruptedException {

		try {
			Logs.Ulog("Maximize window");
			driver.manage().window().maximize();
			return TC.PASS;

		} catch (Throwable e) {
			return TC.PASS;
			//return CatchStatementWebElement(e.getMessage());
			
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String MouseclickAndHold() throws InterruptedException,
			IOException

	{

		Logs.Ulog("Executing MouseclickAndHold Menu");

		try

		{

			WebElement Obj = TORObj();
			Logs.Ulog(" MouseclickAndHold  on obj " + Obj.getText());
			Actions Mouse = new Actions(driver);
			Mouse.clickAndHold(Obj).perform();
			Thread.sleep(1000L);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String MouseMoveOnObj() throws InterruptedException,
			IOException {

		Logs.Ulog("Executing Select Menu");
		try {
			WebElement Obj = TORObj();
			Logs.Ulog(" Mouse move on obj " + Obj.getText());
			Actions Mouse = new Actions(driver);
			Mouse.moveToElement(Obj).perform();
			Thread.sleep(500L);
			// BrowserSync();
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String navigate() throws IOException, InterruptedException {

		Logs.Ulog("Navigating to URL");
		try {
			// driver.navigate(TC.InputData);
			Thread.sleep(6000);
			driver.navigate().to(TC.InputData);
			if (driver.getTitle().contains("Certificate Error")) {
				driver.findElementById("overridelink").click();
				Thread.sleep(10000);
			}
			return UpdateResult.Done();
		} catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String navigateByConcat() throws IOException,
			InterruptedException {

		Logs.Ulog("Navigating to URL");
		try {
			// driver.navigate(TC.InputData);
			driver.navigate().to(
					TC.InputData + TC.ExpectedData + TC.Param1 + TC.Param2);

			return UpdateResult.Done();
		} catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String GetURL() throws IOException, InterruptedException {

		Logs.Ulog("Navigating to URL");
		try {
			driver.get(TC.InputData);
			// driver.navigate().to(TC.InputData);

			return UpdateResult.Done();
		} catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String ClickElementsByIndex() throws IOException,
			InterruptedException

	{

		Logs.Ulog("Verifying the text ObjClickElementsByIndex ");

		try

		{

			List<WebElement> EleCollection = WebElementsTORObj();

			int index = (int) Double.parseDouble(TC.InputData);

			EleCollection.get(index).click();

			UpdateResult.ActualData = TC.PASS;

			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String ObjectGetAfterCount() throws IOException,
			InterruptedException {

		Logs.Ulog("Get Object count before");
		try {
			List<WebElement> EleCollection = WebElementsTORObj();
			StoreNum2 = EleCollection.size();
			return TC.PASS;
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	public static String ObjectGetBeforeCount() throws IOException,
			InterruptedException {

		Logs.Ulog("Get Object count before");

		try {
			List<WebElement> EleCollection = WebElementsTORObj();
			StoreNum1 = EleCollection.size();
			return TC.PASS;
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyObjExist() throws IOException,
			InterruptedException {

		Logs.Ulog("Checking existance of element");
		try {
			boolean a = TORObj().isDisplayed();
			UpdateResult.ActualData = String.valueOf(a);
			TC.ExpectedData = String.valueOf(true);
			TC.PassDescription = "Object Exist ";
			TC.FailDescription = "Object Not Exist ";
			UpdateDescription("Verify Existance of the Object");
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {

			UpdateResult.ActualData = "FALSE";
			TC.ExpectedData = TC.InputData;
			return UpdateResult.UpdateStatus();
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyObjNotExist() throws IOException,
			InterruptedException {

		Logs.Ulog("Checking existance of element");
		try {
			boolean a = NonExceptionTORObj().isDisplayed();
			UpdateResult.ActualData = "Object Displayed";
			TC.ExpectedData = "Object should not Displayed ";
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {

			UpdateResult.ActualData = "Object Not Displayed";
			TC.ExpectedData = "Object Not Displayed";
			return UpdateResult.UpdateStatus();

		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyRegularExpression() throws IOException,
			InterruptedException {

		Logs.Ulog("Verify regular expression");
		try {
			boolean a = TC.TestObjects.matches(TC.InputData);
			UpdateResult.ActualData = String.valueOf(a);
			TC.ExpectedData = String.valueOf(true);
			TC.PassDescription = "Pattern matches ";
			TC.FailDescription = "Pattern don't matche ";
			UpdateDescription("Verify Pattern matches");
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {

			UpdateResult.ActualData = "FALSE";
			TC.ExpectedData = TC.InputData;
			return UpdateResult.UpdateStatus();
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String OpenApp() throws IOException, InterruptedException {

		try {

			driver = null;
			driver = null;
			C_RoboMouse = null;
			RMouse = null;

			if (RMouse == null)
				Logs.Ulog("Kill Process");
			// KillProcess();
			Logs.Ulog("Opening browser");
			// if (StoreTable.get("Execute ON Browser").equals("Mozilla")) {
			if (StoreTable.get("Execute ON Browser").equals("Mozilla")) {

				try {

					if (StoreTable.get("Execute On Grid").equals("YY")) {

						DesiredCapabilities capability = DesiredCapabilities
								.firefox();
						driver = new RemoteWebDriver(new URL(
								(String) StoreTable.get("Set Grid IP")),
								capability);
						UpdateResult.UpDateBrowserVer();

					} else {
						allProfiles = new ProfilesIni();
						FirefoxProfile profile = allProfiles
								.getProfile((String) StoreTable
										.get("BrowserProfile"));
						// profile.setPreference("intl.accept_languages", "es");
						driver = new FirefoxDriver(profile);

						UpdateResult.UpDateBrowserVer();
					}

				} catch (Throwable T) {
					Logs.Ulog("Error While Launching browser Mozilla"
							+ T.getMessage());

				}

			} else if (StoreTable.get("Execute ON Browser").equals(
					"HeadLess_Driver")) {

				Server Headless = new Server();
				Headless.HeadlessDriver();

			} else if (StoreTable.get("Execute ON Browser").equals(
					"HeadLess_MagicDriver")) {

				Server Headless = new Server();
				Headless.MagicDriver();

			} else if (StoreTable.get("Execute ON Browser").equals(
					"jbrowserdriver")) {

				Server Headless = new Server();
				Headless.jbrowserdriver();

			}
			// else if (StoreTable.get("Execute ON Browser").equals("IE")) {
			else if (StoreTable.get("Execute ON Browser").equals("IE")) {
				try {

					DesiredCapabilities capab = DesiredCapabilities
							.internetExplorer();
					capab.setCapability(
							CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION,
							true);
					capab.setCapability("requireWindowFocus", true);
					capab.setCapability("enablePersistentHover", false);
					// capab.setCapability("screen-resolution", "1360x1024");
					capab.setCapability(
							InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
							true);

					capab.setCapability(
							InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING,
							false);
					capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					capab.setCapability(
							CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, "accept");
					// capab.setCapability(CapabilityType.PAGE_LOADING_STRATEGY,
					// true);
					// capab.setCapability(CapabilityType.SUPPORTS_ALERTS,
					// true);
					// capab.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR,
					// true);

					if (StoreTable.get("Execute On Grid").equals("YY")) {

						driver = new RemoteWebDriver(new URL(
								(String) StoreTable.get("Set Grid IP")), capab);

					} else {
						System.setProperty("webdriver.ie.driver",
								System.getProperty("user.dir")
										+ "\\src\\main\\java\\IEDriverServer.exe");
						driver = new InternetExplorerDriver(capab);

						UpdateResult.UpDateBrowserVer();
					}

				} catch (Throwable T) {
					Logs.Ulog("Error While Launching browser IE"
							+ T.getMessage());
				}

			} else if (StoreTable.get("Execute ON Browser").equals("Chrome")) {
				try {

					if (StoreTable.get("Execute On Grid").equals("YY")) {

						DesiredCapabilities capability = DesiredCapabilities
								.chrome();
						driver = new RemoteWebDriver(new URL(
								(String) StoreTable.get("Set Grid IP")),
								capability);

					} else {
						System.setProperty("webdriver.chrome.driver",
								System.getProperty("user.dir")
										+ "\\src\\main\\java\\chromedriver.exe");
						DesiredCapabilities capability = DesiredCapabilities
								.chrome();
						driver = new ChromeDriver();
						UpdateResult.UpDateBrowserVer();

					}

				} catch (Throwable T) {
					Logs.Ulog("Error While Launching browser Chrome"
							+ T.getMessage());
				}
			} else if (StoreTable.get("Execute ON Browser").equals(
					"AndroidBrowser")) {
				try {
					// String ChromePath = "C:\\Users\\" +
					// System.getenv("USERNAME")
					// +
					// "\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe";
					// System.setProperty("webdriver.chrome.driver",
					// System.getProperty("user.dir") +
					// "\\src\\main\\java\\chromedriver.exe");

					DesiredCapabilities capabilities = new DesiredCapabilities();
					// capabilities.setCapability(CapabilityType.BROWSER_NAME,
					// "Browser");
					capabilities.setCapability("deviceName", "SM G920T");
					capabilities.setCapability("platformName", "Android");
					// capabilities.setCapability("app", "Chrome");
					capabilities.setCapability("browserName", "chrome");
					// capabilities.setCapability("newCommandTimeout", 10000);
					// capabilities.setCapability("app-package",
					// "com.android.chrome");
					// capabilities.setCapability("app-activity",
					// "com.google.android.apps.chrome.Main");
					if (TC.InputData.equalsIgnoreCase("SNAPP")){
						System.setProperty("webdriver.chrome.driver",
								System.getProperty("user.dir")
										+ "\\src\\main\\java\\chromedriver.exe");
						driver = new ChromeDriver();
					}
					else {
					driver = new AndroidDriver<MobileElement>(new URL(
							"http://127.0.0.1:4723/wd/hub"), capabilities);
					}
					// driver.navigate().to("http://www.yahoo.com");
					driver.manage()
							.timeouts()
							.implicitlyWait(60,
									java.util.concurrent.TimeUnit.SECONDS);

					/*
					 * ChromeOptions options = new ChromeOptions();
					 * options.addArguments("user-data-dir=C:/Users/"+System.
					 * getenv("USERNAME")+
					 * "/AppData/Local/Google/Chrome/User Data");
					 * options.addArguments("--start-maximized"); driver = new
					 * ChromeDriver(options);
					 */
					// driver = new ChromeDriver();
					System.out.println("driver created");

				}

				catch (Throwable T) {
					Logs.Ulog("Error While Launching browser Chrome on Android device"
							+ T.getMessage());
				}

			} else if(StoreTable.get("Execute ON Browser").equals("IosBrowser")){
				try{
				
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
				//capabilities.setCapability("deviceName", "iPad 2 (9.2) [8F991E07-588F-444B-96AD-FD67273C84AA]");
				capabilities.setCapability("deviceName", "Catalina Automation iPhone");
				capabilities.setCapability("platformName", "iOS");
				capabilities.setCapability("platformVersion", "10.2");
				capabilities.setCapability("udid", "1a882242d158ef70f54f845f7f79c7b6851f8b6c");
				capabilities.setCapability("automationname", "XCUITest");
				capabilities.setCapability("xcodeConfigfile", "/usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent/xcodeConfigFile.xcconfig");
				URL url = new URL("http://127.0.0.1:4723/wd/hub");
				if (TC.InputData.equalsIgnoreCase("SNAPP")){
					System.setProperty("webdriver.chrome.driver",
							System.getProperty("user.dir")
									+ "//src//main//java//chromedriver");
					driver = new ChromeDriver();
				}
				else {
					driver=new RemoteWebDriver(url, capabilities);
				}
				
				driver.manage().timeouts().implicitlyWait(60,java.util.concurrent.TimeUnit.SECONDS);
				driver.manage().deleteAllCookies();
				/*RemoteWebDriver driveriOS = new RemoteWebDriver(url, capabilities);
				driveriOS.get("https://lion-sqa.catalinacoupons.jp/");*/
				}catch(Throwable T) {
					Logs.Ulog("Error While Launching browser SAFARI on Ios Device " + T.getMessage());
				}
				//capabilities.setCapability("udid", "1a882242d158ef70f54f845f7f79c7b6851f8b6c");
			}
			
			else if (StoreTable.get("Execute ON Browser").equals("Opera"))

			{

				try

				{
					if (StoreTable.get("Execute On Grid").equals("YY")) {

						DesiredCapabilities capability = DesiredCapabilities
								.opera();
						driver = new RemoteWebDriver(new URL(
								(String) StoreTable.get("Set Grid IP")),
								capability);

					} else {
						DesiredCapabilities capabilities = new DesiredCapabilities();
						capabilities.setCapability("opera.binary",
								StoreTable.get("Opera_Exe_Location"));
						capabilities.setCapability("opera.autostart ", true);
						driver = new org.openqa.selenium.opera.OperaDriver(
								capabilities);
					}

				} catch (Throwable T) {
					Logs.Ulog("Error While Launching browser Opera"
							+ T.getMessage());
				}

			} else if (StoreTable.get("Execute ON Browser").equals("Safari"))

			{

				try

				{
					if (StoreTable.get("Execute On Grid").equals("YY")) {

						DesiredCapabilities capability = DesiredCapabilities
								.safari();
						driver = new RemoteWebDriver(new URL(
								(String) StoreTable.get("Set Grid IP")),
								capability);

					} else {
						driver = new SafariDriver();
					}

				} catch (Throwable T) {
					Logs.Ulog("Error While Launching browser Safari "
							+ T.getMessage());
				}

			} else if (StoreTable.get("Execute ON Browser").equals(
					"BlackBurrey"))

			// Opera_Exe_Location
			{
				try {

					DesiredCapabilities capability = DesiredCapabilities
							.chrome();
					driver = new RemoteWebDriver(new URL(
							"http://169.254.0.1:1338"), capability);

				} catch (Throwable T) {
					Logs.Ulog("Error While Launching browser chrome on BlackBurrey "
							+ T.getMessage());
				}

			}

			else if (StoreTable.get("Execute ON Browser").equals(
					"PerfectoIOSDevice")) {
				// Opera_Exe_Location

				try {
					DesiredCapabilities capabilities = new DesiredCapabilities();
					if (StoreTable.get("PF_InstallApp").equals("YY")) {
						capabilities.setCapability("app",
								StoreTable.get("PF_IOSAppPath"));
					}

					capabilities.setCapability("deviceName",
							StoreTable.get("PF_IOSdeviceName")); // Galaxy

					if (StoreTable.get("EnableProxy").equals("YY")) {
						System.getProperties().put("http.proxyHost",
								StoreTable.get("http_proxyHost"));
						System.getProperties().put("http.proxyPort",
								StoreTable.get("http_proxyPort"));
						System.getProperties().put("https.proxyHost",
								StoreTable.get("https_proxyHost"));
						System.getProperties().put("https.proxyPort",
								StoreTable.get("https_proxyPort"));
					}
					// capabilities.setCapability("user",
					// "rajasekaran.natarajan@mindtree.com");
					// capabilities.setCapability("password", "work4aig");
					capabilities.setCapability("user",
							StoreTable.get("PF_user"));
					capabilities.setCapability("password",
							StoreTable.get("PF_password"));
					capabilities.setCapability("automationName", "Appium");
					capabilities.setCapability("platformName", "iOS");
					capabilities.setCapability("autoInstrument", true);
					// Call this method if you want the script to share the
					// devices with the recording plugin.

					capabilities.setCapability("unicodeKeyboard",
							StoreTable.get("unicodeKeyboard"));
					capabilities.setCapability("resetKeyboard",
							StoreTable.get("resetKeyboard"));
					capabilities.setCapability("bundleId",
							StoreTable.get("App_bundleId"));
					String host = (String) StoreTable.get("PF_host");
					driver = new IOSDriver(new URL("https://" + host
							+ "/wd/hub"), capabilities);
					Thread.sleep(20000);

					implicitWaitTime = (int) Double
							.parseDouble((String) StoreTable
									.get("Browser time out"));
					// driver.manage().timeouts().implicitlyWait(120,
					// TimeUnit.SECONDS);
					driver.manage().timeouts()
							.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);

					System.out.println(((IOSDriver) driver).getContext());
					System.out.println(((IOSDriver) driver).getContext());

					try {
						((IOSDriver) driver).context("WEBVIEW_1");
						System.out.println(((IOSDriver) driver).getContext());
						((IOSDriver) driver).findElement(
								By.xpath("//*[@id='email']")).sendKeys(
								"Hanuman");
						;

					} catch (Throwable T) {
						System.out.println("Error - " + T.getMessage());
					}

				} catch (Throwable T) {
					Logs.Ulog("Error While Launching IOSDriver "
							+ T.getMessage());
				}

			}

			if (StoreTable.get("Execute ON Browser").equals(
					"PerfectoAndroidDevice"))

			// Opera_Exe_Location
			{
				try {

					DesiredCapabilities capabilities = new DesiredCapabilities();
					if (StoreTable.get("PF_InstallApp").equals("YY")) {
						capabilities.setCapability("app",
								StoreTable.get("PF_AndroidAppPath"));
					}

					capabilities.setCapability("deviceName",
							StoreTable.get("PF_AndroiddeviceName")); // Galaxy
																		// TAB
					if (StoreTable.get("EnableProxy").equals("YY")) {
						System.getProperties().put("http.proxyHost",
								StoreTable.get("http_proxyHost"));
						System.getProperties().put("http.proxyPort",
								StoreTable.get("http_proxyPort"));
						System.getProperties().put("https.proxyHost",
								StoreTable.get("https_proxyHost"));
						System.getProperties().put("https.proxyPort",
								StoreTable.get("https_proxyPort"));
					} // S
						// 10
					capabilities.setCapability("user",
							StoreTable.get("PF_user"));
					capabilities.setCapability("password",
							StoreTable.get("PF_password"));
					capabilities.setCapability("automationName", "Appium");
					capabilities.setCapability("platformName", "Android");
					capabilities.setCapability("autoInstrument", true);
					// Call this method if you want the script to share the
					// devices with the recording plugin.

					capabilities.setCapability("unicodeKeyboard",
							StoreTable.get("unicodeKeyboard"));
					capabilities.setCapability("resetKeyboard",
							StoreTable.get("resetKeyboard"));
					capabilities.setCapability("appPackage",
							StoreTable.get("appPackage"));
					capabilities.setCapability("appActivity",
							StoreTable.get("appActivity"));

					String host = (String) StoreTable.get("PF_host");
					// driver = new AndroidDriver( new
					// URL(StoreTable.get("App_host")), capabilities);
					driver = new AndroidDriver(new URL("https://" + host
							+ "/wd/hub"), capabilities);

					Thread.sleep(20000);

					implicitWaitTime = (int) Double
							.parseDouble((String) StoreTable
									.get("Browser time out"));
					// driver.manage().timeouts().implicitlyWait(120,
					// TimeUnit.SECONDS);
					driver.manage().timeouts()
							.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
				} catch (Throwable T) {
					Logs.Ulog("Error While Launching PerfectoAndroidDevice "
							+ T.getMessage());
				}

			}
			if (StoreTable.get("Execute ON Browser").equals(
					"PerfectoAndroidBrowser"))

			// Opera_Exe_Location
			{
				try {

					String browserName = "mobileOS";
					DesiredCapabilities capabilities = new DesiredCapabilities(
							browserName, "", Platform.ANY);
					// capabilities.setCapability("deviceName",
					// StoreTable.get("PF_AndroiddeviceName"));
					capabilities.setCapability("user",
							StoreTable.get("PF_user"));
					capabilities.setCapability("password",
							StoreTable.get("PF_password"));
					capabilities.setCapability("automationName", "Appium");
					capabilities.setCapability("platformName", "Android");
					if (StoreTable.get("EnableProxy").equals("YY")) {
						System.getProperties().put("http.proxyHost",
								StoreTable.get("http_proxyHost"));
						System.getProperties().put("http.proxyPort",
								StoreTable.get("http_proxyPort"));
						System.getProperties().put("https.proxyHost",
								StoreTable.get("https_proxyHost"));
						System.getProperties().put("https.proxyPort",
								StoreTable.get("https_proxyPort"));
					}
					capabilities.setCapability("unicodeKeyboard",
							StoreTable.get("unicodeKeyboard"));
					capabilities.setCapability("resetKeyboard",
							StoreTable.get("resetKeyboard"));

					String host = (String) StoreTable.get("PF_host");
					// driver = new AndroidDriver( new
					// URL(StoreTable.get("App_host")), capabilities);
					driver = new AndroidDriver(new URL("https://" + host
							+ "/wd/hub"), capabilities);

					Thread.sleep(20000);

					implicitWaitTime = (int) Double
							.parseDouble((String) StoreTable
									.get("Browser time out"));
					// driver.manage().timeouts().implicitlyWait(120,
					// TimeUnit.SECONDS);
					driver.manage().timeouts()
							.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
				} catch (Throwable T) {
					Logs.Ulog("Error While Launching PerfectoAndroidDevice "
							+ T.getMessage());
				}

			}
			if (StoreTable.get("Execute ON Browser").equals(
					"AvailablePerfectoAndroidBrowser"))

			// Opera_Exe_Location
			{
				try {

					String browserName = "mobileOS";
					DesiredCapabilities capabilities = new DesiredCapabilities(
							browserName, "", Platform.ANY);
					// capabilities.setCapability("deviceName",
					// StoreTable.get("PF_AndroiddeviceName"));
					capabilities.setCapability("platformVersion",
							StoreTable.get("PF_platformVersion"));
					// capabilities.setCapability("model",StoreTable.get("PF_model"));
					capabilities.setCapability("user",
							StoreTable.get("PF_user"));
					capabilities.setCapability("password",
							StoreTable.get("PF_password"));
					capabilities.setCapability("automationName", "Appium");
					capabilities.setCapability("platformName", "Android");
					capabilities.setCapability("unicodeKeyboard",
							StoreTable.get("unicodeKeyboard"));
					capabilities.setCapability("resetKeyboard",
							StoreTable.get("resetKeyboard"));
					if (StoreTable.get("EnableProxy").equals("YY")) {
						System.getProperties().put("http.proxyHost",
								StoreTable.get("http_proxyHost"));
						System.getProperties().put("http.proxyPort",
								StoreTable.get("http_proxyPort"));
						System.getProperties().put("https.proxyHost",
								StoreTable.get("https_proxyHost"));
						System.getProperties().put("https.proxyPort",
								StoreTable.get("https_proxyPort"));
					}
					String host = (String) StoreTable.get("PF_host");
					// driver = new AndroidDriver( new
					// URL(StoreTable.get("App_host")), capabilities);
					driver = new AndroidDriver(new URL("https://" + host
							+ "/wd/hub"), capabilities);

					Thread.sleep(20000);

					implicitWaitTime = (int) Double
							.parseDouble((String) StoreTable
									.get("Browser time out"));
					// driver.manage().timeouts().implicitlyWait(120,
					// TimeUnit.SECONDS);
					driver.manage().timeouts()
							.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
				} catch (Throwable T) {
					Logs.Ulog("Error While Launching PerfectoAndroidDevice "
							+ T.getMessage());
				}

			}

			if (StoreTable.get("Execute ON Browser").equals(
					"AvailablePerfectoIOSBrowser"))

			// Opera_Exe_Location
			{
				try {

					String browserName = "mobileOS";
					DesiredCapabilities capabilities = new DesiredCapabilities(
							browserName, "", Platform.ANY);
					// capabilities.setCapability("deviceName",
					// StoreTable.get("PF_IOSdeviceName"));
					capabilities.setCapability("user",
							StoreTable.get("PF_user"));
					capabilities.setCapability("password",
							StoreTable.get("PF_password"));
					capabilities.setCapability("automationName", "Appium");
					capabilities.setCapability("platformName", "iOS");
					if (StoreTable.get("EnableProxy").equals("YY")) {
						System.getProperties().put("http.proxyHost",
								StoreTable.get("http_proxyHost"));
						System.getProperties().put("http.proxyPort",
								StoreTable.get("http_proxyPort"));
						System.getProperties().put("https.proxyHost",
								StoreTable.get("https_proxyHost"));
						System.getProperties().put("https.proxyPort",
								StoreTable.get("https_proxyPort"));
					}
					// Call this method if you want the script to share the
					// devices with the recording plugin.

					capabilities.setCapability("unicodeKeyboard",
							StoreTable.get("unicodeKeyboard"));
					capabilities.setCapability("resetKeyboard",
							StoreTable.get("resetKeyboard"));

					String host = (String) StoreTable.get("PF_host");
					// driver = new AndroidDriver( new
					// URL(StoreTable.get("App_host")), capabilities);
					driver = new IOSDriver(new URL("https://" + host
							+ "/wd/hub"), capabilities);

					Thread.sleep(20000);

					implicitWaitTime = (int) Double
							.parseDouble((String) StoreTable
									.get("Browser time out"));
					// driver.manage().timeouts().implicitlyWait(120,
					// TimeUnit.SECONDS);
					driver.manage().timeouts()
							.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);

				} catch (Throwable T) {
					Logs.Ulog("Error While Launching PerfectoAndroidDevice "
							+ T.getMessage());
				}

			}
			if (StoreTable.get("Execute ON Browser").equals(
					"PerfectoIOSBrowser"))

			// Opera_Exe_Location
			{
				try {

					String browserName = "mobileOS";
					DesiredCapabilities capabilities = new DesiredCapabilities(
							browserName, "", Platform.ANY);
					capabilities.setCapability("deviceName",
							StoreTable.get("PF_IOSdeviceName"));
					capabilities.setCapability("user",
							StoreTable.get("PF_user"));
					capabilities.setCapability("password",
							StoreTable.get("PF_password"));
					capabilities.setCapability("automationName", "Appium");
					capabilities.setCapability("platformName", "Android");
					if (StoreTable.get("EnableProxy").equals("YY")) {
						System.getProperties().put("http.proxyHost",
								StoreTable.get("http_proxyHost"));
						System.getProperties().put("http.proxyPort",
								StoreTable.get("http_proxyPort"));
						System.getProperties().put("https.proxyHost",
								StoreTable.get("https_proxyHost"));
						System.getProperties().put("https.proxyPort",
								StoreTable.get("https_proxyPort"));
					}
					// Call this method if you want the script to share the
					// devices with the recording plugin.

					capabilities.setCapability("unicodeKeyboard",
							StoreTable.get("unicodeKeyboard"));
					capabilities.setCapability("resetKeyboard",
							StoreTable.get("resetKeyboard"));

					String host = (String) StoreTable.get("PF_host");
					// driver = new AndroidDriver( new
					// URL(StoreTable.get("App_host")), capabilities);
					driver = new IOSDriver(new URL("https://" + host
							+ "/wd/hub"), capabilities);

					Thread.sleep(20000);

					implicitWaitTime = (int) Double
							.parseDouble((String) StoreTable
									.get("Browser time out"));
					// driver.manage().timeouts().implicitlyWait(120,
					// TimeUnit.SECONDS);
					driver.manage().timeouts()
							.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
				} catch (Throwable T) {
					Logs.Ulog("Error While Launching PerfectoAndroidDevice "
							+ T.getMessage());
				}

			}
			if (StoreTable.get("Execute ON Browser").equals(
					"PerfectoDesktopBrowser"))

			// Opera_Exe_Location
			{
				try {

					DesiredCapabilities capabilities = new DesiredCapabilities();
					String host = (String) StoreTable.get("PF_host");
					capabilities.setCapability("user",
							StoreTable.get("PF_user"));
					capabilities.setCapability("password",
							StoreTable.get("PF_password"));
					capabilities.setCapability("platformName",
							StoreTable.get("PF_Desk_platformName"));
					capabilities.setCapability("platformVersion",
							StoreTable.get("PF_Desk_platformVersion"));
					capabilities.setCapability("browserName",
							StoreTable.get("PF_Desk_browserName"));
					capabilities.setCapability("browserVersion",
							StoreTable.get("PF_Desk_browserVersion"));
					capabilities.setCapability("resolution",
							StoreTable.get("PF_Desk_resolution"));
					capabilities.setCapability("location",
							StoreTable.get("PF_Desk_location"));

					capabilities.setCapability(
							MobileCapabilityType.NEW_COMMAND_TIMEOUT,
							StoreTable.get("NEW_COMMAND_TIMEOUT"));
					capabilities.setCapability(
							MobileCapabilityType.DEVICE_READY_TIMEOUT,
							StoreTable.get("DEVICE_READY_TIMEOUT"));

					// TODO: Name your script
					// capabilities.setCapability("scriptName",
					// "RemoteWebDriverTest");

					driver = new RemoteWebDriver(new URL("https://" + host
							+ "/wd/hub"), capabilities);
					implicitWaitTime = (int) Double
							.parseDouble((String) StoreTable
									.get("Browser time out"));
					// driver.manage().timeouts().implicitlyWait(120,
					// TimeUnit.SECONDS);
					// driver.manage().timeouts().implicitlyWait(implicitWaitTime,
					// TimeUnit.SECONDS);
					Thread.sleep(20000);

				} catch (Throwable T) {
					Logs.Ulog("Error While Launching PerfectoAndroidDevice "
							+ T.getMessage());
				}

			}

			else if (StoreTable.get("Execute ON Browser").equals(
					"AndroidLocalDevice")) {
				try {
					DesiredCapabilities capabilities = new DesiredCapabilities();
					if (StoreTable.get("InstallApp").equals("YY")) {
						File app = new File(
								(String) StoreTable.get("APKapp_Path"));
						capabilities
								.setCapability("app", app.getAbsolutePath());
					}
					// capabilities.setCapability("deviceName","Android
					// Emulator");
					if (StoreTable.get("EnableProxy").equals("YY")) {
						System.getProperties().put("http.proxyHost",
								StoreTable.get("http_proxyHost"));
						System.getProperties().put("http.proxyPort",
								StoreTable.get("http_proxyPort"));
						System.getProperties().put("https.proxyHost",
								StoreTable.get("https_proxyHost"));
						System.getProperties().put("https.proxyPort",
								StoreTable.get("https_proxyPort"));
					}
					capabilities.setCapability("deviceName",
							StoreTable.get("deviceName"));
					capabilities.setCapability("platformName", "Android");
					capabilities.setCapability("autoInstrument", true);
					capabilities.setCapability("unicodeKeyboard",
							StoreTable.get("unicodeKeyboard"));
					capabilities.setCapability("resetKeyboard",
							StoreTable.get("resetKeyboard"));
					capabilities.setCapability("appPackage",
							StoreTable.get("appPackage"));
					capabilities.setCapability("appActivity",
							StoreTable.get("appActivity"));
					capabilities.setCapability(
							MobileCapabilityType.NEW_COMMAND_TIMEOUT,
							StoreTable.get("NEW_COMMAND_TIMEOUT"));
					capabilities.setCapability(
							MobileCapabilityType.DEVICE_READY_TIMEOUT,
							StoreTable.get("DEVICE_READY_TIMEOUT"));

					driver = new AndroidDriver(new URL(
							(String) StoreTable.get("App_host")), capabilities);

					implicitWaitTime = (int) Double
							.parseDouble((String) StoreTable
									.get("Browser time out"));
					// driver.manage().timeouts().implicitlyWait(120,
					// TimeUnit.SECONDS);
					driver.manage().timeouts()
							.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);

					Thread.sleep(20000);
				} catch (Throwable T) {
					Logs.Ulog("Error While Launching AndroidLocalDevice" + " "
							+ T.getMessage());
				}

			} else if (StoreTable.get("Execute ON Browser").equals(
					"AndroidEmulator")) {
				try {
					DesiredCapabilities capabilities = new DesiredCapabilities();
					if (StoreTable.get("InstallApp").equals("YY")) {
						File app = new File(
								(String) StoreTable.get("APKapp_Path"));
						capabilities
								.setCapability("app", app.getAbsolutePath());
					}

					if (StoreTable.get("EnableProxy").equals("YY")) {
						System.getProperties().put("http.proxyHost",
								StoreTable.get("http_proxyHost"));
						System.getProperties().put("http.proxyPort",
								StoreTable.get("http_proxyPort"));
						System.getProperties().put("https.proxyHost",
								StoreTable.get("https_proxyHost"));
						System.getProperties().put("https.proxyPort",
								StoreTable.get("https_proxyPort"));
					}
					capabilities.setCapability("deviceName",
							StoreTable.get("deviceNameEmulator"));
					capabilities.setCapability("platformName", "Android");
					capabilities.setCapability("autoInstrument", true);
					capabilities.setCapability("unicodeKeyboard",
							StoreTable.get("unicodeKeyboard"));
					capabilities.setCapability("resetKeyboard",
							StoreTable.get("resetKeyboard"));
					capabilities.setCapability("appPackage",
							StoreTable.get("appPackage"));
					capabilities.setCapability("appActivity",
							StoreTable.get("appActivity"));
					capabilities.setCapability(
							MobileCapabilityType.NEW_COMMAND_TIMEOUT,
							StoreTable.get("NEW_COMMAND_TIMEOUT"));
					capabilities.setCapability(
							MobileCapabilityType.DEVICE_READY_TIMEOUT,
							StoreTable.get("DEVICE_READY_TIMEOUT"));

					driver = new AndroidDriver(new URL(
							(String) StoreTable.get("App_host")), capabilities);
					implicitWaitTime = (int) Double
							.parseDouble((String) StoreTable
									.get("Browser time out"));
					// driver.manage().timeouts().implicitlyWait(120,
					// TimeUnit.SECONDS);
					driver.manage().timeouts()
							.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);

					Thread.sleep(20000);

				} catch (Throwable T) {
					Logs.Ulog("Error While Launching AndroidEmulator" + " "
							+ T.getMessage());
				}

			} else if (StoreTable.get("Execute ON Browser").equals("CBT"))

			{
				try {
					DesiredCapabilities caps = new DesiredCapabilities();

					caps.setCapability("name", StoreTable.get("CBT_name"));
					caps.setCapability("build", StoreTable.get("CBT_build"));
					caps.setCapability("browser_api_name",
							StoreTable.get("CBT_browser_api_name"));
					caps.setCapability("os_api_name",
							StoreTable.get("CBT_os_api_name"));
					caps.setCapability("screen_resolution",
							StoreTable.get("CBT_screen_resolution"));
					caps.setCapability("record_video",
							StoreTable.get("CBT_record_video"));
					caps.setCapability("record_network",
							StoreTable.get("CBT_record_network"));

					driver = new RemoteWebDriver(new URL(
							(String) StoreTable.get("CBT_Host")), caps);

					// load the page url
					System.out.println("Loading Url");

				} catch (Throwable T) {
					Logs.Ulog("Error While Launching AndroidLocalDevice" + " "
							+ T.getMessage());
				}

			} else if (StoreTable.get("Execute ON Browser").equals(
					"PerfectoBrowser")) {
				try {
					driver = null;
					String browserName = "mobileOS";
					DesiredCapabilities capabilities = new DesiredCapabilities(
							browserName, "", Platform.ANY);
					capabilities.setCapability("deviceName",
							StoreTable.get("PF_AndroiddeviceName"));
					capabilities.setCapability("user",
							StoreTable.get("PF_user"));
					capabilities.setCapability("password",
							StoreTable.get("PF_password"));

					capabilities.setCapability("resetKeyboard",
							StoreTable.get("resetKeyboard"));

					String host = (String) StoreTable.get("PF_host");
					// driver = new AndroidDriver( new
					// URL(StoreTable.get("App_host")),
					// capabilities);
					driver = new RemoteWebDriver(new URL("https://" + host
							+ "/wd/hub"), capabilities);

					Thread.sleep(20000);

					driver.manage().deleteAllCookies();

					implicitWaitTime = (int) Double
							.parseDouble((String) StoreTable
									.get("Browser time out"));
					// driver.manage().timeouts().implicitlyWait(120,
					// TimeUnit.SECONDS);
					driver.manage().timeouts()
							.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
					return UpdateResult.Done();
				} catch (Throwable T) {
					Logs.Ulog("ERROR -- While start UpdateSetupBrowser_device  "
							+ T.getMessage());
					CatchStatementWebElement("ERROR -- While start UpdateSetupBrowser_device "
							+ T.getMessage());
					return TC.FAIL;

				}
			} else if (StoreTable.get("Execute ON Browser").equals(
					"PerfectoiOSBrowser")) {
				try {
					driver = null;
					String browserName = "mobileOS";
					DesiredCapabilities capabilities = new DesiredCapabilities(
							browserName, "", Platform.ANY);
					capabilities.setCapability("deviceName",
							StoreTable.get("PF_IOSdeviceName"));
					capabilities.setCapability("user",
							StoreTable.get("PF_user"));
					capabilities.setCapability("password",
							StoreTable.get("PF_password"));

					capabilities.setCapability("resetKeyboard",
							StoreTable.get("resetKeyboard"));

					String host = (String) StoreTable.get("PF_host");
					// driver = new AndroidDriver( new
					// URL(StoreTable.get("App_host")),
					// capabilities);
					driver = new RemoteWebDriver(new URL("https://" + host
							+ "/wd/hub"), capabilities);

					Thread.sleep(20000);

					driver.manage().deleteAllCookies();

					implicitWaitTime = (int) Double
							.parseDouble((String) StoreTable
									.get("Browser time out"));
					// driver.manage().timeouts().implicitlyWait(120,
					// TimeUnit.SECONDS);
					driver.manage().timeouts()
							.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
					return UpdateResult.Done();
				} catch (Throwable T) {
					Logs.Ulog("ERROR -- While start UpdateSetupBrowser_device  "
							+ T.getMessage());
					CatchStatementWebElement("ERROR -- While start UpdateSetupBrowser_device "
							+ T.getMessage());
					return TC.FAIL;

				}
			}
			PrintDeviceVersion();
			UpdateResult.ExpectedData = (String) StoreTable
					.get("Execute ON Browser");
			UpdateResult.ActualData = (String) StoreTable
					.get("Execute ON Browser");
			TC.ExpectedData = (String) StoreTable.get("Execute ON Browser");

			if (C_RoboMouse == null)
				C_RoboMouse = new RobotMouse_key(driver);

			if (RMouse == null)
				RMouse = new Robot();

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			UpdateResult.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}

	}

	/*
	 * private static String BrowserSync() throws InterruptedException {
	 * 
	 * for(int i=0; i<=100; i++) { Thread.sleep(2000L); boolean a=
	 * TORObj().isDisplayed(); if(!a) { Logs.Ulog(
	 * "Syc with application  done sucessfully ");
	 * 
	 * 
	 * break;
	 * 
	 * } } return TC.PASS; }
	 */

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String Sync() throws InterruptedException, IOException {
		try {

			TC.InputData = "100";
			Thread.sleep(1000L);
			getORObject("Image(BusyWheel)");
			Logs.Ulog("Waiting for load page form busy- Waiting for form busy not exist");
			OverWheelSync();

		} catch (NoSuchElementException e) {
			SetDefaultSyncTime();
			return TC.PASS;
		}
		SetDefaultSyncTime();
		return TC.PASS;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String ScrollElementPageDown() throws IOException,
			InterruptedException

	{

		try {

			WebElement element = TORObj();

			int ST = Integer.parseInt(TORObj().getAttribute("scrollTop"));

			for (int i = 0; i <= ST; i++) {
				TORObj().sendKeys(Keys.PAGE_DOWN);
				Thread.sleep(200L);
			}

			return UpdateResult.Done();

		}

		catch (Throwable e)

		{

			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String ScrollElementClick() throws IOException,
			InterruptedException

	{

		try {

			int ST = Integer.parseInt(TORObj().getAttribute("scrollTop"));

			for (int i = 0; i <= ST; i++) {
				TORObj().sendKeys(Keys.PAGE_DOWN);
				Thread.sleep(100L);
			}

			return UpdateResult.Done();

		}

		catch (Throwable e)

		{

			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String select() throws IOException, InterruptedException {

		Logs.Ulog("Selecting from list");
		TC.InputData = TC.InputData.replace(".0", "");
		TC.InputData = TC.InputData.replaceAll(" ", "");
		try {

			WebElement Ele = TORObj();
			// HighlightElement();
			if (!TC.InputData.equals("RANDOM")) {
				// droplist.sendKeys(TC.InputData);
				Select droplist = new Select(Ele);
				droplist.getOptions().toArray();
				for (int i = 1; i < droplist.getOptions().size(); i++) {
					String s1 = droplist.getOptions().get(i).getText()
							.toString().replaceAll("[^\\p{ASCII}]", "")
							.replaceAll(" ", "");
					if (TC.InputData.replaceAll("[^\\p{ASCII}]", "")
							.replaceAll(" ", "").equals(s1)) {

						droplist.getOptions().get(i).click();
						Thread.sleep(200L);
						UpdateResult.ActualData = droplist
								.getFirstSelectedOption().getText();
						TC.ExpectedData = droplist.getFirstSelectedOption()
								.getText();
						// Sync();
						return UpdateResult.UpdateStatus();
					}
				}

			}

			if (TC.InputData.equals("RANDOM")) {
				// droplist.sendKeys(TC.InputData);
				Select droplist = new Select(Ele);

				for (int i = 0; i < droplist.getOptions().size(); i++) {
					String s1 = droplist.getOptions().get(i).getText();
					if (!s1.equals("")) {
						droplist.getOptions().get(i).click();
						Thread.sleep(200L);
						UpdateResult.ActualData = droplist
								.getFirstSelectedOption().getText();
						TC.ExpectedData = droplist.getFirstSelectedOption()
								.getText();
						Sync();
						return UpdateResult.UpdateStatus();
					}
				}

			}
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			Logs.Ulog("Error ---  while Seleting list Selecting from list");
			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String selectbyIndex() throws IOException,
			InterruptedException {

		Logs.Ulog("Selecting from list");

		try {

			WebElement Ele = TORObj();

			Select droplist = new Select(Ele);

			for (int i = 0; i < droplist.getOptions().size(); i++) {
				// String s1 = droplist.getOptions().get(i).getText();
				TC.InputData = WebServices.Round(TC.InputData);

				int sl = Integer.parseInt(TC.InputData);
				if (sl == i) {
					droplist.getOptions().get(i).click();
					Thread.sleep(200L);

					UpdateResult.ActualData = droplist.getFirstSelectedOption()
							.getText();
					TC.ExpectedData = droplist.getFirstSelectedOption()
							.getText();
					// Sync();
					return UpdateResult.UpdateStatus();
				}
			}

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			Logs.Ulog("Error ---  while Seleting list Selecting from list");
			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	private static int valueof(String inputData) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static String selectbyIndex_0() throws IOException,
			InterruptedException {

		Logs.Ulog("Selecting from list");

		try {

			WebElement Ele = TORObj();

			Select droplist = new Select(Ele);
			droplist.getOptions().get(0).click();

			UpdateResult.ActualData = "Selection Clear";
			TC.ExpectedData = "Selection Clear";

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			Logs.Ulog("Error ---  while Seleting list Selecting from list");
			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static boolean isEquals(String one, String two) {
		Set<String> temp = new HashSet<String>();
		temp.add(one);
		temp.add(two);
		boolean t = (temp.size() == 1);
		return (temp.size() == 1);
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String SelectList() throws IOException, InterruptedException {

		Logs.Ulog("Selecting from list");
		try {
			WebElement Ele = TORObj();
			if (!TC.InputData.equals("Random")) {
				// droplist.sendKeys(TC.InputData);

				// droplist.findElement(By.xpath("//option[contains(text(),'"+TC.InputData
				// +"')]")).click();
				// droplist.findElement(By.xpath("//option[contains(text(),'"+TC.InputData
				// +"')]")).selectByVisibleText(TC.InputData);
				Select droplist = new Select(Ele);

				droplist.selectByVisibleText(TC.InputData);
				Thread.sleep(200L);
				UpdateResult.ActualData = droplist.getFirstSelectedOption()
						.getText();
				TC.ExpectedData = TC.InputData;
				return UpdateResult.UpdateStatus();
			} else {
				// logic to find a random value in list

				/*
				 * List<WebElement> droplist_cotents =
				 * droplist.findElements(By.tagName("option")); Random num = new
				 * Random( ); int index=num.nextInt(droplist_cotents.size());
				 * String selectedVal=droplist_cotents.get(index).getText();
				 * //droplist.sendKeys(selectedVal);
				 * 
				 * Thread.sleep(1000L); BrowserSync(); Select select = new
				 * Select(droplist); WebElement tmp =
				 * select.getFirstSelectedOption(); UpdateResult.ActualData =
				 * tmp.getText(); TC.ExpectedData = tmp.getText();
				 */
				return UpdateResult.UpdateStatus();
			}
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());

		}

	}

	public static String SelectListBox() throws InterruptedException,
			IOException {

		WebElement select = TORObj();
		Select dropDown = new Select(select);
		String selected = dropDown.getFirstSelectedOption().getText();
		if (selected.equals(TC.InputData)) {// do stuff already selected}
			List<WebElement> Options = dropDown.getOptions();
			for (WebElement option : Options) {
				if (option.getText().equals(TC.InputData)) {
					option.click();
					BrowserSync();
				}
			}
		}
		return TC.PASS;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String SwitchToBrowser_Window_Tab() throws IOException,
			InterruptedException {

		Logs.Ulog("Switching new window" + TC.InputData);
		try {

			Set<String> WinID = driver.getWindowHandles();
			// System.out.println("Child win is displayed");

			Iterator<String> it = WinID.iterator();
			// WinID.size();

			for (int WID = 1; WID <= WinID.size(); WID++) {

				String[] a = TC.InputData.split("Tab");

				int Tno = Integer.parseInt(a[1]);
				String TabID = it.next();
				if (Tno == WID) {
					driver.switchTo().window(TabID);
					// System.out.println("successfully switched");
				}
			}

			Logs.Ulog("Successfully Switched to new window" + TC.InputData);
			return UpdateResult.Done();
		}

		catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			UpdateResult.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static void SwitchToBrowser_Window_TabNonExp() throws IOException,
			InterruptedException {

		Logs.Ulog("Switching new window" + TC.InputData);
		try {

			Set<String> WinID = driver.getWindowHandles();
			// System.out.println("Child win is displayed");

			Iterator<String> it = WinID.iterator();
			// WinID.size();

			for (int WID = 1; WID <= WinID.size(); WID++) {

				String[] a = TC.InputData.split("Tab");

				int Tno = Integer.parseInt(a[1]);
				String TabID = it.next();
				if (Tno == WID) {
					driver.switchTo().window(TabID);
					// System.out.println("successfully switched");
				}
			}

			Logs.Ulog("Successfully Switched to new window" + TC.InputData);

		}

		catch (Throwable e) {

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String SyncForElementVisibility() throws IOException,
			InterruptedException {

		Logs.Ulog("Waiting for an element to be visible");
		int start = 0;
		int time = (int) Double.parseDouble(TC.InputData);

		try {
			while (time == start) {
				if (WebElementsTORObj().size() == 0) {
					Thread.sleep(1000L);
					start++;
				} else {
					break;
				}
			}
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
		return TC.PASS;
	}

	public static String UnCheckCheckBox() throws IOException,
			InterruptedException {

		Logs.Ulog("Unchecking checkBox");
		try {
			WebElement Chkbox = TORObj();
			String checked = Chkbox.getAttribute("checked");
			if (checked != null)// checkbox is unchecked
				Chkbox.click();
			BrowserSync();
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String selectbyVisibleText() throws IOException,
			InterruptedException {

		Logs.Ulog("Selecting from list");

		try {

			WebElement Ele = TORObj();

			Select droplist = new Select(Ele);
			droplist.selectByVisibleText(TC.InputData);

			UpdateResult.ActualData = "Selection Clear";
			TC.ExpectedData = "Selection Clear";

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			Logs.Ulog("Error ---  while Seleting list Selecting from list");
			return CatchStatementWebElement(e.getMessage());

		}

	}

	public static String VerifyAlertPresent() throws InterruptedException,
			IOException {

		try {

			// Check the presence of alert
			Alert alert = driver.switchTo().alert();
			// Alert present; set the flag
			UpdateResult.ActualData = alert.getText();

			// if present consume the alert
			alert.accept();
			BrowserSync();
			TC.ExpectedData = TC.InputData;
			return UpdateResult.UpdateStatus();

		} catch (NoAlertPresentException ex) {
			// Alert not present

			Logs.Ulog("Alert not present" + ex.getMessage());
			return CatchStatementWebElement(ex.getMessage());
		}

	}

	public static String VerifyAllListElements() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the selection of the list");
		try {
			WebElement droplist = TORObj();
			Logs.Ulog("List options " + droplist.getText());
			List<WebElement> droplist_cotents = droplist.findElements(By
					.tagName("li"));

			// extract the expected values excel file
			String temp = TC.InputData;
			String allElements[] = temp.split(",");
			// check if size of array == size if list
			if (allElements.length != droplist_cotents.size()) {

				Logs.Ulog("size of lists do not match");
				UpdateResult.ActualData = Integer.toString(allElements.length);
				TC.ExpectedData = Integer.toString(droplist_cotents.size());

				return UpdateResult.UpdateStatus();
			}

			for (int i = 0; i < droplist_cotents.size(); i++) {
				if (!allElements[i].trim().equals(
						droplist_cotents.get(i).getText().trim())) {
					UpdateResult.ActualData = "- Element not found - "
							+ allElements[i].trim();
					TC.ExpectedData = droplist_cotents.get(i).getText();

					Logs.Ulog("- Element not found -");
					return UpdateResult.UpdateStatus();
				} else {
					UpdateResult.ActualData = allElements[i];
					TC.ExpectedData = droplist_cotents.get(i).getText();

					Logs.Ulog("- Element found -");
					return UpdateResult.UpdateStatus();
				}

			}
		} catch (Throwable e) {
			Logs.Ulog(" - Could not select from list. " + e.getMessage());
			return CatchStatementWebElement(e.getMessage());

		}

		return TC.PASS;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyAttributeVal() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the VerifyAttributeVal");
		try {
			UpdateResult.ActualData = TORObj().getAttribute("value");
			TC.ExpectedData = TC.InputData;
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	public static String VerifyAttribute_Boolean() throws IOException,
			InterruptedException {

		Logs.Ulog(" Executing VerifyAttributeBoolen ");

		try {
			WebElement Obj = TORObj();

			// System.out.println(js.executeScript("return
			// arguments[0].disabled=",
			// Obj));
			Logs.Ulog(" Verify element disabled=="
					+ ((JavascriptExecutor) driver).executeScript(
							"return arguments[0]." + TC.InputData, Obj));
			boolean istrue = (Boolean) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0]." + TC.InputData, Obj);

			if (istrue) {
				UpdateResult.ActualData = "true";

			} else {
				UpdateResult.ActualData = "false";

			}

			return UpdateResult.UpdateStatus();
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	public static String VerifyAttributeNumber() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the VerifyAttributeVal");
		try {
			UpdateResult.ActualData = TORObj().getAttribute(TC.InputData);
			TC.ExpectedData = TC.ExpectedData;
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static void UploadFileWindows() throws InterruptedException,
			IOException {
		try {
			Logs.Ulog(" Executing UploadFileAutoIt " + TC.InputData);
			Logs.Ulog("Text Control: " + TC.ExpectedData);

			Thread.sleep(3000);
			if ((StoreTable.get("Execute ON Browser").toString())
					.equalsIgnoreCase("Mozilla")) {

				Window.ControlSetText("File Upload", TC.ControlText,
						TC.ControlID, TC.InputData);
				Thread.sleep(3000);
				Window.controlClick("File Upload", TC.ControlText, TC.ControlID);
			} else {

				Window.ControlSetText("Open", TC.ControlText, TC.ControlID,
						TC.InputData);
				Thread.sleep(3000);
				Window.controlClick("Open", TC.ControlText, TC.ControlID);
			}

		} catch (Throwable e) {
			CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String OpenWinApp() throws InterruptedException, IOException {
		try {
			Logs.Ulog(" Executing OpenWinApp ");

			Window.run(TC.InputData);

			Thread.sleep(3000l);
			UpdateResult.ActualData = String.valueOf(Window
					.winExists(TC.Param1));
			TC.ExpectedData = "true";
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String InputTextWinControl() throws InterruptedException,
			IOException {
		try {
			Logs.Ulog(" Executing InputTextWinControl " + TC.InputData);
			String OPT_WIN_TITLE_MATCH_MODE = "WinTitleMatchMode";

			Window.ControlSetText(TC.WinTitle, TC.ControlText, TC.ControlID,
					TC.InputData);

			UpdateResult.ActualData = Window.controlGetText(TC.WinTitle,
					TC.ControlText, TC.ControlID);
			TC.ExpectedData = TC.InputData;
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String ClickWinControl() throws InterruptedException,
			IOException {
		try {
			Logs.Ulog(" Executing ClickWinControl ");

			Window.controlClick(TC.WinTitle, TC.ControlText, TC.ControlID);
			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	public static void ClickScreen() throws InterruptedException, IOException {
		try {
			Logs.Ulog(" Executing ClickScreen ");

			WinScreen.click(TC.InputData);

		} catch (Throwable e) {
			CatchStatementWebElement(e.getMessage());
		}

	}

	public static void WinActivate() throws InterruptedException, IOException {
		try {
			Logs.Ulog(" Executing ClickAutoIT ");

			Window.winActive(TC.WinTitle, TC.ControlText);

		} catch (Throwable e) {
			CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	/*
	 * public static String VerifyAttributeVal(){
	 * 
	 * Logs.Ulog("Verifying the VerifyAttributeVal");
	 * 
	 * try
	 * 
	 * {
	 * 
	 * UpdateResult.ActualData = TORObj().getAttribute("value");
	 * 
	 * TC.ExpectedData = TC.InputData;
	 * 
	 * return UpdateResult.UpdateStatus();
	 * 
	 * 
	 * 
	 * }
	 * 
	 * catch(Throwable e)
	 * 
	 * {
	 * 
	 * return CatchStatementWebElement(e.getMessage());
	 * 
	 * }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * }
	 */

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	/*
	 * 
	 * public static String GetChildAttributeVal()
	 * 
	 * {
	 * 
	 * 
	 * 
	 * try{
	 * 
	 * 
	 * 
	 * UpdateResult.ActualData = TOChldRObj().getAttribute(TC.InputData);
	 * 
	 * return UpdateResult.UpdateStatus();
	 * 
	 * 
	 * 
	 * 
	 * 
	 * }
	 * 
	 * catch(Throwable e)
	 * 
	 * {
	 * 
	 * return CatchStatementWebElement(e.getMessage());
	 * 
	 * }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * }
	 */

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyAttributeValWebElements() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the text VerifyAttributeValWebElements ");
		try {
			List<WebElement> EleCollection = WebElementsTORObj();
			String found = "NO";
			for (int i = 0; i <= EleCollection.size(); i++) {
				// System.out.println(EleCollection.get(i).getAttribute("value"));
				if (EleCollection.get(i).getAttribute("value")
						.equals(TC.InputData)) {
					found = "YES";
					UpdateResult.ActualData = EleCollection.get(i)
							.getAttribute("value");
					TC.ExpectedData = TC.InputData;
					return UpdateResult.UpdateStatus();
				}
			}

			if (found.equals("NO")) {
				UpdateResult.ActualData = "Text Not found";
				TC.ExpectedData = TC.InputData;
				Logs.Ulog("Verifying the text VerifyAttributeValWebElements failed ");
				return UpdateResult.UpdateStatus();

			}
			// System.out.println("**********************************************************************");
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyAttributeRANDOMValWebElements()
			throws IOException, InterruptedException {

		Logs.Ulog("Verifying the text VerifyAttributeValWebElements ");
		try {
			List<WebElement> EleCollection = WebElementsTORObj();
			String found = "NO";
			for (int i = 0; i <= EleCollection.size(); i++) {
				// System.out.println(EleCollection.get(i).getAttribute("value"));
				if (EleCollection.get(i).getAttribute("value")
						.equals(Storeval1)) {
					found = "YES";
					UpdateResult.ActualData = EleCollection.get(i)
							.getAttribute("value");
					TC.ExpectedData = Storeval1;
					return UpdateResult.UpdateStatus();
				}
			}

			if (found.equals("NO")) {
				UpdateResult.ActualData = "Text Not found";
				TC.ExpectedData = Storeval1;
				Logs.Ulog("Verifying the text VerifyAttributeValWebElements failed ");
				return UpdateResult.UpdateStatus();

			}
			// System.out.println("**********************************************************************");
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyCheckBox_Checked_UnChecked() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying checkbox selected");
		try {
			UpdateResult.ActualData = TORObj().getAttribute("checked");
			TC.ExpectedData = TC.InputData;
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyCheckboxsSelectedByIndex() throws IOException,
			InterruptedException

	{

		Logs.Ulog("Verifying the text VerifyCheckboxsSelectedByIndex ");

		try

		{

			List<WebElement> EleCollection = WebElementsTORObj();

			int index = (int) Double.parseDouble(TC.InputData);

			UpdateResult.ActualData = String.valueOf(EleCollection.get(index)
					.isSelected());

			return UpdateResult.UpdateStatus();

		}

		catch (Throwable e)

		{

			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String verifyInputBoxText() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying Object Text");
		try {

			WebElement Obj = TORObj();

			UpdateResult.ActualData = Obj.getAttribute("value");
			Logs.Ulog("Actual data  = " + UpdateResult.ActualData);
			TC.ExpectedData = TC.InputData;
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {

			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String verifyListSelection() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying all the list elements");
		try {
			String expectedVal = TC.InputData;
			// System.out.println(driver.findElement(By.xpath(TC.TestObjects)).getText());
			WebElement droplist = TORObj();
			List<WebElement> droplist_cotents = droplist.findElements(By
					.tagName("option"));
			String actualVal = null;
			for (int i = 0; i < droplist_cotents.size(); i++) {
				String selected_status = droplist_cotents.get(i).getAttribute(
						"selected");
				if (selected_status != null)
					actualVal = droplist_cotents.get(i).getText();
				UpdateResult.ActualData = actualVal;
			}

			if (!actualVal.equals(expectedVal)) {
				UpdateResult.ActualData = actualVal;
				TC.ExpectedData = TC.InputData;
				UpdateResult.UpdateStatus();
				Logs.Ulog(TC.FAIL + "Value not in list - " + expectedVal);
				return TC.FAIL;
			}
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());

		}

		TC.ExpectedData = TC.InputData;
		UpdateResult.UpdateStatus();
		return TC.PASS;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyObjectCountGreater() throws IOException,
			InterruptedException {

		Logs.Ulog("Get Object count VerifyObjectCountGreater ");

		try

		{

			// int Sval1 = StoreNum1 + Integer.parseInt(TC.InputData);
			// Updated by sbr to truncate input with .0 to none

			int Sval1 = StoreNum1
					+ Integer.parseInt(TC.InputData.replace(".0", ""));

			UpdateResult.ActualData = String.valueOf(Sval1);

			TC.ExpectedData = String.valueOf(StoreNum2);

			StoreNum1 = 0;

			StoreNum2 = 0;

			return UpdateResult.UpdateStatus();

		}

		catch (Throwable e)

		{

			StoreNum1 = 0;

			StoreNum2 = 0;

			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyObjectCountLess() throws IOException,
			InterruptedException {

		Logs.Ulog("Get Object count VerifyObjectCountLess");
		try {
			int Sval1 = StoreNum1 - Integer.parseInt(TC.InputData);
			UpdateResult.ActualData = String.valueOf(Sval1);
			TC.ExpectedData = String.valueOf(StoreNum2);
			StoreNum1 = 0;
			StoreNum2 = 0;
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			StoreNum1 = 0;
			StoreNum2 = 0;
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyObjectPostionChange() throws IOException,
			InterruptedException {

		Logs.Ulog("Get Object GetPostionWebEle");
		try {
			if (StoreNum1 != StoreNum3) {
				UpdateResult.ActualData = "Position changed";
				TC.ExpectedData = "Position changed";
				return UpdateResult.UpdateStatus();
			} else {
				UpdateResult.ActualData = StoreNum1 + "Position not changed";
				TC.ExpectedData = StoreNum3 + "Position changed";
				return UpdateResult.UpdateStatus();
			}
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String RestoreDB() throws SQLException {
		if (Db == null)
			Db = new Database();
		Db.RestoreDB();

		return TC.PASS;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String ExecuteSQLStatement() throws SQLException {
		if (Db == null)
			Db = new Database();
		Db.ExecuteSQL();

		return TC.PASS;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyObjectPostionNotChange() throws IOException,
			InterruptedException {

		Logs.Ulog("Get Object GetPostionWebEle");
		try {
			UpdateResult.ActualData = String.valueOf(StoreNum1);
			TC.ExpectedData = String.valueOf(StoreNum3);
			UpdateResult.UpdateStatus();
			UpdateResult.ActualData = String.valueOf(StoreNum2);
			TC.ExpectedData = String.valueOf(StoreNum4);
			StoreNum1 = 0;
			StoreNum2 = 0;
			StoreNum3 = 0;
			StoreNum4 = 0;
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			StoreNum1 = 0;
			StoreNum2 = 0;
			StoreNum3 = 0;
			StoreNum4 = 0;
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String verifyRadioSelected() throws IOException,
			InterruptedException {

		Logs.Ulog("Verify Radio button is selected");
		try {
			WebElement Obj = TORObj();
			UpdateResult.ActualData = Obj.getAttribute("checked");
			Logs.Ulog("Actual data  = " + UpdateResult.ActualData);
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {

			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyText() throws IOException, InterruptedException {

		Logs.Ulog("Verifying the text");
		try {
			WebElement Obj = TORObj();
			UpdateResult.ActualData = Obj.getText().trim();
			TC.ExpectedData = TC.InputData;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyInnertext() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the text");
		try {
			WebElement Obj = TORObj();

			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			wrappedElement.getWrappedDriver();

			UpdateResult.ActualData = (String) js.executeScript(
					"return arguments[0].innerText", Obj);
			// System.out.println(UpdateResult.ActualData );
			TC.ExpectedData = TC.InputData;
			TC.PassDescription = "Inner text is Matching ";
			TC.FailDescription = "Inner text is not Matching  ";

			UpdateDescription("Verify text of Object " + TC.TestObjects);

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyInnertextContains() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the VerifyInnertextContains");
		try {
			WebElement Obj = TORObj();

			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			wrappedElement.getWrappedDriver();

			UpdateResult.ActualData = (String) js.executeScript(
					"return arguments[0].innerText", Obj);
			// System.out.println(UpdateResult.ActualData );

			if (UpdateResult.ActualData.contains(TC.InputData)) {
				TC.ExpectedData = UpdateResult.ActualData;
			} else {

				TC.ExpectedData = " Innertext not Contains --- " + TC.InputData;
			}

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyTextContains() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the text contains...");
		try {
			WebElement Obj = TORObj();
			UpdateResult.ActualData = Obj.getText();
			// TC.ExpectedData = TC.InputData;

			/*
			 * if (UpdateResult.ActualData.contains(TC.InputData)) {
			 * TC.ExpectedData = UpdateResult.ActualData;
			 */
			if (TC.InputData.contains(UpdateResult.ActualData)) {
				TC.ExpectedData = UpdateResult.ActualData;
			} else {

				TC.ExpectedData = " Innertext not Contains --- " + TC.InputData;
			}

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyInnerhtmlContains() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the VerifyInnerhtml");
		try {
			WebElement Obj = TORObj();

			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			wrappedElement.getWrappedDriver();

			UpdateResult.ActualData = (String) js.executeScript(
					"return arguments[0].innerHTML", Obj);

			if (UpdateResult.ActualData.contains(TC.InputData)) {
				TC.ExpectedData = UpdateResult.ActualData;
			} else {

				TC.ExpectedData = " Innertext not Contains --- " + TC.InputData;
			}

			// TC.ExpectedData = TC.InputData;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyInnertextTrim() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the text");
		try {
			WebElement Obj = TORObj();

			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			wrappedElement.getWrappedDriver();

			UpdateResult.ActualData = (String) js.executeScript(
					"return arguments[0].innerText", Obj);
			// UpdateResult.ActualData =
			// UpdateResult.ActualData.replaceAll("[^\\p{ASCII}]",
			// "").replaceAll("?", "").replaceAll(" ", "");
			// UpdateResult.ActualData = UpdateResult.ActualData.replace(" ",
			// "").replaceAll("[^\\x00-\\x7f]", "");;
			UpdateResult.ActualData = UpdateResult.ActualData.replaceAll(
					"[^a-zA-Z0-9]+", "").replaceAll(" ", "");
			// System.out.println(UpdateResult.ActualDsata );
			// TC.ExpectedData = TC.InputData.replaceAll("[^\\p{ASCII}]",
			// "").replaceAll("?", "").replaceAll(" ", "");
			// TC.ExpectedData =TC.ExpectedData.replace(" ",
			// "").replaceAll("[^\\x00-\\x7f]", "");
			TC.ExpectedData = TC.InputData.replaceAll("[^a-zA-Z0-9]+", "")
					.replaceAll(" ", "");

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyInnertextRemoveCarNew() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the text");
		try {
			WebElement Obj = TORObj();

			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			wrappedElement.getWrappedDriver();

			UpdateResult.ActualData = (String) js.executeScript(
					"return arguments[0].innerText", Obj);
			// UpdateResult.ActualData =
			// UpdateResult.ActualData.replaceAll("[^\\p{ASCII}]",
			// "").replaceAll("?", "").replaceAll(" ", "");
			// UpdateResult.ActualData = UpdateResult.ActualData.replace(" ",
			// "").replaceAll("[^\\x00-\\x7f]", "");;
			UpdateResult.ActualData = UpdateResult.ActualData.replaceAll(
					"\\r\\n|\\r|\\n", "");
			// System.out.println(UpdateResult.ActualDsata );
			// TC.ExpectedData = TC.InputData.replaceAll("[^\\p{ASCII}]",
			// "").replaceAll("?", "").replaceAll(" ", "");
			// TC.ExpectedData =TC.ExpectedData.replace(" ",
			// "").replaceAll("[^\\x00-\\x7f]", "");
			TC.ExpectedData = TC.InputData.replaceAll("\\r\\n|\\r|\\n", "");

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyInnerhtml() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the VerifyInnerhtml");
		try {
			WebElement Obj = TORObj();

			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			wrappedElement.getWrappedDriver();

			UpdateResult.ActualData = (String) js.executeScript(
					"return arguments[0].innerHTML", Obj);

			TC.ExpectedData = TC.InputData;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyTitle() throws IOException, InterruptedException {

		Logs.Ulog("Verifying title");
		try {

			UpdateResult.ActualData = driver.getTitle();
			TC.ExpectedData = TC.InputData;
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyWebElmsAttrVal_InAlphaOrder()
			throws IOException, InterruptedException {

		Logs.Ulog("Verifying the text VerifyCheckboxsSelectedByIndex ");

		try

		{
			List<WebElement> EleCollection = WebElementsTORObj();

			UpdateResult.ActualData = TC.PASS;

			TC.ExpectedData = TC.PASS;

			for (int i = 0; i < EleCollection.size(); i++) {

				if (EleCollection
						.get(i)
						.getAttribute(TC.InputData)
						.compareTo(
								EleCollection.get(i + 1).getAttribute(
										TC.InputData)) >= 0)

				{

					UpdateResult.ActualData = TC.FAIL;

				}

			}

			return UpdateResult.UpdateStatus();

		}

		catch (Throwable e)

		{

			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String Wait() throws InterruptedException {

		try {
			Logs.Ulog("Pausing the Execution for " + TC.InputData);
			String WaitTime = "Test_" + TC.InputData;
			String[] a = WaitTime.split("_");

			int x = (int) Double.parseDouble(TC.InputData);
			TimeUnit.SECONDS.sleep(x);
			// Thread.sleep(WaitTime );

			UpdateDescription("Wait for  " + TC.ExpectedData + " Sec to Sync  ");
			return UpdateResult.Done();
		} catch (Throwable e) {
			Logs.Ulog("Error while pausing the execution  " + TC.InputData
					+ " " + e.getMessage());
			return TC.FAIL;
		}
	}

	public static String Sleep() throws InterruptedException {

		try {
			Logs.Ulog("Pausing the Execution for " + TC.InputData);
			String WaitTime = "Test_" + TC.InputData;
			String[] a = WaitTime.split("_");

			int x = (int) Double.parseDouble(TC.InputData);
			TimeUnit.SECONDS.sleep(x);
			// Thread.sleep(WaitTime );

			UpdateDescription("Wait for  " + TC.ExpectedData + " Sec to Sync  ");
			return UpdateResult.Done();
		} catch (Throwable e) {
			Logs.Ulog("Error while pausing the execution  " + TC.InputData
					+ " " + e.getMessage());
			return TC.FAIL;
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	private static WebElement WebeleFluentSync() {

		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(30, TimeUnit.SECONDS)
				.pollingEvery(500, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class);

		return wait.until(new Function<WebDriver, WebElement>() {

			public WebElement apply(WebDriver webDriver) {

				try {
					return TORObj();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}
		});

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String ClickElementsByRefAndVerify() throws IOException,
			InterruptedException

	{

		Logs.Ulog("Verifying the text ClickElementsByRef ");

		try

		{

			List<WebElement> EleCollection = WebElementsTORObj();

			getORObject(TC.Param1);

			List<WebElement> EleCollectionRef = WebElementsTORObj();

			String found = "NO";

			for (int i = 0; i <= EleCollection.size(); i++)

			{

				// System.out.println(EleCollection.get(i).getAttribute("value"));

				// System.out.println(EleCollectionRef.get(i).getAttribute("value"));

				if (EleCollection.get(i).getAttribute(TC.InputData)
						.equals(TC.ExpectedData))

				{

					found = "YES";

					EleCollectionRef.get(i).click();

					UpdateResult.ActualData = TC.PASS;

					TC.ExpectedData = TC.PASS;

					return UpdateResult.UpdateStatus();

				}

			}

			if (found.equals("NO"))

			{

				UpdateResult.ActualData = "Text Not found";

				TC.ExpectedData = TC.InputData;

				Logs.Ulog("Verifying the text ClickElementsByRef failed ");

				return UpdateResult.UpdateStatus();

			}

			return UpdateResult.UpdateStatus();

		}

		catch (Throwable e)

		{

			return CatchStatementWebElement(e.getMessage());

		}

	}

	public static String ClickElementsByRef() throws IOException,
			InterruptedException

	{

		Logs.Ulog("Verifying the text ClickElementsByRef ");

		try

		{
			List<WebElement> EleCollection = WebElementsTORObj();
			getORObject(TC.Param1);
			List<WebElement> EleCollectionRef = WebElementsTORObj();
			String found = "NO";
			for (int i = 0; i <= EleCollection.size(); i++) {
				if (EleCollection.get(i).getAttribute(TC.ExpectedData)
						.equals(TC.Param2)) {
					found = "YES";
					EleCollectionRef.get(i).click();
					UpdateResult.ActualData = TC.PASS;
					TC.ExpectedData = TC.PASS;
					return UpdateResult.UpdateStatus();
				}
			}
			if (found.equals("NO")) {
				UpdateResult.ActualData = TC.Param2 + "Text Not found";
				TC.ExpectedData = TC.Param2 + "Text Not found";
				Logs.Ulog("Verifying the text ClickElementsByRef failed ");
				return UpdateResult.UpdateStatus();
			}
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String TempLogin() throws InterruptedException, AWTException,
			IOException {

		// Selenium-WebDriver Java Code for entering Username & Password as
		// below:
		if (ReusableFunctions.ReuseTableData.get("D_URL").equals(
				"http://localhost:8080/timetrax-web/timetraxApp/#/timetraxApp")) {
			Thread.sleep(1000l);

			RMouse.mousePress(0);
			// aa.sendKeys("timetrax");
			RMouse.keyPress(KeyEvent.VK_T);
			RMouse.keyPress(KeyEvent.VK_I);
			RMouse.keyPress(KeyEvent.VK_M);
			RMouse.keyPress(KeyEvent.VK_E);
			RMouse.keyPress(KeyEvent.VK_T);
			RMouse.keyPress(KeyEvent.VK_R);
			RMouse.keyPress(KeyEvent.VK_A);
			RMouse.keyPress(KeyEvent.VK_X);

			RMouse.keyPress(KeyEvent.VK_TAB);
			Thread.sleep(1000);

			RMouse.keyPress(KeyEvent.VK_T);
			RMouse.keyPress(KeyEvent.VK_I);
			RMouse.keyPress(KeyEvent.VK_M);
			RMouse.keyPress(KeyEvent.VK_E);

			RMouse.keyPress(KeyEvent.VK_T);
			RMouse.keyPress(KeyEvent.VK_R);
			RMouse.keyPress(KeyEvent.VK_A);
			RMouse.keyPress(KeyEvent.VK_X);

			Thread.sleep(1000);
			RMouse.keyPress(KeyEvent.VK_TAB);
			// RMouse.keyPress(KeyEvent.VK_TAB);
			try {
				ReSetSyncTime();
				driver.findElement(
						By.xpath("//div[@class='loginSubmitButton fontBold']"))
						.click();
				SetDefaultSyncTime();
			} catch (Throwable t) {
				RMouse.keyPress(KeyEvent.VK_ENTER);
				SetDefaultSyncTime();
				return TC.PASS;
			}
			SetDefaultSyncTime();
			// aa.accept();
			return UpdateResult.Done();
		} else {
			Thread.sleep(1000l);

			RMouse.mousePress(0);
			// aa.sendKeys("timetrax");
			RMouse.keyPress(KeyEvent.VK_T);
			RMouse.keyPress(KeyEvent.VK_I);
			RMouse.keyPress(KeyEvent.VK_M);
			RMouse.keyPress(KeyEvent.VK_E);
			RMouse.keyPress(KeyEvent.VK_T);
			RMouse.keyPress(KeyEvent.VK_R);
			RMouse.keyPress(KeyEvent.VK_A);
			RMouse.keyPress(KeyEvent.VK_X);
			RMouse.keyPress(KeyEvent.VK_1);

			RMouse.keyPress(KeyEvent.VK_TAB);
			Thread.sleep(1000);

			RMouse.keyPress(KeyEvent.VK_T);
			RMouse.keyPress(KeyEvent.VK_I);
			RMouse.keyPress(KeyEvent.VK_M);
			RMouse.keyPress(KeyEvent.VK_E);
			RMouse.keyPress(KeyEvent.VK_SHIFT);
			RMouse.keyPress(KeyEvent.VK_1);
			RMouse.keyRelease(KeyEvent.VK_SHIFT);

			// RMouse.keyPress(KeyEvent.VK_SHIFT);
			RMouse.keyPress(KeyEvent.VK_T);
			RMouse.keyPress(KeyEvent.VK_R);
			RMouse.keyPress(KeyEvent.VK_A);
			RMouse.keyPress(KeyEvent.VK_X);

			RMouse.keyPress(KeyEvent.VK_1);
			Thread.sleep(1000);
			RMouse.keyPress(KeyEvent.VK_TAB);
			// RMouse.keyPress(KeyEvent.VK_TAB);
			try {
				ReSetSyncTime();
				driver.findElement(
						By.xpath("//div[@class='loginSubmitButton fontBold']"))
						.click();
				SetDefaultSyncTime();
			} catch (Throwable t) {
				RMouse.keyPress(KeyEvent.VK_ENTER);
				return TC.PASS;
			}

			// aa.accept();
			return UpdateResult.Done();
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String KillProcess() throws IOException, InterruptedException {

		try {
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Thread.sleep(200);
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(200);
			Runtime.getRuntime().exec("TASKKILL /F /IM Safari.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Thread.sleep(200);
			Logs.Ulog("Pocess Killed");
			return TC.PASS;
			// System.exit(0);timetrax timetrax
		} catch (Throwable t) {
			return TC.PASS;
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// ######################################################################################################################################
	public static String MouseRelease() throws IOException,
			InterruptedException {

		try {
			Logs.Ulog("Mouse Release");

			Actions Mouse = new Actions(driver);
			Mouse.release();
			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// ######################################################################################################################################
	public static String MouseReleaseBuiler() throws IOException,
			InterruptedException {

		try {
			Logs.Ulog("Mouse Release");

			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			driver.switchTo().defaultContent();
			Actions builder1 = new Actions(driver);

			Action dragAndDrop = builder1.clickAndHold(TORObj()).build();
			dragAndDrop.perform();

			getORObject("Page(Content).LeftTree(Image)");

			driver.switchTo().frame("cq-cf-frame");
			getORObject("Dialog(\"EditComponent\").Image(\"Drop an Image\")");

			dragAndDrop = builder1.moveToElement(TORObj()).release().build();
			dragAndDrop.perform();

			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// //#####################################************************************************#############################################

	public static String StartJboss() throws IOException, InterruptedException {
		try {
			Logs.Ulog("---- Start  Jboss----");
			File file = new File((String) StoreTable.get("JbossServerLog"));

			file.delete();
			file.createNewFile();
			Process p = Runtime.getRuntime().exec(
					"cmd.exe /C start " + StoreTable.get("JbossPath"));

			int JbossStartTime = (int) Double.parseDouble((String) StoreTable
					.get("JbossStartTime"));
			TimeUnit.SECONDS.sleep(JbossStartTime);

			CheckForJbossStart();

			return TC.PASS;
		} catch (Throwable t) {
			Logs.Ulog("ERROR ----- Start J boss Not Successfull - check if JBOSS is closed  -------  "
					+ t.getMessage());
			return CatchStatementWebElement(t.getMessage());
		}
	}

	// //#####################################************************************************#############################################
	// started in 65628ms - Started 717 of 861 services (139 serv

	public static String CheckForJbossStart() throws IOException,
			InterruptedException {
		try {
			boolean JbossStarted = false;
			FileReader fin = new FileReader(
					(String) StoreTable.get("JbossServerLog"));

			final Scanner scanner = new Scanner(fin);
			while (scanner.hasNextLine()) {
				final String lineFromFile = scanner.nextLine();
				if (lineFromFile.matches((String) StoreTable
						.get("JbossStartMsg"))) {
					// a match!
					Logs.Ulog("---- SOUNDS GOOD JBOSS STARTED ----");
					System.out.println("---- SOUNDS GOOD JBOSS STARTED ----");
					JbossStarted = true;
					return UpdateResult.Done();
				}
			}
			if (JbossStarted == false) {
				if (StoreTable.get("FailStatement1SendMailGmail").toString()
						.equalsIgnoreCase("Yes")) {
					FailStatement1GmailSendMail JbossFail = new FailStatement1GmailSendMail();
					JbossFail.WriteEmail();
					Process p = Runtime.getRuntime().exec(
							"cmd.exe /C start C:\\Fail1SendMail.vbs");
					Runtime.getRuntime().exit(0);
					return TC.FAIL;
				}

			}
			return TC.PASS;
		} catch (Throwable t) {

			Logs.Ulog("ERROR ----- Start J boss Not Successfull - check if JBOSS is closed  -------  "
					+ t.getMessage());
			if (StoreTable.get("FailStatement1SendMailGmail").toString()
					.equalsIgnoreCase("Yes")) {
				FailStatement1GmailSendMail JbossFail = new FailStatement1GmailSendMail();
				JbossFail.WriteEmail();
				Process p = Runtime.getRuntime().exec(
						"cmd.exe /C start C:\\Fail1SendMail.vbs");
				Runtime.getRuntime().exit(0);
				return TC.FAIL;
			}
			return CatchStatementWebElement(t.getMessage());
		}
	}

	public static String CheckForJbossStart_old() throws IOException,
			InterruptedException {
		try {

			Logs.Ulog("---- Start  CheckForJbossStart ----");
			BufferedReader reader = new BufferedReader(new FileReader(
					(String) StoreTable.get("JbossServerLog")));
			String line = null;
			int count = 0;
			while ((line = reader.readLine()) != null) {
				// System.out.println(reader.readLine().matches("started in (.*)
				// - Started"));

				if (reader.readLine().toString().toLowerCase()
						.contains("started in")
						| reader.readLine().contains("- Started")) {

					// System.out.println("JBOSS STARTED");
					Logs.Ulog("---- SOUNDS GOOD JBOSS STARTED ----");
					break;
				}
				count++;
				if (count > 2000) {
					break;
				}

			}
			return TC.PASS;
		} catch (Throwable t) {

			Logs.Ulog("ERROR ----- Start J boss Not Successfull - check if JBOSS is closed  -------  "
					+ t.getMessage());
			return CatchStatementWebElement(t.getMessage());
		}
	}

	// //#####################################************************************************#############################################

	public static String BUILDTimeTrax() throws IOException,
			InterruptedException {
		try {
			Logs.Ulog("---- Start  BUILDTimeTrax  ----");

			File file = new File((String) StoreTable.get("BuildLogPath"));

			file.delete();
			file.createNewFile();
			Process p = Runtime.getRuntime().exec(
					"cmd.exe /C start " + StoreTable.get("BuildPath"));

			int BuildTime = (int) Double.parseDouble((String) StoreTable
					.get("BuildTime"));
			TimeUnit.SECONDS.sleep(BuildTime);
			Logs.Ulog("---- Executed build file ----");

			boolean BuildSuccess = false;
			FileReader fin = new FileReader(
					(String) StoreTable.get("BuildLogPath"));

			final Scanner scanner = new Scanner(fin);
			while (scanner.hasNextLine()) {
				final String lineFromFile = scanner.nextLine();
				if (lineFromFile.contains((String) StoreTable.get("BuildMsg"))) {
					// a match!
					Logs.Ulog("---- SOUNDS GOOD BUILD SUCCESS ----");
					System.out.println("---- SOUNDS GOOD BUILD SUCCESS ----");
					BuildSuccess = true;
					// p = Runtime.getRuntime().exec("cmd.exe /C start " +
					// StoreTable.get("JbossDeploy"));
					return UpdateResult.Done();
				}
			}
			if (BuildSuccess == false) {
				if (StoreTable.get("FailStatement2SendMailGmail").toString()
						.equalsIgnoreCase("Yes")) {
					FailStatement2GmailSendMail BuildFail = new FailStatement2GmailSendMail();
					BuildFail.WriteEmail();
					p = Runtime.getRuntime().exec(
							"cmd.exe /C start C:\\Fail2SendMail.vbs");
					Runtime.getRuntime().exit(0);
				}

			}
			return TC.PASS;

		} catch (Throwable t) {
			Logs.Ulog("ERROR ----- Build Not Successfull - check build log  -------  "
					+ t.getMessage());
			if (StoreTable.get("FailStatement2SendMailGmail").toString()
					.equalsIgnoreCase("Yes")) {
				FailStatement2GmailSendMail BuildFail = new FailStatement2GmailSendMail();
				BuildFail.WriteEmail();
				Runtime.getRuntime().exit(0);
			}
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String MenuOptionSelectMouseOver() throws IOException,
			InterruptedException {
		// StoreTable.put("Fluent_Time" , 5);
		Logs.Ulog("Verifying Object Text");
		try {

			WebElement Menu = TORObj();
			boolean IsObjdisp = false;
			Actions Mouse = new Actions(driver);
			Mouse.moveToElement(Menu).perform();
			try {
				getORObject(TC.Param1);
				WebElement Option = TORObj();
				Option.click();
			} catch (Throwable T) {
				Logs.Ulog(TC.Param1 + "Object Not Present");
			}
			try {
				getORObject(TC.Param2);
				WebElement ObjDisp = TORObj();
				IsObjdisp = ObjDisp.isDisplayed();
			} catch (Throwable T) {
				Logs.Ulog(TC.Param2 + "Object Not Present");
			}

			int count = 0;
			while (!IsObjdisp) {
				Logs.Ulog("In While loop");

				try {
					getORObject(TC.TestObjects);
					Menu = TORObj();
					Mouse = new Actions(driver);
					Mouse.moveToElement(Menu).perform();

					getORObject(TC.Param1);
					WebElement Option = TORObj();
					Option.click();
				} catch (Throwable T) {
					Logs.Ulog(TC.Param1 + "Object Not Present");
				}

				Logs.Ulog("Mouse move done - in while loop");
				try {
					getORObject(TC.Param2);
					WebElement ObjDisp = TORObj();
					IsObjdisp = ObjDisp.isDisplayed();
				} catch (Throwable T) {
					Logs.Ulog(TC.Param2 + "Object Not Present");
				}

				if (IsObjdisp)
					break;
				Thread.sleep(100l);
				count++;
				if (count > 5)
					break;
				Logs.Ulog("Count " + count);
			}

			UpdateResult.ActualData = Menu.getAttribute("value");
			Logs.Ulog("Actual data  = " + UpdateResult.ActualData);

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {

			Logs.Ulog("ERROR---  on Mouse Over= " + e.getMessage());
			return UpdateResult.UpdateStatus();

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String MenuOptionSelectClickV1() throws IOException,
			InterruptedException {
		// StoreTable.put("Fluent_Time" , 5);
		Logs.Ulog("Verifying Object Text");
		try {

			WebElement Menu = TORObj();
			boolean IsObjdisp = false;
			// Menu.click();
			RoboMouseMoveOnObj();
			// RoboMouseClickOnObj();
			try {
				getORObject(TC.Param1);
				WebElement Option = TORObj();
				Option.click();
				// RoboMouseClickOnObj();

			} catch (Throwable T) {
				Logs.Ulog(TC.Param1 + "Object Not Present");
			}
			try {
				getORObject(TC.Param2);
				WebElement ObjDisp = TORObj();
				IsObjdisp = ObjDisp.isDisplayed();
			} catch (Throwable T) {
				Logs.Ulog(TC.Param2 + "Object Not Present");
			}

			int count = 0;
			while (!IsObjdisp) {
				Logs.Ulog("In While loop");

				try {
					getORObject(TC.TestObjects);
					Menu = TORObj();
					// Menu.click();
					RoboMouseMoveOnObj();
					// RoboMouseClickOnObj();

					getORObject(TC.Param1);
					WebElement Option = TORObj();
					Option.click();
					// RoboMouseClickOnObj();
				} catch (Throwable T) {
					Logs.Ulog(TC.Param1 + "Object Not Present");
				}

				Logs.Ulog("Mouse move done - in while loop");
				try {
					getORObject(TC.Param2);
					WebElement ObjDisp = TORObj();
					IsObjdisp = ObjDisp.isDisplayed();
				} catch (Throwable T) {
					Logs.Ulog(TC.Param2 + "Object Not Present");
				}

				if (IsObjdisp)
					break;
				Thread.sleep(100l);
				count++;
				if (count > 5)
					break;
				Logs.Ulog("Count " + count);
			}

			UpdateResult.ActualData = Menu.getAttribute("value");
			Logs.Ulog("Actual data  = " + UpdateResult.ActualData);

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {

			Logs.Ulog("ERROR---  on Mouse Over= " + e.getMessage());
			return UpdateResult.UpdateStatus();

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String MenuOptionSelectClick() throws IOException,
			InterruptedException {
		// StoreTable.put("Fluent_Time" , 5);
		Logs.Ulog("Verifying Object Text");
		try {
			WebElement Menu = TORObj();
			RoboMouseMoveOnObj();
			ReSetSyncTime();
			for (int i = 0; i <= 15; i++) {

				try {

					// Menu.click();
					Thread.sleep(100L);
					getORObject(TC.Param1);
					WebElement Option = NonExceptionTORObj();
					Menu.click();
					Option.click();

					Thread.sleep(3000L);
					getORObject(TC.Param2);
					WebElement ObjDisp = NonExceptionTORObj();
					if (ObjDisp.isDisplayed()) {
						SetDefaultSyncTime();
						break;

					}

				} catch (Throwable T) {

				}

			}
			SetDefaultSyncTime();
			Logs.Ulog("Actual data  = " + UpdateResult.ActualData);
			Sync();
			return UpdateResult.Done();
		} catch (Throwable e) {
			SetDefaultSyncTime();
			Logs.Ulog("ERROR---  on Mouse Over= " + e.getMessage());
			return UpdateResult.UpdateStatus();

		}
	}

	public static String VerifyDisabled() throws IOException,
			InterruptedException {

		Logs.Ulog(" Executing InputText ");

		try {
			WebElement Obj = TORObj();

			// System.out.println(js.executeScript("return
			// arguments[0].disabled=",
			// Obj));
			Logs.Ulog(" Verify element disabled=="
					+ ((JavascriptExecutor) driver).executeScript(
							"return arguments[0].disabled", Obj));
			boolean disabled = (Boolean) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0].disabled", Obj);
			if (disabled) {
				UpdateResult.ActualData = "DISABLED";
				TC.ExpectedData = "DISABLED";
			} else {
				UpdateResult.ActualData = "NOT DISABLED";
				TC.ExpectedData = "DISABLED";
			}

			return UpdateResult.UpdateStatus();
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyEnabled() throws IOException,
			InterruptedException {

		Logs.Ulog(" Executing InputText ");

		try {
			WebElement Obj = TORObj();

			// System.out.println(js.executeScript("return
			// arguments[0].disabled=",
			// Obj));
			Logs.Ulog(" Verify element disabled=="
					+ ((JavascriptExecutor) driver).executeScript(
							"return arguments[0].disabled", Obj));
			boolean disabled = (Boolean) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0].disabled", Obj);

			if (!disabled) {
				UpdateResult.ActualData = "ENABLED";
				TC.ExpectedData = "ENABLED";
			} else {
				UpdateResult.ActualData = "NOT ENABLED";
				TC.ExpectedData = "ENABLED";
			}

			return UpdateResult.UpdateStatus();
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyAutoFocus_True() throws IOException,
			InterruptedException {

		Logs.Ulog(" Executing VerifyAutoFocus_True ");

		try {
			WebElement Obj = TORObj();

			// System.out.println(js.executeScript("return
			// arguments[0].disabled=",
			// Obj));
			Logs.Ulog(" Verify Attribute autofocus=="
					+ ((JavascriptExecutor) driver).executeScript(
							"return arguments[0].autofocus", Obj));
			boolean Autofocous = (Boolean) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0].autofocus", Obj);

			if (Autofocous) {
				UpdateResult.ActualData = "autofocus=TRUE";
				TC.ExpectedData = "autofocus=TRUE";
			} else {
				UpdateResult.ActualData = "NOT autofocus";
				TC.ExpectedData = "autofocus=TRUE";
			}

			return UpdateResult.UpdateStatus();
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyAutoFocus_False() throws IOException,
			InterruptedException {

		Logs.Ulog(" Executing InputText ");

		try {
			WebElement Obj = TORObj();

			// System.out.println(js.executeScript("return
			// arguments[0].disabled=",
			// Obj));
			Logs.Ulog(" Verify Attribute autofocus=="
					+ ((JavascriptExecutor) driver)
							.executeScript("return arguments[0].autofocus"
									+ TC.InputData, Obj));
			boolean autofocus = (Boolean) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0].autofocus", Obj);

			if (!autofocus) {
				UpdateResult.ActualData = "autofocus=FALSE";
				TC.ExpectedData = "autofocus=TRUE";
			} else {
				UpdateResult.ActualData = "autofocus=TRUE";
				TC.ExpectedData = "autofocus=FALSE";
			}

			return UpdateResult.UpdateStatus();
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String SendKeyTab() throws IOException, InterruptedException {

		try {
			Logs.Ulog("Executing SendKeyTab");

			TORObj().sendKeys(Keys.TAB);
			TORObj().sendKeys(Keys.TAB);
			// RMouse.mousePress(0);
			// RMouse.keyPress(KeyEvent.VK_TAB);

			Thread.sleep(200L);
			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	public static String SendKeyTabRobo() throws IOException,
			InterruptedException {

		try {
			Logs.Ulog("Executing SendKeyTabRobo");

			RMouse.keyPress(KeyEvent.VK_TAB);
			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// ######################################################################################################################################
	public static String SendKeyALT_A() throws IOException,
			InterruptedException {

		try {
			Logs.Ulog("Executing SendKeyTab");

			// TORObj().sendKeys(Keys.TAB);
			RMouse.mousePress(0);
			RMouse.keyPress(KeyEvent.VK_ALT);
			RMouse.keyPress(KeyEvent.VK_A);
			Thread.sleep(200L);
			RMouse.keyRelease(KeyEvent.VK_ALT);
			RMouse.keyRelease(KeyEvent.VK_A);
			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// ######################################################################################################################################
	public static String SendKeyCTR_A() throws IOException,
			InterruptedException {

		try {
			Logs.Ulog("Executing SendKeyTab");
			HighlightElement();
			// DbClickJS();
			driver.findElement(By.xpath("//body")).click();
			// TORObj().sendKeys(Keys.TAB);
			RMouse.mousePress(0);
			RMouse.keyPress(KeyEvent.VK_CONTROL);
			RMouse.keyPress(KeyEvent.VK_A);
			Thread.sleep(200L);
			RMouse.keyRelease(KeyEvent.VK_CONTROL);
			RMouse.keyRelease(KeyEvent.VK_A);
			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	public static String OpenNewTab() throws IOException, InterruptedException {

		try {
			Logs.Ulog("Executing SendKeyTab");
			HighlightElement();
			// DbClickJS();
			String selectnewTab = Keys.chord(Keys.CONTROL, "t");
			driver.findElement(By.xpath("//body")).sendKeys(selectnewTab);
			// TORObj().sendKeys(Keys.TAB);
			/*
			 * RMouse.mousePress(0); RMouse.keyPress(KeyEvent.VK_CONTROL);
			 * RMouse.keyPress(KeyEvent.VK_T); Thread.sleep(200L);
			 * RMouse.keyRelease(KeyEvent.VK_CONTROL);
			 * RMouse.keyRelease(KeyEvent.VK_T);
			 */
			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// ######################################################################################################################################
	public static String SendKeyCTR_W() throws IOException,
			InterruptedException {

		try {
			Logs.Ulog("Executing SendKeyTab");
			HighlightElement();
			// DbClickJS();
			driver.findElement(By.xpath("//body")).click();
			// TORObj().sendKeys(Keys.TAB);
			RMouse.mousePress(0);
			RMouse.keyPress(KeyEvent.VK_CONTROL);
			RMouse.keyPress(KeyEvent.VK_W);
			Thread.sleep(200L);
			RMouse.keyRelease(KeyEvent.VK_CONTROL);
			RMouse.keyRelease(KeyEvent.VK_W);
			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// //#####################################************************************************#############################################

	public static String RoboMouseMoveOnObj() throws IOException,
			InterruptedException {

		Logs.Ulog("Start of RoboMouseMoveOnObj");
		try {

			C_RoboMouse.MoveMouseToWebEleCoord(TORObj());

			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String RoboMouseClickOnObj() throws IOException,
			InterruptedException {

		Logs.Ulog("Start of RoboMouseClickOnObj");
		try {

			C_RoboMouse.MoveMouseToWebEleCoord(TORObj());
			C_RoboMouse.robotPoweredClick();

			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String RoboMouseDown() throws IOException,
			InterruptedException {

		Logs.Ulog("Start of RoboMouseClickOnObj");
		try {

			C_RoboMouse.MoveMouseToWebEleCoord(TORObj());
			RMouse.mousePress(InputEvent.BUTTON1_MASK);
			Thread.sleep(2000l);
			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String RoboMouseRtClickOnObj() throws IOException,
			InterruptedException {

		Logs.Ulog("Start of RoboMouseClickOnObj");
		try {

			C_RoboMouse.MoveMouseToWebEleCoord(TORObj());

			RMouse.keyPress(InputEvent.BUTTON3_DOWN_MASK);

			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String RoboMouseRelease() throws IOException,
			InterruptedException {

		Logs.Ulog("Start of the RoboMouseRelease");
		try {

			// C_RoboMouse.robotPoweredMouseUp();
			RMouse.mouseRelease(InputEvent.BUTTON1_MASK);

			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String RoboMouseMoveOnPage() throws IOException,
			InterruptedException {

		Logs.Ulog("Start of RoboMouseMoveOnPage");
		try {

			C_RoboMouse.MoveMouseToCoordOnPg(Integer.parseInt(TC.Param1),
					Integer.parseInt(TC.Param2));

			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String RoboMouseMoveOnAbsCord() throws IOException,
			InterruptedException {

		Logs.Ulog("Start of RoboMouseMoveOnPage");
		try {

			C_RoboMouse.MoveMouseToAbsCoord(Integer.parseInt(TC.Param1),
					Integer.parseInt(TC.Param2));

			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyGetTextRANDOMPlusValWebElements()
			throws IOException, InterruptedException {

		Logs.Ulog("Verifying the text VerifyAttributeValWebElements ");
		try {
			List<WebElement> EleCollection = WebElementsTORObj();
			String found = "NO";

			for (int i = 0; i < EleCollection.size(); i++) {
				// System.out.println(EleCollection.get(i).getAttribute("innerHTML"));
				EleCollection.get(i).sendKeys(Keys.PAGE_DOWN);
				// EleCollection.get(i).sendKeys(Keys.PAGE_DOWN);
				// String ObjtextString = (String) ((JavascriptExecutor)
				// driver).executeScript("return
				// arguments[0].title",EleCollection.get(i)
				// );
				if (EleCollection.get(i).getText().toUpperCase()
						.contains((RANDOMCONSTPlusStoreval1.toUpperCase()))) {
					// if
					// (EleCollection.get(i).getText().toUpperCase().equals((Storeval1.toUpperCase())))
					// {
					found = "YES";
					UpdateResult.ActualData = RANDOMCONSTPlusStoreval1
							.toUpperCase();
					TC.ExpectedData = RANDOMCONSTPlusStoreval1.toUpperCase();
					return UpdateResult.UpdateStatus();
				}
			}

			if (found.equals("NO")) {
				UpdateResult.ActualData = "Text Not found";
				TC.ExpectedData = RANDOMCONSTPlusStoreval1;
				Logs.Ulog("Verifying the text VerifyAttributeValWebElements failed ");
				return UpdateResult.UpdateStatus();

			}
			// System.out.println("**********************************************************************");
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyGetTextElementsByRef_scrollDown()
			throws IOException, InterruptedException {

		Logs.Ulog("Verifying the text VerifyAttributeValWebElements ");
		try {
			WebElement refWebEl = TORObj();
			// refWebEl.sendKeys(Keys.PAGE_DOWN);
			ReSetSyncTime();

			// int ST = Integer.parseInt(refWebEl.getAttribute("scrollTopMax"));

			for (int i = 0; i < 100; i++) {
				// System.out.println(EleCollection.get(i).getAttribute("innerHTML"));

				try {

					getORObject(TC.ExpectedData);
					// ST =
					// Integer.parseInt(refWebEl.getAttribute("scrollTopMax"));
					Thread.sleep(500);
					if (TORObj().isDisplayed()) {

						// TORObj().click();

						UpdateResult.ActualData = TORObj().getText();
						TC.ExpectedData = TORObj().getText();
						// HighlightElement();
						Logs.Ulog("Verifying the text VerifyGetTextElementsByRef_scroll Passed ");

						for (int j = 0; j <= i; j++)
							refWebEl.sendKeys(Keys.PAGE_UP);

						SetDefaultSyncTime();
						return UpdateResult.UpdateStatus();

					}

				} catch (Throwable T) {
					refWebEl.sendKeys(Keys.PAGE_DOWN);
				}
				SetDefaultSyncTime();

			}
			// System.out.println("**********************************************************************");
			UpdateResult.ActualData = "Text Not found";
			TC.ExpectedData = TC.InputData;
			Logs.Ulog("Verifying the text VerifyAttributeValWebElements failed ");

			SetDefaultSyncTime();
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			SetDefaultSyncTime();
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyGetTextRANDOMValWebElements()
			throws IOException, InterruptedException {

		Logs.Ulog("Verifying the text VerifyAttributeValWebElements ");
		try {
			List<WebElement> EleCollection = WebElementsTORObj();
			String found = "NO";

			for (int i = 0; i < EleCollection.size(); i++) {
				// System.out.println(EleCollection.get(i).getAttribute("innerHTML"));
				EleCollection.get(i).sendKeys(Keys.PAGE_DOWN);
				// EleCollection.get(i).sendKeys(Keys.PAGE_DOWN);
				// String ObjtextString = (String) ((JavascriptExecutor)
				// driver).executeScript("return
				// arguments[0].title",EleCollection.get(i)
				// );
				if (EleCollection.get(i).getText().toUpperCase()
						.contains((RANDOMStoreval1.toUpperCase()))) {
					// if
					// (EleCollection.get(i).getText().toUpperCase().equals((Storeval1.toUpperCase())))
					// {
					found = "YES";
					UpdateResult.ActualData = RANDOMStoreval1.toUpperCase();
					TC.ExpectedData = RANDOMStoreval1.toUpperCase();
					return UpdateResult.UpdateStatus();
				}
			}

			if (found.equals("NO")) {
				UpdateResult.ActualData = "Text Not found";
				TC.ExpectedData = RANDOMStoreval1;
				Logs.Ulog("Verifying the text VerifyAttributeValWebElements failed ");
				return UpdateResult.UpdateStatus();

			}
			// System.out.println("**********************************************************************");
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// #######################################################################################################################################################

	public static String SelectSaturday() throws IOException,
			InterruptedException {

		Logs.Ulog("Selecting Current Saturday of the week");
		try {
			Calendar cal = Calendar.getInstance();
			int date, month, saturday, dow;
			date = cal.get(Calendar.DATE);
			dow = cal.get(Calendar.DAY_OF_WEEK);

			WebElement ele = TORObj();
			while (dow != Calendar.SATURDAY) {
				cal.add(Calendar.DATE, 1);
				dow = cal.get(Calendar.DAY_OF_WEEK);
			}
			saturday = cal.get(Calendar.DATE);
			month = cal.get(Calendar.MONTH);

			// System.out.println("Saturday Date is "+saturday);
			if (date > saturday) {
				Logs.Ulog("Using Next Month's Saturday");
				// month++;
				Select select = new Select(ele.findElement(By
						.xpath("//select[@class='ui-datepicker-month']")));
				select.selectByIndex(month);
			}
			ele.findElement(By.xpath("//td/a[text()='" + saturday + "']"))
					.click();

			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	public static String VerifyTextPDFReports() throws IOException,
			InterruptedException {

		Logs.Ulog("---- in VerifyPDFReports---");
		try {

			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			URL url = new URL(driver.getCurrentUrl());
			/*
			 * BufferedInputStream fileToParse = new
			 * BufferedInputStream(url.openStream()); PDFParser parser = new
			 * PDFParser(fileToParse); parser.parse(); String text = new
			 * PDFTextStripper().getText(parser.getPDDocument());
			 * System.out.println(text); parser.getPDDocument().close(); return
			 * text;
			 */

			Cookie authorizationCookie = driver.manage().getCookieNamed(
					"JSESSIONID");
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("Cookie", authorizationCookie.toString());
			InputStream fileToParse = conn.getInputStream();
			PDFParser parser = new PDFParser((RandomAccessRead) fileToParse);
			parser.parse();

			String Aspl[] = new PDFTextStripperByArea()
					.getText(parser.getPDDocument()).toString()
					.split(TC.ExpectedData);
			UpdateResult.ActualData = Aspl[0];

			// System.out.println(UpdateResult.ActualData);
			parser.getPDDocument().close();

			PDDocument pdf = PDDocument.load(fileToParse,
					System.getProperty("user.dir") + "\\src\\TestData\\"
							+ TC.InputData);

			PDFTextStripperByArea stripper = new PDFTextStripperByArea();
			String DateReplace = new SimpleDateFormat("M/dd/yy")
					.format(new Date());

			String Spl[] = stripper.getText(pdf).toString()
					.split(TC.ExpectedData);
			// TC.ExpectedData = Spl[0].replace(TC.Param1, TC.Param2);
			TC.ExpectedData = Spl[0];

			// System.out.println(TC.ExpectedData );
			// UpdateResult.ActualData = Aspl[0].replace(TC.Param1, TC.Param2);

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyTextPDFReports_DTS() throws IOException,
			InterruptedException {

		Logs.Ulog("---- in VerifyPDFReports---");
		try {

			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			URL url = new URL(driver.getCurrentUrl());
			/*
			 * BufferedInputStream fileToParse = new
			 * BufferedInputStream(url.openStream()); PDFParser parser = new
			 * PDFParser(fileToParse); parser.parse(); String text = new
			 * PDFTextStripper().getText(parser.getPDDocument());
			 * System.out.println(text); parser.getPDDocument().close(); return
			 * text;
			 */

			Cookie authorizationCookie = driver.manage().getCookieNamed(
					"JSESSIONID");
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("Cookie", authorizationCookie.toString());
			InputStream fileToParse = conn.getInputStream();
			PDFParser parser = new PDFParser((RandomAccessRead) fileToParse);
			parser.parse();

			String Aspl[] = new PDFTextStripperByArea()
					.getText(parser.getPDDocument()).toString()
					.split(TC.ExpectedData);
			UpdateResult.ActualData = Aspl[0];

			// System.out.println(UpdateResult.ActualData);
			parser.getPDDocument().close();

			PDDocument pdf = PDDocument.load(fileToParse,
					System.getProperty("user.dir") + "\\src\\TestData\\"
							+ TC.InputData);

			PDFTextStripperByArea stripper = new PDFTextStripperByArea();
			String DateReplace = new SimpleDateFormat("m/d/yy")
					.format(new Date());

			String Spl[] = stripper.getText(pdf).toString()
					.split(TC.ExpectedData);
			TC.ExpectedData = Spl[0].replace(TC.Param1, DateReplace);

			// System.out.println(TC.ExpectedData );
			// UpdateResult.ActualData = Aspl[0].replace(TC.Param1, TC.Param2);

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	public static String VerifyTextPDFReports_Contains() throws IOException,
			InterruptedException {

		Logs.Ulog("---- in VerifyPDFReports---");
		try {

			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			URL url = new URL(driver.getCurrentUrl());
			/*
			 * BufferedInputStream fileToParse = new
			 * BufferedInputStream(url.openStream()); PDFParser parser = new
			 * PDFParser(fileToParse); parser.parse(); String text = new
			 * PDFTextStripper().getText(parser.getPDDocument());
			 * System.out.println(text); parser.getPDDocument().close(); return
			 * text;
			 */

			Cookie authorizationCookie = driver.manage().getCookieNamed(
					"JSESSIONID");
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("Cookie", authorizationCookie.toString());
			InputStream fileToParse = conn.getInputStream();
			PDFParser parser = new PDFParser((RandomAccessRead) fileToParse);
			parser.parse();
			String ActualPDF = new PDFTextStripperByArea().getText(parser
					.getPDDocument());
			System.out.println(ActualPDF);
			String Aspl[] = new PDFTextStripperByArea()
					.getText(parser.getPDDocument()).toString()
					.split(TC.ExpectedData);
			UpdateResult.ActualData = Aspl[0];

			String ExpEntireFileText = new Scanner(new File(
					System.getProperty("user.dir") + "\\src\\TestData\\"
							+ TC.InputData)).useDelimiter("\\A").next();
			if (Aspl[0].matches(ExpEntireFileText)) {
				UpdateResult.ActualData = Aspl[0];
				TC.ExpectedData = Aspl[0];
			} else {
				UpdateResult.ActualData = Aspl[0];
				TC.ExpectedData = ExpEntireFileText;
			}

			// System.out.println(UpdateResult.ActualData);
			parser.getPDDocument().close();

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyTextPDFReports_WritetoFile() throws IOException,
			InterruptedException {

		Logs.Ulog("---- in VerifyTextPDFReports_WritetoFile---");
		try {

			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			URL url = new URL(driver.getCurrentUrl());

			Cookie authorizationCookie = driver.manage().getCookieNamed(
					"JSESSIONID");
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("Cookie", authorizationCookie.toString());
			InputStream fileToParse = conn.getInputStream();
			PDFParser parser = new PDFParser((RandomAccessRead) fileToParse);
			parser.parse();
			String ActualPDF = new PDFTextStripperByArea().getText(parser
					.getPDDocument());
			System.out.println(ActualPDF);
			String Aspl[] = new PDFTextStripperByArea()
					.getText(parser.getPDDocument()).toString()
					.split(TC.Param1);
			UpdateResult.ActualData = Aspl[0];

			String ExpEntireFileText = new Scanner(new File(
					System.getProperty("user.dir") + "\\src\\TestData\\"
							+ TC.ExpectedData)).useDelimiter("\\A").next();

			FileWriter writer = new FileWriter(System.getProperty("user.dir")
					+ "\\src\\TestData\\" + TC.InputData);

			writer.write(Aspl[0]);

			writer.flush();
			writer.close();
			String ActEntireFileText = new Scanner(new File(
					System.getProperty("user.dir") + "\\src\\TestData\\"
							+ TC.InputData)).useDelimiter("\\A").next();

			System.out.println(ActEntireFileText.trim().matches(
					ExpEntireFileText.trim()));
			if (ActEntireFileText.trim().matches(ExpEntireFileText.trim())) {
				UpdateResult.ActualData = Aspl[0];
				TC.ExpectedData = Aspl[0];
			} else {
				UpdateResult.ActualData = Aspl[0];
				TC.ExpectedData = ExpEntireFileText;
			}

			// System.out.println(UpdateResult.ActualData);
			parser.getPDDocument().close();

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyTextPDFReports_WritetoFile_2Split()
			throws IOException, InterruptedException {

		Logs.Ulog("---- in VerifyTextPDFReports_WritetoFile_2Split---");
		try {

			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			URL url = new URL(driver.getCurrentUrl());

			Cookie authorizationCookie = driver.manage().getCookieNamed(
					"JSESSIONID");
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("Cookie", authorizationCookie.toString());
			InputStream fileToParse = conn.getInputStream();
			PDFParser parser = new PDFParser((RandomAccessRead) fileToParse);
			parser.parse();
			String ActualPDF = new PDFTextStripperByArea().getText(parser
					.getPDDocument());
			System.out.println(ActualPDF);
			String Aspl[] = new PDFTextStripperByArea()
					.getText(parser.getPDDocument()).toString()
					.split(TC.Param1);
			UpdateResult.ActualData = Aspl[0];
			String Aspl1[] = Aspl[0].toString().split(TC.Param2);
			String ExpEntireFileText = new Scanner(new File(
					System.getProperty("user.dir") + "\\src\\TestData\\"
							+ TC.ExpectedData)).useDelimiter("\\A").next();

			FileWriter writer = new FileWriter(System.getProperty("user.dir")
					+ "\\src\\TestData\\" + TC.InputData);

			writer.write(Aspl1[1]);

			writer.flush();
			writer.close();
			String ActEntireFileText = new Scanner(new File(
					System.getProperty("user.dir") + "\\src\\TestData\\"
							+ TC.InputData)).useDelimiter("\\A").next();

			System.out.println(ActEntireFileText.trim().matches(
					ExpEntireFileText.trim()));
			if (ActEntireFileText.trim().matches(ExpEntireFileText.trim())) {
				UpdateResult.ActualData = Aspl1[1];
				TC.ExpectedData = Aspl1[1];
			} else {
				UpdateResult.ActualData = Aspl1[1];
				TC.ExpectedData = ExpEntireFileText;
			}

			// System.out.println(UpdateResult.ActualData);
			parser.getPDDocument().close();

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String DeleAllFileInFolderWithXtn() throws IOException,
			InterruptedException {
		Logs.Ulog("Start Deleting DeleAllFileInFolderWithXtn ");

		try {

			File folder = new File(TC.InputData);
			File[] listOfFiles = folder.listFiles();

			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					if (listOfFiles[i].getName().endsWith(TC.ExpectedData)) {
						try {

							listOfFiles[i].delete();
						} catch (Throwable t) {
							Logs.Ulog("Error while Deleting the file ----"
									+ listOfFiles[i].getName());
						}

					}

				}
			}

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

		UpdateResult.Done();
		Logs.Ulog("End Deleting DeleAllFileInFolderWithXtn");
		return TC.PASS;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String DeleAllSubFolders() throws IOException,
			InterruptedException {
		Logs.Ulog("Start Deleting DeleAllSubFolders ");

		try {

			File folder = new File(TC.InputData);
			File[] listOfFiles = folder.listFiles();

			for (int i = 0; i < listOfFiles.length; i++) {

				try {
					listOfFiles[i].delete();
				} catch (Throwable t) {
					Logs.Ulog("Error while Deleting the file ----"
							+ listOfFiles[i].getName());
				}

			}

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

		UpdateResult.Done();
		Logs.Ulog("End Deleting DeleAllSubFolders");
		return TC.PASS;

	}

	public static void DeleAllSubFoldersV2()

	{
		Logs.Ulog("Start DeleAllSubFolders ");

		File file = new File(TC.InputData);

		recursiveDelete(file);

		Logs.Ulog("End DeleAllSubFolders ");
	}

	public static void recursiveDelete(File root) {
		try {

			if (root == null) {
				return;
			}

			if (root.isDirectory()) {
				File[] files = root.listFiles();
				if (files != null) {
					for (int i = 0; i < files.length; i++) {
						File file = files[i];
						if (file.isDirectory()) {
							// TimeCount++;
							//
							// if (TimeCount > 1000)
							// break;
							recursiveDelete(file);

						} else {
							try {
								file.delete();
							} catch (Throwable T) {
							}

						}
					}
				}
			}

			try {
				root.delete();
			} catch (Throwable T) {
			}

		} catch (Throwable T) {
		}
	}

	public static String ClickByRef_scrollDown() throws IOException,
			InterruptedException {

		Logs.Ulog(" Start ClickByRef_scrollDown ");
		try {
			WebElement refWebEl = TORObj();

			ReSetSyncTime();

			// int ST = Integer.parseInt(refWebEl.getAttribute("scrollTopMax"));

			for (int i = 0; i < 1000; i++) {
				// System.out.println(EleCollection.get(i).getAttribute("innerHTML"));

				try {

					getORObject(TC.ExpectedData);
					// ST =
					// Integer.parseInt(refWebEl.getAttribute("scrollTopMax"));
					Thread.sleep(500);
					if (TORObj().isDisplayed()) {
						Logs.Ulog("Click element By SCrolling element has found ");
						Click();

						Logs.Ulog("Click element By SCrolling element has found -  Clicked");

						for (int j = 0; j <= i; j++)
							refWebEl.sendKeys(Keys.PAGE_UP);
						return UpdateResult.Done();

					}

				} catch (Throwable T) {
					refWebEl.sendKeys(Keys.PAGE_DOWN);
				}

			}
			// System.out.println("**********************************************************************");
			UpdateResult.ActualData = "Element Not Found";
			TC.ExpectedData = "";
			Logs.Ulog("Click element By SCrolling element has found -  Fail");

			SetDefaultSyncTime();
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			SetDefaultSyncTime();
			return CatchStatementWebElement(e.getMessage());
		}

	}

	public static String StepSyncV1() throws InterruptedException, IOException

	{

		try {
			SetDefaultSyncTime();

			for (int i = 0; i <= 40; i++) {
				Thread.sleep(2000L);

				try {
					if (!driver
							.findElement(
									By.xpath((String) StoreTable
											.get("PageSyncObject")))
							.isDisplayed())
						SetDefaultSyncTime();
					break;

				} catch (Throwable t) {
					Logs.Ulog("Waiting for load page form busy- Waiting for form busy not exist");
				}

			}
		} catch (NoSuchElementException e) {
			SetDefaultSyncTime();
			return TC.PASS;
		}
		SetDefaultSyncTime();
		return TC.PASS;

	}

	public static String StepSyncV3() throws InterruptedException, IOException

	{

		try {

			TC.InputData = "100";
			Thread.sleep(1000L);
			getORObject("Form(BusyForm)");
			Logs.Ulog("Waiting for load page form busy- Waiting for form busy not exist");
			waitForElementNotPresent();

		} catch (NoSuchElementException e) {
			Logs.Ulog("Error - while Waiting for load page form busy- Waiting for form busy not exist");
			return TC.PASS;
		}
		SetDefaultSyncTime();
		return TC.PASS;

	}

	public static String StepSync() throws InterruptedException, IOException

	{
		return TC.PASS;
		/*
		 * ReSetSyncTime();
		 * 
		 * 
		 * 
		 * 
		 * for (int i = 0; i <= 180; i++) {
		 * 
		 * try {
		 * 
		 * if (driver.findElement(By.xpath((String)
		 * StoreTable.get("PageSyncObject"))).isDisplayed()) {
		 * 
		 * Logs.Ulog(
		 * "Waiting for load page form busy- Waiting for form busy not exist");
		 * Thread.sleep(1000l); }
		 * 
		 * } catch (Throwable T) { SetDefaultSyncTime(); break; } }
		 * SetDefaultSyncTime(); return TC.PASS;
		 */
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String CheckForAll() throws InterruptedException, IOException {

		int val = Integer.valueOf(TC.InputData);
		List<WebElement> testobjs = WebElementsTORObj();
		// System.out.println(testobjs);
		// System.out.println(testobjs.size());
		for (int i = 0; i < testobjs.size(); i++) {
			// System.out.println(testobjs.get(i).isDisplayed());

			if (testobjs.get(i).getAttribute("type").equals("checkbox")) {
				WrapsDriver wrappedElement = (WrapsDriver) testobjs.get(i);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				wrappedElement.getWrappedDriver();
				js.executeScript("arguments[0].click();", testobjs.get(i));
			}

		}

		return UpdateResult.Done();

	}

	public static String UpdateView() throws IOException

	{
		Logs.Ulog("Start UpdateView ");

		Process p = Runtime.getRuntime().exec(
				"cmd.exe /C start " + System.getProperty("user.dir")
						+ "\\src\\Homebase.exe");

		Logs.Ulog("End UpdateView ");
		return UpdateResult.Done();
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String DayCostReportValidation() throws IOException,
			InterruptedException {
		Logs.Ulog("Start Report Validation VerifyExcelReportRowCount ");
		try {

			Excel_xls ReportActual = new Excel_xls(
					(String) StoreTable.get("FilePath"));
			Excel_xls ReportExpected = new Excel_xls(
					System.getProperty("user.dir")
							+ "\\src\\TestData\\ReportFiles\\DayCostReportTemplate.xls");

			UpdateResult.ActualData = Integer.toString(ReportActual
					.getRowCount(TC.Param1));
			TC.ExpectedData = Integer.toString(ReportExpected
					.getRowCount(TC.Param1));
			TC.TestDescription = "Verify Day cost report row Count";
			TC.PassDescription = "Row count is matching";
			TC.PassDescription = "Row count is not matching";
			UpdateResult.UpdateStatus();
			int Rowcount = Integer.parseInt(TC.Param2);

			for (int i = 0; i <= Rowcount; i++) {

				for (int j = 0; j <= ReportExpected.getColumnCount(TC.Param1); j++) {
					if (i == 0 && j == 2) {
						TC.TestDescription = "Verify Celldata at Row : COl" + i
								+ ":" + j;
						TC.PassDescription = "Cell data is matching";
						TC.FailDescription = "Cell data is not matching";
						String DateReplace = new SimpleDateFormat("MM/dd/yyyy")
								.format(new Date());
						TC.ExpectedData = "Work Date :-" + DateReplace;
						TC.ExpectedData = TC.ExpectedData.replaceAll(" ", "");

						UpdateResult.ActualData = ReportActual.getCellData(
								TC.Param1, i, j).replaceAll(" ", "");
						UpdateResult.UpdateStatus();
					} else {

						TC.TestDescription = "Verify Celldata at Row : COl" + i
								+ ":" + j;
						TC.PassDescription = "Cell data is matching";
						TC.FailDescription = "Cell data is not matching";
						TC.ExpectedData = ReportExpected.getCellData(TC.Param1,
								i, j);
						UpdateResult.ActualData = ReportActual.getCellData(
								TC.Param1, i, j);
						UpdateResult.UpdateStatus();

					}
				}

			}

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// ################################################################################################################################

	public static String GetFilepathByPartialName() throws IOException,
			InterruptedException {
		try {
			Logs.Ulog("Start VerifyFileExistWithPartialName ");
			boolean FileExist = false;

			File folder = new File(TC.InputData);
			File[] listOfFiles = folder.listFiles();

			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					if (listOfFiles[i].getName().contains(TC.ExpectedData)) {
						FileExist = true;

						StoreTable.put("FilePath",
								listOfFiles[i].getAbsolutePath());

					}

				}
			}

			return (String) StoreTable.get("FilePath");

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyFileExistWithPartialName() throws IOException,
			InterruptedException {
		try {
			Logs.Ulog("Start VerifyFileExistWithPartialName ");
			boolean FileExist = false;

			File folder = new File(TC.InputData);
			File[] listOfFiles = folder.listFiles();

			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					if (listOfFiles[i].getName().contains(TC.ExpectedData)) {
						FileExist = true;
						listOfFiles[i].getAbsolutePath();

					}

				}
			}

			if (FileExist) {
				UpdateResult.ActualData = "File Downloaded";
				TC.ExpectedData = "File Downloaded";
				Logs.Ulog("Verifying the text VerifyFileExistWithPartialName Passed ");
				return UpdateResult.UpdateStatus();

			}

			else {
				UpdateResult.ActualData = "File not Downloaded";
				TC.ExpectedData = "File Downloaded";
				Logs.Ulog("Verifying the text VerifyFileExistWithPartialName Failed ");
				return UpdateResult.UpdateStatus();
			}
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String LoadObjectsByEnterKey_ClickRefObj()
			throws IOException, InterruptedException {

		Logs.Ulog("Verifying the LoadObjectByEnterKey ");
		try {

			int NumElements = Integer.parseInt(TC.InputData);

			getORObject(TC.ExpectedData);
			WebElement StartCard = TORObj();

			getORObject(TC.Param1);
			WebElement Yes = TORObj();

			List<WebElement> ObjElems = WebElementsTORObj();

			for (int i = 0; i < NumElements; i++) {
				ObjElems.get(i).sendKeys(Keys.ENTER);
				Sync();

				TORObj().click();

			}

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
		return RANDOMCONST;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String selectbyPartialName() throws IOException,
			InterruptedException {

		Logs.Ulog("selectbyPartialName from list");

		try {

			WebElement Ele = TORObj();

			Select droplist = new Select(Ele);

			for (int i = 0; i < droplist.getOptions().size(); i++) {
				String s1 = droplist.getOptions().get(i).getText();
				if (s1.matches(TC.InputData)) {

					droplist.getOptions().get(i).click();
					Thread.sleep(200L);
					UpdateResult.ActualData = droplist.getFirstSelectedOption()
							.getText();
					TC.ExpectedData = droplist.getFirstSelectedOption()
							.getText();
					return UpdateResult.UpdateStatus();
				}

			}

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			Logs.Ulog("Error ---  while Seleting list selectbyPartialName from list");
			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String PageScrollBottom() throws IOException,
			InterruptedException {

		Logs.Ulog("Start of PageScrollBottom");
		try {

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");

			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String NewFunction() throws IOException {
		return RANDOMCONST;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String SwitchToWindowandVerifyText() throws IOException,
			InterruptedException {

		Logs.Ulog("Switching to new window and verify text from pdf");
		String parent = null, newwin = null;
		try {

			String filename = TC.InputData;
			String data = "", currLine = "";
			BufferedReader br = new BufferedReader(new FileReader(
					System.getProperty("user.dir") + "\\src\\TestData\\"
							+ filename + ".txt"));
			while ((currLine = br.readLine()) != null) {
				data += currLine + " ";
			}
			data = data.replace('\r', ' ').replace('\n', ' ')
					.replaceAll("( )+", " ");
			Set<String> handles = driver.getWindowHandles();
			Iterator<String> it = handles.iterator();

			while (it.hasNext()) {
				parent = it.next();
				newwin = it.next();
			}

			driver.switchTo().window(newwin);
			URL url = new URL(driver.getCurrentUrl());
			// System.out.println(url);

			Cookie authorizationCookie = driver.manage().getCookieNamed(
					"JSESSIONID");
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("Cookie", authorizationCookie.toString());
			InputStream fileToParse = conn.getInputStream();
			PDFParser parser = new PDFParser((RandomAccessRead) fileToParse);
			parser.parse();

			String pdfdata = new PDFTextStripperByArea().getText(parser
					.getPDDocument());

			parser.getPDDocument().close();
			pdfdata = pdfdata.replace('\r', ' ').replace('\n', ' ')
					.replaceAll("( )+", " ");

			// System.out.println(pdfdata);
			// System.out.println("data is\n"+data);

			if (pdfdata.matches(data)) {
				UpdateResult.ActualData = TC.PASS;

			} else {
				UpdateResult.ActualData = TC.FAIL;

			}
			driver.close();
			TC.ExpectedData = TC.PASS;
			driver.switchTo().window(parent);

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			System.out.println("Throwable: " + e.getMessage());
			driver.close();
			driver.switchTo().window(parent);
			return CatchStatementWebElement(e.getMessage());
		}

	}

	public static String OverWheelSync() throws IOException,
			InterruptedException {
		Logs.Ulog("------ OverWheelSync------");

		ReSetSyncTime();
		try {

			int WT = (int) Double.parseDouble(TC.InputData);
			for (int i = 0; i <= WT; i++) {

				try {

					if (NonExceptionTORObj().isDisplayed()) {

						Logs.Ulog("------Object Displayed----  ");
						Thread.sleep(1000l);
					}

				} catch (Throwable T) {
					SetDefaultSyncTime();
					break;
				}

			}

			Logs.Ulog("Executed---  OverWheelSync   ");
			SetDefaultSyncTime();
			return TC.PASS;
		} catch (Throwable e) {
			SetDefaultSyncTime();
			Logs.Ulog("ERROR---  on OverWheelSync= " + e.getMessage());
			return TC.PASS;

		}
	}

	public static String VerifyContentNotEqual() throws IOException,
			InterruptedException {

		Logs.Ulog("Checking VerifyContentNotEqual of elements");
		Logs.Ulog("Checking VerifyContentNotEqual of elements"
				+ "ExpectedData = " + TC.ExpectedData + "Actual = "
				+ TC.InputData);
		try {

			if (!TC.InputData.equals(TC.ExpectedData)) {
				UpdateResult.ActualData = TC.InputData + " - Not Equal  - "
						+ TC.ExpectedData;
				TC.ExpectedData = TC.InputData + " - Not Equal  - "
						+ TC.ExpectedData;
				return UpdateResult.UpdateStatus();
			} else {
				UpdateResult.ActualData = TC.InputData + " - Equal - "
						+ TC.ExpectedData;
				TC.ExpectedData = TC.InputData + " - Should Not be Equal - "
						+ TC.ExpectedData;
				return UpdateResult.UpdateStatus();
			}

		} catch (Throwable e) {

			return CatchStatementWebElement(e.getMessage());
		}

	}

	// #######################################################################################
	public static String VerifyContentEqual() throws IOException,
			InterruptedException {

		Logs.Ulog("Checking VerifyContentEqual of elements");
		Logs.Ulog("Checking VerifyContentEqual of elements" + "ExpectedData = "
				+ TC.ExpectedData + "Actual = " + TC.InputData);
		try {

			if (TC.InputData.equals(TC.ExpectedData)) {
				UpdateResult.ActualData = TC.InputData + " - Equal  - "
						+ TC.ExpectedData;
				TC.ExpectedData = TC.InputData + " - Equal  - "
						+ TC.ExpectedData;
				return UpdateResult.UpdateStatus();
			} else {
				UpdateResult.ActualData = TC.InputData + " - Equal - "
						+ TC.ExpectedData;
				TC.ExpectedData = TC.InputData + " - Not Equal - "
						+ TC.ExpectedData;
				return UpdateResult.UpdateStatus();
			}

		} catch (Throwable e) {

			return CatchStatementWebElement(e.getMessage());
		}
	}

	// #######################################################################################

	public static String PressALT_A() throws IOException, InterruptedException {

		try {
			Logs.Ulog("Executing CTRL A function");

			RMouse.keyPress(KeyEvent.VK_ALT);
			RMouse.keyPress(KeyEvent.VK_A);
			Thread.sleep(1000L);
			RMouse.keyRelease(KeyEvent.VK_ALT);
			RMouse.keyRelease(KeyEvent.VK_A);
			Thread.sleep(200L);
			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	public static void Enter4digitrobo() throws IOException,
			InterruptedException {

		RMouse.keyPress(KeyEvent.VK_1);
		RMouse.keyPress(KeyEvent.VK_5);
		Thread.sleep(1000L);
		RMouse.keyRelease(KeyEvent.VK_0);
		RMouse.keyRelease(KeyEvent.VK_1);

	}

	public static void EnterCharA() throws IOException, InterruptedException {

		RMouse.keyPress(KeyEvent.VK_A);

	}

	public static String ClearText() throws IOException, InterruptedException {

		UpdateDescription(" Clear text in " + TC.TestObjects);

		try {
			Logs.Ulog("Executing Clear Text function");

			TORObj().clear();
			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String ClickByRefwithAttribute_scrollDown()
			throws IOException, InterruptedException {

		Logs.Ulog(" Start ClickByRefwithAttribute_scrollDown ");
		try {
			WebElement refWebEl = TORObj();

			ReSetSyncTime();

			// int ST = Integer.parseInt(refWebEl.getAttribute("scrollTopMax"));

			for (int i = 0; i < 50; i++) {
				// System.out.println(EleCollection.get(i).getAttribute("innerHTML"));

				try {

					getORObject(TC.InputData);
					// ST =
					// Integer.parseInt(refWebEl.getAttribute("scrollTopMax"));
					List<WebElement> EleCollection = WebElementsTORObj();

					getORObject(TC.ExpectedData);
					List<WebElement> EleCollectionRef = WebElementsTORObj();
					String found = "NO";
					for (int j = 0; j <= EleCollection.size(); j++) {
						if (EleCollection.get(j).getAttribute(TC.Param1)
								.equals(TC.Param2)) {
							found = "YES";

							try {
								WrapsDriver wrappedElement = (WrapsDriver) EleCollectionRef
										.get(j);
								JavascriptExecutor js = (JavascriptExecutor) driver;
								wrappedElement.getWrappedDriver();
								js.executeScript("arguments[0].click();",
										EleCollectionRef.get(j));
							} catch (Throwable e) {
								EleCollectionRef.get(j).click();
							}

							UpdateResult.ActualData = TC.PASS;
							TC.ExpectedData = TC.PASS;

							return UpdateResult.UpdateStatus();
						}
					}

				} catch (Throwable T) {
					refWebEl.sendKeys(Keys.PAGE_DOWN);

				}

			}
			// System.out.println("**********************************************************************");
			UpdateResult.ActualData = "Element Not Found";
			TC.ExpectedData = "";
			Logs.Ulog("Click element By SCrolling element has found -  Fail");

			SetDefaultSyncTime();
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			SetDefaultSyncTime();
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String IsGreaterOrEqual() throws IOException,
			InterruptedException {

		Logs.Ulog("Checking the in put is grater");
		try {

			if (Integer.parseInt(TC.ExpectedData) >= Integer
					.parseInt(TC.Param1)) {

				Logs.Ulog(TC.InputData + "Is greater than " + TC.ExpectedData);
				return "true";
			}

		} catch (Throwable e) {

			return CatchStatementWebElement(e.getMessage());
		}
		return "false";

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static void login() throws AWTException {
		Robot RMouse = new Robot();
		RMouse.mousePress(0);
		RMouse.keyPress(KeyEvent.VK_S);
		RMouse.keyPress(KeyEvent.VK_A);
		RMouse.keyPress(KeyEvent.VK_N);
		RMouse.keyPress(KeyEvent.VK_T);
		RMouse.keyPress(KeyEvent.VK_H);
		RMouse.keyPress(KeyEvent.VK_O);
		RMouse.keyPress(KeyEvent.VK_S);
		RMouse.keyPress(KeyEvent.VK_H);
		RMouse.keyPress(KeyEvent.VK_A);
		RMouse.keyPress(KeyEvent.VK_C);

		RMouse.keyPress(KeyEvent.VK_TAB);

		RMouse.keyPress(KeyEvent.VK_SHIFT);
		RMouse.keyPress(KeyEvent.VK_M);
		RMouse.keyRelease(KeyEvent.VK_SHIFT);
		RMouse.keyPress(KeyEvent.VK_A);
		RMouse.keyPress(KeyEvent.VK_R);
		RMouse.keyPress(KeyEvent.VK_U);
		RMouse.keyPress(KeyEvent.VK_T);
		RMouse.keyPress(KeyEvent.VK_H);
		RMouse.keyPress(KeyEvent.VK_I);
		RMouse.keyPress(KeyEvent.VK_SHIFT);
		RMouse.keyPress(KeyEvent.VK_2);
		RMouse.keyRelease(KeyEvent.VK_SHIFT);
		RMouse.keyPress(KeyEvent.VK_2);
		RMouse.keyPress(KeyEvent.VK_3);
		RMouse.keyPress(KeyEvent.VK_4);
		RMouse.keyPress(KeyEvent.VK_ENTER);

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyObjectNotHidden() throws IOException,
			InterruptedException {
		Logs.Ulog(" Verify VerifyObjectNotHidden");

		try {
			WebElement Obj = TORObj();

			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			int x = Obj.getLocation().x;
			int y = Obj.getLocation().y;

			if (x > 0 & y > 0) {
				UpdateResult.ActualData = "Elemet is displayed OR not hidden";
				TC.ExpectedData = "Elemet is displayed OR not hidden";
				return UpdateResult.UpdateStatus();
			} else {
				UpdateResult.ActualData = "Elemet Should not  displayed OR hidden";
				TC.ExpectedData = "Elemet is displayed OR not hidden";
				return UpdateResult.UpdateStatus();
			}

		} catch (Throwable e) {

			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyObjectHidden() throws IOException,
			InterruptedException {
		Logs.Ulog(" Verify VerifyObjectNotHidden");

		try {
			WebElement Obj = TORObj();

			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			int x = Obj.getLocation().x;
			int y = Obj.getLocation().y;

			if (x > 0 & y > 0) {
				UpdateResult.ActualData = "Elemet Should not  displayed OR hidden";
				TC.ExpectedData = "Elemet is displayed OR not hidden";
				return UpdateResult.UpdateStatus();
			} else {
				UpdateResult.ActualData = "Elemet is displayed OR not hidden";
				TC.ExpectedData = "Elemet is displayed OR not hidden";
				return UpdateResult.UpdateStatus();
			}

		} catch (Throwable e) {

			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyInnerTextContainRegEx() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the VerifyInnerTextContainregEx");
		try {
			WebElement Obj = TORObj();

			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			wrappedElement.getWrappedDriver();

			UpdateResult.ActualData = (String) js.executeScript(
					"return arguments[0].innerText", Obj);
			// System.out.println(UpdateResult.ActualData );

			if (Pattern.compile(TC.InputData).matcher(UpdateResult.ActualData)
					.find()) {
				TC.ExpectedData = UpdateResult.ActualData;
			} else {

				TC.ExpectedData = " Innertext not Contains --- " + TC.InputData;
			}

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String ClickOnObject1VerifyinnertextObject2()
			throws IOException, InterruptedException {
		// StoreTable.put("Fluent_Time" , 5);
		Logs.Ulog("Verifying Object Text");
		try {
			WebElement Obj1 = TORObj();

			ReSetSyncTime();
			for (int i = 0; i <= 5; i++) {

				try {

					// Menu.click();
					Thread.sleep(100L);

					Obj1.click();
					Thread.sleep(3000L);

					getORObject(TC.Param1);

					WebElement Obj2 = NonExceptionTORObj();
					if (Obj2.isDisplayed()) {
						VerifyInnertext();
						SetDefaultSyncTime();
						break;

					}

				} catch (Throwable T) {

				}

			}
			SetDefaultSyncTime();
			Logs.Ulog("Actual data  = " + UpdateResult.ActualData);

			return TC.PASS;
		} catch (Throwable e) {
			SetDefaultSyncTime();
			Logs.Ulog("ERROR---  on ClickOnObject1VerifyinnertextObject2 = "
					+ e.getMessage());
			return UpdateResult.UpdateStatus();

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String clickwd() throws IOException, InterruptedException {

		Logs.Ulog("Executing ClickJS ");
		UpdateDescription("Click on ");
		try {
			UpdateDescription("Click on  " + TC.TestObjects);
			TORObj().click();

			return UpdateResult.Done();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String StoreCurrentUrl() throws IOException,
			InterruptedException {
		try {
			Logs.Ulog("Store a Value StoreCurrentUrl");
			StoreTable.put(TC.InputData, driver.getCurrentUrl());
			Logs.Ulog("Store " + driver.getCurrentUrl() + " in " + TC.InputData);
			// System.out.println("Stored Variablename = " +
			// StoreTable.get(TC.TestObjects));
			TC.TestDescription = "Store " + driver.getCurrentUrl() + " in "
					+ TC.InputData;
			return UpdateResult.Done();
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While stor object " + T.getMessage());
			CatchStatementWebElement("ERROR -- While stor StoreCurrentUrl  "
					+ T.getMessage());
			return TC.FAIL;

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String ReplaceAndStoreString() throws IOException,
			InterruptedException {
		try {
			Logs.Ulog("ReplaceAndStoreString");

			StoreTable.put(TC.Param2,
					TC.InputData.replace(TC.ExpectedData, TC.Param1));

			Logs.Ulog("Store " + TC.InputData + " in " + TC.Param2);

			return UpdateResult.Done();
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While stor object " + T.getMessage());
			CatchStatementWebElement("ERROR -- While stor StoreCurrentUrl  "
					+ T.getMessage());
			return TC.FAIL;

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String ClickOnElements() throws IOException,
			InterruptedException {

		Logs.Ulog("Starting of ClickOnElements ");

		try

		{
			List<WebElement> EleCollection = WebElementsTORObj();

			for (int i = 0; i < EleCollection.size(); i++) {

				try {
					WrapsDriver wrappedElement = (WrapsDriver) EleCollection
							.get(i);
					JavascriptExecutor js = (JavascriptExecutor) driver;
					wrappedElement.getWrappedDriver();
					js.executeScript("arguments[0].click();",
							EleCollection.get(i));
				} catch (Throwable e) {
					EleCollection.get(i).click();
					;
				}

			}
			UpdateDescription("Click on ");
			return UpdateResult.Done();

		}

		catch (Throwable e)

		{

			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static void UpdateDescription(String TestDesc) {
		Logs.Ulog("Starting of UpdateDescription ");
		try {
			if (TC.TestDescription.isEmpty()) {

				String C[] = TC.TestObjects.split("\\)");
				// System.out.println(T[ T.length]);

				String Childclass = C[C.length - 1].replace("(", ">").replace(
						".", "");

				String Parentclass = C[0].replace("(", ">");

				TC.TestDescription = TestDesc + " " + Childclass + ">on "
						+ Parentclass + ".";

			}
		} catch (Throwable e)

		{

			Logs.Ulog("Error in UpdateDescription " + e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static void RemoveItemsFormBasket() throws IOException,
			InterruptedException {
		Logs.Ulog("Starting of RemoveItemsFormBasket ");
		// StoreTable.put("Fluent_Time" , 5);
		Logs.Ulog("RemoveItemsFormBasket");
		// String a =
		// driver.findElement(By.id("minishopcart_total")).getAttribute("textContent").toString().replaceAll("[^\\p{ASCII}]",
		// "").replaceAll(" ","");
		// int b = Integer.parseInt(a);
		if (!driver.findElement(By.id("minishopcart_total"))
				.getAttribute("innerText").replaceAll(" ", "").equals("0")) {
			try {

				try {
					try {
						GetTobjNonException(
								"Pg(Home).Header(Common).Link(Your Basket)")
								.click();
						Thread.sleep(2000L);
						GetTobjNonException(
								"Pg(Your Basket).Button(View Basket)").click();
					} catch (Throwable T) {

						driver.navigate().refresh();
						Thread.sleep(10000L);
						GetTobjNonException(
								"Pg(Home).Header(Common).Link(Your Basket)")
								.click();
						Thread.sleep(2000L);
						GetTobjNonException(
								"Pg(Your Basket).Button(View Basket)").click();
					}

					ReSetSyncTime();
					GetTobjNonException(
							"Pg(Your Basket).Element(Your shopping cart is empty)")
							.isDisplayed();

				} catch (Throwable T) {
					for (int i = 0; i <= 5; i++) {

						try {
							GetTobjNonException(
									"Pg(Your Basket).Element(Your shopping cart is empty)")
									.isDisplayed();
							SetDefaultSyncTime();
							return;
						}

						catch (Throwable T1) {

							WebElement Obj = GetTobjNonException("Pg(Your Basket).Button(Remove Product)");

							try {
								WrapsDriver wrappedElement = (WrapsDriver) Obj;
								JavascriptExecutor js = (JavascriptExecutor) driver;
								wrappedElement.getWrappedDriver();
								js.executeScript("arguments[0].click();", Obj);
							} catch (Throwable e) {
								Obj.click();
							}
							Thread.sleep(2000L);
							Obj = GetTobjNonException("Pg(Your Basket)Element(Are you sure you want to).Button(Yes, Remove)");

							try {
								WrapsDriver wrappedElement = (WrapsDriver) Obj;
								JavascriptExecutor js = (JavascriptExecutor) driver;
								wrappedElement.getWrappedDriver();
								js.executeScript("arguments[0].click();", Obj);
							} catch (Throwable e) {
								Obj.click();
							}
							Thread.sleep(7000L);
						}

					}
				}

			} catch (Throwable e) {
				SetDefaultSyncTime();
				Logs.Ulog("ERROR---  on RemoveItemsFormBasket = "
						+ e.getMessage());
			}
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static WebElement GetTobj(String TO) throws IOException,
			InterruptedException {
		Logs.Ulog("Starting of GetORObject ");
		try {
			getORObject(TO);
			return TORObj();
		} catch (Throwable e) {

			Logs.Ulog("ERROR---  on ClickOnObject1VerifyinnertextObject2 = "
					+ e.getMessage());
			UpdateResult.UpdateStatus();

		}
		return null;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static WebElement GetTobjNonException(String TO) throws IOException,
			InterruptedException {
		Logs.Ulog("Starting of GetORObject ");
		try {
			getORObject(TO);
			return NonExceptionTORObj();
		} catch (Throwable e) {

			Logs.Ulog("ERROR---  on ClickOnObject1VerifyinnertextObject2 = "
					+ e.getMessage());
			UpdateResult.UpdateStatus();

		}
		return null;

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String CreateGoogleAccount() throws IOException,
			InterruptedException {

		driver.findElement(By.xpath("//*[@id='link-signup']/a")).click();

		try {
			driver.findElement(By.id("gmail-create-account")).click();
		} catch (Throwable t) {

		}

		try {
			driver.findElement(By.id("FirstName")).sendKeys(TC.Param1);
		} catch (Throwable t) {

		}
		try {
			driver.findElement(By.id("LastName")).sendKeys(TC.Param2);
		} catch (Throwable t) {

		}

		driver.findElement(By.id("Passwd")).sendKeys("afg@1234");
		driver.findElement(By.id("PasswdAgain")).sendKeys("afg@1234");

		driver.findElement(By.id("HiddenBirthMonth")).click();
		driver.findElement(By.id("//*[@id=':0']")).clear();

		driver.findElement(By.id("BirthDay")).sendKeys("1");

		driver.findElement(By.id("BirthYear")).sendKeys("1990");

		driver.findElement(By.id("HiddenGender")).click();

		driver.findElement(By.id("RecoveryEmailAddress")).sendKeys(
				"afgwebautotest@Gmail.com");

		driver.findElement(By.id("SkipCaptcha")).click();
		driver.findElement(By.id("TermsOfService")).click();
		driver.findElement(By.id("submitbutton")).click();
		driver.findElement(By.cssSelector("#expertContent > button")).click();
		return TC.PASS;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	@SuppressWarnings("rawtypes")
	public static String SwitchContext() throws IOException,
			InterruptedException {
		try {
			((AndroidDriver) driver).context(TC.InputData);

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
		return TC.PASS;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// / #######################################################################

	public static String KeyBordkeyPress() throws IOException,
			InterruptedException {
		Logs.Ulog("KeyBordkeyPress");
		try {
			int K = Integer.parseInt(TC.InputData);
			((AndroidDriver) driver).pressKeyCode(K);

			return TC.PASS;
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());

		}
	}

	// / #######################################################################

	public static String InputTextActiveElemnet() throws IOException,
			InterruptedException {
		Logs.Ulog("InputTextActiveElemnet");
		try {
			driver.switchTo().activeElement().clear();
			driver.switchTo().activeElement().sendKeys(TC.InputData);
			;

			UpdateResult.ActualData = TC.InputData;
			TC.ExpectedData = TC.InputData;
			Logs.Ulog("InputText on obj Done ");

			UpdateDescription("Enter text  " + TC.ExpectedData + " in ");
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());

		}
	}

	// / #######################################################################

	public static void Break() throws IOException, InterruptedException {
		try {
			System.out.println("Break the test");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			/*
			 * driver.switchTo().frame(0);
			 * driver.findElement(By.xpath("//h3[text()='Provision VM']"
			 * )).getText(); driver.findElement(By.xpath(
			 * "//*[@id='selfservicepagetitle']/div/a[2]/span")).getText();
			 * getORObject
			 * ("Browser(ICO).Page(Provision VM).List(\"Request Type\")");
			 * TC.InputData = "0"; selectbyIndex();
			 */

			/*
			 * driver.findElement(By.xpath(
			 * "(//span[text()='Comment Editor']/following::Button)[1]"
			 * )).click();
			 * 
			 * driver.findElement(By.xpath(
			 * "(//span[text()='Comment Editor']/following::textarea)[1]"
			 * )).sendKeys("GtafAutomation Please ignore");
			 * 
			 * 
			 * TC.FindByPropVal =
			 * "(//span[text()='Comment Chooser']/following::div[@class='x-grid3-cell-inner x-grid3-col-0'])[1]"
			 * ;
			 */

			// driver.findElement(By.xpath("(//button[text()='Preview'])[last()]")).click();
			/*
			 * driver.findElement(By.xpath(
			 * "(//button[text()='Preview'])[1]//..//..//.")).click();
			 * driver.findElement
			 * (By.xpath("(//button[text()='Finalize Quote Option'])[1]"
			 * )).click();;
			 * 
			 * driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			 * driver.switchTo().frame("cq-cf-frame"); // div[text()='Header
			 * Component']/../../td//button
			 * 
			 * driver.findElement( By.xpath(
			 * "(//div[text()='Footer Component']/following::button[contains(text(),'Edit')])[1]"
			 * )) .click(); // div[text()='Header Component']
			 * System.out.println(driver .findElement(By .xpath(
			 * "//*[@class='conifure-text-cq' and contains(text(),'Configure Site Selection Component')]"
			 * )) .getText()); // div[contains(text(),'Configure Site Selection
			 * Component')] RoboMouseRtClickOnObj();
			 */
		}

		catch (Throwable T) {
			System.out.println(T.getMessage());
		}

		System.out.println("Break the test");
	}

	public static String SwitchToBrowser_Frame() throws IOException,
			InterruptedException {

		Logs.Ulog("Successfully Switched to new frame");
		try {

			driver.switchTo().frame(TC.InputData);
			Logs.Ulog("Successfully Switched to new frame");

			return UpdateResult.Done();
		}

		catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			UpdateResult.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String SwitchToBrowser_DefaltContent() throws IOException,
			InterruptedException {

		Logs.Ulog("Successfully Switched to defaultContent");
		try {

			driver.switchTo().defaultContent();

			Logs.Ulog("Successfully Switched to defaultContent");
			return UpdateResult.Done();
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyTextFromList() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the text from list of elements");
		try {
			// Get the common Locator
			List<WebElement> EleCollection = WebElementsTORObj();

			// extract the expected values excel file
			String temp = TC.InputData;
			String allElementText[] = temp.split(",");
			// check if size of array == size if list
			if (allElementText.length != EleCollection.size()) {
				Logs.Ulog("size of lists do not match");
				UpdateResult.ActualData = Integer
						.toString(allElementText.length);
				TC.ExpectedData = Integer.toString(EleCollection.size());

				return UpdateResult.UpdateStatus();
			}

			for (int i = 0; i < EleCollection.size(); i++) {
				if (!allElementText[i].trim().equalsIgnoreCase(
						EleCollection.get(i).getText().trim())) {
					UpdateResult.ActualData = "- Element not found - "
							+ allElementText[i].trim();
					TC.ExpectedData = EleCollection.get(i).getText();

					Logs.Ulog("- Text/Element not found -");
					return UpdateResult.UpdateStatus();
				} else {
					UpdateResult.ActualData = allElementText[i];
					TC.ExpectedData = EleCollection.get(i).getText();

					Logs.Ulog("- Element found -");
					return UpdateResult.UpdateStatus();
				}

			}
		} catch (Throwable e) {
			Logs.Ulog(" - Could not select from list. " + e.getMessage());
			return CatchStatementWebElement(e.getMessage());

		}

		return TC.PASS;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String SendKeySpace() throws IOException,
			InterruptedException {

		try {
			Logs.Ulog("Executing SendKeySpace");

			// TORObj().sendKeys(Keys.TAB);
			RMouse.mousePress(0);
			RMouse.keyPress(KeyEvent.VK_SPACE);

			Thread.sleep(200L);
			RMouse.keyRelease(KeyEvent.VK_SPACE);

			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// public static String VerifyTextFromMasterList() throws IOException,
	// InterruptedException {
	//
	// Logs.Ulog("Verifying the text from list of elements with Master List");
	// try {
	// // Get the elements from common Locator in object column
	// List<WebElement> EleCollection = WebElementsTORObj();
	//
	// // extract the expected values excel file
	// String temp = TC.InputData;
	// String empElementText[] = temp.split(",");
	//
	// List<String> actualTextList = new ArrayList<String>();
	// for (int i = 0; i < EleCollection.size(); i++) {
	//
	// if(!(EleCollection.get(i).getText().trim().isEmpty()) &&
	// !(EleCollection.get(i).getText().trim().equalsIgnoreCase(""))){
	// //System.out.println(EleCollection.get(i).getText().trim());
	// actualTextList.add(EleCollection.get(i).getText().trim());
	// }
	// }
	// // check if size of actialtext array is nulll
	// if (actualTextList.size() != 0) {
	// for(String expItem : empElementText){
	// if(actualTextList.contains(expItem)){
	// Logs.Ulog("- Text/Element not found - : " + expItem);
	// UpdateResult.ActualData = expItem;
	// TC.ExpectedData = expItem;
	// UpdateResult.UpdateStatus();
	// }else{
	// Logs.Ulog("- Text/Element not found - : " + expItem);
	// TC.ExpectedData = "- Element not found - " + expItem;
	// UpdateResult.UpdateStatus();
	// }
	// }
	//
	// }else{
	// Logs.Ulog("size of actualTextList is zero");
	// }
	//
	// return TC.PASS;
	//
	// } catch (Throwable e) {
	// Logs.Ulog(" - Could not select from list. " + e.getMessage());
	// return CatchStatementWebElement(e.getMessage());
	//
	// }
	//
	//
	// }

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyTextFromMasterList() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the text from list of elements with Master List");
		try {
			// Get the elements from common Locator in object column
			List<WebElement> EleCollection = WebElementsTORObj();

			// extract the expected values excel file
			String temp = TC.InputData;
			String empElementText[] = temp.split(",");

			List<String> actualTextList = new ArrayList<String>();
			for (int i = 0; i < EleCollection.size(); i++) {

				if (!(EleCollection.get(i).getText().trim().isEmpty())
						&& !(EleCollection.get(i).getText().trim()
								.equalsIgnoreCase(""))) {
					actualTextList.add(EleCollection.get(i).getText().trim());
				}
			}
			// check if size of actialtext array is nulll
			if (actualTextList.size() != 0) {
				for (String expItem : empElementText) {
					if (actualTextList.contains(expItem)) {
						Logs.Ulog("- Text/Element not found - : " + expItem);
						UpdateResult.ActualData = expItem;
						TC.ExpectedData = expItem;
						UpdateResult.UpdateStatus();

					} else {
						Logs.Ulog("- Text/Element not found - : " + expItem);
						TC.ExpectedData = "- Element not found - " + expItem;
						UpdateResult.UpdateStatus();
					}
				}

			} else {
				Logs.Ulog("size of actualTextList is zero");
			}

			return TC.PASS;

		} catch (Throwable e) {
			Logs.Ulog(" - Could not select from list. " + e.getMessage());
			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String SwitchToFrameByElement() throws IOException,
			InterruptedException {
		Logs.Ulog("Switched SwitchToFrameByElement");
		try {
			WebElement element = TORObj();
			driver.switchTo().frame(element);
			Logs.Ulog("Successfully Switched ToFrameByElement");
			return UpdateResult.Done();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// ////////////////////////////// USER FUNCTIONS
	// ////////////////////////////// ////////////////////////////////////////

	public static String contextClick() throws IOException,
			InterruptedException {
		Logs.Ulog("Successfully Switched to context item");
		try {
			WebElement element = TORObj();

			// WebElement element =
			// driver.findElement(By.xpath("//table[@id='mapping-table']/tbody/tr[2]/td[text()='/content/qaaig/r5c5b001/us']"));
			Actions action = new Actions(driver);
			action.contextClick(element).build().perform();
			return UpdateResult.Done();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String SendKeyENTER() throws IOException,
			InterruptedException {

		try {
			Logs.Ulog("Executing SendKeyEnter");

			TORObj().sendKeys(Keys.ENTER);

			Thread.sleep(200L);
			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return TC.PASS; // CatchStatementWebElement(t.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	/*
	 * public void testJavaAutoIT() throws InterruptedException {
	 * System.setProperty("webdriver.chrome.driver",
	 * 
	 * loadJacobLibrary();
	 * 
	 * ChromeDriver driver = new ChromeDriver();
	 * driver.get("http://cgi-lib.berkeley.edu/ex/fup.html");
	 * Thread.sleep(20000);
	 * driver.findElement(By.xpath("/html/body/form/input")).click();
	 * Thread.sleep(3000);
	 * 
	 * // AutoItX atIt = new AutoItX(); AutoItX atIt = new AutoItX();
	 * atIt.winActivate("Open"); atIt.winWaitActive("Open");
	 * 
	 * atIt.ControlSetText("Open", "", "1148",
	 * "C:/Users/VRai/workspace/JarSelenium_2.40/jacob.jar");
	 * Thread.sleep(5000); atIt.controlClick("Open", "", "1"); //
	 * Thread.sleep(5000); // atIt.controlClick("Open", "", "2");
	 * Thread.sleep(5000); driver.quit(); }
	 */

	/*
	 * public static void loadJacobLibrary() { String jvmBitVersion =
	 * System.getProperty("sun.arch.data.model"); System.out.println(
	 * "JVM Bit Version : " + jvmBitVersion); if (jvmBitVersion.contains("32"))
	 * { System.setProperty("jacob.dll.path",
	 * "C:/Users/VRai/workspace/JarSelenium_2.40/jacob-1.18-x86.dll"); } else {
	 * System.setProperty("jacob.dll.path",
	 * "C:/Users/VRai/workspace/JarSelenium_2.40/jacob-1.18-x64.dll"); }
	 * LibraryLoader.loadJacobLibrary(); }
	 */
	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String SwitchToBrowser_Frame_Number() throws IOException,
			InterruptedException {

		Logs.Ulog("Successfully Switched to new frame");
		try {

			List<WebElement> frames = driver.findElementsByTagName("iframe");
			for (WebElement frame : frames) {
				// System.out.println("Frame: " + frame.getAttribute("id"));
				// System.out.println("Frame: " + frame.getAttribute("name"));
				// System.out.println("Frame Src : " +
				// frame.getAttribute("src"));

				if (frame.getAttribute("src").contains(TC.InputData)) {
					driver.switchTo().frame(frame);
					break;
				}
			}
			// / libs/cq/ui/resources/0.html
			// / libs/cq/ui/resources/0.html

			// int frameNum=Integer.parseInt(TC.InputData);
			// driver.switchTo().frame(frameNum);
			Logs.Ulog("Successfully Switched to new frame");

			return UpdateResult.Done();
		}

		catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			UpdateResult.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String getCurrentURL() throws IOException,
			InterruptedException {
		Logs.Ulog("Get the current URL");
		try {
			TC.InputData.contains(driver.getCurrentUrl());
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String DbClickSel() throws IOException, InterruptedException {

		Logs.Ulog("Executing DbClickJs ");
		try {
			WebElement Obj = TORObj();

			TS_Obj().doubleClick(TC.FindByPropVal);

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// public static String verifyIfButtonClickable() throws IOException,
	// InterruptedException {
	// try {
	// Logs.Ulog("Executing verifyAlert");
	//
	// UpdateResult.ActualData = TS_Obj().is;
	// TC.ExpectedData = TC.InputData;
	// return UpdateResult.UpdateStatus();
	//
	// } catch (Throwable T) {
	// Logs.Ulog("ERROR -- While verifyAlert" + T.getMessage());
	// CatchStatementWebElement("ERROR -- While storEval object "
	// + T.getMessage());
	// return TC.FAIL;
	//
	// }
	//
	// }
	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyTextContent() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the VerifyTextContent");
		try {
			WebElement Obj = TORObj();

			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			wrappedElement.getWrappedDriver();

			UpdateResult.ActualData = (String) js.executeScript(
					"return arguments[0].textContent", Obj);

			if (UpdateResult.ActualData.contains(TC.InputData)) {
				TC.ExpectedData = UpdateResult.ActualData;
			} else {

				TC.ExpectedData = " textContent not Contains --- "
						+ TC.InputData;
			}

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyTextAlignmentCSS() throws IOException,
			InterruptedException {
		Logs.Ulog("Start --  VerifyTextAlignmentCSS");
		try {
			WebElement Obj = TORObj();

			UpdateResult.ActualData = Obj.getCssValue("text-align")
					.toLowerCase();
			TC.ExpectedData = TC.InputData.toLowerCase();
			UpdateResult.UpdateStatus();

			return TC.PASS;
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());

		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyTextbackgroundColorCSS() throws IOException,
			InterruptedException {
		Logs.Ulog("Start --  VerifyTextbackgroundColorCSS");
		try {
			WebElement Obj = TORObj();

			UpdateResult.ActualData = Obj.getCssValue("background-color")
					.toLowerCase();
			TC.ExpectedData = TC.InputData.toLowerCase();
			UpdateResult.UpdateStatus();

			return TC.PASS;
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());

		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyTextForegroundColorCSS() throws IOException,
			InterruptedException {
		Logs.Ulog("Start --  VerifyTextbackgroundColorCSS");
		try {
			WebElement Obj = TORObj();

			UpdateResult.ActualData = Obj.getCssValue("foreground-color")
					.toLowerCase();
			TC.ExpectedData = TC.InputData.toLowerCase();
			UpdateResult.UpdateStatus();

			return TC.PASS;
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());

		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// public static String VerifyBackgroundColor() throws IOException,
	// InterruptedException {
	//
	// Logs.Ulog("Verifying the VerifyBackgroundColor");
	// try {
	// WebElement Obj = TORObj();
	// //rgb =
	// findelement_by_class_name("bar").value_of_css_property('background-color')
	// WebElement
	// rgb=driver.findElement(By.className(TC.Param1)).getCssValue("background-color");
	// String style = "background-color: rgb(221, 81, 76); background-image:
	// -moz-linear-gradient(center top , rgb(238, 95, 91), rgb(196, 60, 53));
	// background-repeat: repeat-x; color: rgb(255, 255, 255); width: 11.5%;'"
	//
	// r,g,b = map(int, re.search(r"background-color:
	// rgb\((\d+),\s*(\d+),\s*(\d+)\)", style).groups());
	// //print('{:X}{:X}{:X}'.format(r, g, b))
	//
	// } catch (Throwable e) {
	// return CatchStatementWebElement(e.getMessage());
	// }
	//

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String ViewElement() throws IOException, InterruptedException {
		Logs.Ulog("Get Object GetPostionWebEleBefore");
		try {
			WebElement element = TORObj();
			// System.out.println(Obj.getText());
			// HighlightElement();
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView(true);", element);
			Thread.sleep(3000L);
			// element.click();

			return TC.PASS;
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// #################################################################################################################
	public static String selectbyIndex_LastIndex() throws IOException,
			InterruptedException {

		Logs.Ulog("Selecting from list");

		try {

			WebElement Ele = TORObj();

			Select droplist = new Select(Ele);

			for (int i = 0; i < droplist.getOptions().size(); i++) {
				// String s1 = droplist.getOptions().get(i).getText();
				int sl = droplist.getOptions().size() - 2;
				if (sl == i) {
					droplist.getOptions().get(i).click();
					Thread.sleep(200L);

					UpdateResult.ActualData = droplist.getFirstSelectedOption()
							.getText();
					TC.ExpectedData = droplist.getFirstSelectedOption()
							.getText();
					// Sync();
					return UpdateResult.UpdateStatus();
				}
			}

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			Logs.Ulog("Error ---  while Seleting list Selecting from list");
			return CatchStatementWebElement(e.getMessage());

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String HideKeyBoard() throws IOException,
			InterruptedException {

		Logs.Ulog("HideKeyBoard");
		// UpdateDescription( "Click on " );
		try {
			if (StoreTable.get("Execute ON Browser")
					.equals("PerfectoIOSDevice"))
				((IOSDriver) driver).hideKeyboard();

			if (StoreTable.get("Execute ON Browser").equals(
					"PerfectoAndroidDevice")
					|| StoreTable.get("Execute ON Browser").equals(
							"AndroidEmulator")
					|| StoreTable.get("Execute ON Browser").equals(
							"AndroidLocalDevice"))
				((AndroidDriver) driver).hideKeyboard();

			return UpdateResult.Done();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifytextContentContains() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the VerifytextContentContains");
		try {
			WebElement Obj = TORObj();

			WrapsDriver wrappedElement = (WrapsDriver) Obj;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			wrappedElement.getWrappedDriver();

			UpdateResult.ActualData = (String) js.executeScript(
					"return arguments[0].textContent", Obj);
			// System.out.println(UpdateResult.ActualData );

			if (UpdateResult.ActualData.contains(TC.InputData)) {
				TC.ExpectedData = UpdateResult.ActualData;
			} else {

				TC.ExpectedData = " TextContent not Contains --- "
						+ TC.InputData;
			}

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String StoreSetup_Browser_device() throws IOException,
			InterruptedException {
		try {
			Logs.Ulog("start UpdateSetupBrowser_device ");

			StoreTable.put("Previous_Browser_Device",
					StoreTable.get("Execute ON Browser"));
			StoreTable.get("Execute ON Browser");

			return UpdateResult.Done();
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While start UpdateSetupBrowser_device  "
					+ T.getMessage());
			CatchStatementWebElement("ERROR -- While start UpdateSetupBrowser_device "
					+ T.getMessage());
			return TC.FAIL;

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String ResetSetup_Browser_device() throws IOException,
			InterruptedException {
		try {
			Logs.Ulog("start UpdateSetupBrowser_device ");

			StoreTable.put("Execute ON Browser",
					StoreTable.get("Previous_Browser_Device"));

			return UpdateResult.Done();
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While start UpdateSetupBrowser_device  "
					+ T.getMessage());
			CatchStatementWebElement("ERROR -- While start UpdateSetupBrowser_device "
					+ T.getMessage());
			return TC.FAIL;

		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String PerfectoBrowser() throws IOException,
			InterruptedException {
		try {
			driver = null;
			String browserName = "mobileOS";
			DesiredCapabilities capabilities = new DesiredCapabilities(
					browserName, "", Platform.ANY);
			capabilities.setCapability("deviceName",
					StoreTable.get("PF_AndroiddeviceName"));
			capabilities.setCapability("user", StoreTable.get("PF_user"));
			capabilities.setCapability("password",
					StoreTable.get("PF_password"));

			capabilities.setCapability("resetKeyboard",
					StoreTable.get("resetKeyboard"));

			String host = (String) StoreTable.get("PF_host");
			// driver = new AndroidDriver( new URL(StoreTable.get("App_host")),
			// capabilities);
			driver = new RemoteWebDriver(
					new URL("https://" + host + "/wd/hub"), capabilities);

			Thread.sleep(20000);

			driver.manage().deleteAllCookies();

			implicitWaitTime = (int) Double.parseDouble((String) StoreTable
					.get("Browser time out"));
			// driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
			driver.manage().timeouts()
					.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
			return UpdateResult.Done();
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While start UpdateSetupBrowser_device  "
					+ T.getMessage());
			CatchStatementWebElement("ERROR -- While start UpdateSetupBrowser_device "
					+ T.getMessage());
			return TC.FAIL;

		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String PerfectoiOSBrowser() throws IOException,
			InterruptedException {
		try {
			driver = null;
			String browserName = "mobileOS";
			DesiredCapabilities capabilities = new DesiredCapabilities(
					browserName, "", Platform.ANY);
			capabilities.setCapability("deviceName",
					StoreTable.get("PF_IOSdeviceName"));
			capabilities.setCapability("user", StoreTable.get("PF_user"));
			capabilities.setCapability("password",
					StoreTable.get("PF_password"));

			capabilities.setCapability("resetKeyboard",
					StoreTable.get("resetKeyboard"));

			String host = (String) StoreTable.get("PF_host");
			// driver = new AndroidDriver( new URL(StoreTable.get("App_host")),
			// capabilities);
			driver = new RemoteWebDriver(
					new URL("https://" + host + "/wd/hub"), capabilities);

			Thread.sleep(20000);

			// driver.manage().deleteAllCookies();

			implicitWaitTime = (int) Double.parseDouble((String) StoreTable
					.get("Browser time out"));
			// driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
			driver.manage().timeouts()
					.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
			return UpdateResult.Done();
		} catch (Throwable T) {
			Logs.Ulog("ERROR -- While start UpdateSetupBrowser_device  "
					+ T.getMessage());
			CatchStatementWebElement("ERROR -- While start UpdateSetupBrowser_device "
					+ T.getMessage());
			return TC.FAIL;

		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String GetTestDataURL() throws IOException,
			InterruptedException {
		Logs.Ulog("Satrt GetTestDataURL");
		try {

			ExcelObj Testdata = new ExcelObj(System.getProperty("user.dir")
					+ "\\src\\TestData\\TestData.xlsm");
			int RN = Integer.parseInt(TC.InputData);

			driver.navigate()
					.to(Testdata.getCellData("DMPURL", "PublishingInstanceURL",
							RN));

			Logs.Ulog("Successfully GetTestDataURL");
			return UpdateResult.Done();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String GetTestData() throws IOException, InterruptedException {
		Logs.Ulog("Satrt GetTestData");
		try {

			File file = new File(System.getProperty("user.dir"));

			ExcelObj Testdata = new ExcelObj(TC.TestDataPath + "/TestData.xlsm");
			int RN = Integer.parseInt(TC.InputData);
			String CN = TC.ExpectedData;
			StoreTable.put(TC.Param1,
					Testdata.getCellData(TC.TestObjects, CN, RN));
			StoreTable.get(TC.Param1);

			Logs.Ulog("Successfully end GetTestData  = "
					+ StoreTable.get(TC.Param1));
			return UpdateResult.Done();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String SetTestData() throws IOException, InterruptedException {
		Logs.Ulog("Satrt GetTestData");
		try {

			File file = new File(System.getProperty("user.dir"));

			ExcelObj Testdata = new ExcelObj(TC.TestDataPath + "/TestData.xlsm");
			int RN = Integer.parseInt(TC.Param1);
			String CN = TC.ExpectedData;
			Testdata.setCellData(TC.TestObjects, CN, RN, TC.InputData);

			Logs.Ulog("Successfully end GetTestData");
			return UpdateResult.Done();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String UpdateTestDataURL() throws IOException,
			InterruptedException {
		Logs.Ulog("Satrt UpdateTestData");
		try {

			ExcelObj Testdata = new ExcelObj(System.getProperty("user.dir")
					+ "\\src\\TestData\\TestData.xlsm");
			int RN = Integer.parseInt(TC.InputData);
			Testdata.setCellData("DMPURL", "PublishingInstanceURL", RN,
					TC.ExpectedData + TC.Param1 + TC.Param2);

			Logs.Ulog("Successfully UpdateTestData");
			return UpdateResult.Done();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String WaitForElementVisibility() throws InterruptedException {

		try {
			Logs.Ulog("Waiting For Element Visibility " + TC.InputData);
			WebElement Obj = TORObj();
			long waitTime = Integer.parseInt(TC.InputData);

			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			wait.until(ExpectedConditions.visibilityOf(Obj));

			UpdateDescription("Waiting For Element Visibility  " + TC.InputData
					+ " Sec to Sync  ");
			return UpdateResult.Done();
		} catch (Throwable e) {
			Logs.Ulog("Error while Waiting For Element Visibility  "
					+ TC.InputData + " " + e.getMessage());
			return TC.FAIL;
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String waitForElementClickable() throws InterruptedException {

		try {
			Logs.Ulog("Waiting For Element To be Clickable " + TC.InputData);
			WebElement Obj = TORObj();
			long waitTime = Integer.parseInt(TC.InputData);

			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			wait.until(ExpectedConditions.elementToBeClickable(Obj));

			UpdateDescription("Waiting For Element To be Clickable  "
					+ TC.InputData + " Sec to Sync  ");
			return UpdateResult.Done();
		} catch (Throwable e) {
			Logs.Ulog("Error while waiting For Element To be Clickable "
					+ TC.InputData + " " + e.getMessage());
			return TC.FAIL;
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String waitForElementSelected() throws InterruptedException {

		try {
			Logs.Ulog("Waiting For Element To be Selected " + TC.InputData);
			WebElement Obj = TORObj();
			long waitTime = Integer.parseInt(TC.InputData);

			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			wait.until(ExpectedConditions.elementToBeSelected(Obj));

			UpdateDescription("Waiting For Element To be Selected  "
					+ TC.InputData + " Sec to Sync  ");
			return UpdateResult.Done();
		} catch (Throwable e) {
			Logs.Ulog("Error while Waiting For Element To be Selected "
					+ TC.InputData + " " + e.getMessage());
			return TC.FAIL;
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String waitForAlert() throws InterruptedException {

		try {
			Logs.Ulog("Waiting For Alert " + TC.InputData);
			long waitTime = Integer.parseInt(TC.InputData);

			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			wait.until(ExpectedConditions.alertIsPresent());

			UpdateDescription("Waiting For Alert  " + TC.InputData
					+ " Sec to Sync  ");
			return UpdateResult.Done();
		} catch (Throwable e) {
			Logs.Ulog("Error while Waiting For Alert " + TC.InputData + " "
					+ e.getMessage());
			return TC.FAIL;
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String waitForTextPresent() throws InterruptedException {

		try {
			Logs.Ulog("Waiting For Text Present in Element" + TC.InputData);
			WebElement Obj = TORObj();
			long waitTime = Integer.parseInt(TC.InputData);
			String text = TC.ExpectedData;

			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			wait.until(ExpectedConditions.textToBePresentInElement(Obj, text));

			UpdateDescription("Waiting For Text Present in Element  "
					+ TC.InputData + " Sec to Sync  ");
			return UpdateResult.Done();
		} catch (Throwable e) {
			Logs.Ulog("Error while Waiting For Text Present in Element "
					+ TC.InputData + " " + e.getMessage());
			return TC.FAIL;
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String getFormattedDate() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy-h_mm_ss a");
		String formattedDate = sdf.format(date);
		formattedDate = formattedDate.replace("-", "").replace(" ", "");

		return formattedDate;
	}

	/**
	 * Convert the array to String with the given delimiter
	 * 
	 * @param values
	 * @param delimiter
	 * @return
	 * @throws SQLException
	 * @throws java.text.ParseException
	 */
	public static String getArrayAsString(String[] values, String delimiter)
			throws SQLException, java.text.ParseException {
		String arrStr = "";

		for (int i = 0; i < values.length; i++) {
			if (i + 1 == values.length) {
				arrStr += values[i].toString();
			} else {
				arrStr += values[i].toString() + delimiter;
			}
		}
		return arrStr;
	}

	/**
	 * Returns the String array, from a string by splitting with a separator
	 * ";~"
	 * 
	 * @param values
	 * @return String[]
	 * @throws SQLException
	 * @throws java.text.ParseException
	 */
	public static String[] getStringAsArray(String values) throws SQLException,
			java.text.ParseException {
		String newValue = "";
		if (values.contains("\n")) {
			newValue = values.replaceAll("\n", "");
		} else {
			newValue = values;
		}
		String[] arrStr = newValue.split(";;");

		return arrStr;
	}

	/**
	 * Splits the given array & add them into the HashMap
	 * 
	 * @param arr
	 * @param splitter
	 * @return HashMap
	 */

	public static HashMap<String, String> getMapFromArray(String[] arr,
			String splitter) {
		HashMap<String, String> map = new HashMap<>();

		for (int i = 0; i < arr.length; i++) {
			String str = arr[i].toString();
			if (str.contains(":")) {
				int st = str.indexOf(splitter);
				String K = str.substring(0, st);
				String V = str.substring(st + 1);
				map.put(K, V);
			}

		}
		return map;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static void setSystemProxy(String proxyhost, String proxyPort) {

		Properties systemProperties = System.getProperties();
		systemProperties.setProperty("http.proxyHost", proxyhost);
		systemProperties.setProperty("http.proxyPort", proxyPort);
		systemProperties.setProperty("https.proxyHost", proxyhost);
		systemProperties.setProperty("https.proxyPort", proxyPort);
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String StartTransation() throws IOException,
			InterruptedException {
		Logs.Ulog("StartTransation");
		try {

			UpdateResult.ActualData = "";
			TC.ExpectedData = "";
			// start timer
			nanoStart = String.valueOf(System.nanoTime());
			milliStart = String.valueOf(System.currentTimeMillis());
			TC.TestDescription = "Start Response time <<<>>> Start Time "
					+ new Date();
			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String EndTransation() throws IOException,
			InterruptedException {
		try {
			Logs.Ulog("EndTransation");
			// stop timer
			nanoEnd = String.valueOf(System.nanoTime());
			milliEnd = String.valueOf(System.currentTimeMillis());
			milliEnd = String.valueOf(System.currentTimeMillis());
			TC.TestDescription = "End Response time <<<>>> End Time "
					+ new Date();
			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String TotalResponseTime() throws IOException,
			InterruptedException {
		try {
			Logs.Ulog("TotalResponseTime");
			// stop timer

			milliTime = Long.parseLong(milliEnd) - Long.parseLong(milliStart);
			nanoTime = Long.parseLong(nanoEnd) - Long.parseLong(nanoStart);

			TC.TestDescription = "Total Response time in Sec =  "
					+ String.valueOf(milliTime / 1000);
			// TC.TestDescription = "TOTAL RESPONSE TIME(in Sec): " +
			// String.valueOf(milliTime/1000);
			// TC.TestDescription = "TOTAL RESPONSE TIME(in Sec): " + ;
			TC.PassDescription = "Response time in Milliseconds  == "
					+ String.valueOf(milliTime);
			TC.FailDescription = "Response time in NanoSeconds  == "
					+ String.valueOf(nanoTime);
			return UpdateResult.Done();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String PrintDeviceVersion() throws IOException,
			InterruptedException {
		try {
			Logs.Ulog("Start PrintDeviceVersion");

			String deviceName = (String) driver.getCapabilities()
					.getCapability("deviceName");
			String deviceBrowsername = (String) driver.getCapabilities()
					.getBrowserName();
			String devicePlatformName = (String) driver.getCapabilities()
					.getPlatform().name();
			String automationName = (String) driver.getCapabilities()
					.getCapability("automationName");

			Logs.Ulog("End  PrintDeviceVersion ");

			TC.TestDescription = " Device details: " + automationName;
			TC.PassDescription = " Device Name/Id: " + deviceName;
			TC.FailDescription = "Device Browser Name " + deviceBrowsername;
			TC.ExpectedData = "Device Name =" + deviceName;
			UpdateResult.ActualData = "";

			// TC.TestDescription = " Device details " + automationName;
			// TC.PassDescription = " devicePlatformVersion =" +
			// devicePlatformName;
			// TC.FailDescription = "deviceBrowsername "+ deviceBrowsername;
			// TC.ExpectedData = "deviceName =" + deviceName;
			// UpdateResult.ActualData = "" ;

			return UpdateResult.Done();

		} catch (Throwable e) {
			return null; // CatchStatementWebElement(e.getMessage());
		}

	}

	public static String Print() throws InterruptedException {

		try {
			System.out.println("Printed Meassage = " + TC.InputData);
			JOptionPane.showMessageDialog(null, TC.InputData, "InfoBox: "
					+ "Printed Meassage", JOptionPane.INFORMATION_MESSAGE);

		} catch (Throwable e) {
			Logs.Ulog("Error while print message " + TC.InputData + " "
					+ e.getMessage());

		}
		return TC.PASS;
	}

	public static String UserInputBox_Store() throws InterruptedException {

		try {

			String message = JOptionPane.showInputDialog(null,
					"Enter User value to store");
			JOptionPane.showMessageDialog(null, message);

			StoreTable.put("D_UserInput", message);
		} catch (Throwable e) {
			Logs.Ulog("Error while input  message " + TC.InputData + " "
					+ e.getMessage());

		}
		return TC.PASS;
	}

	public static String PrintResponce() throws InterruptedException {

		try {

			JTextArea ta = new JTextArea(20, 20);
			ta.setText(TC.InputData);
			ta.setWrapStyleWord(true);
			ta.setLineWrap(true);
			ta.setCaretPosition(0);
			ta.setEditable(false);

			JOptionPane.showMessageDialog(null, new JScrollPane(ta),
					"Responce", JOptionPane.INFORMATION_MESSAGE);

		} catch (Throwable e) {
			Logs.Ulog("Error while input  message " + TC.InputData + " "
					+ e.getMessage());

		}
		return TC.PASS;
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String ListOptionSelect() throws IOException,
			InterruptedException {
		// StoreTable.put("Fluent_Time" , 5);
		Logs.Ulog("Verifying Object Text");
		try {
			WebElement Menu = TORObj();
			RoboMouseMoveOnObj();
			ReSetSyncTime();
			for (int i = 0; i <= 5; i++) {

				try {

					// Menu.click();
					Thread.sleep(100L);
					getORObject(TC.Param1);
					WebElement Option = NonExceptionTORObj();
					Menu.click();
					Thread.sleep(1000L);
					Option.click();

					Thread.sleep(2000L);
					getORObject(TC.Param2);
					WebElement Objattr = NonExceptionTORObj();
					if (Objattr.getAttribute("value").contains(TC.InputData)) {
						SetDefaultSyncTime();
						break;

					}

				} catch (Throwable T) {

				}

			}
			SetDefaultSyncTime();
			Logs.Ulog("Actual data  = " + UpdateResult.ActualData);

			return UpdateResult.Done();
		} catch (Throwable e) {
			SetDefaultSyncTime();
			Logs.Ulog("ERROR---  on Mouse Over= " + e.getMessage());
			return UpdateResult.UpdateStatus();

		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Get the html page source of current web page
	public static String GetPagesource() throws IOException,
			InterruptedException {

		Logs.Ulog("Get the title of the window opened");
		try {

			StoreTable.put(TC.InputData, driver.getPageSource());
			return UpdateResult.Done();
		} catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHH Android Native App API - RMS
	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// Scroll to the element whose text is specified
	public static String ScrollToElement() throws IOException,
			InterruptedException {

		UpdateDescription("Scroll To Element ");
		Logs.Ulog("Executing ObjScroll");
		try {

			((AndroidDriver) driver).scrollTo(TC.InputData);

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Install the app from specified path location on device or emulator
	public static String installApp() throws IOException, InterruptedException {

		UpdateDescription("Install the application on device ");
		Logs.Ulog("Executing install application");
		try {

			((AndroidDriver) driver).installApp(TC.InputData);

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Verify app is installed for given package
	public static String isAppInstalled() throws IOException,
			InterruptedException {

		UpdateDescription("isAppInstalled on device or emulator");
		Logs.Ulog("Executing isAppInstalled");
		try {

			((AndroidDriver) driver).isAppInstalled(TC.InputData);

			TC.InputData = store();

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// For Debugging
	public static String printValueinConsole() throws IOException,
			InterruptedException {

		UpdateDescription("Prints the value in console");
		Logs.Ulog("Executing printValueinConsole");
		try {

			System.out.println("Value***********************=" + TC.InputData);

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Verify device or emulator is locked
	public static String isLocked() throws IOException, InterruptedException {

		UpdateDescription("is device or emulator are locked");
		Logs.Ulog("Executing isLocked");
		try {

			// TC.ObjectName=((AndroidDriver) driver).isLocked();

			TC.InputData = store();

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Runs the current app as a background app for the number of seconds
	// requested
	public static String runAppInBackground() throws IOException,
			InterruptedException {

		UpdateDescription("Run App in Background");
		Logs.Ulog("Executing runAppInBackground");
		try {

			((AndroidDriver) driver).runAppInBackground(Integer
					.parseInt(TC.InputData));

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Pinch the element specified
	public static String pinchElement() throws IOException,
			InterruptedException {

		UpdateDescription("Pinch the element specified");
		Logs.Ulog("Executing pinch");
		try {

			((AndroidDriver) driver).pinch(TORObj());

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Perform pinch action with cooridate specifed
	public static String pinchByCoordinate() throws IOException,
			InterruptedException {

		UpdateDescription("Perform pinch action with cooridate specifed");
		Logs.Ulog("Executing pinchByCoordinate");
		try {

			((AndroidDriver) driver).pinch(Integer.parseInt(TC.InputData),
					Integer.parseInt(TC.ExpectedData));

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Close Application
	public static String closeApp() throws IOException, InterruptedException {

		UpdateDescription("Close App");
		Logs.Ulog("Executing closeApp");
		try {

			((AndroidDriver) driver).closeApp();

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Rotate the screen to speified orienation
	public static String chnageScreenOrientation() throws IOException,
			InterruptedException {

		UpdateDescription("Perform change orientation");
		Logs.Ulog("Executing rotate");
		try {

			if (TC.InputData.equals("LANDSCAPE")) {
				((AndroidDriver) driver).rotate(ScreenOrientation.LANDSCAPE);
			} else if (TC.InputData.equals("PORTRAIT")) {
				((AndroidDriver) driver).rotate(ScreenOrientation.PORTRAIT);
			}

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Zoom the element specified
	public static String zoomElement() throws IOException, InterruptedException {
		UpdateDescription("Perform zoom action on the element specified");
		Logs.Ulog("Executing zoomElement");

		try {

			((AndroidDriver) driver).zoom(TORObj());

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Perform zoom action with cooridate specifed
	public static String zoomByCoordinate() throws IOException,
			InterruptedException {

		UpdateDescription("Perform zoom action with cooridate specifed");
		Logs.Ulog("Executing zoomByCoordinate");

		try {

			((AndroidDriver) driver).zoom(Integer.parseInt(TC.InputData),
					Integer.parseInt(TC.ExpectedData));

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Tap the element specified
	public static String tapElement() throws IOException, InterruptedException {
		UpdateDescription("Perform tap action on the element specified");
		Logs.Ulog("Executing tapElement");

		try {

			((AndroidDriver) driver).tap(2, TORObj(), 10);

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Perform tap action with cooridate specifed
	public static String tapByCoordinate() throws IOException,
			InterruptedException {
		UpdateDescription("Perform tap action with cooridate specifed");
		Logs.Ulog("Executing tapByCoordinate");

		try {

			((AndroidDriver) driver).tap(2, Integer.parseInt(TC.InputData),
					Integer.parseInt(TC.ExpectedData), 10);

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Lock the device or emulator screen
	public static String lockScreen() throws IOException, InterruptedException {

		UpdateDescription("Lock device or emulator screen");
		Logs.Ulog("Executing lockScreen");
		try {

			((AndroidDriver) driver).lockScreen(Integer.parseInt(TC.InputData));

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Reset the currently running app for this session.
	public static String resetApp() throws IOException, InterruptedException {

		UpdateDescription("Reset the currently running app for this session");
		Logs.Ulog("Executing resetApps");
		try {

			((AndroidDriver) driver).resetApp();

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// Remove the specified app from the device (uninstall).
	public static String removeApp() throws IOException, InterruptedException {

		UpdateDescription("Remove the specified app from the device (uninstall)");
		Logs.Ulog("Executing removeApp");
		try {

			((AndroidDriver) driver).removeApp(TC.InputData);
			;

			// TimeUnit.SECONDS.sleep(2);
			UpdateResult.ActualData = TC.PASS;
			TC.ExpectedData = TC.PASS;

			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// #####################################************************************************#############################################

	public static String DeletAllCookies() throws IOException,
			InterruptedException {

		Logs.Ulog("---- Delet All cookies ----");
		// driver.manage().deleteCookieNamed("__utmb");
		driver.manage().deleteAllCookies();
		return UpdateResult.Done();

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifySourceCodeElement() throws IOException,
			InterruptedException {

		Logs.Ulog("to verify a particular text is present or not on the page,");
		try {
			driver.getPageSource().contains(TC.InputData);
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String GetCSSAttributeVal_Verify() throws IOException,
			InterruptedException {

		Logs.Ulog("Verifying the VerifyAttributeVal");
		try {
			UpdateResult.ActualData = TORObj().getCssValue(TC.InputData);

			// TORObj().isDisplayed();
			// TC.ExpectedData = TC.Param1;
			return UpdateResult.UpdateStatus();

		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String CheckSearchBoxText() throws IOException,
			InterruptedException {

		Logs.Ulog("---- Start  CheckForJbossStart ----");

		String firstValue = TORObj().getCssValue("placeholder");

		Click();

		String secondtValue = TORObj().getCssValue("placeholder");

		if (firstValue.equals(secondtValue)) {
			return TC.PASS;

		} else {
			return TC.FAIL;
		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	// ######################################################################################################################################
	public static String SwitchToBrowser_DynamicFrame() throws IOException,
			InterruptedException {

		Logs.Ulog("Successfully Switched to Dynamic frame");
		try {
			WebElement frame = TORObj();
			driver.switchTo().frame(frame);
			Logs.Ulog("Successfully Switched to Dynamic frame");

			return UpdateResult.Done();
		}

		catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			UpdateResult.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}

	}

	// ######################################################################################################################################
	public static String SwitchToBrowser_Frame_Index_0() throws IOException,
			InterruptedException {

		Logs.Ulog(" Switched to Dynamic SwitchToBrowser_Frame_Index_0 ");
		try {

			driver.switchTo().frame(0);

			return UpdateResult.Done();
		}

		catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String VerifyTextFontSizeCSS() throws IOException,
			InterruptedException {
		Logs.Ulog("Start --  VerifyTextbackgroundColorCSS");
		try {
			WebElement Obj = TORObj();

			UpdateResult.ActualData = String.valueOf(Obj
					.getCssValue("font-size"));
			TC.ExpectedData = String.valueOf(TC.InputData);
			UpdateResult.UpdateStatus();

			return TC.PASS;
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());

		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	public static String VerifyTextFontFamilyCSS() throws IOException,
			InterruptedException {
		Logs.Ulog("Start --  VerifyTextbackgroundColorCSS");
		try {
			WebElement Obj = TORObj();

			UpdateResult.ActualData = String.valueOf(Obj
					.getCssValue("font-family"));
			TC.ExpectedData = String.valueOf(TC.InputData);
			if (UpdateResult.ActualData.contains(TC.ExpectedData)) {
				UpdateResult.UpdateStatus();
			}
			return TC.PASS;
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());

		}
	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String SendKeyEsc() throws IOException, InterruptedException {

		try {
			Logs.Ulog("Executing SendKeyEsc");

			TORObj().sendKeys(Keys.ESCAPE);
			// TORObj().sendKeys(Keys.ESCAPE);
			// RMouse.mousePress(0);
			// RMouse.keyPress(KeyEvent.VK_TAB);

			Thread.sleep(200L);
			return UpdateResult.Done();
			// System.exit(0);
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// ######################################################################################################################################

	public static String SendKeyCTRL_A() throws IOException,
			InterruptedException {

		try {
			Logs.Ulog("Executing SendKeyTab");
			HighlightElement();
			// DbClickJS();
			TORObj().sendKeys(Keys.chord(Keys.CONTROL, "a"));
			// System.exit(0);
			return TC.PASS;
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

	public static String ReadImageTextandStore() throws IOException,
			InterruptedException {

		try {
			Logs.Ulog("Executing ReadImageTextandStore");
			WebElement ImgObj = TORObj();
			String ImgUrl = ImgObj.getAttribute("src");

			Logs.Ulog("Image source path : \n" + ImgUrl);

			URL url = new URL(ImgUrl);
			Image image = ImageIO.read(url);
			String ImgText = new OCR()
					.recognizeCharacters((RenderedImage) image);
			StoreTable.put(TC.InputData, ImgText);
			// System.exit(0);
			return TC.PASS;
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}
	// HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

}
