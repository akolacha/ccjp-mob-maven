package Utility;

import static Utility.R_Start.StoreTable;
import static Utility.R_Start.myRest;
import static Utility.R_Start.mySoapProj;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.xmlbeans.XmlException;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.xml.sax.SAXException;

import com.eviware.soapui.impl.WsdlInterfaceFactory;
import com.eviware.soapui.impl.wsdl.WsdlInterface;
import com.eviware.soapui.impl.wsdl.WsdlOperation;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.WsdlRequest;
import com.eviware.soapui.impl.wsdl.WsdlSubmit;
import com.eviware.soapui.impl.wsdl.WsdlSubmitContext;
import com.eviware.soapui.model.iface.Request.SubmitException;
import com.eviware.soapui.model.iface.Response;
import com.eviware.soapui.support.SoapUIException;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.path.xml.XmlPath;

/*
 ********************************************************************************
 ********************************************************************************
//Authoer santhosha HC
// Date 19 March 2015
// Test :    Service library  
//Last updated by santhosha on proxy settings and other settings

********************************************************************************
********************************************************************************/

public class ServiceLibrary {
	public static String[] keys = null;	
	public static String[] expvalues = null;
	public static String Resp;

	public String executePostRequest(String url,HashMap<String, String> headerParams,String reqBody) {
		if(reqBody.equalsIgnoreCase("") || reqBody.equalsIgnoreCase("BLANK") || reqBody.isEmpty()){
    	   return myRest.given().log().all()
                   .headers(headerParams).relaxedHTTPSValidation().when().post(url).asString();
       }else{
    	   return myRest.given().log().all()
                   .headers(headerParams)
                   .body(reqBody).relaxedHTTPSValidation().when()
                   .post(url).asString();
       }
		
    }

	public String executeGetSOAPRequest(String url, String SOAPMethod,String ValidationKeys) throws XmlException, IOException, SoapUIException, SAXException, ParserConfigurationException, TransformerException, SubmitException {
	
		
		mySoapProj = new WsdlProject();
		
		// import amazon wsdl
	
		WsdlInterface wsdl = WsdlInterfaceFactory.importWsdl(mySoapProj, url , true )[0];
		//WsdlInterface wsdl = WsdlInterfaceFactory.importWsdl(project, "https://localhost/retail/retail.php?wsdl", true )[0];
				
		// get desired operation
	
		//WsdlOperation op = wsdl.getOperationAt(0);
		WsdlOperation op = wsdl.getOperationByName(SOAPMethod);
		//WsdlOperation op = wsdl.getOperationByName("GetCityForecastByZIP");

		String reqContent = op.createRequest(true);
		//reqContent = replaceNode ( reqContent) ;
		//reqContent = reqContent.replaceAll("ZIP\\>\\?" , "ZIP\\>94701");
		//reqContent = reqContent.replaceAll("ToCurrency\\>\\?" , "ToCurrency\\>USD");
		
		String k = ValidationKeys;
		
		HashMap SOAPKeys =   new HashMap();
		String a[] = k.split(";");
		
		for(int sk=0; sk<a.length;sk++)
		{
			String[] SplitedKey =  a[sk].split(":~");
			SOAPKeys.put(SplitedKey[0], SplitedKey[1]);
		}
		

		reqContent = new UpdateXmlValue().updateXML(reqContent, SOAPKeys);
		
		WsdlRequest req = op.addNewRequest("myReq");
		System.out.println(reqContent);
		req.setRequestContent(reqContent);
		
		WsdlSubmitContext wsdlSubmitContext = new WsdlSubmitContext(req);

		WsdlSubmit<?> submit = (WsdlSubmit<?>) req.submit(wsdlSubmitContext, false);

		Response response = submit.getResponse();

		String result = response.getContentAsString();

		System.out.println("The result ="+result);
		return result;
	
		
		}
		

	
	/**
	 * Execute the Get request & returns the response
	 * @param url
	 * @param headerParams
	 * @return
	 * @throws MalformedURLException 
	 */
	public String executeGetRequest(String url, HashMap<String, String> headerParams) throws MalformedURLException {
       // return myRest.given().log().all().headers(headerParams).get(url).asString();
        
        return  myRest.given().log().all().headers(headerParams).relaxedHTTPSValidation().when().get(new URL(url)).asString();
    }
	
	/**
	 * Execute the Get request & returns the response code
	 * @param url
	 * @param headerParams
	 * @return
	 * @throws MalformedURLException 
	 */
	public int getResponseCode(String url, HashMap<String, String> headerParams) throws MalformedURLException {
		//return myRest.given().log().all().headers(headerParams).get(url).getStatusCode();
		
		 return  myRest.given().log().all().headers(headerParams).relaxedHTTPSValidation().when().get(new URL(url)).getStatusCode();
	}
	


	public int getResponseCode(String url, HashMap<String, String> headerParams, String requestBody) throws MalformedURLException{
		//return myRest.given().log().all().headers(headerParams).body(requestBody).post(url).getStatusCode();
		 return  myRest.given().log().all().headers(headerParams).relaxedHTTPSValidation().when().post(new URL(url)).getStatusCode();
	}
	

	public String getJsonString(String jsonResp, String path){
		JsonPath resp = new JsonPath(jsonResp);
		return resp.getString(path);
		
	}


	public String getXmlString(String xmlResp, String path){
		XmlPath resp = new XmlPath(xmlResp);
		return resp.getString(path);
		
	}
	
	/**
	 * 
	 * @param testcase
	 * @param jsonResp
	 * @param keys
	 * @param expvalues
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public boolean validateJSONResponse(int row, String testcase, String jsonResp, String[] keys, String[] expvalues) throws SQLException, ParseException{
		JsonPath resp = new JsonPath(jsonResp);
		ArrayList<String> actValues = new ArrayList<>();
		boolean status = false;
		
		try{
			if(keys.length == expvalues.length){
				for (int i = 0; i < keys.length; i++) {
					System.out.println(resp.getString(keys[i]));
					actValues.add(resp.getString(keys[i]));
				}
			}
			else{
				System.out.println("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
			}
			
			String[] actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
			status = Arrays.equals(expvalues, actValues.toArray());
			
			if(!status){
				addServiceLogToExcel(row, testcase, "FAIL", FunctionLibrary.getArrayAsString(expvalues,";"), FunctionLibrary.getArrayAsString(actArr, ";"),jsonResp);
				System.out.println(actValues.get(0));
			}else{
				addServiceLogToExcel(row,testcase, "PASS", FunctionLibrary.getArrayAsString(expvalues,";"), FunctionLibrary.getArrayAsString(actArr, ";"),jsonResp);
				System.out.println(actValues.get(0));
			}
		}catch(Exception e){
			System.out.println("Error:" + e.getMessage());
			addServiceLogToExcel(row,testcase, "FAIL", FunctionLibrary.getArrayAsString(expvalues,";"), e.getMessage() + "OR \n Expected values and actual length not equal.",jsonResp);
		}
		
		return status;
	}
	
	
	/**
	 * Validates the XML response with the expected values
	 * @param testcase
	 * @param xmlResp
	 * @param keys
	 * @param expvalues
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public boolean validateXMLResponse(int row, String testcase, String xmlResp, String[] keys, String[] expvalues) throws SQLException, ParseException{
		XmlPath resp = new XmlPath(xmlResp);
		ArrayList<String> actValues = new ArrayList<>();
		boolean status = false;
		
		try{
			if(keys.length == expvalues.length){
				for (int i = 0; i < keys.length; i++) {
					System.out.println(resp.getString(keys[i]));
					actValues.add(resp.getString(keys[i]));
				}
			}
			else{
				System.out.println("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
			}
			
			String[] actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
			status = Arrays.equals(expvalues, actValues.toArray());
			
			if(!status){
				addServiceLogToExcel(row,testcase, "FAIL", FunctionLibrary.getArrayAsString(expvalues,";"), FunctionLibrary.getArrayAsString(actArr, ";"),xmlResp);
				System.out.println(actValues.get(0));
			}else{
				addServiceLogToExcel(row,testcase, "PASS", FunctionLibrary.getArrayAsString(expvalues,";"), FunctionLibrary.getArrayAsString(actArr, ";"),xmlResp);
				System.out.println(actValues.get(0));
			}
		}catch(Exception e){
			System.out.println("Error:" + e.getMessage());
			addServiceLogToExcel(row,testcase, "FAIL", FunctionLibrary.getArrayAsString(expvalues,";"), e.getMessage(),xmlResp);
		}
		return status;
	}
	
	/**
	 * 
	 * @param testcase
	 * @param htmlResp
	 * @param expvalue
	 * @return
	 */
	public boolean validateHTMLResponse(int row,String testcase, String htmlResp, String expvalue){
		boolean status = false;
		
		Document d = Jsoup.parse(htmlResp);
//		String resptemp = d.body().getElementsByTag("h1").text();
		String resptemp = d.body().getElementsByTag("b").text();
		
		if(resptemp.equalsIgnoreCase(expvalue)){
			status = true;
			addServiceLogToExcel(row,testcase, "PASS", expvalue, resptemp,htmlResp);
		}else{
			status = false;
			addServiceLogToExcel(row,testcase, "FAIL", expvalue, resptemp,htmlResp);
		}
		
		return status;
	}
	
	public boolean validateSOAPResponse(int row,String testcase, String XMLResp, String expvalue) throws SAXException, IOException{
		boolean status = false;
		
	
		Diff diff = XMLUnit.compareXML(XMLResp, expvalue); 
		if(diff.identical())			
		{
			status = true;
			addServiceLogToExcel(row,testcase, "PASS", expvalue, XMLResp,XMLResp);
		}else{
			status = false;
			addServiceLogToExcel(row,testcase, "FAIL", expvalue, diff.toString(),diff.toString());
		}
		
		return status;
	}
	
	public HashMap<String, String> generateHeaderMap(String headerdata){
		
		HashMap<String, String> headermap = new HashMap<>();
		String[] headerarr = null;
		
		try {
			headerarr = FunctionLibrary.getStringAsArray(headerdata);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return headermap;
	}
	
	
	public void addServiceSheet(ExcelObj exObj, String sheetName) throws Exception{
		eObj = exObj;
		resultsheetName = sheetName;
		eObj.addSheetAndColumns(resultsheetName);
	}
	
	public void addServiceLogToExcel( int row,String testcase, String result, String expectedValue, String actualValue, String resp){
		try {
			eObj.setCellData(resultsheetName, "TESTCASE", row, testcase);
			eObj.setCellData(resultsheetName, "EXPECTED_VALUE", row, expectedValue);
			eObj.setCellData(resultsheetName, "ACTUAL_VALUE", row, actualValue);
			eObj.setCellData(resultsheetName, "RESULT", row, result);
			eObj.setCellData(resultsheetName, "OUTPUT_RESPONSE", row, resp);
			
		} 
		catch (Exception e) { 
			e.printStackTrace(); 
		}
	}
	
	
	ExcelObj eObj;
	String resultsheetName = "";
	
	public static String WebServiceGet() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServiceGet");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(myRest.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .get(new URL(TC.TestObjects)).asString());
			}
			try
			{
				validateResponce();	
			}
			catch( Throwable t)
			{
				Logs.Ulog("Error while  validateResponce------" + t);
			}
				
			return Resp;
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
		
			
	}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	
	
	public static String WebServiceGetStoreKey() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServiceGetStoreKey");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(myRest.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .get(new URL(TC.TestObjects)).asString());
			}
			try
			{
				StoreResponcebyKey();	
			}
			catch( Throwable t)
			{
				Logs.Ulog("Error while  validateResponce------" + t);
			}
				
			return Resp;
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
		
			
	}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
	
	
	public static String WebServiceGetContains() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServiceGetContains");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
				
			}
			else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty())
			{
				Resp=  String.valueOf(myRest.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .get(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(myRest.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .get(new URL(TC.TestObjects)).asString());
			}
			
			try
			{
				validateResponceContains();	
			}
			catch( Throwable t)
			{
				Logs.Ulog("Error while  validateResponce------" + t);
			}
				
			return Resp;
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
			
	}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	

	
	public static String WebServicePost() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePost");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			
			
			HashMap<String, String> headermapbody = new HashMap<>();
			String[] headerarrbody = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarrbody = FunctionLibrary.getStringAsArray(TC.Param2);
			headermapbody = FunctionLibrary.getMapFromArray(headerarrbody, ":");
		
			
			 Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
				
			}
			else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty())
			{
				Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).relaxedHTTPSValidation().when()
		                   .post(new URL(TC.TestObjects)).asString());				
							
			}
			else
			{
				Resp=  String.valueOf(myRest.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .post(new URL(TC.TestObjects)).asString());
			}
			
			try
			{
				validateResponce();	
			}
			catch( Throwable t)
			{
				Logs.Ulog("Error while  validateResponce------" + t);
			}
				
			return Resp;
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
	}
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	

	
		public static String WebServicePostContains() throws IOException, InterruptedException 
		{
			Logs.Ulog("Verifying the WebServicePost");
			try {
				
				HashMap<String, String> headermap = new HashMap<>();
				String[] headerarr = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
				headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
				
				
				HashMap<String, String> headermapbody = new HashMap<>();
				String[] headerarrbody = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarrbody = FunctionLibrary.getStringAsArray(TC.Param2);
				headermapbody = FunctionLibrary.getMapFromArray(headerarrbody, ":");
			
				
				 Resp= null;
				
				if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 
					 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
				}
				else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
					
				}
				else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty())
				{
					Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).relaxedHTTPSValidation().when()
			                   .post(new URL(TC.TestObjects)).asString());				
								
				}
				else
				{
					Resp=  String.valueOf(myRest.given().log().all()
			                   .headers(headermap)
			                   .body(TC.Param2).relaxedHTTPSValidation().when()
			                   .post(new URL(TC.TestObjects)).asString());
				}
				
				try
				{
					validateResponceContains();	
				}
				catch( Throwable t)
				{
					Logs.Ulog("Error while  validateResponce------" + t);
				}
					
				return Resp;
			}

			catch (Exception e)
			{
				    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
			}
		}
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	

	
		public static String WebServicePostWithFormParameters() throws IOException, InterruptedException 
		{
			Logs.Ulog("Verifying the WebServicePost");
			try {
				
				HashMap<String, String> headermap = new HashMap<>();
				String[] headerarr = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
				headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
				
				
				HashMap<String, String> headermapbody = new HashMap<>();
				HashMap<String, String> headermapForm = new HashMap<>();
				String[] headerarrbody = null;
				String[] headerarrForm = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarrbody = FunctionLibrary.getStringAsArray(TC.Param2);
				headermapbody = FunctionLibrary.getMapFromArray(headerarrbody, ":");
				headerarrForm =FunctionLibrary.getStringAsArray(TC.Param3);
				headermapForm = FunctionLibrary.getMapFromArray(headerarrForm, ":");
			
				
				 Resp= null;
				
				if(TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
				{
					 
					 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
				}
				else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
				{
					 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
					
				}
				else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() && TC.Param3.isEmpty())
				{
					Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).relaxedHTTPSValidation().when()
			                   .post(new URL(TC.TestObjects)).asString());	
				}
				else if( TC.Param1.isEmpty() && TC.Param2.isEmpty() && !TC.Param3.isEmpty())
				{
					Resp=  String.valueOf(myRest.given().log().all().formParameters(headermapForm).relaxedHTTPSValidation().when()
			                   .post(new URL(TC.TestObjects)).asString());				
								
				}
				else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() &&  !TC.Param3.isEmpty())
				{
				
					Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
			                   .post(new URL(TC.TestObjects)).asString());	
					
				}
				else
				{
					Resp=  String.valueOf(myRest.given().log().all().headers(headermap).body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
			                   .post(new URL(TC.TestObjects)).asString());
				}
				
				
				try
				{
					validateResponce();	
				}
				catch( Throwable t)
				{
					Logs.Ulog("Error while  validateResponce------" + t.getMessage());
				}
					
				return Resp;
			}

			catch (Exception e)
			{
				    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
			}
		}
	
		//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH			
		public static String WebServiceGetWithFormParameters() throws IOException, InterruptedException 
		{
			Logs.Ulog("Verifying the WebServicePost");
			try {
				
				HashMap<String, String> headermap = new HashMap<>();
				String[] headerarr = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
				headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
				
				
				HashMap<String, String> headermapbody = new HashMap<>();
				HashMap<String, String> headermapForm = new HashMap<>();
				String[] headerarrbody = null;
				String[] headerarrForm = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarrbody = FunctionLibrary.getStringAsArray(TC.Param2);
				headermapbody = FunctionLibrary.getMapFromArray(headerarrbody, ":");
				headerarrForm =FunctionLibrary.getStringAsArray(TC.Param3);
				headermapForm = FunctionLibrary.getMapFromArray(headerarrForm, ":");
			
				
				 Resp= null;
				
				if(TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
				{
					 
					 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
				}
				else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
				{
					 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
					
				}
				else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() && TC.Param3.isEmpty())
				{
					Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).relaxedHTTPSValidation().when()
			                   .get(new URL(TC.TestObjects)).asString());	
				}
				else if( TC.Param1.isEmpty() && TC.Param2.isEmpty() && !TC.Param3.isEmpty())
				{
					Resp=  String.valueOf(myRest.given().log().all().formParameters(headermapForm).relaxedHTTPSValidation().when()
			                   .get(new URL(TC.TestObjects)).asString());				
								
				}
				else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() &&  !TC.Param3.isEmpty())
				{
				
					Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
			                   .get(new URL(TC.TestObjects)).asString());	
					
				}
				else
				{
					Resp=  String.valueOf(myRest.given().log().all().headers(headermap).body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
			                   .get(new URL(TC.TestObjects)).asString());
				}
				
				
				try
				{
					validateResponce();	
				}
				catch( Throwable t)
				{
					Logs.Ulog("Error while  validateResponce------" + t.getMessage());
				}
					
				return Resp;
			}

			catch (Exception e)
			{
				    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
			}
		}
	
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	

		
			public static String WebServicePostWithFormParametersContains() throws IOException, InterruptedException 
			{
				Logs.Ulog("Verifying the WebServicePost");
				try {
					
					HashMap<String, String> headermap = new HashMap<>();
					String[] headerarr = null;
					keys = FunctionLibrary.getStringAsArray(TC.InputData);
					expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
					headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
					headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
					
					
					HashMap<String, String> headermapbody = new HashMap<>();
					HashMap<String, String> headermapForm = new HashMap<>();
					String[] headerarrbody = null;
					String[] headerarrForm = null;
					keys = FunctionLibrary.getStringAsArray(TC.InputData);
					expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
					headerarrbody = FunctionLibrary.getStringAsArray(TC.Param2);
					headermapbody = FunctionLibrary.getMapFromArray(headerarrbody, ":");
					headerarrForm =FunctionLibrary.getStringAsArray(TC.Param3);
					headermapForm = FunctionLibrary.getMapFromArray(headerarrForm, ":");
				
					
					 Resp= null;
					
					if(TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						 
						 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
					}
					else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
						
					}
					else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).relaxedHTTPSValidation().when()
				                   .post(new URL(TC.TestObjects)).asString());	
					}
					else if( TC.Param1.isEmpty() && TC.Param2.isEmpty() && !TC.Param3.isEmpty())
					{
						Resp=  String.valueOf(myRest.given().log().all().formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .post(new URL(TC.TestObjects)).asString());				
									
					}
					else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() &&  !TC.Param3.isEmpty())
					{
					
						Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .post(new URL(TC.TestObjects)).asString());	
						
					}
					else
					{
						Resp=  String.valueOf(myRest.given().log().all().headers(headermap).body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .post(new URL(TC.TestObjects)).asString());
					}
					
					
					try
					{
						validateResponceContains();	
					}
					catch( Throwable t)
					{
						Logs.Ulog("Error while  validateResponce------" + t.getMessage());
					}
						
					return Resp;
				}

				catch (Exception e)
				{
					    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
				}
			}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
			public static String WebServicePostWithFormParametersGetStatusCode() throws IOException, InterruptedException 
			{
				Logs.Ulog("Verifying the WebServicePost");
				try {
					
					HashMap<String, String> headermap = new HashMap<>();
					String[] headerarr = null;
					keys = FunctionLibrary.getStringAsArray(TC.InputData);
					expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
					headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
					headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
					
					
					HashMap<String, String> headermapbody = new HashMap<>();
					HashMap<String, String> headermapForm = new HashMap<>();
					String[] headerarrbody = null;
					String[] headerarrForm = null;
					keys = FunctionLibrary.getStringAsArray(TC.InputData);
					expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
					headerarrbody = FunctionLibrary.getStringAsArray(TC.Param2);
					headermapbody = FunctionLibrary.getMapFromArray(headerarrbody, ":");
					headerarrForm =FunctionLibrary.getStringAsArray(TC.Param3);
					headermapForm = FunctionLibrary.getMapFromArray(headerarrForm, ":");
				
					
					 Resp= null;
					
					if(TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						 
						 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).statusCode());
					}
					else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).statusCode());
						
					}
					else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).relaxedHTTPSValidation().when()
				                   .post(new URL(TC.TestObjects)).statusCode());	
					}
					else if( TC.Param1.isEmpty() && TC.Param2.isEmpty() && !TC.Param3.isEmpty())
					{
						Resp=  String.valueOf(myRest.given().log().all().formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .post(new URL(TC.TestObjects)).statusCode());				
									
					}
					else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() &&  !TC.Param3.isEmpty())
					{
					
						Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .post(new URL(TC.TestObjects)).statusCode());	
						
					}
					else
					{
						Resp=  String.valueOf(myRest.given().log().all().headers(headermap).body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .post(new URL(TC.TestObjects)).statusCode());
					}
					
					
					UpdateResult.ActualData = Resp;	
					TC.ExpectedData  = Round(TC.ExpectedData );
					StoreVarbyMethodName (Resp);
							
							return	UpdateResult.UpdateStatus();
					
				}

				catch (Exception e)
				{
					    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
				}
				
			}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
			public static String WebServiceGetWithFormParametersGetStatusCode() throws IOException, InterruptedException 
			{
				Logs.Ulog("Verifying the WebServicePost");
				try {
					
					HashMap<String, String> headermap = new HashMap<>();
					String[] headerarr = null;
					keys = FunctionLibrary.getStringAsArray(TC.InputData);
					expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
					headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
					headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
					
					
					HashMap<String, String> headermapbody = new HashMap<>();
					HashMap<String, String> headermapForm = new HashMap<>();
					String[] headerarrbody = null;
					String[] headerarrForm = null;
					keys = FunctionLibrary.getStringAsArray(TC.InputData);
					expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
					headerarrbody = FunctionLibrary.getStringAsArray(TC.Param2);
					headermapbody = FunctionLibrary.getMapFromArray(headerarrbody, ":");
					headerarrForm =FunctionLibrary.getStringAsArray(TC.Param3);
					headermapForm = FunctionLibrary.getMapFromArray(headerarrForm, ":");
				
					
					 Resp= null;
					
					if(TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						 
						 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
					}
					else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
						
					}
					else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).relaxedHTTPSValidation().when()
				                   .get(new URL(TC.TestObjects)).asString());	
					}
					else if( TC.Param1.isEmpty() && TC.Param2.isEmpty() && !TC.Param3.isEmpty())
					{
						Resp=  String.valueOf(myRest.given().log().all().formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .get(new URL(TC.TestObjects)).asString());				
									
					}
					else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() &&  !TC.Param3.isEmpty())
					{
					
						Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .get(new URL(TC.TestObjects)).asString());	
						
					}
					else
					{
						Resp=  String.valueOf(myRest.given().log().all().headers(headermap).body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .get(new URL(TC.TestObjects)).asString());
					}
					
					UpdateResult.ActualData = Resp;	
					TC.ExpectedData  = Round(TC.ExpectedData );
					StoreVarbyMethodName (Resp);
							
							return	UpdateResult.UpdateStatus();
					
				}
				catch (Exception e)
				{
					    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
				}
			}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
			
			public static String WebServiceGetWithFormParametersContains() throws IOException, InterruptedException 
			{
				Logs.Ulog("Verifying the WebServicePost");
				try {
					
					HashMap<String, String> headermap = new HashMap<>();
					String[] headerarr = null;
					keys = FunctionLibrary.getStringAsArray(TC.InputData);
					expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
					headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
					headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
					
					
					HashMap<String, String> headermapbody = new HashMap<>();
					HashMap<String, String> headermapForm = new HashMap<>();
					String[] headerarrbody = null;
					String[] headerarrForm = null;
					keys = FunctionLibrary.getStringAsArray(TC.InputData);
					expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
					headerarrbody = FunctionLibrary.getStringAsArray(TC.Param2);
					headermapbody = FunctionLibrary.getMapFromArray(headerarrbody, ":");
					headerarrForm =FunctionLibrary.getStringAsArray(TC.Param3);
					headermapForm = FunctionLibrary.getMapFromArray(headerarrForm, ":");
				
					
					 Resp= null;
					
					if(TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						 
						 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
					}
					else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
						
					}
					else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() && TC.Param3.isEmpty())
					{
						Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).relaxedHTTPSValidation().when()
				                   .get(new URL(TC.TestObjects)).asString());	
					}
					else if( TC.Param1.isEmpty() && TC.Param2.isEmpty() && !TC.Param3.isEmpty())
					{
						Resp=  String.valueOf(myRest.given().log().all().formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .get(new URL(TC.TestObjects)).asString());				
									
					}
					else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty() &&  !TC.Param3.isEmpty())
					{
					
						Resp=  String.valueOf(myRest.given().log().all().body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .get(new URL(TC.TestObjects)).asString());	
						
					}
					else
					{
						Resp=  String.valueOf(myRest.given().log().all().headers(headermap).body(headermapbody).formParameters(headermapForm).relaxedHTTPSValidation().when()
				                   .get(new URL(TC.TestObjects)).asString());
					}
					
					
					try
					{
						validateResponceContains();	
					}
					catch( Throwable t)
					{
						Logs.Ulog("Error while  validateResponce------" + t.getMessage());
					}
						
					return Resp;
				}

				catch (Exception e)
				{
					    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
				}
			}
			
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	
	
	public static String WebServicePostStoreKey() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePostStoreKey ");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			
			Resp= null;
			
			if(TC.Param2.isEmpty())
			{
				 Resp=  myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString();
			}
			else
			{
				Resp=  myRest.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .post(new URL(TC.TestObjects)).asString();
			}
			
			try
			{
				StoreResponcebyKey();	
			}
			catch( Throwable t)
			{
				Logs.Ulog("Error while  validateResponce------" + t);
			}
				
			return Resp;
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
	}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	
	
	
	public static String WebServicePut() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePut");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().put(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{   
				 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().put(new URL(TC.TestObjects)).asString());
				 
			}
			else
			{
				Resp=  String.valueOf(myRest.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .put(new URL(TC.TestObjects)).asString());
				
			}
			
			try
			{
				validateResponce();	
			}
			catch( Throwable t)
			{
				Logs.Ulog("Error while  validateResponce------" + t.getMessage());
			}
				
			return Resp;
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}			
	}
	
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	
	
	
		public static String WebServicePutContains() throws IOException, InterruptedException 
		{
			Logs.Ulog("Verifying the WebServicePut");
			try {
				
				HashMap<String, String> headermap = new HashMap<>();
				String[] headerarr = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
				headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
				Resp= null;
				
				if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 
					 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().put(new URL(TC.TestObjects)).asString());
				}
				else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{   
					 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().put(new URL(TC.TestObjects)).asString());
					 
				}
				else
				{
					Resp=  String.valueOf(myRest.given().log().all()
			                   .headers(headermap)
			                   .body(TC.Param2).relaxedHTTPSValidation().when()
			                   .put(new URL(TC.TestObjects)).asString());
					
				}
				
				try
				{
					validateResponceContains();	
				}
				catch( Throwable t)
				{
					Logs.Ulog("Error while  validateResponce------" + t.getMessage());
				}
					
				return Resp;
			}

			catch (Exception e)
			{
				    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
			}			
		}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
	public static String WebServicePutStoreKey() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePutStoreKey");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().put(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{   
				 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().put(new URL(TC.TestObjects)).asString());
				 
			}
			else
			{
				Resp=  String.valueOf(myRest.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .put(new URL(TC.TestObjects)).asString());
				
			}
			
			try
			{
				StoreResponcebyKey();	
			}
			catch( Throwable t)
			{
				Logs.Ulog("Error while  validateResponce------" + t);
			}
				
			return Resp;
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
			
	}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
	public static String WebServicePatch() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePatch");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().patch(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().patch(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(myRest.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .patch(new URL(TC.TestObjects)).asString());
			}
			
			try
			{
				validateResponce();	
			}
			catch( Throwable t)
			{
				Logs.Ulog("Error while  validateResponce------" + t.getMessage());
			}
				
			return Resp;
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}			

	}
	
	public static String WebServicePatchContains() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePatch");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().patch(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().patch(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(myRest.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .patch(new URL(TC.TestObjects)).asString());
			}
			
			try
			{
				validateResponceContains();	
			}
			catch( Throwable t)
			{
				Logs.Ulog("Error while  validateResponce------" + t.getMessage());
			}
				
			return Resp;
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}			

	}
	
	
	
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
	public static String WebServicePatchStoreKey() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePatchStoreKey");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().patch(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().patch(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(myRest.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .patch(new URL(TC.TestObjects)).asString());
			}
			
			try
			{
				StoreResponcebyKey();
				
			}
			catch( Throwable t)
			{
				Logs.Ulog("Error while  validateResponce------" + t.getMessage());
			}
				
			return Resp;
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}			

			
	}
	
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
		public static String WebServiceDelete() throws IOException, InterruptedException 
		{
			Logs.Ulog("Verifying the WebServiceDelete");
			try {
				
				HashMap<String, String> headermap = new HashMap<>();
				String[] headerarr = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
				headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
				Resp= null;
				
				if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 
					 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString());
				}
				else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString());
					
				}
				else
				{
					Resp=  String.valueOf(myRest.given().log().all()
			                   .headers(headermap)
			                   .body(TC.Param2).relaxedHTTPSValidation().when()
			                   .delete(new URL(TC.TestObjects)).asString());
				}
				
				try
				{
					validateResponce();
					
				}
				catch( Throwable t)
				{
					Logs.Ulog("Error while  validateResponce------" + t.getMessage());
				}
					
				return Resp;
			}

			catch (Exception e)
			{
				    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
			}			

				
		}
		
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
		
		public static String WebServiceDeleteContains() throws IOException, InterruptedException 
		{
			Logs.Ulog("Verifying the WebServiceDelete");
			try {
				
				HashMap<String, String> headermap = new HashMap<>();
				String[] headerarr = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
				headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
				Resp= null;
				
				if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 
					 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString());
				}
				else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString());
					
				}
				else
				{
					Resp=  String.valueOf(myRest.given().log().all()
			                   .headers(headermap)
			                   .body(TC.Param2).relaxedHTTPSValidation().when()
			                   .delete(new URL(TC.TestObjects)).asString());
				}
				
				try
				{
					validateResponceContains();
					
				}
				catch( Throwable t)
				{
					Logs.Ulog("Error while  validateResponce------" + t.getMessage());
				}
					
				return Resp;
			}

			catch (Exception e)
			{
				    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
			}			

				
		}
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
		
		public static String WebServiceDeleteStoreKey() throws IOException, InterruptedException 
		{
			Logs.Ulog("Verifying the WebServiceDeleteStoreKey");
			try {
				
				HashMap<String, String> headermap = new HashMap<>();
				String[] headerarr = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
				headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
				Resp= null;
				
				if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 
					 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString());
				}
				else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString());
					
				}
				else
				{
					Resp=  String.valueOf(myRest.given().log().all()
			                   .headers(headermap)
			                   .body(TC.Param2).relaxedHTTPSValidation().when()
			                   .delete(new URL(TC.TestObjects)).asString());
				}
				
				try
				{
					StoreResponcebyKey();
					
				}
				catch( Throwable t)
				{
					Logs.Ulog("Error while  validateResponce------" + t.getMessage());
				}
					
				return Resp;
			}

			catch (Exception e)
			{
				    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
			}		
				
		}
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH		
		
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
		//Last updated by santhosh h c for mysaop			
				public static String WebServiceSoap() throws IOException, InterruptedException 
				{
					try {
						StoreTable.put("GeneratePDFReport" , "NN");
						StoreTable.put("CaptureScreeShotOnError" , "NN");
						StoreTable.put("CaptureScreeShotOnEachStep" , "NN");					

					} catch (Throwable e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					Logs.Ulog("Verifying the WebServiceSoap");
					try {
						 
						mySoapProj = new WsdlProject();
						
						// import amazon wsdl
					
						WsdlInterface wsdl = WsdlInterfaceFactory.importWsdl(mySoapProj, TC.TestObjects , true )[0];
						//WsdlInterface wsdl = WsdlInterfaceFactory.importWsdl(project, "https://localhost/retail/retail.php?wsdl", true )[0];
						
						
						// get desired operation
					
						//WsdlOperation op = wsdl.getOperationAt(0);
						WsdlOperation op = wsdl.getOperationByName(TC.Param1);
						//WsdlOperation op = wsdl.getOperationByName("GetCityForecastByZIP");

						String reqContent = op.createRequest(true);
						//reqContent = replaceNode ( reqContent) ;
						//reqContent = reqContent.replaceAll("ZIP\\>\\?" , "ZIP\\>94701");
						//reqContent = reqContent.replaceAll("ToCurrency\\>\\?" , "ToCurrency\\>USD");
						
						String k = TC.InputData;
						
						HashMap SOAPKeys =   new HashMap();
						String a[] = k.split(";;");
						
						for(int sk=0; sk<a.length;sk++)
						{
							String[] SplitedKey =  a[sk].split("~");
							SOAPKeys.put(SplitedKey[0], SplitedKey[1]);
						}
						

						reqContent = new UpdateXmlValue().updateXML(reqContent, SOAPKeys);
						
						WsdlRequest req = op.addNewRequest("myReq");
						System.out.println(reqContent);
						req.setRequestContent(reqContent);
						
						WsdlSubmitContext wsdlSubmitContext = new WsdlSubmitContext(req);

						WsdlSubmit<?> submit = (WsdlSubmit<?>) req.submit(wsdlSubmitContext, false);

						Response response = submit.getResponse();

						String result = response.getContentAsString();

						Logs.Ulog("The result ="+result);

						Diff diff = XMLUnit.compareXML(result, TC.ExpectedData); 
						if(diff.identical())			
						{
							UpdateResult.ActualData = TC.ExpectedData; 
						}else{
							UpdateResult.ActualData = result; 
						}
						StoreVarbyMethodName (response.toString());
						
							return	UpdateResult.UpdateStatus();
						
					      
						
					}

					catch (Exception e)
					{
						    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
					}
					
						
				}

				//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				public static String WebServiceStoreSessionID() throws IOException, InterruptedException 
				{
					Logs.Ulog("Verifying the WebServiceDelete");
					try {
						
						HashMap<String, String> headermap = new HashMap<>();
						String[] headerarr = null;
						keys = FunctionLibrary.getStringAsArray(TC.InputData);
						expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
						headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
						headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
						Resp= null;
						
						if(TC.Param2.isEmpty())
						{
							 Resp=  myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString();
						}
						else
						{
							Resp=  myRest.given().log().all()
					                   .headers(headermap)
					                   .body(TC.Param2).relaxedHTTPSValidation().when()
					                   .delete(new URL(TC.TestObjects)).asString();
						}
						
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
						Logs.Ulog(Resp);
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
						String[] actArr = null;
						ArrayList<String> actValues = null;
						if (Resp.contains("<html"))
						{
						Document d = Jsoup.parse(Resp);
//						String resptemp = d.body().getElementsByTag("h1").text();
						UpdateResult.ActualData = d.body().getElementsByTag("b").text();
						
						}	
						else
						{
							
						
						if (TC.Param1.contains("application/json"))
						{
							JsonPath resp = new JsonPath(Resp);
							actValues = new ArrayList<>();
							
							
							
								if(keys.length == expvalues.length){
									for (int i = 0; i < keys.length; i++) {
										Logs.Ulog(resp.getString(keys[i]));
										actValues.add(resp.getString(keys[i]));
									}
									
									actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
								
								}
								else{
									Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
									
								}
						}	
								if (TC.Param1.contains("application/xml"))
								{
									XmlPath Xmlresp = new XmlPath(Resp);
									 actValues = new ArrayList<>();
												
										if(keys.length == expvalues.length){
											for (int i = 0; i < keys.length; i++) {
												Logs.Ulog(Xmlresp.getString(keys[i]));
												actValues.add(Xmlresp.getString(keys[i]));
											}
										}
										else{
											Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
										}
										
										actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
									
										
								}
								UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
						}						
						StoreVarbyMethodName (Resp);
								return	UpdateResult.UpdateStatus();
						
					}

					catch (Exception e)
					{
						    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
					}
					
						
				}	
				
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				public static String WebServicePostVerifyStatusCode() throws IOException, InterruptedException 
				{
					Logs.Ulog("Verifying the WebServicePostVerifyStatusCode");
					try {
						
						HashMap<String, String> headermap = new HashMap<>();
						String[] headerarr = null;
						keys = FunctionLibrary.getStringAsArray(TC.InputData);
						expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
						headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
						headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
						Resp= null;
						
						
						if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 
							 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).getStatusCode());
						}
						else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).getStatusCode());
							 
							 
						}
						else
						{
							Resp=  String.valueOf(myRest.given().log().all()
					                   .headers(headermap)
					                   .body(TC.Param2).relaxedHTTPSValidation().when()
					                   .post(new URL(TC.TestObjects)).getStatusCode());
							
						}
						
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
						Logs.Ulog(Resp);
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
						
						UpdateResult.ActualData = Resp;	
						TC.ExpectedData  = Round(TC.ExpectedData );
						StoreVarbyMethodName (Resp);
								
								return	UpdateResult.UpdateStatus();
						
					}

					catch (Exception e)
					{
						    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
					}
					
						
				}
				//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				public static String WebServiceGetVerifyStatusCode() throws IOException, InterruptedException 
				{
					Logs.Ulog("Verifying the WebServiceGetVerifyStatusCode");
					try {
						
						HashMap<String, String> headermap = new HashMap<>();
						String[] headerarr = null;
						keys = FunctionLibrary.getStringAsArray(TC.InputData);
						expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
						headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
						headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
						Resp= null;
						
						
						if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 
							 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).getStatusCode());
						}
						else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).getStatusCode());
							 
						}
						else
						{
							Resp=  String.valueOf(myRest.given().log().all()
					                   .headers(headermap)
					                   .body(TC.Param2).relaxedHTTPSValidation().when()
					                   .get(new URL(TC.TestObjects)).getStatusCode());
						}
						
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
						Logs.Ulog(Resp);
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
						
						UpdateResult.ActualData = Resp;	
						TC.ExpectedData  = Round(TC.ExpectedData );
						StoreVarbyMethodName (Resp);
							
								return	UpdateResult.UpdateStatus();
						
					}

					catch (Exception e)
					{
						    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
					}
					
						
				}
				
				
				

				
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				public static String WebServiceDeleteVerifyStatusCode() throws IOException, InterruptedException 
				{
					Logs.Ulog("Verifying the WebServiceDeleteVerifyStatusCode");
					try {
						
						HashMap<String, String> headermap = new HashMap<>();
						String[] headerarr = null;
						keys = FunctionLibrary.getStringAsArray(TC.InputData);
						expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
						headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
						headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
						Resp= null;
						
						
						if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 
							 Resp=  String.valueOf(myRest.given().log().all().relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).getStatusCode());
						}
						else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 Resp=  String.valueOf(myRest.given().log().all().headers(headermap).relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).getStatusCode());
							 
						}
						else
						{
							Resp=  String.valueOf(myRest.given().log().all()
					                   .headers(headermap)
					                   .body(TC.Param2).relaxedHTTPSValidation().when()
					                   .delete(new URL(TC.TestObjects)).getStatusCode());
						}
						
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
						Logs.Ulog(Resp);
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
						
						UpdateResult.ActualData = Resp;	
						TC.ExpectedData  = Round(TC.ExpectedData );
						StoreVarbyMethodName (Resp);
							
								return	UpdateResult.UpdateStatus();
						
					}

					catch (Exception e)
					{
						    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
					}
					
						
				}				
				
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				
				public static String Round( String val)
				{
					
					try
					{
						Logs.Ulog("Start Round the string function ");
						double angle = Double.parseDouble(val);
					    DecimalFormat df = new DecimalFormat("#");
					    String angleFormated = df.format(angle);
					    return angleFormated;
					}
					catch (Throwable t)
					{
						Logs.Ulog("Error while  Round the string function " + t.getMessage());
					}
					
					return val;
					
					
				}
				
					
				
				public static String StoreVarbyMethodName(String Retval )
				{
				
					 try 
					 {
						 Throwable t = new Throwable(); 
					      StackTraceElement[] elements = t.getStackTrace(); 
					      String Cmn = elements[2].getMethodName();
					      StoreTable.put(Cmn, Retval); 
					 }
					 catch( Throwable T)
					 {
						 Logs.Ulog("Error while  StoreVarbyMethodName  the string function " + T.getMessage());
					 }
			       
			      
				   return Retval;
			      
				}	
	
				public static boolean isJSONValid(String test) {
				    try {
				        new JSONObject(test);
				    } catch (JSONException ex) {
				       
				        try {
				            new JSONArray(test);
				        } catch (JSONException ex1) {
				        	 Logs.Ulog("Error Not a valid jsonformat   " + ex1.getMessage());
				        	
				            return false;
				        }
				    }
				    return true;
				}
				
				public static boolean isXMLValid(String inXMLStr) {

			        boolean retBool = false;
			        if (inXMLStr.matches("(?s).*?<(\\S+?)[^>]*>.*?</\\1>.*")) {
			            // String has a XML tag
			        	 retBool = true;
			        }
			              
			      

			        return retBool;
			    }
				
				public static String  validateResponceContains() throws IOException, InterruptedException, SQLException, ParseException
				{
					
					Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
					Logs.Ulog(Resp);
					Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
					String[] actArr = null;
					ArrayList<String> actValues = null;
					StoreVarbyMethodName (Resp);
					StoreTable.put(TC.ExpectedData, UpdateResult.ActualData);
					if (Resp.contains("<html"))
					{
					Document d = Jsoup.parse(Resp);
//				
					UpdateResult.ActualData = d.body().getElementsByTag(TC.InputData).text().toString();
					}	
					else
					{
						
					
					if (isJSONValid(Resp))
					{
						JsonPath resp = new JsonPath(Resp);
						actValues = new ArrayList<>();
						
						
						
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(resp.getString(keys[i]));
									actValues.add(resp.getString(keys[i]));
								}
								
								actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
							
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
								
							}
					}	
							if (isXMLValid(Resp))
							{
								XmlPath Xmlresp = new XmlPath(Resp);
								 actValues = new ArrayList<>();
											
									if(keys.length == expvalues.length){
										for (int i = 0; i < keys.length; i++) {
											Logs.Ulog(Xmlresp.getString(keys[i]));
											actValues.add(Xmlresp.getString(keys[i]));
										}
									}
									else{
										Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
									}
									
									actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
								
									
							}
							if (TC.Param1.toLowerCase().contains("text") | actArr== null)
							{
								
									
									
									UpdateResult.ActualData = Resp;
							}
							else
							{
								
								UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");	
							}
							
							
					}						
					
					StoreTable.put(TC.ExpectedData, UpdateResult.ActualData); 
					
					
					 if( UpdateResult.ActualData.contains( TC.ExpectedData))
					 {
						 UpdateResult.ActualData = Resp;
						 TC.ExpectedData = Resp;
					 }
					 else
					 {
						 UpdateResult.ActualData = Resp;
						 TC.ExpectedData ="Responce Not Matching" +  Resp;
					 }
					        
							return	UpdateResult.UpdateStatus();
					
				}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				public static String  StoreResponcebyKey() throws IOException, InterruptedException, SQLException, ParseException
				{
					StoreTable.put("GeneratePDFReport" , "NN");
					StoreTable.put("CaptureScreeShotOnError" , "NN");
					StoreTable.put("CaptureScreeShotOnEachStep" , "NN");
					Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
					Logs.Ulog(Resp);
					Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
					String[] actArr = null;
					ArrayList<String> actValues = null;
					StoreVarbyMethodName (Resp);
					
					if (Resp.contains("<html"))
					{
					Document d = Jsoup.parse(Resp);
//				
					UpdateResult.ActualData = d.body().getElementsByTag(TC.InputData).text().toString();
					}	
					else
					{
						
					
					if (isJSONValid(Resp))
					{
						JsonPath resp = new JsonPath(Resp);
						actValues = new ArrayList<>();
						
						
						
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(resp.getString(keys[i]));
									actValues.add(resp.getString(keys[i]));
								}
								
								actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
							
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
								
							}
					}	
							if (isXMLValid(Resp))
							{
								XmlPath Xmlresp = new XmlPath(Resp);
								 actValues = new ArrayList<>();
											
									if(keys.length == expvalues.length){
										for (int i = 0; i < keys.length; i++) {
											Logs.Ulog(Xmlresp.getString(keys[i]));
											actValues.add(Xmlresp.getString(keys[i]));
										}
									}
									else{
										Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
									}
									
									actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
								
									
							}
							if (TC.Param1.toLowerCase().contains("text") | actArr== null)
							{
								
									
									
									UpdateResult.ActualData = Resp;
							}
							else
							{
								
								UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");	
							}
							
							
					}						
					
					StoreTable.put(TC.ExpectedData, UpdateResult.ActualData);
					return	UpdateResult.Done();
					
				}

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				
				public static String  validateResponce() throws IOException, InterruptedException, SQLException, ParseException
				{
					StoreTable.put("GeneratePDFReport" , "NN");
					StoreTable.put("CaptureScreeShotOnError" , "NN");
					StoreTable.put("CaptureScreeShotOnEachStep" , "NN");					

					
					Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
					Logs.Ulog(Resp);
					Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
					String[] actArr = null;
					ArrayList<String> actValues = null;
					StoreVarbyMethodName (Resp);
					StoreTable.put(TC.ExpectedData, UpdateResult.ActualData);
					if (Resp.contains("<html"))
					{
					Document d = Jsoup.parse(Resp);
//				
					UpdateResult.ActualData = d.body().getElementsByTag(TC.InputData).text().toString();
					}	
					else
					{
						
					
					if (isJSONValid(Resp))
					{
						JsonPath resp = new JsonPath(Resp);
						actValues = new ArrayList<>();
						
						
						
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(resp.getString(keys[i]));
									actValues.add(resp.getString(keys[i]));
								}
								
								actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
							
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
								
							}
					}	
							if (isXMLValid(Resp))
							{
								XmlPath Xmlresp = new XmlPath(Resp);
								 actValues = new ArrayList<>();
											
									if(keys.length == expvalues.length){
										for (int i = 0; i < keys.length; i++) {
											Logs.Ulog(Xmlresp.getString(keys[i]));
											actValues.add(Xmlresp.getString(keys[i]));
										}
									}
									else{
										Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
									}
									
									actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
								
									
							}
							if (TC.Param1.toLowerCase().contains("text") | actArr== null)
							{
								
									
									
									UpdateResult.ActualData = Resp;
							}
							else
							{
								
								UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");	
							}
							
							
					}						
					
					StoreTable.put(TC.ExpectedData, UpdateResult.ActualData); 
					
					
					 if( TC.InputData.isEmpty() && TC.ExpectedData.isEmpty())
					 {
						 UpdateResult.ActualData = Resp;
						 TC.ExpectedData = Resp;
					 }
					        
							return	UpdateResult.UpdateStatus();
					
				}
				
}

