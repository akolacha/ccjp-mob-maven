package Utility;

 
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import static Utility.R_Start.StoreTable;
import static Utility.HTMLResults.MsgBody;
public class GmailSendMail {

	
	public static File SendmailFile;
	public static BufferedWriter SendmailWrite;
//	public static  String SenMailFilepath = "C:\\SendMail.vbs";
	public static  String ModuleSenMailFilepath = System.getProperty("java.io.tmpdir")+"/ModuleSendMail.vbs";
	public static  String AllModuleSenMailFilepath = System.getProperty("java.io.tmpdir")+"/AllModuleSendMail.vbs";

public static String WriteEmail( String mbody , String SenMailFilepath  , String Modulename ,  String TCStatus) throws IOException
{
	
	   File SendmailFile = new File(SenMailFilepath); 		
	   SendmailFile.createNewFile();
	  
	    FileWriter FW = new FileWriter(SenMailFilepath);
	    SendmailWrite = new BufferedWriter(FW);
	    SendmailWrite.write("  On Error Resume Next"+"\n");
	        SendmailWrite.write(" Set iMsg = CreateObject(\"CDO.Message\")"+"\n");
	        SendmailWrite.write(" Set iConf = CreateObject(\"CDO.Configuration\")"+"\n");
     
			SendmailWrite.write("  iConf.Load -1  "+"\n");//' CDO Source Defaults
			SendmailWrite.write("   Set Flds = iConf.Fields "+"\n");
			SendmailWrite.write("  With Flds "+"\n");
			SendmailWrite.write(" .Item(\"http://schemas.microsoft.com/cdo/configuration/smtpusessl\") = True "+"\n");
			SendmailWrite.write(" .Item(\"http://schemas.microsoft.com/cdo/configuration/smtpauthenticate\") = 1 "+"\n");
			SendmailWrite.write(" .Item(\"http://schemas.microsoft.com/cdo/configuration/sendusername\") = \"" +StoreTable.get("FromEmailID")+"\""+"\n");
			SendmailWrite.write(".Item(\"http://schemas.microsoft.com/cdo/configuration/sendpassword\") = \""+StoreTable.get("FromEmailPassword")+"\""+"\n");
			SendmailWrite.write(" .Item(\"http://schemas.microsoft.com/cdo/configuration/smtpserver\") = \"" + StoreTable.get("Host")+"\""+"\n");
         
		    SendmailWrite.write(".Item(\"http://schemas.microsoft.com/cdo/configuration/sendusing\") = 2 "+"\n");
		    SendmailWrite.write(".Item(\"http://schemas.microsoft.com/cdo/configuration/smtpserverport\") =\""+ StoreTable.get("Port") +"\""+"\n");
		    SendmailWrite.write(".Update "+"\n");
			SendmailWrite.write("End With "+"\n");
			
			//SendmailWrite.write(" strbody = \"" + StoreTable.get("Body")+"\""+"\n");
		    
			SendmailWrite.write("  With iMsg "+"\n");
			SendmailWrite.write(" Set .Configuration = iConf "+"\n");
			SendmailWrite.write(" .To = \""+StoreTable.get("To")+"\""+"\n");
			SendmailWrite.write(".CC = \""+ StoreTable.get("CC")+"\""+"\n");
			SendmailWrite.write(" .BCC = \"" + StoreTable.get("BCC")+"\""+"\n");
			SendmailWrite.write(" .From = \"" +StoreTable.get("FromName")+"\""+"\n");
			SendmailWrite.write(" .Subject =\"" + Modulename +StoreTable.get("Subject")+UpdateResult.browserName +"_Ver_" + UpdateResult.version +"_"+ TCStatus +"\""+"\n");
			SendmailWrite.write(" .HTMLBody =\"" + mbody +"</table>"+StoreTable.get("Signature")+"\""+"\n");
			
			SendmailWrite.write(" .AddAttachment (\""  + ResultExcel.ResultExlFilepath+"\")"+"\n");
			SendmailWrite.write(" .Send"+"\n");     
			SendmailWrite.write("End With "+"\n");

			SendmailWrite.flush();
			SendmailWrite.close();
	return TC.PASS;
	
}


public static String WriteEmailEnd() throws IOException
{
//	  Date date = new Date();
//	SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy-h_mm_ss a");
//    String formattedDate = sdf.format(date);
//    formattedDate =  formattedDate.replace("-", "").replace(" ", "");
//	   Zip.zip(ResultExcel.ResPathFolderDate.getAbsolutePath(), formattedDate);
//			FileWriter FW = new FileWriter(SenMailFilepath);
//		    SendmailWrite = new BufferedWriter(FW);
//     
//			//SendmailWrite.write(" strbody = " + StoreTable.get("Body"));
//    
//			SendmailWrite.write("  With iMsg ");
//			SendmailWrite.write(" Set.Configuration = iConf ");
//			SendmailWrite.write(" .To = "+StoreTable.get("To"));
//			SendmailWrite.write(".CC = " + StoreTable.get("CC"));
//			SendmailWrite.write(" .BCC = "+ StoreTable.get("BCC"));
//			SendmailWrite.write(" .From = "+StoreTable.get("FromName"));
//			SendmailWrite.write(" .Subject = "+StoreTable.get("Subject"));
//			SendmailWrite.write(" .HTMLBody =  "+ MsgBody );
//			SendmailWrite.flush();
//			//SendmailWrite.write(" .AddAttachment =  " + ResultExcel.ResultExlFilepath);
//			//SendmailWrite.write(" .Send");      
//			SendmailWrite.write("End With ");
//
//			SendmailWrite.flush();
	return TC.PASS;
	
}

public static String WriteEmailOutLooKEmail(  String mbody , String SenMailFilepath  , String Modulename ,  String TCStatus) throws IOException
{
	
Properties props = new Properties();
if(StoreTable.get("AuthOutLookRequired").toString().equalsIgnoreCase("YY"))
{
	props.put("mail.smtp.auth", StoreTable.get("AuthOutLook"));
}
if(StoreTable.get("StarttlsEnableOutLookRequired").toString().equalsIgnoreCase("YY"))
{
	props.put("mail.smtp.starttls", StoreTable.get("StarttlsEnableOutLook"));
}
if(StoreTable.get("PortOutLookRequired").toString().equalsIgnoreCase("YY"))
{
	props.put("mail.smtp.port",(String)  StoreTable.get("PortOutLook"));
	
}

props.put("mail.smtp.host", (String)StoreTable.get("HostOutLook"));


Session session = Session.getInstance(props,
  new javax.mail.Authenticator() {
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication((String) StoreTable.get("FromEmailIDOutLook"), (String) StoreTable.get("FromEmailPasswordOutLook"));
    }
  });

try {

    Message message = new MimeMessage(session);
    message.setFrom(new InternetAddress((String) StoreTable.get("FromNameOutLook")));
    message.setRecipients(Message.RecipientType.TO,
        InternetAddress.parse((String) StoreTable.get("To")));
    message.setSubject( Modulename +StoreTable.get("Subject")+UpdateResult.browserName +"_Ver_" + UpdateResult.version +"_"+ TCStatus +"\""+"\n");
    //message.setContent(message, "text/html");
   
//    FileDataSource fds = new FileDataSource(ResultExcel.ResultExlFilepath);
//    message.setDataHandler(new DataHandler(fds));
//    message.setFileName(fds.getName());
    setFileAsAttachment (message , ResultExcel.ResultExlFilepath , mbody +"</table>"+StoreTable.get("Signature") );
   // message.setContent( mbody +"</table>"+StoreTable.get("Signature"),  "text/html");
    //message.setText( mbody +"</table>"+StoreTable.get("Signature"));
   // message.ATTACHMENT( ResultExcel.ResultExlFilepath+"\")"+"\n");
    Transport.send(message);

    System.out.println("OutLook Emil Sent -----   Done");

} catch (MessagingException e) {
    throw new RuntimeException(e);
}
return TC.PASS;

}
public static void setFileAsAttachment(Message msg, String filename ,String Mbody )
        throws MessagingException {

   // Create and fill first part
   MimeBodyPart p1 = new MimeBodyPart();
   p1.setContent( Mbody , "text/html");
  //p1.setText("This is part one of a test multipart e-mail." +           "The second part is file as an attachment");

   // Create second part
   MimeBodyPart p2 = new MimeBodyPart();

   // Put a file in the second part
   FileDataSource fds = new FileDataSource(filename);
   p2.setDataHandler(new DataHandler(fds));
   p2.setFileName(fds.getName());

   // Create the Multipart.  Add BodyParts to it.
   Multipart mp = new MimeMultipart();
   mp.addBodyPart(p1);
   mp.addBodyPart(p2);

   // Set Multipart as the message's content
   msg.setContent(mp);
}

public static String WriteEmailOutLookWithoutPort( String mbody , String SenMailFilepath  , String Modulename ,  String TCStatus) throws IOException
{
	
	   File SendmailFile = new File(SenMailFilepath); 		
	   SendmailFile.createNewFile();
	  
	    FileWriter FW = new FileWriter(SenMailFilepath);
	    SendmailWrite = new BufferedWriter(FW);
	    SendmailWrite.write("  On Error Resume Next"+"\n");
	    SendmailWrite.write(" Set OutApp = CreateObject(\"Outlook.Application\")"+"\n");
	    SendmailWrite.write(" 	    Set OutMail = OutApp.CreateItem(0)"+"\n");

	  
	    SendmailWrite.write("      With OutMail"+"\n");
//*****************HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	    
		
		SendmailWrite.write(" .To = \""+StoreTable.get("To")+"\""+"\n");
		SendmailWrite.write(" .CC = \""+ StoreTable.get("CC")+"\""+"\n");
		SendmailWrite.write(" .BCC = \"" + StoreTable.get("BCC")+"\""+"\n");
		SendmailWrite.write(" .Subject =\"" + Modulename +StoreTable.get("Subject")+UpdateResult.browserName +"_Ver_" + UpdateResult.version +"_"+ TCStatus +"\""+"\n");
		SendmailWrite.write(" .HTMLBody =\"" + mbody +"</table>"+StoreTable.get("Signature")+"\""+"\n");		
		
		
		SendmailWrite.write(" .Attachments.Add  (\""  +  ResultExcel.ResultExlFilepath+"\")"+"\n");
		
		if(StoreTable.get("AttachPDFReport").toString().equalsIgnoreCase("YY"))
		{
		SendmailWrite.write(" .Attachments.Add  (\""  +  HTMLResults.DetailedReportPDFFilePath+"\")"+"\n");
		}
		
		
		
		SendmailWrite.write(" .Send"+"\n"); 
	    	       
	    	        
	    	        SendmailWrite.write(" End With "+"\n");
	    	    
	    	   // SendmailWrite.write("On Error GoTo 0"+"\n");

	    	    SendmailWrite.write(" With Application  "+"\n");
	    	    SendmailWrite.write("  .EnableEvents = True  "+"\n");
	    	   SendmailWrite.write("  .ScreenUpdating = True    "+"\n");
	    	        		SendmailWrite.write("  End With "+"\n");

	    	        SendmailWrite.write(" Set OutMail = Nothing  "+"\n");
	    	        		SendmailWrite.write(" OutApp = Nothing"+"\n");

	    
	 
		

			SendmailWrite.flush();
			SendmailWrite.close();
	return TC.PASS;
	
}


}