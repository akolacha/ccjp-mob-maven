package ExcelDB;


import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Provides the methods for opening the excel file to perform 
 * the reading or writing to excel
 * @author ShivaPrakash
 *
 */
public class AbstractExcelConnection extends AbstractQueryBuilder{

	protected AbstractExcelConnection( boolean isWriteMode ) {
		m_isWriteMode = isWriteMode;
	}
	
	/**
	 * Opens the excel file application for reading.
	 * @param fileName
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 */
	@Override
	public void open( String fileName ) throws SQLException, IllegalArgumentException {
		if ( fileName == null || fileName.isEmpty() )
			throw new IllegalArgumentException("Excel file name cannot be empty");
		//DriverId=1046;UID=admin;FIL = Excel 12.0
		String connectionString = String.format("jdbc:odbc:Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DBQ=%s;ReadOnly=%d", 
				fileName, m_isWriteMode ? 0 : 1 );
		
		/*String connectionString = String.format("jdbc:odbc:Driver={Microsoft Excel Driver (*.xls)};DBQ=%s;ReadOnly=%d", 
				fileName, m_isWriteMode ? 0 : 1 );*/
		
				
		super.open(connectionString);
	}
	
	/**
	 * Returns true, if the connection should be opened in the write mode.
	 * @return boolean
	 */
	protected boolean getIsWriteMode( ) {
		return m_isWriteMode;
	}
	
	protected String convertToFilterExpression( HashMap<String,Object> values ) throws IllegalArgumentException {
		return convertToFilterExpression( values, "and" );
	}
	
	/**
	 * Converts a set of values into a filter expression.
	 * @param values
	 * @return String
	 * @throws IllegalArgumentException
	 */
	@SuppressWarnings("rawtypes")
	protected String convertToFilterExpression( HashMap<String,Object> values, String separator ) throws IllegalArgumentException {
		if( values.size() == 0 )
			throw new IllegalArgumentException("Filter values cannot be empty");
		
		StringBuffer sqlBuffer = new StringBuffer( );
		Set<Entry<String,Object>> entries = values.entrySet();
		int count = 0;
		for( Iterator<Entry<String,Object>> it = entries.iterator();it.hasNext(); ) {
			Entry<String,Object> current = it.next();
			if ( count++ > 0 )
				sqlBuffer.append(String.format( " %s ", separator) );
			
			Object value = current.getValue( );
			String key = current.getKey( );
			Class cls = value.getClass( );
			if ( cls == Integer.class || cls == Double.class || cls == Float.class || cls == Short.class ) {
				sqlBuffer.append( String.format( " [%s] = %s", key, value ) );
			}
			else {
				sqlBuffer.append( String.format( " [%s] = '%s'", key, value ) );
			}
		}
		return sqlBuffer.toString();
	}
	
	// Load the driver.
	static {
		try {
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	boolean m_isWriteMode = false;
}
