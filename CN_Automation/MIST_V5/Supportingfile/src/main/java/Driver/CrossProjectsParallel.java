package Driver;
/*********************************************************************************
/********************************************************************************
//Author : Raghavendra Pai & Santhosha H C
//Test   : CrossProject execution script V 1.0 
********************************************************************************
********************************************************************************/

import static Utility.R_Start.parentFolder;
import Utility.ExcelObj;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import Utility.*;


public class CrossProjectsParallel  {
	public static  ExcelObj Resource;
	 public static void main (String args[]) throws InterruptedException, IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
			File file = new File(System.getProperty("user.dir"));
			parentFolder = file.getParentFile();
	        Resource = new ExcelObj(parentFolder+ "/Projects/" + "GlobalSetup.xlsm");
	        String  Div_Prj =  Resource.getCellData("GlobalSetup", "ConfigValue" , 2);
	        String  Div_BR =  Resource.getCellData("GlobalSetup", "ConfigValue" , 3);
			String[] Prjlist = Resource.getCellData("GlobalSetup", "ConfigValue" , 5).split("," );
			String[] Brlist = Resource.getCellData("GlobalSetup", "ConfigValue" , 6).split("," );
			for( int Div =0 ; Div< Prjlist.length; Div++)
			{
				System.out.println("******** "+ Prjlist[Div] + " ********");
				Resource.setCellData("GlobalSetup", "ConfigValue", 2, Prjlist[Div]);
				Resource.setCellData("GlobalSetup", "ConfigValue", 3, Brlist[Div]);
				String Testdir2 ="cmd /c start " +System.getProperty("user.dir")+ "\\Run.bat";
				Runtime.getRuntime().exec(Testdir2);
				Thread.sleep(10000L);
				
			}
			Resource.setCellData("GlobalSetup", "ConfigValue", 2, Div_Prj);
			Resource.setCellData("GlobalSetup", "ConfigValue", 3, Div_BR);
			Thread.sleep(10000L);
		//TODO Auto-generated method stub
		 
	}
		
		 
	        
	    
}
