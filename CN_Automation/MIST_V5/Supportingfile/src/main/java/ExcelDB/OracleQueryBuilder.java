package ExcelDB;


/**
 * Initializes the Oracle driver and provides methods to execute queries on Oracle database.
 * @author ShivaPrakash
 * 
 */
public class OracleQueryBuilder extends AbstractQueryBuilder {
	
	// Initialize oracle driver.
	static {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
