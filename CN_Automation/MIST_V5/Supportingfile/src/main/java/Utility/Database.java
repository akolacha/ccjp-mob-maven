package Utility;


import static Utility.R_Start.StoreTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import OR_TestParm.OR;
 
public class Database  {
	public static  Connection con1 = null;
	private String dbName = null;
	 Connection con;
 

 
	public Database() throws SQLException
	{
		 try
		 {		
		  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		  String url =  (String) StoreTable.get("DbURL");
		 
	      this.con1 = DriverManager.getConnection(url+ ";user=" + StoreTable.get("UserName") + ";password=" + StoreTable.get("Password"));
	      
	      
		 }
		 catch(ClassNotFoundException e)
		 { 	
			Logs.Ulog("ERROR ----- Restore DB Class Not Found Exception ------  " + e.getMessage());
				
		 }	
		
		 
	}
 
	public static String RestoreDB() throws SQLException
	{
		try
		{
			Logs.Ulog("RestoreDB  Started");
			String Rdb = "RESTORE DATABASE " + StoreTable.get("DbName") +" FROM  DISK = " + StoreTable.get("FromDbPath") +" WITH REPLACE ";
			//String Rdb = "RESTORE DATABASE [timetrax_qa_selenium] FROM  DISK = N'F:\\MSSQL\\Backup\\timetrax_qa_Swapna\\timetrax_qa_Swapna_backup_2014_03_16_230013_9126280.bak' WITH REPLACE ";
			 con1.createStatement().execute(Rdb);
			 Logs.Ulog("Restore DB is Successfull");
			 
			return TC.PASS;
		}
		 
		catch(Throwable T)
		{
			Logs.Ulog("ERROR ----- Restore DB is Not Successfull - check if JBOSS is closed  -------  " + T.getMessage());
			return TC.FAIL;
		}
		
	}
	
	
	
	public static String ExecuteSQL() throws SQLException
	{
		
		try
		{
			Logs.Ulog("Execute SQL Started");
			 String Stmt = TC.InputData;
			 con1.createStatement().execute(Stmt);
		
			 
			 Logs.Ulog("Executed SQL Query Successfull");
			  return TC.PASS;
		}
		 
		catch(Throwable T)
		{
			Logs.Ulog("ERROR ----- Execute SQL Failed Check the Statement " + T.getMessage());
			return TC.FAIL;
		}
		
		
	}
	
	public static String GetRSSQL() throws SQLException
	{
		
		try
		{
			Logs.Ulog("Execute SQL GetRSSQL");
			 Class.forName("COM.ibm.db2.jdbc.app.DB2Driver");
			  String url =  (String) StoreTable.get("DbURL");
			 
			  Connection con1 = DriverManager.getConnection(url+ ";user=" + StoreTable.get("UserName") + ";password=" + StoreTable.get("Password"));
		      
		
			 Statement stmt = con1.createStatement();
			 ResultSet rs = stmt.executeQuery("TC.InputData");
			 
//			 UpdateResult.ActualData = (String) rs;
//					 TC.ExpectedData;
//			
//			UpdateDescription( "Enter text  "+ TC.ExpectedData +" in " );
//						return UpdateResult.UpdateStatus();
			 
			 Logs.Ulog("Executed SQL Query Successfull");
			  return TC.PASS;
		}
		 
		catch(Throwable T)
		{
			Logs.Ulog("ERROR ----- Execute SQL Failed Check the Statement " + T.getMessage());
			return TC.FAIL;
		}
		
		
	}
 
}
