package Controller;

import static Utility.GmailSendMail.AllModuleSenMailFilepath;
import static Utility.HTMLResults.AllModulesMsgBody;
import static Utility.R_Start.GenerateLogs;
import static Utility.R_Start.ModuleFinalScriptStartTime;
import static Utility.R_Start.PrintLogs;
import static Utility.R_Start.ResPathFolderOuter;
import static Utility.R_Start.Resourcedata;
import static Utility.R_Start.ReuseTableData;
import static Utility.R_Start.StoreTable;
import static Utility.R_Start.TTlogs;
import static Utility.TestNgReportsForDashboard.TestNgSteps;

/*
 ********************************************************************************
 ********************************************************************************
//  Automatiotn Selenium
//  Start date 1 Jan 2014
//  Author : R a g h a v e n d r a Pai & S a n t h o s h a H C
//  Test :     script V 3  

********************************************************************************
********************************************************************************/

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.commons.io.DirectoryWalker.CancelException;
import org.apache.log4j.Logger;

import OR_TestParm.OR;
import Utility.FunctionLibrary;
import Utility.GmailSendMail;
import Utility.HTMLResults;
import Utility.Logs;
import Utility.PrograssBar;
import Utility.R_Start;
import Utility.ResultExcel;
import Utility.ReusableFunctions;
import Utility.Setup;
import Utility.TC;
import Utility.TestNgReportsForDashboard;
import Utility.UpdateResult;
import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.model.Build;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;
import op2Utilities.TargetProcess;
/*
 ********************************************************************************
 ********************************************************************************
//  Automatiotn Selenium
//  Start date 2 Oct 2014
//  Author : R agha vendra Pai & S anthosha HC
//  Test :     script V 3  

********************************************************************************
********************************************************************************/
public class Controller{
	
	//Added on 13-1-17 by Avanish
	public static TestLinkAPI api;
	public static String TestlinkTesterAssigned;
	//till here
	public static Date date;
	public static PrograssBar Pbar;
	public static String  ModuleDriver;
	public static String TestLinkBuildName;
	public static String TestLinkBuildID;
	public static String TestLinkProjectID;
	public static String TestLinkPlanID;
	public static String TestLinkPlatform;
	public static String TestLinkServerURL;
	public static String TestLinkDevKey;
//	public static java.util.Date buildDate;
	public static int ModuleRow;
	public static int ModuleRowCount;
	/*
	 ********************************************************************************
	 ********************************************************************************
	
	//  Start date 27 july 2014
	//  Author : Raghavendra Pai & Santhosha H C
	//  Test :    Controller script V 1.0 
	 //  updated by S a n t hosha H C - July 15 2016
	//  Test :    Controller script V 2.0
	********************************************************************************
	********************************************************************************/
	
public static void  ExecuteModules() throws InterruptedException, IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, CancelException
{
		// TODO Auto-generated method stub
	 Date now = new Date();
	 TTlogs = Logger.getLogger("devpinoyLogger");
    /* System.setProperty("log4j.hours",  now.getHours());
     System.setProperty("log4j.minutes", new String(null, now.getMinutes()));
     System.setProperty("log4j.seconds", new String(null, now.getSeconds()));
     BasicConfigurator.resetConfiguration();
     PropertyConfigurator.configure("log4j.properties");
	*/
	
	

	
	try {

			FileInputStream output = new FileInputStream(System.getProperty("user.dir")+ "/src/main/java/Config.properties");
			Properties prop = new Properties();
		    prop.load(output);
		    OR.Createfiles();
		    if(prop.get("PrintLogs").equals("YY"))
			{
				//PrintLogs ="YY";
			}
			  
	
		// set the properties value
		if(prop.get("MinimizeAllWindows").equals("YY"))
		{			
				try
				{
					 //MinimizeAllWindows();	
				}
				catch 	(Throwable t) {
					Logs.Ulog("Error while -  Maximizing the window" +   t.getMessage());
				}				
		}
		
	} catch (Throwable t) {
		Logs.Ulog("Error while - reading  my configuration file / Config file not exist in the src folder  " + System.getProperty("user.dir")+ "/src/main/java/Config.properties" + t.getMessage());
	}	
			 
			
	
		
		
	
	
	    ReuseTableData = new Hashtable();
		StoreTable = new Hashtable();
		
	    ReusableFunctions.LoadTestData();
	    Setup.LoadSetup();
	    Setup.LoadGlobalSetup();
	    FunctionLibrary.EnableProxyGlobal();
	    
	    
	    

	    try {

	    	try {
		    if(((String) StoreTable.get("PrintLogs")).equalsIgnoreCase("YY"))
			{
				PrintLogs ="YY";
			}
		    if(((String) StoreTable.get("GenerateLogs")).equalsIgnoreCase("YY"))
			{
		    	GenerateLogs = "YY";
			}
		    
	    	} 
		    catch 	(Throwable t) {
				Logs.Ulog("Error while -  MinimizeAllWindows" +   t.getMessage());
			}	
		
			try
			{
		// set the properties value
			if( ((String) StoreTable.get("MinimizeAllWindows")).equalsIgnoreCase("YY"))
			{			
					try
					{
						 MinimizeAllWindows();	
					}
					catch 	(Throwable t) {
						Logs.Ulog("Error while -  MinimizeAllWindows" +   t.getMessage());
					}				
    		}
			}
			catch 	(Throwable t) {
				Logs.Ulog("Error while -  MinimizeAllWindows" +   t.getMessage());
			}	
		
	} catch (Throwable t) {
		Logs.Ulog("Error while - reading  my configuration file / Config file not exist in the src folder  " + System.getProperty("user.dir")+ "/src/main/java/Config.properties" + t.getMessage());
	}	
			 
	    
	    
	    
	    date = new Date();   
	    SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy-h_mm_ss a");
	    String formattedDate = sdf.format(date);
	    formattedDate =  formattedDate.replace("-", "").replace(" ", "");
	    
	    
	    File ResPathFolder =  new File( StoreTable.get("ResultPathExel") +  "\\SeleniumAutomationResult" );
	    ResPathFolder.mkdir();
	 
	    ResPathFolderOuter =  new File( ResPathFolder.getAbsolutePath()+"\\Result_" +TC.ProjectName +"_" + formattedDate );
	    ResPathFolderOuter.mkdir();
	   
	    
	    
		Pbar = new PrograssBar();
		Pbar.CreateProgressBar();
		date = new Date();
		
		HTMLResults.FWGenerateAllModulesMainReport();
	    
		ModuleRowCount = Resourcedata.getRowCount("Modules");
		date = new Date();
	    ModuleFinalScriptStartTime = sdf.format(date);
	    //Added by Avanish on 13-12
	    if(((String)StoreTable.get(TC.TestLinkUpdateStatusConfigProp)).equalsIgnoreCase("YY")){
	    	TestLinkDevKey=(String) StoreTable.get(TC.TestLinkDevkeyConfigProp);
		    TestLinkServerURL=(String) StoreTable.get(TC.TestLinkServerURLConfigProp);
		    TestlinkTesterAssigned=(String) StoreTable.get(TC.TestLinkAssignTCToTesterConfigProp);
		    /*Boolean checkTestLinkConfiguration=createTestLinkAPIObject(TestLinkDevKey,TestLinkServerURL); 
		    if(checkTestLinkConfiguration==null){
		    	System.out.println("Testlink URL or Devkey is not proper. Please check the URL and devKey and reexecute.");
		    	System.exit(0);
		    }*/
	    }
	    //till here
	    /*TestLinkDevKey=(String) StoreTable.get(TC.TestLinkDevkeyConfigProp);
	    TestLinkServerURL=(String) StoreTable.get(TC.TestLinkServerURLConfigProp);*/
	for (ModuleRow = 2 ; ModuleRow <= ModuleRowCount; ModuleRow++ )
	{
		
		
				
				ModuleDriver = Resourcedata.getCellData(TC.ModulesSheet,
						TC.TestModuleNameCol, ModuleRow);
				//updated 30/12
				TestLinkBuildName=Resourcedata.getCellData(TC.ModulesSheet,
						TC.TestLinkBuildNameCol, ModuleRow);
				TestLinkProjectID=Resourcedata.getCellData(TC.ModulesSheet,
						TC.TestLinkProjectIDCol, ModuleRow);
				TestLinkPlanID=Resourcedata.getCellData(TC.ModulesSheet,
						TC.TestLinkPlanIDCol, ModuleRow);
				TestLinkPlatform=Resourcedata.getCellData(TC.ModulesSheet,
						TC.TestLinkPlatformCol, ModuleRow);
				TC.ExecuteModule = Resourcedata.getCellData(TC.ModulesSheet,
						TC.ExecuteModuleCol, ModuleRow);
				TC.ExecuteModule = TC.ExecuteModule.toUpperCase();
				TC.Dependency = Resourcedata.getCellData(TC.ModulesSheet,
						TC.DependencyModuleCol, ModuleRow);
				 new  TestNgReportsForDashboard();
				 TestNgSteps = new ArrayList<String>();
				if (TC.ExecuteModule.equalsIgnoreCase(TC.ExecuteModuleYes))
				{
					//create build for testlink call here
					 if(((String)StoreTable.get(TC.TestLinkUpdateStatusConfigProp)).equalsIgnoreCase("YY")){
						 
						 DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
						 Date buildDate = new Date();

						//for generating build name with current date comment if not required
//						 TestLinkBuildName=TestLinkBuildName+"_"+dateFormat.format(buildDate);
//							TestLinkBuildID=createBuild(TestLinkPlanID,TestLinkBuildName,TestLinkServerURL,TestLinkDevKey);
						 TargetProcess TP= new TargetProcess();
						 TestLinkBuildID=String.valueOf(TP.getBuild((int)Double.parseDouble(TestLinkPlanID), TestLinkBuildName));
							TP.getBuild((int)Double.parseDouble(TestLinkPlanID), TestLinkBuildName);	
						 
						 if(TestLinkBuildID==null){
								System.out.println("Exiting the execution... build is not created!");
								System.exit(0);
							}
						    }
				
					if(  TC.Dependency.equalsIgnoreCase(TC.ExecuteTestYes) )
					{
							if (UpdateResult.ModuleFinalStatus.equalsIgnoreCase("PASS") )
							{			
										       ResultExcel.FW_CreateResultFile();
											   R_Start ST = new R_Start(ModuleDriver);								
												ST.initialize();					        
					         }
							else
							{
								//is skipped
								TC.SkipModule = true;
								HTMLResults.UpdateModuleMainReport();
								TC.SkipModule =false;
								
							}
					
					}
					else
					{
						if (TC.ExecuteModule.equalsIgnoreCase(TC.ExecuteModuleYes))
						{
							       ResultExcel.FW_CreateResultFile();
								   R_Start ST = new R_Start(ModuleDriver);								
								   ST.initialize();					
							
				         } //End of if loop
					}
					
				 } 
				
				UpdateResult.FinalStatus = "PASS";
				TC.ExecuteModule = Resourcedata.getCellData(TC.ModulesSheet,
						TC.ExecuteModuleCol, ModuleRow);
				//Update TesngXml report for dashboard
				if (TC.ExecuteModule.equalsIgnoreCase(TC.ExecuteModuleYes))
				{
				TestNgReportsForDashboard.WriteTestNgSteps();
				}
	}
	
	HTMLResults.UpdateTotalModuleMainReport();
	//TestNgReportsForDashboard.WriteTestNgEndReport();
	
	GmailSendMail Sm = new  GmailSendMail();
	Sm.WriteEmail(AllModulesMsgBody , AllModuleSenMailFilepath , "" , UpdateResult.ModuleFinalStatus);
	
	if(StoreTable.get("SendMailGmail").toString().equalsIgnoreCase("YY"))
	{
	  Process p = Runtime.getRuntime().exec("cmd.exe /C start " +AllModuleSenMailFilepath);
	}
	if(StoreTable.get("SendMailOutLook").toString().equalsIgnoreCase("YY"))
	{
	  Process p = Runtime.getRuntime().exec("cmd.exe /C start " +AllModuleSenMailFilepath);
	}
	if(StoreTable.get("SendMailOutLookWithoutPort").toString().equalsIgnoreCase("YY"))
	{
		Sm.WriteEmailOutLookWithoutPort(AllModulesMsgBody , AllModuleSenMailFilepath , "" , UpdateResult.ModuleFinalStatus);
	    Process p = Runtime.getRuntime().exec("cmd.exe /C start " +AllModuleSenMailFilepath);
	}

	
	
	
	try {
		
		File file = new File(OR.myConfigfilepathTemp);
		file.delete();
		file = new File(OR.myResourcetemp);
		file.delete();
		file = new File(OR.mySetupPathTemp);
		file.delete();
	
	} catch (Throwable t) {
		Logs.Ulog("Error in while deleting temp files " + t.getMessage() );
	}

}//End of module for loop

	
	public static String MinimizeAllWindows()
	{

		try {
			
			Process p = Runtime.getRuntime().exec("cmd.exe /C start " +System.getProperty("user.dir")+ "\\src\\main\\java\\MinimizeAllWindows.exe");
	
			
			Thread.sleep(200);
		
			return TC.PASS;
		
		} catch (Throwable t) {
			return TC.PASS;
		}

	}	
	
	
public static String createBuild(String testPlanID,String testbuildName, String Server_URL, String DevKey) throws TestLinkAPIException, MalformedURLException{
        
        int planID=(int)Double.parseDouble(testPlanID);
        Build testBuildNumber;
        String buildName = new String();
        String buildNotes = new String();
        String planName = new String();
        buildNotes = "Automation Build for Mobile Execution";
        
        buildName = "A-"+System.getProperty("buildID");
                                   
        System.out.println(buildName);
        
        TestLinkAPI api = new TestLinkAPI(new URL(Server_URL), DevKey);
        
        try{
               api.createBuild(planID, testbuildName, buildNotes);
               testBuildNumber = api.getLatestBuildForTestPlan(planID);
               Build []build=api.getBuildsForTestPlan(planID);
               for(Build itr:build){
            	   if(itr.getName().toString().equals(testbuildName)){
            		   return itr.getId().toString();
            	   }
               }
               return null;
               
        }
        catch(Exception e){
        	 System.out.println("Exception occured in createBuild method"+e.getMessage());
 
             return null;
        }

 }
//Added by Avanish on 13-1-17
public static boolean createTestLinkAPIObject(String DevKey, String serverURL){
	 
	 try {
		 api=new TestLinkAPI(new URL(serverURL), DevKey);
		 
	} catch (TestLinkAPIException | MalformedURLException e) {
		api=null;
		e.printStackTrace();
	}
	 return true;
}
//till here 
}
