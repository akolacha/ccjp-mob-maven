package Utility;

 
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static Controller.Controller.ModuleDriver;
import static Utility.R_Start.StoreTable;

public class ALM {

	
	public static File ALMFile;
	public static BufferedWriter ALMWrite;
 
	
	static Date date = new Date();
	static SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy : h-mm-ss a");
    static String formattedDate = sdf.format(date);
    public static String  ALMFilepath =  System.getProperty("java.io.tmpdir")+"ALM_TESTID_"+ WebServices.Round( TC.AlmTestID) +"_RUNS_APPENDR" + formattedDate.replace("-", "").replace(" ", "").replace(":", "")+".vbs";

      

	
	public static void main(String[]args)throws IOException {
		
		new  ALM().ALMRunAppender();
	
		
	}
	

public static String ALMRunAppender() throws IOException
{
	try
	{
		
	
		
	   File ALMFile = new File(ALMFilepath); 		
	   ALMFile.createNewFile();
	   
	    FileWriter FW = new FileWriter(ALMFilepath);
	    ALMWrite = new BufferedWriter(FW);
		
	       

	        ALMWrite.write(" On Error Resume Next"+"\n");
	        	
	        ALMWrite.write("    Set tdc = CreateObject(\"TDAPIOLE80.TDConnection\")"+"\n");

 ALMWrite.write(" tdc.InitConnectionEx \"" +StoreTable.get("ALM_URL")+"\""+"\n");
 ALMWrite.write(" tdc.Login \""+StoreTable.get("ALM_User") + "\",\""+ StoreTable.get("ALM_Password") +"\""+"\n");
 ALMWrite.write(" tdc.Connect \""+StoreTable.get("ALM_Domain")+ "\",\""+ StoreTable.get("ALM_Project")+"\""+"\n");

 ALMWrite.write(" If (tdc.Connected <> True) Then"+"\n");
 ALMWrite.write(" MsgBox \"qc project failed to connect to \" & \"Project\""+"\n");
 ALMWrite.write(" WScript.Quit"+"\n");
 ALMWrite.write(" End If"+"\n");

 ALMWrite.write(" Set tfact = tdc.testSetFactory"+"\n");
 ALMWrite.write(" Set tsTreeMgr = tdc.TestSetTreeManager"+"\n");
 ALMWrite.write(" Set tcTreeMgr = tdc.TreeManager"+"\n");
 ALMWrite.write(" npath = \""+StoreTable.get("ALM_TestSet_RootFolderPath")+"\""+"\n");
 ALMWrite.write(" Set TestSetFolder = tsTreeMgr.NodeByPath(npath)"+"\n");

 ALMWrite.write(" Set TestSetF = TestSetFolder.testSetFactory 'Retreive test from given folder in test lab"+"\n");
 ALMWrite.write(" Set aTestSetArray = TestSetF.NewList(\"\")"+"\n");
 ALMWrite.write(" tsSet_cnt = aTestSetArray.Count"+"\n");
 ALMWrite.write(" For i = 1 To tsSet_cnt ' Loop through the Test Sets to pick the desired test Set"+"\n");
 ALMWrite.write("   Set tstests = aTestSetArray.Item(i)"+"\n");
 ALMWrite.write("  TestSet_Name = tstests.Name"+"\n");
   
 ALMWrite.write(" If TestSet_Name = \""+StoreTable.get("ALM_TestSet_Name")+"\" Then"+"\n");
	 		
 ALMWrite.write("  Set TestCaseF = tstests.TSTestFactory 'Retreive Test Cases from the test set"+"\n");
 ALMWrite.write("  Set aTestCaseArray = TestCaseF.NewList(\"\")"+"\n");
 ALMWrite.write("  test_qc_cnt = aTestCaseArray.Count"+"\n");
 ALMWrite.write("  For n = 1 To test_qc_cnt 'Loop through the Test cases in QC to find the same test case as in Excel"+"\n");
 ALMWrite.write("    Set ts_obj = aTestCaseArray.Item(n) "+"\n");
 ALMWrite.write(" tname_QC = ts_obj.Test.ID "+"\n");
 
 ALMWrite.write("  If tname_QC = "+ WebServices.Round( TC.AlmTestID) + " Then "+"\n");
 ALMWrite.write("runName = ts_obj.RunFactory.UniqueRunName "+"\n");
 ALMWrite.write("Set RunF = ts_obj.RunFactory ' for managing test runs. "+"\n");
 ALMWrite.write(" Set theRun = RunF.AddItem(runName) "+"\n");
 ALMWrite.write(" theRun.Name = runName 'assign a run name "+"\n");
 if( UpdateResult.GlobalStatus.equals("PASS"))
{
	 ALMWrite.write(" theRun.Status = \"Passed\" 'you can parametrize this value as per your need "+"\n");
}
else if (UpdateResult.GlobalStatus.toString().equals("FAIL"))
 {
	 ALMWrite.write(" theRun.Status = \"Failed\" 'you can parametrize this value as per your need "+"\n");
}
else
{
	 ALMWrite.write(" theRun.Status = \"No Run\" 'you can parametrize this value as per your need "+"\n");
}
 




 //ALMWrite.write("  theRun.CopyDesignSteps "+"\n");
 ALMWrite.write(" If \""+ TC.ALM_UpdateStatus_HTMLAttchment +"\" = \"YY" + "\" Then "+"\n");
 ALMWrite.write(" Set attf = theRun.Attachments "+"\n");
 ALMWrite.write(" Set run2attach = attf.AddItem(Null) "+"\n");
 ALMWrite.write(" run2attach.Filename = \""+HTMLResults.DetailedReportFilePath + "\" "+"\n");
 ALMWrite.write(" run2attach.Type = 1"+"\n");
 ALMWrite.write(" run2attach.Post"+"\n");
 ALMWrite.write(" End If "+"\n");
 
 ALMWrite.write("  If \""+ TC.ALM_UpdateStatus_XLSAttchment +"\" = \"YY" + "\" Then "+"\n");
 ALMWrite.write(" Set attf = theRun.Attachments "+"\n");
 ALMWrite.write(" Set run2attach = attf.AddItem(Null) "+"\n");
 ALMWrite.write(" run2attach.Filename = \""+ResultExcel.ResultExlFilepath + "\" "+"\n");
 ALMWrite.write(" run2attach.Type = 1"+"\n");
 ALMWrite.write("  run2attach.Post"+"\n");
 ALMWrite.write(" End If "+"\n");
 
 
 ALMWrite.write("  If \""+ TC.ALM_UpdateStatus_PDFAttchment +"\" = \"YY" + "\" Then "+"\n");
 ALMWrite.write(" Set attf = theRun.Attachments "+"\n");
 ALMWrite.write(" Set run2attach = attf.AddItem(Null) "+"\n");
 ALMWrite.write(" run2attach.Filename = \""+HTMLResults.DetailedReportPDFFilePath +"\" "+"\n");
 ALMWrite.write(" run2attach.Type = 1"+"\n");
 ALMWrite.write(" run2attach.Post"+"\n");
 ALMWrite.write(" End If "+"\n");
 
 
 ALMWrite.write(" theRun.Post "+"\n");

 ALMWrite.write(" End If "+"\n");
 ALMWrite.write("    Next "+"\n");
 
 ALMWrite.write("      End If "+"\n");
 ALMWrite.write(" Next "+"\n");


 ALMWrite.write(" tdc.Disconnect "+"\n");
 ALMWrite.write(" tdc.Logout "+"\n");
 ALMWrite.write(" tdc.ReleaseConnection"+"\n");


ALMWrite.flush();
ALMWrite.close();
	return TC.PASS;
	}
	catch (Throwable e) {
		 Logs.Ulog("Error While ALM RUN FILE  Statement " + e.getMessage() );
		 return TC.PASS;
	}
	
}


public static String ALMStepScreenAttach() throws IOException
{
	try
	{
		
	
		
	   File ALMFile = new File(ALMFilepath); 		
	   ALMFile.createNewFile();
	   
	    FileWriter FW = new FileWriter(ALMFilepath);
	    ALMWrite = new BufferedWriter(FW);
		
	       

	        ALMWrite.write(" On Error Resume Next"+"\n");
	        	
	        ALMWrite.write("    Set tdc = CreateObject(\"TDAPIOLE80.TDConnection\")"+"\n");

 ALMWrite.write(" tdc.InitConnectionEx \"" +StoreTable.get("ALM_URL")+"\""+"\n");
 ALMWrite.write(" tdc.Login \""+StoreTable.get("ALM_User") + "\",\""+ StoreTable.get("ALM_Password") +"\""+"\n");
 ALMWrite.write(" tdc.Connect \""+StoreTable.get("ALM_Domain")+ "\",\""+ StoreTable.get("ALM_Project")+"\""+"\n");

 ALMWrite.write(" If (tdc.Connected <> True) Then"+"\n");
 ALMWrite.write(" MsgBox \"qc project failed to connect to \" & \"Project\""+"\n");
 ALMWrite.write(" WScript.Quit"+"\n");
 ALMWrite.write(" End If"+"\n");

 ALMWrite.write(" Set tfact = tdc.testSetFactory"+"\n");
 ALMWrite.write(" Set tsTreeMgr = tdc.TestSetTreeManager"+"\n");
 ALMWrite.write(" Set tcTreeMgr = tdc.TreeManager"+"\n");
 ALMWrite.write(" npath = \""+StoreTable.get("ALM_TestSet_RootFolderPath")+"\""+"\n");
 ALMWrite.write(" Set TestSetFolder = tsTreeMgr.NodeByPath(npath)"+"\n");

 ALMWrite.write(" Set TestSetF = TestSetFolder.testSetFactory 'Retreive test from given folder in test lab"+"\n");
 ALMWrite.write(" Set aTestSetArray = TestSetF.NewList(\"\")"+"\n");
 ALMWrite.write(" tsSet_cnt = aTestSetArray.Count"+"\n");
 ALMWrite.write(" For i = 1 To tsSet_cnt ' Loop through the Test Sets to pick the desired test Set"+"\n");
 ALMWrite.write("   Set tstests = aTestSetArray.Item(i)"+"\n");
 ALMWrite.write("  TestSet_Name = tstests.Name"+"\n");
   
 ALMWrite.write(" If TestSet_Name = \""+StoreTable.get("ALM_TestSet_Name")+"\" Then"+"\n");
	 		
 ALMWrite.write("  Set TestCaseF = tstests.TSTestFactory 'Retreive Test Cases from the test set"+"\n");
 ALMWrite.write("  Set aTestCaseArray = TestCaseF.NewList(\"\")"+"\n");
 ALMWrite.write("  test_qc_cnt = aTestCaseArray.Count"+"\n");
 ALMWrite.write("  For n = 1 To test_qc_cnt 'Loop through the Test cases in QC to find the same test case as in Excel"+"\n");
 ALMWrite.write("    Set ts_obj = aTestCaseArray.Item(n) "+"\n");
 ALMWrite.write(" tname_QC = ts_obj.Test.ID "+"\n");
 
 ALMWrite.write("  If tname_QC = "+ WebServices.Round( TC.AlmTestID) + " Then "+"\n");
 ALMWrite.write("  Set attachmentFac = ts_obj.Attachments  "+"\n");
 ALMWrite.write("  Set attachment = attachmentFac.AddItem(Null) "+"\n");
 ALMWrite.write(" attachment.Filename = \""+ UpdateResult.CurrentFilepath + "\" "+"\n");
 ALMWrite.write("  attachment.Type = 1  "+"\n");
 ALMWrite.write("  attachment.Post "+"\n");


 ALMWrite.write(" End If "+"\n");
 ALMWrite.write("    Next "+"\n");
 
 ALMWrite.write("      End If "+"\n");
 ALMWrite.write(" Next "+"\n");


 ALMWrite.write(" tdc.Disconnect "+"\n");
 ALMWrite.write(" tdc.Logout "+"\n");
 ALMWrite.write(" tdc.ReleaseConnection"+"\n");


ALMWrite.flush();
ALMWrite.close();
	return TC.PASS;
	}
	catch (Throwable e) {
		 Logs.Ulog("Error While ALM RUN FILE  Statement " + e.getMessage() );
		 return TC.PASS;
	}
	
}



	

}