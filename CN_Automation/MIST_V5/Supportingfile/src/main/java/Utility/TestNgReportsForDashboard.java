package Utility;

import static Controller.Controller.ModuleDriver;
import static Utility.R_Start.StoreTable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

public class TestNgReportsForDashboard {

	/**
	 * @param args
	 */
	
	public static String TestNG_XML_forDashboardFolderPath;
	public static String TestNG_XML_forDashboardFilePath;
	public static BufferedWriter TSTNG_XML_Write;
	public static  List<String> TestNgSteps;
	public static String ModuleTestNgReportData = "";
	public static String TestNgAllReportData = "";
	public static String TestNgReportData ="";
	public static String TestNgReportDataEndTag;
	public static SimpleDateFormat   DATE_FORMAT;
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public TestNgReportsForDashboard() throws IOException
	{
		File ResPathFolder =  new File( StoreTable.get("ResultPathHTML") +  "\\SeleniumAutomationResult" );
		ResPathFolder.mkdir();
		
		File TestSetFolder =new File( ResPathFolder.getAbsolutePath() + "\\" + "TestNG_XML_forDashboard");
		TestSetFolder.mkdir();
		TestNG_XML_forDashboardFolderPath = TestSetFolder.getAbsolutePath();
		 Date date = new Date();
		 SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy-h_mm_ss a");
	    String formattedDate = sdf.format(date);
		TestNG_XML_forDashboardFilePath = TestNG_XML_forDashboardFolderPath + "\\" +"testng-results"+formattedDate+".xml";
		 //  MainResultFilePath = TestSetFolderPath + "\\" + "Main Automated Test Result"+ "Browser Nme_ver "+".html";
	   File XMLFile= new File( TestNG_XML_forDashboardFilePath); 
	   XMLFile.createNewFile();
	   
	   FileWriter FW = new FileWriter(TestNG_XML_forDashboardFilePath);
	   TSTNG_XML_Write = new BufferedWriter(FW);	
		
	   TSTNG_XML_Write.write( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<testng-results>\n");
	
	}
	
	public static void updateTestNG_XmlFIleModuleStatus() throws IOException
	{
		
		ModuleDriver = ModuleDriver.replace("_", "-");
		 DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
			       		
		ModuleTestNgReportData = "\n<suite name=\"" + ModuleDriver +  "\" duration-ms=\"" + R_Start.ModuleFinaltotalTime + "\" started-at=\"" + R_Start.TngFinalScriptStartTime + "\" finished-at=\""+ R_Start.TngScriptEndTime +"\">\n";
		
	}
	
	public static void updateTestNG_XmlFIleTestStatus() throws IOException
	{
		
		
		TestNgReportData = TestNgReportData + "<test name=\""  + TC.TestModuleName +"\" duration-ms=\"" + R_Start.totalTime +"\" started-at=\"" +  R_Start.TngScriptStartTime  +"\" finished-at=\"" +  R_Start.TngScriptEndTime  + "\">\n";
		TestNgReportData = TestNgReportData + "<class>\n";
		TestNgReportData = TestNgReportData + "<test-method status=\"" + UpdateResult.GlobalStatus +"\" name=\""+ TC.TestModuleName + "\" duration-ms=\"" + R_Start.totalTime +"\" started-at=\"" + R_Start.TngScriptStartTime  +"\" finished-at=\"" +  R_Start.TngScriptEndTime  + "\">\n";
		TestNgReportData = TestNgReportData + "<reporter-output>\n";
		 
		 
	}
	public static void updateTestNG_XmlEndTags() throws IOException
	{	 
		
		TestNgReportData = TestNgReportData + "\n</reporter-output>\n" ;
		TestNgReportData = TestNgReportData + "</test-method>\n";
		TestNgReportData = TestNgReportData + "</class>\n";
		TestNgReportData = TestNgReportData + "</test>\n";
		

	}
	
	public static void StoreTestNgSteps() throws IOException
	{	
		
		

	  for( int i= 0; i < TestNgSteps.size() ; i++ )	
	  {		
          
		  TestNgReportData = TestNgReportData + "<line>"  +StringEscapeUtils.escapeXml( TestNgSteps.get(i).toString().replaceAll("[^\\p{ASCII}]", "").toString().replaceAll("[^\\w\\s-]", "") ) + "</line>\n";

	  }
	  
	  
	}
	
	public static void WriteTestNgSteps() throws IOException
	{	
		if(StoreTable.get("GenerateTestNgxmlFile").toString().equalsIgnoreCase("YY"))
		{
			TestNgAllReportData = TestNgAllReportData + ModuleTestNgReportData  + TestNgReportData+ "\n</suite>\n";
			//TestNgReportData = ModuleTestNgReportData + TestNgReportData ;
//TestNgReportData = TestNgReportData + "\n</suite>\n"; 
		   TSTNG_XML_Write.write( TestNgAllReportData );
		   
		   TSTNG_XML_Write.flush();
		   TestNgReportData = "";
		   ModuleTestNgReportData = "";
		}   
		   
	}
	public static void WriteTestNgEndReport() throws IOException
	{
		   
		 TSTNG_XML_Write.write( TestNgAllReportData+ "\n</testng-results>\n");
		 TSTNG_XML_Write.flush();
		 File	source = new File(TestNG_XML_forDashboardFilePath);
		 File    dest = new File((String) StoreTable.get("CopyTestNgXmlToLocaton")); 


		  // 	copyFileUsingFileChannels(source, dest);
	}
	

	private static void copyFileUsingFileChannels(File source, File dest)			throws IOException {

		FileChannel inputChannel = null;

		FileChannel outputChannel = null;

		try {

			inputChannel = new FileInputStream(source).getChannel();
			outputChannel = new FileOutputStream(dest).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());

		} finally {

		//	inputChannel.close();
		//	outputChannel.close();

		}

	}




}
