package Controller;
import static Utility.R_Start.StoreTable;
import static Utility.FunctionLibrary.driver;
import static Utility.R_Start.Appiumserver;
import static Utility.R_Start.SeleniumLocalServer;

import org.openqa.selenium.Platform;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.server.DriverFactory;
import org.openqa.selenium.server.RemoteControlConfiguration;
import org.openqa.selenium.server.SeleniumServer;

import Utility.FunctionLibrary;
import Utility.Logs;
import Utility.TC;
import Utility.UpdateResult;

import com.machinepublishers.jbrowserdriver.JBrowserDriver;
import com.machinepublishers.jbrowserdriver.Settings;
import com.machinepublishers.jbrowserdriver.Timezone;
import com.thoughtworks.selenium.Selenium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

import java.io.File;
import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.Set;
import java.util.concurrent.TimeUnit;
 
public class Server {
    public static SeleniumServer server;
    public static void startSeleniumServer() throws Exception {
 
       try {
     ServerSocket serverSocket = new ServerSocket(RemoteControlConfiguration.DEFAULT_PORT);
     serverSocket.close();
             //Server not up, start it
             try {
              RemoteControlConfiguration RCConfig = new RemoteControlConfiguration();
              RCConfig.setPort(RemoteControlConfiguration.DEFAULT_PORT);
              SeleniumLocalServer = new SeleniumServer(false, RCConfig);
 
             } catch (Exception T) {
            	 Logs.Ulog("Error While startSeleniumServer " + T.getMessage());
             }
             try {
            	 SeleniumLocalServer.start();
              
              Logs.Ulog("started SeleniumServer " );
             } 
             catch (Exception T) {
            	 Logs.Ulog("Error While startSeleniumServer " + T.getMessage());
             }
             
         } catch (BindException T) {
             
             Logs.Ulog("Selenium server already up and running " + T.getMessage());
         }
 }
 
 public static void stopSeleniumServer(Selenium selenium){
 selenium.stop();
 if (server != null)
       {
          try
          {
          selenium.shutDownSeleniumServer();
          SeleniumLocalServer.stop();
 
          SeleniumLocalServer = null;
          }
          catch (Exception T)
          {
        	  Logs.Ulog("Error While stopSeleniumServer " + T.getMessage());
          }
       }
 }
 
 
 public static String MagicDriver() throws IOException, InterruptedException
 {
	
	 
	 try {

			if (StoreTable.get("Execute On Grid").equals("YY")) {

				DesiredCapabilities capability = DesiredCapabilities.phantomjs();
				driver = new RemoteWebDriver(new URL((String) StoreTable.get("Set Grid IP")), capability);
				UpdateResult.UpDateBrowserVer();

			} else {
			
				File file = new File(System.getProperty("user.dir") + "/tools/Magic/bin/phantomjs.exe");						 
	            System.setProperty("phantomjs.binary.path", file.getAbsolutePath());		
	               driver = new PhantomJSDriver();
				
	               return UpdateResult.UpDateBrowserVer();
			}

		} catch (Throwable T) {
			Logs.Ulog("Error While Launching browser MagicDriver" + T.getMessage());
			return FunctionLibrary.CatchStatementWebElement(T.getMessage());

		}
	 return UpdateResult.Done();
	 
 }
 
 public static String HeadlessDriver() throws IOException, InterruptedException 
 {
	
	 try {

			if (StoreTable.get("Execute On Grid").equals("YY")) {

				DesiredCapabilities capability = DesiredCapabilities.htmlUnit();
				driver = new RemoteWebDriver(new URL((String) StoreTable.get("Set Grid IP")), capability);
				UpdateResult.UpDateBrowserVer();

			} else {
				server.start();
				DesiredCapabilities cap = DesiredCapabilities.htmlUnitWithJs();
				cap.setJavascriptEnabled(true);
				
				driver= new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),cap);
				
				return UpdateResult.UpDateBrowserVer();
			}

		} catch (Throwable T) {
			Logs.Ulog("Error While Launching browser HeadlessDriver" + T.getMessage());
			return FunctionLibrary.CatchStatementWebElement(T.getMessage());

		}
	 return UpdateResult.Done();
	
 }
 
 public static String jbrowserdriver() throws IOException, InterruptedException
 {
	
	 
	 try {

			if (StoreTable.get("Execute On Grid").equals("YY")) {

				DesiredCapabilities capabilities =        new DesiredCapabilities("jbrowserdriver", "1", Platform.ANY);

				    // Optionally customize the settings
				    capabilities.merge(
				        Settings.builder().
				        buildCapabilities());

				  
				driver = new RemoteWebDriver(new URL((String) StoreTable.get("Set Grid IP")), capabilities);
				UpdateResult.UpDateBrowserVer();

			} else {
			
				// driver = new JBrowserDriver();	
				 JBrowserDriver driver = new JBrowserDriver(Settings.builder().
					      timezone(Timezone.AMERICA_NEWYORK).build());
	              
				
	               return UpdateResult.UpDateBrowserVer();
			}

		} catch (Throwable T) {
			Logs.Ulog("Error While Launching browser MagicDriver" + T.getMessage());
			return FunctionLibrary.CatchStatementWebElement(T.getMessage());

		}
	 return UpdateResult.Done();
	 
 }
 
 
 public static String SwitchtoWebView() throws IOException, InterruptedException
 {
     try{
    	 
	 Set<String> contextNames = ((AppiumDriver) driver).getContextHandles();
     for (String contextName : contextNames) {
         if (contextName.contains("WEBVIEW"))
            ((AppiumDriver) driver).context(contextName);
        
     }
     return UpdateResult.Done();
     
    } catch (Throwable T) {
		Logs.Ulog("Error While SwitchtoWebView" + T.getMessage());
		return FunctionLibrary.CatchStatementWebElement(T.getMessage());

	}
   
	
 }

public static String SwitchtoNativeView() throws IOException, InterruptedException {
	try{
	 Set<String> contextNames = ((AppiumDriver) driver).getContextHandles();
	 for (String contextName : contextNames)
	 {
	     if (contextName.contains("NATIVE"))
	         ((AppiumDriver) driver).context(contextName);
	 }
	     return UpdateResult.Done();
	 
	 } catch (Throwable T) {
			Logs.Ulog("Error While SwitchtoNativeView" + T.getMessage());
			return FunctionLibrary.CatchStatementWebElement(T.getMessage());

		}
}


public static String StartAppiumServer() throws IOException, InterruptedException {
	try{
		 Appiumserver = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
        .usingDriverExecutable(new File(System.getProperty("user.dir") + "/lib/Appium/node.exe"))
        .withAppiumJS(new File(System.getProperty("user.dir") + "/lib/Appium/appium.js"))
        .withIPAddress("127.0.0.1")
        .usingPort(4723)
        .withStartUpTimeOut(30, TimeUnit.SECONDS));
		 
		  return UpdateResult.Done();
	 } catch (Throwable T) {
			Logs.Ulog("Error While StartAppiumServer" + T.getMessage());
			return FunctionLibrary.CatchStatementWebElement(T.getMessage());

		}
}

 
 
}