package Utility;


/*
 ********************************************************************************
 ********************************************************************************

//  Start date 7 Jan 2014
//  Author : Raghavendra Pai & Santhosha HC
//  Test :    Test Constant script V 1.9 
//Last updated by santhosha on proxy settings and other settings

********************************************************************************
********************************************************************************/

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static Utility.FunctionLibrary.driver;
import static Utility.R_Start.S_Obj;
import static Utility.R_Start.StoreTable;
import static Utility.R_Start.Resourcedata;







import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import com.jayway.restassured.RestAssured;
import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

import OR_TestParm.OR;
import Utility.Logs;
import static Utility.R_Start.Testdata;
import static Utility.R_Start.TTlogs;
public class TC extends OR
{	

	

	public TC() throws InterruptedException {
		super();
		// TODO Auto-generated constructor stub
	}
	public static int stepCtr=0;
	public static int failStepCtr=0;
	public static double testFailPercent=0;
	public static String DriverSheet = "Driver";
	public static String ModulesSheet = "Modules";
	public static String ReusableFunctionsSheet = "ReusableFunctions";
	public static String TestDataSheet = "TestData";
	public static String  JavaScriptsSheet = "JavaScripts";
	public static String  WindowsObjectsSheet = "WindowsObjects";
	public static RestAssured WebDriverMock; 		
	public static String FunctionAreaCol = "FunctionArea";
	public static String ExecuteModuleYes = "YY";
	public static String ExecuteTestYes = "YY";
	public static String StopTestYes = "YY";
	public static String ExecuteModule;
	public static String SettingsYes = "YY";
	public static String SettingsNo="NN";
	public static String DD=null;
	public static String TempFolder = System.getProperty("java.io.tmpdir");
	
	public static String TCIDCol = "TCID";
	
	public static String TestDescriptionCol = "TestDescription";
	public static String KeywordCol = "Keyword";
	public static String InputDataCol = "InputData";
	public static String ExpectedDataCol = "ExpectedData";
	public static String PassDescriptionCol = "PassDescription";
	public static String FailDescriptionCol = "FailDescription";	
	public static String ExecuteCol = "Execute";
	public static String OnFailExitCol = "OnFailExit";
	public static String TestObjectsCol = "TestObjects";
	
	public static String ModuleCol = "Module";
	public static String TestModuleNameCol = "TestModuleName";
	public static String TestScenarioNameCol = "TestScenarioName";
	public static String RequirementsCol = "Requirements";
	public static String ExecuteModuleCol = "ExecuteModule";
	public static String DependencyModuleCol = "Dependency";
	
	//Added by Avanish for testlink integration
		//Server URL and devkey present in global sheet
		public static String TestLinkServerURLConfigProp = "TestLinkServerURL";
		public static String TestLinkDevkeyConfigProp = "TestLinkDevKey";
		public static String TestLinkPlanIDCol = "TestLink_PlanID";
		public static String TestLinkProjectIDCol = "TestLink_ProjectID";
		public static String TestLinkTCIDCol = "TestLink_TCID";
		public static String TestLinkTCVersionCol = "TestLink_TCVersion";
		public static String TestLinkExternalIDCol = "TestLink_ExternalID";
		public static String ApplicationBuildNumberCol = "ApplicationBuildNumber";
		public static String TestLinkPlatformCol = "TestLinkPlatform";
		public static String flagUpdateresultTotestLinkCol="UpdateToTestLink";
		public static String flagUpdateresultTotestLink;
		public static String TestLinkTCVersion;
		public static String TestLinkTCID;
		public static String TestLinkExternalID;
		//added on 13-1-17
		public static String TestLinkUpdateStatusConfigProp = "TestLinkUpdateStatus";
		public static String TestLinkAssignTCToTesterConfigProp = "AssignTestCasesToTester";
		//till here
		public static String TestLinkStatus;
		public static String TestLinkBuildNameCol="TestLinkBuildName";
		
		
		//added till here by Avanish
	public static String AlmTestIDCol = "AlmTestId";
	public static String MoveNextCol = "MoveNext";
	
	public static String TCID;
	public static String Requirements;
	public static String TestDescription;
	public static String Keyword ;
	public static String ExecuteTest; 
	public static String InputData;
	public static String PassDescription;
	public static String FailDescription;
	public static String ExpectedData;
	public static String OnFailExit;
	public static String TestObjects;
	public static boolean  SkipModule; 
	public static boolean  SkipTestCase; 
	public static String Module;
	public static String TestModuleName;	
	public static String Dependency = "YY";
	public static String AlmTestID;
	public static String ALM_UpdateStatus_HTMLAttchment = "NN";
	public static String ALM_UpdateStatus_XLSAttchment = "NN";
	public static String ALM_UpdateStatus_PDFAttchment = "NN";
	public static String Param1Col = "Param1";
	public static String Param2Col = "Param2";
	public static String Param3Col = "Param3";
	public static String Param4Col = "Param4";
	public static String Param5Col = "Param5";
	public static String Param6Col = "Param6";
	public static String Param7Col = "Param7";
	public static String Param8Col = "Param8";
	public static String Param9Col = "Param9";
	public static String Param10Col = "Param10";
	public static String Param11Col = "Param11";
	public static String Param12Col = "Param12";
	public static String Param13Col = "Param13";
	public static String Param14Col = "Param14";
	public static String Param15Col = "Param15";
	public static String Param16Col = "Param16";
	public static String Param17Col = "Param17";
	public static String Param18Col = "Param18";
	public static String Param19Col = "Param19";
	public static String Param20Col = "Param20";
	public static String Param21Col = "Param21";
	public static String Param22Col = "Param22";
	public static String Param23Col = "Param23";
	public static String Param24Col = "Param24";
	public static String Param25Col = "Param25";
	public static String Param26Col = "Param26";
	public static String Param27Col = "Param27";
	public static String Param28Col = "Param28";
	public static String Param29Col = "Param29";
	public static String Param1;
	public static String Param2;
	public static String Param3;
	public static String Param4;
	public static String Param5;
	public static String Param6;
	public static String Param7;
	public static String Param8;
	public static String Param9;
	public static String Param10;
	public static String Param11;
	public static String Param12;
	public static String Param13;
	public static String Param14;
	public static String Param15;
	public static String Param16;
	public static String Param17;
	public static String Param18;
	public static String Param19;
	public static String Param20;
	public static String Param21;
	public static String Param22;
	public static String Param23;
	public static String Param24;
	public static String Param25;
	public static String Param26;
	public static String Param27;
	public static String Param28;
	public static String Param29;
	
	public static String TestFolder = "TestScript.SmokeTest.";
	public static String PASS = "PASS";
	public static String FAIL = "FAIL";
	public static String BLOCK = "BLOCK";
	public static int PageLoadTime = 10;	
	public static String RANDOM_VALUE;
	public static String ObjectRepositorySheet = "ObjectRepository";
	public static String SetupSheet = "Setup";
	public static String GloablSetupSheet = "GlobalSetup";
	public static String SelectProjectResourceSheet = "SelectProjectResource";
	public static String FindByCol = "FindBy";
	public static String PropertiesCol = "Properties";
	public static String FindByProp;
	public static String FindByPropVal;
	public static String WinObjectNameCol = "WinObjectName";
	public static String WinTitleCol="WinTitle";
	public static String ControlTextCol="ControlText";
	public static String ControlIDCol="ControlID";	
	public static String WinObjectName;
	public static String WinTitle;
	public static String ControlText;
	public static String ControlID;
	
	
	public static String ConfigurationsCol = "Configurations";
	public static String SelectCol = "Select";
	public static String ConfigValuecol = "ConfigValue";
	public static String ResourceFileNameCol = "ResourceFileName";
	public static String Configurations;
	public static String ConfigValue;
	
	public static String ObjectNameCol = "ObjectName";
	public static String ObjectName;
	public static String BrowserName;
	//public static WebElement Tobj;
	public static String PauseExecution = "NN";
	public static String TestDataPath;
	
	public static void LoadTestData(int Testrow) throws IOException, InterruptedException
	{		
		ExecuteTest =  Testdata.getCellData(TC.TestModuleName, ExecuteCol, Testrow);
		ExecuteTest =  ExecuteTest.toUpperCase();
		
		TCID =  Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, TCIDCol, Testrow)));	
		Module =  Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, ModuleCol, Testrow)));
		Requirements = Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, RequirementsCol, Testrow)));
		TestDescription = Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, TestDescriptionCol, Testrow)));	
		Keyword =  Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, KeywordCol, Testrow)));
		
		InputData  = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, InputDataCol, Testrow)));
		
		ExpectedData =Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, ExpectedDataCol, Testrow)));
		
		PassDescription  = Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, PassDescriptionCol, Testrow)));
		FailDescription  = Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, FailDescriptionCol, Testrow)));
		OnFailExit = Testdata.getCellData(TC.TestModuleName, OnFailExitCol, Testrow);
		TestObjects =  Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, TestObjectsCol, Testrow)));	
		
		
		Param1 = Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, Param1Col, Testrow)));
		Param2 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param2Col, Testrow)));
		Param3 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param3Col, Testrow)));
		Param4 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param4Col, Testrow)));
		Param5 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param5Col, Testrow)));
		Param6 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param6Col, Testrow)));
		Param7 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param7Col, Testrow)));
		Param8 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param8Col, Testrow)));
		Param9 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param9Col, Testrow)));
		Param10 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param10Col, Testrow)));
		Param11 = Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, Param11Col, Testrow)));
		Param12 = Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, Param12Col, Testrow)));
		Param13 = Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, Param13Col, Testrow)));
		Param14 = Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, Param14Col, Testrow)));
		Param15 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param15Col, Testrow)));
		Param16 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param16Col, Testrow)));
		Param17 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param17Col, Testrow)));
		Param18 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param18Col, Testrow)));
		Param19 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param19Col, Testrow)));
		Param20 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param20Col, Testrow)));
		Param21 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param21Col, Testrow)));
		Param22 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param22Col, Testrow)));
		Param23 =Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, Param23Col, Testrow)));
		Param24 =Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, Param24Col, Testrow)));
		Param25 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param25Col, Testrow)));
		Param26 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param26Col, Testrow)));
		Param27 =Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, Param27Col, Testrow)));
		Param28 = Dataswipe(ismodifydata( Testdata.getCellData(TC.TestModuleName, Param28Col, Testrow)));
		Param29 =Dataswipe(ismodifydata(  Testdata.getCellData(TC.TestModuleName, Param29Col, Testrow)));		
		
				 
		if (!Keyword.equals("storeEval") && !Keyword.equals("store") && !TestObjects.isEmpty() )
		{
			getORObject(TestObjects);
			getWinORObject(TestObjects);
		}
			
//*********************************************************************** 

		
		 Logs.Ulog("Test Data Loaded Successfully on Row number " + Testrow  );	 
		 
	}	
	public static void NoexcepLoadTestData(int Testrow) throws IOException, InterruptedException
	{		
		
		ExecuteTest =  Testdata.getCellData(TC.TestModuleName, ExecuteCol, Testrow);
		ExecuteTest =  ExecuteTest.toUpperCase();
		TCID =   NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, TCIDCol, Testrow)));	
		Module =   NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, ModuleCol, Testrow)));
		Requirements =  NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, RequirementsCol, Testrow)));
		TestDescription =  NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, TestDescriptionCol, Testrow)));	
		Keyword =   NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, KeywordCol, Testrow)));
		
		InputData  =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, InputDataCol, Testrow)));
		
		ExpectedData = NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, ExpectedDataCol, Testrow)));
		
		PassDescription  =  NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, PassDescriptionCol, Testrow)));
		FailDescription  =  NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, FailDescriptionCol, Testrow)));
		OnFailExit = Testdata.getCellData(TC.TestModuleName, OnFailExitCol, Testrow);
		TestObjects =   NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, TestObjectsCol, Testrow)));	
		
		
		Param1 =  NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, Param1Col, Testrow)));
		Param2 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param2Col, Testrow)));
		Param3 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param3Col, Testrow)));
		Param4 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param4Col, Testrow)));
		Param5 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param5Col, Testrow)));
		Param6 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param6Col, Testrow)));
		Param7 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param7Col, Testrow)));
		Param8 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param8Col, Testrow)));
		Param9 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param9Col, Testrow)));
		Param10 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param10Col, Testrow)));
		Param11 =  NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, Param11Col, Testrow)));
		Param12 =  NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, Param12Col, Testrow)));
		Param13 =  NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, Param13Col, Testrow)));
		Param14 =  NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, Param14Col, Testrow)));
		Param15 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param15Col, Testrow)));
		Param16 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param16Col, Testrow)));
		Param17 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param17Col, Testrow)));
		Param18 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param18Col, Testrow)));
		Param19 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param19Col, Testrow)));
		Param20 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param20Col, Testrow)));
		Param21 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param21Col, Testrow)));
		Param22 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param22Col, Testrow)));
		Param23 = NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, Param23Col, Testrow)));
		Param24 = NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, Param24Col, Testrow)));
		Param25 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param25Col, Testrow)));
		Param26 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param26Col, Testrow)));
		Param27 = NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, Param27Col, Testrow)));
		Param28 =  NoExpReuseDataswipe(NoExpReuseismodifydata( Testdata.getCellData(TC.TestModuleName, Param28Col, Testrow)));
		Param29 = NoExpReuseDataswipe(NoExpReuseismodifydata(  Testdata.getCellData(TC.TestModuleName, Param29Col, Testrow)));		
		
				 
		if (!Keyword.equals("storeEval") && !Keyword.equals("store") && !TestObjects.isEmpty() )
		{
			getORObject(TestObjects);
			getWinORObject(TestObjects);
		}
//*********************************************************************** 

		
		 Logs.Ulog("Test Data Loaded Successfully on Row number " + Testrow  );	 
		 
	}	

	
	public static Selenium TS_Obj()
	{
	
		  	S_Obj = new WebDriverBackedSelenium(driver, driver.getCurrentUrl());
	
	     return S_Obj;
		   
	}
	
	public static void LoadReuseTestData(int Testrow) throws IOException, InterruptedException
	{	
		
		ExecuteTest =  Resourcedata.getCellData(TC.TestModuleName, ExecuteCol, Testrow);
		ExecuteTest =  ExecuteTest.toUpperCase();
		TCID =  ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, TCIDCol, Testrow)));	
		Module =   ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, ModuleCol, Testrow)));
		Requirements = ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, RequirementsCol, Testrow)));
		TestDescription = ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, TestDescriptionCol, Testrow)));	
		Keyword =  ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, KeywordCol, Testrow)));
		
		InputData  = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, InputDataCol, Testrow)));
		
		ExpectedData =ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, ExpectedDataCol, Testrow)));
		
		PassDescription  = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, PassDescriptionCol, Testrow)));
		FailDescription  =  ReuseDataswipe(Reuseismodifydata(Resourcedata.getCellData(TC.TestModuleName, FailDescriptionCol, Testrow)));
		OnFailExit = Resourcedata.getCellData(TC.TestModuleName, OnFailExitCol, Testrow);
		TestObjects =  ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, TestObjectsCol, Testrow)));	
		
		
		Param1 = ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param1Col, Testrow)));
		Param2 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param2Col, Testrow)));
		Param3 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param3Col, Testrow)));
		Param4 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param4Col, Testrow)));
		Param5 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param5Col, Testrow)));
		Param6 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param6Col, Testrow)));
		Param7 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param7Col, Testrow)));
		Param8 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param8Col, Testrow)));
		Param9 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param9Col, Testrow)));
		Param10 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param10Col, Testrow)));
		Param11 = ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param11Col, Testrow)));
		Param12 = ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param12Col, Testrow)));
		Param13 = ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param13Col, Testrow)));
		Param14 = ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param14Col, Testrow)));
		Param15 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param15Col, Testrow)));
		Param16 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param16Col, Testrow)));
		Param17 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param17Col, Testrow)));
		Param18 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param18Col, Testrow)));
		Param19 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param19Col, Testrow)));
		Param20 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param20Col, Testrow)));
		Param21 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param21Col, Testrow)));
		Param22 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param22Col, Testrow)));
		Param23 =ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param23Col, Testrow)));
		Param24 =ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param24Col, Testrow)));
		Param25 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param25Col, Testrow)));
		Param26 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param26Col, Testrow)));
		Param27 =ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param27Col, Testrow)));
		Param28 = ReuseDataswipe(Reuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param28Col, Testrow)));
		Param29 =ReuseDataswipe(Reuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param29Col, Testrow)));	
		
		
		//*********************************************************************** 
		if (!Keyword.equals("storeEval") && !Keyword.equals("store") && !TestObjects.isEmpty() )
		{
			ReusegetORObject(TestObjects);
			getWinORObject(TestObjects);
		}
		
		      Logs.Ulog("Reusable Test Data Loaded Successfully on Row number " + Testrow  );
		 
		 
	}
	
	
	
	public static void NoexcepLoadReuseTestData(int Testrow) throws IOException, InterruptedException
	{		
		
		ExecuteTest =  Resourcedata.getCellData(TC.TestModuleName, ExecuteCol, Testrow);
		ExecuteTest =  ExecuteTest.toUpperCase();
		TCID =  NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, TCIDCol, Testrow)));	
		Module =   NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, ModuleCol, Testrow)));
		Requirements = NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, RequirementsCol, Testrow)));
		TestDescription = NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, TestDescriptionCol, Testrow)));	
		Keyword =  NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, KeywordCol, Testrow)));
		
		InputData  = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, InputDataCol, Testrow)));
		
		ExpectedData =NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, ExpectedDataCol, Testrow)));
		
		PassDescription  = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, PassDescriptionCol, Testrow)));
		FailDescription  =  NoExpReuseDataswipe(NoExpReuseismodifydata(Resourcedata.getCellData(TC.TestModuleName, FailDescriptionCol, Testrow)));
		OnFailExit = Resourcedata.getCellData(TC.TestModuleName, OnFailExitCol, Testrow);
		TestObjects =  NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, TestObjectsCol, Testrow)));	
		
		
		Param1 = NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param1Col, Testrow)));
		Param2 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param2Col, Testrow)));
		Param3 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param3Col, Testrow)));
		Param4 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param4Col, Testrow)));
		Param5 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param5Col, Testrow)));
		Param6 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param6Col, Testrow)));
		Param7 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param7Col, Testrow)));
		Param8 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param8Col, Testrow)));
		Param9 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param9Col, Testrow)));
		Param10 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param10Col, Testrow)));
		Param11 = NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param11Col, Testrow)));
		Param12 = NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param12Col, Testrow)));
		Param13 = NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param13Col, Testrow)));
		Param14 = NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param14Col, Testrow)));
		Param15 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param15Col, Testrow)));
		Param16 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param16Col, Testrow)));
		Param17 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param17Col, Testrow)));
		Param18 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param18Col, Testrow)));
		Param19 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param19Col, Testrow)));
		Param20 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param20Col, Testrow)));
		Param21 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param21Col, Testrow)));
		Param22 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param22Col, Testrow)));
		Param23 =NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param23Col, Testrow)));
		Param24 =NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param24Col, Testrow)));
		Param25 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param25Col, Testrow)));
		Param26 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param26Col, Testrow)));
		Param27 =NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param27Col, Testrow)));
		Param28 = NoExpReuseDataswipe(NoExpReuseismodifydata( Resourcedata.getCellData(TC.TestModuleName, Param28Col, Testrow)));
		Param29 =NoExpReuseDataswipe(NoExpReuseismodifydata(  Resourcedata.getCellData(TC.TestModuleName, Param29Col, Testrow)));	
		
		
		//*********************************************************************** 
		if (!Keyword.equals("storeEval") && !Keyword.equals("store") && !TestObjects.isEmpty() )
		{
			ReusegetORObject(TestObjects);
			getWinORObject(TestObjects);
		}
		
		      Logs.Ulog("Reusable Test Data Loaded Successfully on Row number " + Testrow  );
		 
		 
	}
	
	public static String ismodifydata( String TData ) throws IOException, InterruptedException
	{
		 String result = StringEscapeUtils.unescapeJava(TData) ;
		 
		try
		{
		if (TData.contains("${"))
		{	    String str = TData;
		        Pattern pattern = Pattern.compile("(\\$\\{)(.*?)(\\})");
		  //      Pattern pattern = Pattern.compile("(\\[)(.*?)(\\])");
		       
		        Matcher matcher = pattern.matcher(str);

		        List<String> listMatches = new ArrayList<String>();

		        while(matcher.find())
		        {
		            listMatches.add(matcher.group(2));
		        }

		        for(String s : listMatches)
		        {		         
		        	  String  TT1 = (String) StoreTable.get(s);
			          str = str.replaceAll("\\$\\{"+s+"\\}" , TT1 ); 
		        }
		       
		        return    str;
		     
		}
		 
	       
		}
		catch (Exception e) {
			 Logs.Ulog("Error Unble to find the vaule. Please the store the value and retrive with ${var} " + e.getMessage() );
			 UpdateResult.ActualData = TC.FAIL;
				TC.FailDescription ="Error Unble to find the vaule. Please the store the value and retrive with ${var} ---- " + TData + " ----- "+ e.getMessage();
				TC.ExpectedData = "Unable to Find the value in the stor table or error while Stroe";
				UpdateResult.ActualData = "Unable to Find the value" + TData;
				if(TC.ExecuteTest.equalsIgnoreCase("YY"))		
					UpdateResult.UpdateStatus();
		}
		return result;	
		
	}
	
	public static String Reuseismodifydata( String TData ) throws IOException, InterruptedException
	{
		 String result =StringEscapeUtils.unescapeJava( TData ) ;
		try
		{	
			
			
		if (TData.contains("${"))
		{	    String str = TData;
		        Pattern pattern = Pattern.compile("(\\$\\{)(.*?)(\\})");
		  //      Pattern pattern = Pattern.compile("(\\[)(.*?)(\\])");
		       
		        Matcher matcher = pattern.matcher(str);

		        List<String> listMatches = new ArrayList<String>();

		        while(matcher.find())
		        {
		            listMatches.add(matcher.group(2));
		        }

		        for(String s : listMatches)
		        {
		         
		        	  String  TT1 = (String) StoreTable.get(s);
			          str = str.replaceAll("\\$\\{"+s+"\\}" , TT1 ); 
		        
	
		        }
		       
		       
		        return   str;  
		}
		 
	        
		}
		catch (Exception e) {
			 Logs.Ulog("Error Unble to find the vaule. Please the store the value and retrive with ${var} " + e.getMessage() );
			 UpdateResult.ActualData = TC.FAIL;
				TC.FailDescription ="Error Unble to find the vaule. Please the store the value and retrive with ${var} ---- " + TData + " ----- "+ e.getMessage();
				
				if(TC.ExecuteTest.equalsIgnoreCase("YY"))		
					UpdateResult.UpdateStatus();
		}
		return result;	
		
	}
	
	public static String NoExpReuseismodifydata( String TData ) throws IOException, InterruptedException
	{
		 String result = StringEscapeUtils.unescapeJava(TData) ;
		try
		{	
			
			
		if (TData.contains("${"))
		{	    String str = TData;
		        Pattern pattern = Pattern.compile("(\\$\\{)(.*?)(\\})");
		  //      Pattern pattern = Pattern.compile("(\\[)(.*?)(\\])");
		       
		        Matcher matcher = pattern.matcher(str);

		        List<String> listMatches = new ArrayList<String>();

		        while(matcher.find())
		        {
		            listMatches.add(matcher.group(2));
		        }

		        for(String s : listMatches)
		        {
		         
		        	  String  TT1 = (String) StoreTable.get(s);
			          str = str.replaceAll("\\$\\{"+s+"\\}" , TT1 ); 
		        
	
		        }
		       
		       
		        return   str;  
		}
		 
	        
		}
		catch (Exception e) {
			 Logs.Ulog("Error  in Function 'NoExpReuseismodifydata' - Unble to find the vaule. Please the store the value and retrive with ${var} " + e.getMessage() );
			
		}
		return result;	
		
	}
	public static String ReuseDataswipe( String TData ) throws IOException, InterruptedException 
	{
		try
		{
			
		
		 
		 
		
		 if (TData.contains(":~"))
			{
	        	
	        	String[] GetReusedata = TData.split(":~");
				StoreTable.put(GetReusedata[0].trim(), GetReusedata[1].trim()); 
				TData=GetReusedata[0].trim();
				
			}
		 if (TData.startsWith("RD_"))
			{ 
			 TData =(String) StoreTable.get(TData);
					
			}
		 
		 if (TData.startsWith("D_"))
			{ TData =(String) StoreTable.get(TData);
					
			}
		 return TData;
		}
		
		catch(Throwable t)
		{
			 Logs.Ulog("Error Unble to find the vaule. Chceck The test data veriable name exits in Test data sheet , Please store proper test data variable . Please store the value and retrive with ${var} ---- " + TData + " ----- " + t.getMessage() );
			 UpdateResult.ActualData = TC.FAIL;
				TC.FailDescription ="Error Unble to find the vaule.Chceck The test data veriable name exits in Test data sheet . Please store proper test data variable . Please store the value and retrive with ${var} ---- " + TData + " ----- "+ t.getMessage();
				
				if(TC.ExecuteTest.equalsIgnoreCase("YY"))		
					UpdateResult.UpdateStatus();
				
		}
		
		 return TData;
	}
	
	
	public static String NoExpReuseDataswipe( String TData ) throws IOException 
	{
		try
		{		 
		
		 if (TData.contains(":~"))
			{
	        	
	        	String[] GetReusedata = TData.split(":~");
				StoreTable.put(GetReusedata[0].trim(), GetReusedata[1].trim()); 
				TData=GetReusedata[0].trim();
				
				
			}
		 if (TData.startsWith("RD_"))
			{ 
			 TData =(String) StoreTable.get(TData);
					
			}
		 
		 if (TData.startsWith("D_"))
			{ TData =(String) StoreTable.get(TData);
					
			}
		 
		 return TData;
		}
		
		catch(Throwable t)
		{
			 Logs.Ulog("Error in function 'NoExpReuseDataswipe' Unble to find the vaule. Chceck The test data veriable name exits in Test data sheet , Please store proper test data variable . Please the value and retrive with ${var} ---- " + TData + " ----- " + t.getMessage() );
			 
		}
		
		 return TData;
	}
	public static String Dataswipe( String TData ) throws IOException, InterruptedException 
	{
		try
		{
		 
	
		 if (TData.contains(":~"))
			{
	        	
	        	String[] GetReusedata = TData.split(":~");
				StoreTable.put(GetReusedata[0].trim(), GetReusedata[1].trim()); 
				TData=GetReusedata[0].trim();
				
			}
	
		 if (TData.startsWith("RD_"))
			{ 
			 TData =(String) StoreTable.get(TData);
					
			}
		 
		 if (TData.startsWith("D_"))
			{ TData =(String) StoreTable.get(TData);
					
			}
		
		 return TData;
		}
		catch(Throwable t)
		{
			
			 Logs.Ulog("Error in function 'Dataswipe' Unble to find the vaule. Chceck The test data veriable name exits in Test data sheet , Please store proper test data variable . Please store the value and retrive with ${var} ---- " + TData + " ----- " + t.getMessage() );
			 UpdateResult.ActualData = TC.FAIL;
				TC.FailDescription ="Error Unble to find the vaule.Chceck The test data veriable name exits in Test data sheet . Please store proper test data variable . Please store the value and retrive with ${var} ---- " + TData + " ----- "+ t.getMessage();
		if(TC.ExecuteTest.equalsIgnoreCase("YY"))		
				UpdateResult.UpdateStatus();
				
			
		}
		return TData;
		 
	
	}
	
	
}
