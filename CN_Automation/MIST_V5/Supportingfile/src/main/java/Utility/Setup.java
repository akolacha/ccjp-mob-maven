package Utility;

import static Utility.R_Start.ORTable;
import static Utility.R_Start.SetupData;
import static Utility.R_Start.parentFolder;
import static Utility.R_Start.GlobalSetupData;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Hashtable;

import static Utility.R_Start.StoreTable;
public class Setup {

	public Setup() {
		
	}

	public static void LoadSetup() throws UnknownHostException
	{
		
		int SRC = SetupData.getRowCount(TC.SetupSheet);
		for(int SRow = 1 ; SRow <=SRC;  SRow++ )
		{		
				TC.Configurations = SetupData.getCellData(TC.SetupSheet, TC.ConfigurationsCol , SRow);
				TC.ConfigValue= SetupData.getCellData(TC.SetupSheet, TC.ConfigValuecol , SRow);	
				 
				 //Logs.Ulog("Get Property Type "  + TC.FindByProp + " Property Value  " + TC.FindByPropVal )  ;
				 
				StoreTable.put(TC.Configurations, TC.ConfigValue );
		}
		
		
        StoreTable.put("LoginUser", System.getProperty("user.name"));		
		InetAddress address = InetAddress.getLocalHost(); 
	    String hostIP = address.getHostAddress().toString();
	    String hostName = address.getHostName().toString();

	    StoreTable.put("LoginMachineIP", hostIP);
		StoreTable.put("LoginMachine", hostName);
		
		Logs.Ulog("Started Loading LoadTestData");
		
		int RFRowNum = 2;
		int tclo = 0;
		String TDValue = null;
		

		
		Logs.Ulog("End of Loading LoadTestData");
		
	}	
	
	
	public static void LoadWindowsObjects() {
		
	
		int ORRC = GlobalSetupData.getRowCount(TC.WindowsObjectsSheet);
		for (int ORRow = 1; ORRow <= ORRC; ORRow++) {

			TC.WinObjectName = GlobalSetupData.getCellData(TC.WindowsObjectsSheet,
					TC.WinObjectNameCol, ORRow);
			TC.WinTitle = GlobalSetupData.getCellData(TC.WindowsObjectsSheet,
					TC.WinTitleCol, ORRow);
			TC.ControlText = GlobalSetupData.getCellData(TC.WindowsObjectsSheet,
					TC.ControlTextCol, ORRow);
			
			TC.ControlID = GlobalSetupData.getCellData(TC.WindowsObjectsSheet,
					TC.ControlIDCol, ORRow);			

			StoreTable.put(TC.WinObjectName, TC.WinTitle+ "~" +TC.ControlText  + "~" + TC.ControlID);

		}
		 

	}
	
	
	public static void LoadGlobalSetup() throws UnknownHostException
	{
		
		int SRC = GlobalSetupData.getRowCount(TC.GloablSetupSheet);
		for(int SRow = 1 ; SRow <=SRC;  SRow++ )
		{		
				TC.Configurations = GlobalSetupData.getCellData(TC.GloablSetupSheet, TC.ConfigurationsCol , SRow);
				TC.ConfigValue= GlobalSetupData.getCellData(TC.GloablSetupSheet, TC.ConfigValuecol , SRow);	
				 
				 //Logs.Ulog("Get Property Type "  + TC.FindByProp + " Property Value  " + TC.FindByPropVal )  ;
				 
				StoreTable.put(TC.Configurations, TC.ConfigValue );
		}
		
		
        StoreTable.put("LoginUser", System.getProperty("user.name"));		
		InetAddress address = InetAddress.getLocalHost(); 
	    String hostIP = address.getHostAddress().toString();
	    String hostName = address.getHostName().toString();

	    StoreTable.put("LoginMachineIP", hostIP);
		StoreTable.put("LoginMachine", hostName);
		
		Logs.Ulog("Started Loading LoadTestData");
		
		int RFRowNum = 2;
		int tclo = 0;
		String TDValue = null;
		for ( RFRowNum =2; RFRowNum <= GlobalSetupData.getRowCount(TC.JavaScriptsSheet); RFRowNum++) {
			for (tclo = 0; tclo <= GlobalSetupData.getColumnCount(TC.JavaScriptsSheet); tclo++) {

				//TDKey = Testdata.getCellData(TC.JavaScriptsSheet,  tclo, RFRowNum);
				if (GlobalSetupData.getCellData(TC.JavaScriptsSheet,  tclo, RFRowNum).startsWith("RD_")| GlobalSetupData.getCellData(TC.JavaScriptsSheet,  tclo, RFRowNum).startsWith("D_"))
				{
					String TDKey = GlobalSetupData.getCellData(TC.JavaScriptsSheet,  tclo, RFRowNum);
								
					tclo++;
					try
				     {
						 TDValue =TC.ismodifydata(GlobalSetupData.getCellData(TC.JavaScriptsSheet, tclo, RFRowNum));
						StoreTable.put(TDKey, TDValue);
				     }
				     catch(Exception e)
				     {
				    	 Logs.Ulog("Error while  Setupsheet  JavaScriptsSheet data");
				     }
					StoreTable.put(TDKey, TDValue);
					
					
				}
				


			}

		
				
	}
		
		Logs.Ulog("End of Loading LoadTestData");
		
	}	
	
	
}
