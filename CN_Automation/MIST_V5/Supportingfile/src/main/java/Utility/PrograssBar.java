package Utility;

/*
 ********************************************************************************
 ********************************************************************************

//  Start date 7 Jan 2014
//  Author : Raghavendra Pai & Santhosha HC
//  Test :    Progress bar script V 1.9
// updated by  Santhosha HC - Date 25 July 2016
// updated by  Santhosha HC - Date 1 August 2016 - Added image
********************************************************************************
********************************************************************************/
 
import static Utility.R_Start.StoreTable;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.LabelView;

import com.eviware.downloader.ProgressBar;
import com.gargoylesoftware.htmlunit.javascript.host.geo.Position;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.sql.Time;
import java.util.Date;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class PrograssBar 
{
	
	    public static JLabel DriverLabel ;
	    public JLabel TestlabelLabel;
	    public JLabel PreviousFunctionLabel;
	
	    public static JProgressBar DriverProgressBar;
	    public static JProgressBar TestProgressBar;
	    public static ImageIcon runImage ;
	    public static ImageIcon pauseImage;
	    public static JButton Run;
	    public static JButton Pause ;
	    public static JPanel p;
	    public static  JFrame f;
    
	    
	    public PrograssBar()
	    {
	    	DriverLabel = new JLabel("Executing Test Suite @ Row ", JLabel.LEFT);
	 	    TestlabelLabel = new JLabel("Current Test @ Row", JLabel.LEFT);
	 	    PreviousFunctionLabel = new JLabel("Previous function executed  = ", JLabel.LEFT);
	 	    DriverProgressBar = new JProgressBar(0, 100);
	 	    TestProgressBar = new JProgressBar(0, 100);
	    }
	   
	    public static void main(String[] args) throws InterruptedException, IOException
	    {
			// TODO Auto-generated method stub
	    	PrograssBar Pbar = new PrograssBar();
	      
	    	Pbar.CreateProgressBar();
		    for(int i= 0 ; i<=100 ; i ++)
		    {
		    
			       Pbar.Driverprogress(i, "Driver Progress ");
			      for (int j=0; j<=100; j++)
			      {
				    	
			    	  Pbar.Testprogress( j, "Kyword", "Test Progress  "  );
			      
			      } 		

		    }
	    }  
	    public void Driverprogress(int val  ,  String Dtext)
	    {
	    	 DriverProgressBar.setValue(val);
		     DriverLabel.setText(Dtext);
	    }
	
	    
	    public void Testprogress(int val  ,String KyFun,   String Dtext) throws InterruptedException
	    {
	    	PreviousFunctionLabel.setText("Previous function executed  = " + KyFun);
	    	TestlabelLabel.setText( Dtext );
    		TestProgressBar.setValue(val);
    		
    		Thread.sleep(10);
	    }
	    
	public void CreateProgressBar() throws InterruptedException, IOException
	{
			 Date date = new Date();
			// TODO Auto-generated method stub
	       f = new JFrame("Automation Progress - Executing on "+StoreTable.get("Execute ON Browser"));
	        f.setLocation(550, 550);
	        f.setAlwaysOnTop(true);
	       
	         p = new JPanel();
	        
	        p.setLayout(new GridLayout(0,1 ));
	        
	        
	        //p.setBackground(Color.orange);
	        p.setBackground(new Color(189,183,107));
	        
	        JLabel Loglink = new JLabel("View Logs");
	        Loglink.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	        Loglink.addMouseListener(new MouseAdapter() {
	   	   public void mouseClicked(MouseEvent e) {
	   	      if (e.getClickCount() > 0) {
	   	          if (Desktop.isDesktopSupported()) {
	   	                Desktop desktop = Desktop.getDesktop();
	   	                try {
	   	                	
	   	                   //URI uri = new URI("file://"+System.getProperty("user.dir")+"\\log\\GTAF.log");
	   	                	String path = System.getProperty("user.dir")+"/log/GTAF.log";
	   	                	URI uri = new URI(URLEncoder.encode(path, "UTF-8"));
	   	                	
	   	                	File myFile = new File( path);
		   	                 Desktop.getDesktop().open(myFile);
	   	                	
	   	                    
	   	                } catch (IOException ex) {
	   	                    ex.printStackTrace();
	   	                } catch (URISyntaxException ex) {
	   	                    ex.printStackTrace();
	   	                }
	   	        }
	   	      }
	   	   }
	   	});
	   	 
	   	JLabel Resultslink = new JLabel("View Results");
	   	
	   	

	   	Resultslink.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	   	
	   	Resultslink.addMouseListener(new MouseAdapter() {
		   	   public void mouseClicked(MouseEvent e) {
		   	      if (e.getClickCount() > 0) {
		   	          if (Desktop.isDesktopSupported()) {
		   	                Desktop desktop = Desktop.getDesktop();
		   	                try {
		   	                	File myFile = new File( HTMLResults.FrontMainResultFilePath);
		   	                 Desktop.getDesktop().open(myFile);
		   	                } catch (IOException ex) {
		   	                    ex.printStackTrace();
		   	                }
		   	        }
		   	      }
		   	   }
		   	});
	   	
	   	  
 
 
 
  runImage = new ImageIcon(new ImageIcon(System.getProperty("user.dir")+ "\\config\\run.jpg").getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
  pauseImage = new ImageIcon(new ImageIcon(System.getProperty("user.dir")+ "\\config\\pause.jpg").getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
  Run = new JButton(runImage);
  Run.setBackground(Color.white);
  Pause = new JButton(pauseImage);
  Pause.setBackground(Color.white);
 /* Pause = new JButton("||Paused");
  Run = new JButton(">Run");*/
	    
  Run.addMouseListener(new MouseAdapter() {
 	   public void mouseClicked(MouseEvent e) {
 	      if (e.getClickCount() > 0) {
 	          
 	                try {
 	                	System.out.println("Script Started ");
 	                	if(TC.PauseExecution.equals("YY"))
 	                	{
 	                		TC.PauseExecution="NN";
 	                		 f.setTitle("Automation Progress - Executing on "+StoreTable.get("Execute ON Browser"));
 	                		p.setBackground(new Color(189,183,107));
 	                	
 	                	}
 	                	
 	                	
 	                } catch (Throwable T) {
 	                	System.out.println("Error While clicking pause");
 	                }
 	                   
 	                
 	        }
 	      }
 	   
 	});
  
Pause.addMouseListener(new MouseAdapter() {
	   public void mouseClicked(MouseEvent e) {
	      if (e.getClickCount() > 0) {
	          
	                try {
	                	System.out.println("Script Paused");
	                	if(TC.PauseExecution.equals("NN"))
	                	{
	                		TC.PauseExecution="YY";
	                		/*p.remove(Run); 
	                		Thread.sleep(1000);
	                		p.add(Pause, BorderLayout.WEST);*/
	                		 f.setTitle("**************SCRIPT PAUSED******************");
	                		 p.setBackground(Color.YELLOW);
	                	}
	                	
	                	
	                } catch (Throwable T) {
	                	System.out.println("Error While clicking pause");
	                }
	                   
	                
	        }
	      }
	   
	});
	    
	    DriverLabel.setBackground(Color.YELLOW);
	    DriverLabel.setForeground(Color.BLACK);
	    DriverLabel.setFont(new Font("SansSerif", Font.PLAIN+Font.BOLD,12));
	    
	    TestlabelLabel.setBackground(Color.YELLOW);
	    TestlabelLabel.setForeground(Color.BLACK);
	    TestlabelLabel.setFont(new Font("SansSerif", Font.PLAIN+Font.BOLD,12));
	    
	    PreviousFunctionLabel.setBackground(Color.YELLOW);
	    PreviousFunctionLabel.setForeground(new Color (0,100,0));
	    PreviousFunctionLabel.setFont(new Font("SansSerif", Font.PLAIN+Font.BOLD,10));
	   
	        
	    
	    Border Driverborder = BorderFactory.createTitledBorder("Test Suite Progress -- Started @ " + date.toString()  );
	   
	   
	    DriverProgressBar.setForeground(new Color(0,176,80));
	    DriverProgressBar.setBorder(Driverborder);
	    DriverProgressBar.setValue(2);
	    DriverProgressBar.setBackground(Color.WHITE);
	    
	    Border Testborder = BorderFactory.createTitledBorder("Current Test Progress " );
	   
	    TestProgressBar.setForeground(new Color(0,176,80));
	  
	    TestProgressBar.setAlignmentX(10);
	    TestProgressBar.setAlignmentY(10);
	    TestProgressBar.setBackground(Color.WHITE);
	    TestProgressBar.setBorder(Testborder);
	    TestProgressBar.setValue(2);
	    
	    
	    f.setSize(650, 200);
	  
	   p.add(DriverLabel);
	   p.add(DriverProgressBar, BorderLayout.NORTH);
	  // p.add(PreviousFunctionLabel);
	   p.add(TestlabelLabel);
	   p.add(TestProgressBar, BorderLayout.AFTER_LINE_ENDS);
	  
	   Loglink.setForeground(Color.BLUE);
	   Resultslink.setForeground(Color.BLUE);
	   JPanel subPanel = new JPanel();
       GridLayout subLayout = new GridLayout(2,1);
       subPanel.setLayout(subLayout);
     //  p.add(Loglink,  BorderLayout.EAST);
	 //  p.add(Resultslink, BorderLayout.WEST); 
	  // p.add(Run, BorderLayout.WEST);
	 //  p.add(Pause, BorderLayout.EAST);
	   
	   
	   JPanel LinkPane = new JPanel();
	   LinkPane.setLayout(new BoxLayout(LinkPane, BoxLayout.X_AXIS));
	   LinkPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
	   LinkPane.add(Box.createVerticalGlue());
	   LinkPane.add(Loglink);
	   LinkPane.add(Box.createRigidArea(new Dimension(10, 0)));
	   LinkPane.add(Resultslink);
	
	 //Lay out the buttons from left to right.
	   JPanel buttonPane = new JPanel();
	   buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
	   buttonPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
	   buttonPane.add(Box.createVerticalGlue());
	   buttonPane.add(Run);
	   buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
	   buttonPane.add(Pause);

	   //Put everything together, using the content pane's BorderLayout.
	   p.add(LinkPane, BorderLayout.EAST);
	   p.add(buttonPane, BorderLayout.WEST);
	   
	   
	    f.setVisible(true);
	    f.setContentPane(p);
	    Thread.sleep(2000);
	   
	}
	
	
	
	
	
}



