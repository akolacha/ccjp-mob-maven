package Utility;



import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;



public class testemail {
	private String username;
	private String password;
	private String recepientEmail;
	private String smtpHost;

	
	public static void main(String[]args)throws IOException {
		
		new testemail().sendTestFailureEmailNotification("test");
	
		
	}
	
	/**
	 * Constructor to fetch credential sender , receipent and smtp port details from excel
	 */
	public testemail() {
		List<String> credentials = new ArrayList<String>();
		//Comment this method you can directly provide data in variables 
	
		this.setUsername("Santhosha.Chandrashekharappa@adc.mindtree.com");//adc mail id (AIG mail)
		this.setPassword("Hanuman@123");//adc mail password(AIG mail password)
		this.setRecepientEmail("Santhosha.Chandrashekharappa@adc.mindtree.com");//Receipent mail id (AIG mail)
		this.setSmtpHost("odc01exe");//odc01exe
	}

	
	
	public void sendTestFailureEmailNotification(String failedTestCaseName ) {
		final String username = this.getUsername();
		final String password = this.getPassword();
		final String recepientEmail = this.getRecepientEmail();
		Properties props = new Properties();
		props.put("mail.smtp.host", getSmtpHost());
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(recepientEmail));
			message.setSubject("Automation Test Failure");
			message.setText("There was test failure while automation test execution of "
					+ failedTestCaseName
					+ "PFA attached screesnshot of failure"
					+ "\n\n No Replies please , This email is auto triggered !");

			/*MimeBodyPart messageBodyPart = new MimeBodyPart();

			Multipart multipart = new MimeMultipart();

			messageBodyPart = new MimeBodyPart();
			//String file = screenshotFileName;
			//String fileName = "FailedTestScreenShot";
			//DataSource source = new FileDataSource(file);
			//messageBodyPart.setDataHandler(new DataHandler(source));
			//messageBodyPart.setFileName(fileName);
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);*/
			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRecepientEmail() {
		return recepientEmail;
	}

	public void setRecepientEmail(String recepientEmail) {
		this.recepientEmail = recepientEmail;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
}