package Controller;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import Utility.ExcelObj;
import Utility.FunctionLibrary;
import Utility.ServiceLibrary;

/**
 * ServiceController includes all the logic required to get the service data, build the service request data and analyze the response.
 * @author M1026473
 *
 */
public class ServiceController {

	private String serviceDataPath = System.getProperty("user.dir") + "\\src\\TestScripts\\ServicesData.xlsx";
	private String excelResultPath = System.getProperty("user.dir") + "\\Results\\ServicesResult.xlsx";
	String payloadTemplatePath = System.getProperty("user.dir") + "/payloads/";

	private String formattedDate = FunctionLibrary.getFormattedDate();
	String resultSheetName = "Result_" + formattedDate;
	static String dataSheetName = "SERVICEDATA";
	static String payLoadSheetName = "PAYLOAD";
	static String soapReqSheet = "SOAP";

	String testcase = "";
	String isExeEnabled = "";
	boolean isPayloadAvailable = false;
	String serviceType = "";
	String baseUrl = "";
	String reqBody = "";
	String keyStr = null;
	String[] keys = null;
	String expValueStr = null;
	String[] expValues = null;
	String expValue = null;
	String resp = "";
	String headerStr = "";
	HashMap<String, String> headermap = new HashMap<>();

	ExcelObj eWObj = new ExcelObj(excelResultPath);
	ExcelObj eRObj = new ExcelObj(serviceDataPath);
	ServiceLibrary srvsLib = new ServiceLibrary();

	int testCount = 0;
	int logCounter = 1;

	/**
	 * Execute the test cases which are marked "Y" in the service data sheet.
	 */
	public void executeModules() {
		try {
			// Enable only if required for the respective services
			FunctionLibrary.setSystemProxy("172.22.218.218", "8085");

			testCount = eRObj.getRowCount(dataSheetName);
			srvsLib.addServiceSheet(eWObj, resultSheetName);

			for (int i = 2; i <= testCount; i++) {
				isExeEnabled = eRObj.getCellData(dataSheetName, "EXECUTE", i);

				if (isExeEnabled.equalsIgnoreCase("Y") || isExeEnabled.equalsIgnoreCase("YES")) {
					logCounter++;
					// Read Data
					testcase = eRObj.getCellData(dataSheetName, "TESTCASE", i) == null ? "" : eRObj.getCellData(dataSheetName, "TESTCASE", i).toString();
					serviceType = eRObj.getCellData(dataSheetName,"SERVICE_TYPE", i) == null ? "" : eRObj.getCellData(dataSheetName, "SERVICE_TYPE", i).toString();
					baseUrl = eRObj.getCellData(dataSheetName, "BASE_URL", i) == null ? "": eRObj.getCellData(dataSheetName, "BASE_URL", i).toString();
					
					// Get Validation keys
					keyStr = eRObj.getCellData(dataSheetName,"VALIDATION_KEYS", i) == null ? "" : eRObj.getCellData(dataSheetName, "VALIDATION_KEYS", i).toString();
					keys = FunctionLibrary.getStringAsArray(keyStr);
					
					// Get HeaderData
					headerStr = eRObj.getCellData(dataSheetName, "HEADER", i) == null ? "" : eRObj.getCellData(dataSheetName, "HEADER", i).toString();
					headermap = srvsLib.generateHeaderMap(headerStr);
					
					// Get Expected Values
					expValueStr = eRObj.getCellData(dataSheetName,"EXPECTED_VALUES", i) == null ? "" : eRObj.getCellData(dataSheetName, "EXPECTED_VALUES", i).toString();
					expValues = FunctionLibrary.getStringAsArray(expValueStr);
					
					if (serviceType.equalsIgnoreCase("SOAP"))
					{
						System.out.println("Service Type : SOAP");
						resp = srvsLib.executeGetSOAPRequest(baseUrl, headerStr, keyStr);
					    srvsLib.validateSOAPResponse(logCounter, testcase, resp, expValueStr);
					}
				else
				{
					//Check if Payload is available for the service
					if (eRObj.getCellData(dataSheetName, "PAYLOAD", i).toString().equalsIgnoreCase("Y")
							|| eRObj.getCellData(dataSheetName, "PAYLOAD", i).toString().equalsIgnoreCase("Yes")) {
						isPayloadAvailable = true;
					}

				

					

				

					// Execute the Service Request
					if (serviceType.equalsIgnoreCase("GET"))
						resp = srvsLib.executeGetRequest(baseUrl, headermap);
					else {
						if (isPayloadAvailable)
							reqBody = getRequestPayload(testcase);
						else
							reqBody = "";

						resp = srvsLib.executePostRequest(baseUrl, headermap,reqBody);
					}

					
				
					
				{
					// Validate the Service Response
					if (!resp.contains("<html")) {
						if (headerStr.contains("application/xml"))
							srvsLib.validateXMLResponse(logCounter, testcase,resp, keys, expValues);
						if (headerStr.contains("application/json"))
							srvsLib.validateJSONResponse(logCounter, testcase,resp, keys, expValues);
					} else {
						expValue = eRObj.getCellData(dataSheetName,"EXPECTED_VALUES", i).toString();
						srvsLib.validateHTMLResponse(logCounter, testcase,resp, expValue);
					}
				}
					keys = expValues = null;
					headerStr = keyStr = expValueStr = expValue = null;
					testcase = null;
					isPayloadAvailable = false;
				}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	
	
	public void generateSoapRequest(){
		String soapreq = getRequestPayload("TestService_8");
	}
	
	/**
	 * Get the Post request payload data  
	 * @param testcase
	 * @return
	 */
	public String getRequestPayload(String testcase) {
		String payloaddata = "";
		String templateName = "";
		try {
			int rowNum = eRObj.getCellRowNum(payLoadSheetName, "TESTCASE",testcase);
			templateName = eRObj.getCellData(payLoadSheetName, "TEMPLATE",rowNum).toString();

			payloaddata = generatePayload(templateName, getpayloadData(rowNum));
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Payload: \n" + payloaddata);
		return payloaddata;

	}

	/**
	 * To generate the request body from the given template & data
	 * @param templateName
	 * @param values
	 * @return
	 */
	private String generatePayload(String templateName,HashMap<String, String> values) {
		FileInputStream fstream;
		String strLine = "";
		String strLinetemp = "";
		String filePath = payloadTemplatePath + templateName + ".txt";
		try {
			fstream = new FileInputStream(filePath);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			while ((strLine = br.readLine()) != null) {
				Set<Entry<String, String>> entries = values.entrySet();
				strLinetemp = strLine;
				for (Iterator<Entry<String, String>> it = entries.iterator(); it.hasNext();) {
					Entry<String, String> current = it.next();

					if (strLinetemp.contains(current.getKey())) {
						strLinetemp = strLinetemp.replaceFirst(current.getKey(), current.getValue());
					}

				}
			}

			strLine = strLinetemp;
			fstream.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return strLine;

	}

	/**
	 * Get the payload data corresponding to the template name
	 * @param row
	 * @return
	 */
	private HashMap<String, String> getpayloadData(int row) {
		Object[] colNames;
		String key = "";
		String val = "";
		HashMap<String, String> map = new HashMap<String, String>();

		colNames = (eRObj.getColumnNames(payLoadSheetName)).toArray();
		for (Object colName : colNames) {
			key = colName.toString();
			val = eRObj.getCellData(payLoadSheetName, key, row);
			map.put(key, val);
		}

		return map;

	}

}
