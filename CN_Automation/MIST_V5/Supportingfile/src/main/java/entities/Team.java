package entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Team {
	
	int id;

	public Team(int id) {
		this.id = id;
	}

	public Team(){
		
	}
}
