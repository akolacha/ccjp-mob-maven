package ExcelDB;


import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Provides the methods to add/update rows into the excel
 * @author ShivaPrakash
 *
 */
public class ExcelWriter extends AbstractExcelConnection{
	
	public ExcelWriter( ) {
		super( true );
	}
	
	public ExcelWriter(String filepath ) {
		super( true );
		try {
			open(filepath);
		} catch (IllegalArgumentException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Inserts a new row to the given work sheet using the values provided.
	 * @param sheetName
	 * @param values
	 * @return integer
	 * @throws IllegalArgumentException
	 * @throws SQLException
	 */
	@SuppressWarnings("rawtypes")
	public int insertRow( String sheetName, HashMap<String, Object> values ) throws IllegalArgumentException, SQLException {
		validateParameters( sheetName, values );
		
		StringBuffer queryBuilder = new StringBuffer();
		queryBuilder.append( String.format( "Insert into [%s$]", sheetName ) );
		queryBuilder.append('(');
		
		StringBuffer sqlBuffer = new StringBuffer();
		Set<Entry<String,Object>> entries = values.entrySet();
		int count = 0;
		for( Iterator<Entry<String,Object>> it = entries.iterator();it.hasNext(); ) {
			Entry<String,Object> current = it.next();
			if ( count++ > 0 ) {
				sqlBuffer.append(',');
				queryBuilder.append(',');
			}
			
			Object value = current.getValue( );
			String key = current.getKey( );
			queryBuilder.append( key );
			
			Class cls = value.getClass( );
			if ( cls == Integer.class || cls == Double.class || cls == Float.class || cls == Short.class ) {
				sqlBuffer.append( String.format( "%s", value ) );
			}
			else {
				sqlBuffer.append( String.format( "'%s'", value ) );
			}
		}
		// append values.
		queryBuilder.append(") values (");
		queryBuilder.append(sqlBuffer.toString());
		queryBuilder.append(')');
		
		// execute the query.
		return this.executeUpdate(queryBuilder.toString());
	}
	
	/**
	 * Updates the rows with the specified values using the filter provided.
	 * @param sheetName
	 * @param values
	 * @param filterValues
	 * @return integer
	 * @throws IllegalArgumentException
	 * @throws SQLException
	 */
	public int updateRow( String sheetName, HashMap<String, Object> values, HashMap<String, Object> filterValues ) throws IllegalArgumentException, SQLException {
		validateParameters( sheetName, values );
		
		String setExpression = convertToFilterExpression(values, ",");
		String query = filterValues != null && filterValues.size() > 0 ?
						String.format( "update [%s$] set %s where %s",sheetName, setExpression, convertToFilterExpression(filterValues) )
						: String.format( "update [%s$] set %s",sheetName, setExpression );
		return this.executeUpdate(query);
	}
	
	/*
	public int deleteRow( String sheetName, HashMap<String, Object> values) throws IllegalArgumentException, SQLException {
		
		
		String setExpression = convertToFilterExpression(values, ",");
		String query = String.format( "update [%s$] set %s",sheetName, setExpression );
		String query = filterValues != null && filterValues.size() > 0 ?
				String.format( "update [%s$] set %s where %s",sheetName, setExpression, convertToFilterExpression(filterValues) )
				: String.format( "update [%s$] set %s",sheetName, setExpression );
		return this.executeUpdate(query);
	}*/
	
	/**
	 * Validates the parameters
	 * @param sheetName
	 * @param values
	 * @throws IllegalArgumentException
	 */
	void validateParameters( String sheetName, HashMap<String, Object> values ) throws IllegalArgumentException {
		if ( sheetName == null || sheetName.isEmpty( ) )
			throw new IllegalArgumentException("Sheet name cannot be empty");
		
		if ( values == null || values.size() == 0 )
			throw new IllegalArgumentException( "Cannot insert row with empty values" );
	}
}
